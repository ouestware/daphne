import { pick } from "lodash";
import { Pool } from "mariadb";
import { sha1 } from "object-hash";
import { Factoid, FactoidRefersToSource, FactoidType, PersonName, PhysicalPerson } from "../graphql/generated/graphql";
import { addEdgeOperation, addNodeOperation, DateISO, generateDateValue } from "./daphne";

export type Quantifier = "=" | "ca." | "<" | ">" | "<=" | ">=";

export interface Fait {
  id_fait: BigInt;
  if_ref: string;
  page?: BigInt; // format=PPPCA; PPP = num. page/folio; C = 0 (p), 1 (folio recto), 2 (folio verso); A = 0, 1 (note marginale), 2 (volet adjoint)
  rang: BigInt; // 	numero d'ordre dans la source
  type?:
    | "texte"
    | "section"
    | "ssection"
    | "alias"
    | "auteur"
    | "lieu"
    | "MAT"
    | "GRAD"
    | "STUD"
    | "EXITUNIV"
    | "ENTR"
    | "APLDISP"
    | "INC"
    | "PROF"
    | "B"
    | "D"
    | "sign"
    | "dom";
  dateQuant?: Quantifier; // 	enum('=','ca.','<','>','<=','>=') NULL [=]
  dateLue?: string;
  dateNormee?: string;
  id_prefixe?: BigInt; // 	reference d'origine geographique
  nom?: string;
  suffixe?: string;
  id_agent1?: BigInt;
  id_agent2?: BigInt;
  origine?: string; //nom de lieu
  origineNormee?: string;
  id_grade?: BigInt;
  id_institution?: BigInt;
  examinateur?: string;
  commentaire?: string;
  modif_timbre: Date;
  modif_uid?: string;
}

export interface Grade {
  id_grade: bigint;
  abrege: string;
  label: string;
  typeRAP: "MAT" | "GRAD" | "STUD" | "EXITUNIV" | "ENTR" | "APLDISP" | "INC" | "PROF" | "B" | "D";
  classeRdf: string;
  statut: number;
}

const ROLES = {
  studium: "Person who studied.",
  dominus: "Person who act as dominus, a teaching master.",
  signum: "Person who certified the factoïd information by signing a register.",
};

export type RoleType = keyof typeof ROLES;

interface FactoidInline {
  id: string;
  description: string;
}

export function* factoid(
  factoid: FactoidInline,
  dates: DateISO[],
  typeIds: string[],
  sourceId?: string,
  sourceEdgeProps?: FactoidRefersToSource,
) {
  yield addNodeOperation<Pick<Factoid, "id" | "description" | "originalText">>("Factoid", factoid);

  //SOURCE
  if (sourceId)
    yield addEdgeOperation(
      "REFERS_TO",
      { id: factoid.id, type: "Factoid" },
      { id: sourceId, type: "Source" },
      sourceEdgeProps || {},
    );

  // DATE
  for (const date of dates) {
    yield addNodeOperation<DateISO>("DateValue", date);
    yield addEdgeOperation("OCCURED_AT", { id: factoid.id, type: "Factoid" }, { id: date.id, type: "DateValue" }, {});
  }

  // FACTOID TYPES
  for (const typeId of typeIds)
    yield addEdgeOperation(
      "HAS_FACTOID_TYPE",
      { type: "Factoid", id: factoid.id },
      { type: "FactoidType", id: typeId },
      {},
    );
}

const factoidParticipation = (idFactoid: string, idPerson: string, role?: RoleType) => {
  const factoidPersonId = `${idFactoid}-${idPerson}`;
  // Participation hyperedge
  const hyperedge = [
    addNodeOperation<{}>("FactoidPersonParticipate", { id: factoidPersonId }),
    addEdgeOperation(
      "PARTICIPATE",
      { type: "FactoidPersonParticipate", id: factoidPersonId },
      { type: "Factoid", id: idFactoid },
      {},
    ),

    // PERSON
    addEdgeOperation(
      "PARTICIPATE",
      { id: idPerson, type: "PhysicalPerson" },
      { id: factoidPersonId, type: "FactoidPersonParticipate" },
      {},
    ),
  ];
  // ROLE
  if (role)
    hyperedge.push(
      addEdgeOperation(
        "WITH_ROLE",
        { id: factoidPersonId, type: "FactoidPersonParticipate" },
        { id: role as string, type: "Role" },
        {},
      ),
    );

  return hyperedge;
};

export async function* faitsToFactoids(pool: Pool) {
  // create factoidType from rap_classesEven

  // mainConn is used to stream groups of faits
  const mainConn = await pool.getConnection();
  await mainConn.query("USE repertorium");
  // subConne is used to retrieve data for each groups of faits
  const subConn = await pool.getConnection();
  await subConn.query("USE repertorium");

  // Grades are FactoidTypes
  const grades: Grade[] = await mainConn.query("SELECT * FROM rap_classesEvenements");
  for (const grade of grades) {
    yield addNodeOperation<Pick<FactoidType, "id" | "name" | "description">>("FactoidType", {
      id: "grade|" + grade.id_grade.toString(),
      name: grade.label,
      description: [grade.abrege, grade.typeRAP, grade.classeRdf].join(" - "),
    });
    //TODO: what to do with grade table ?
  }

  // FactoidTypes which are not grade are Roles
  for (const roleId in ROLES) {
    yield addNodeOperation<{ id: string; name: string; description: string }>("Role", {
      id: roleId,
      name: roleId,
      description: ROLES[roleId as keyof typeof ROLES],
    });
  }

  // GROUP Fait By source and rang to build factoid
  // filter out transcriptions only faits
  const faitsGroups =
    mainConn.queryStream(`SELECT  f.id_ref as id_ref, f.rang as rang, GROUP_CONCAT(f.id_fait) as id_faits
  FROM faits f 
  WHERE f.type NOT IN ("ssection", "texte", "section", "auteur", "")
  GROUP BY f.id_ref,rang;`);
  //TODO: create authorship factoid from "auteur" faits?

  console.log(`tranforming faits`);

  try {
    for await (const faitsGroup of faitsGroups) {
      // retrieve groups data
      const faits = await subConn.query(`
      SELECT *
      FROM faits f 
        LEFT OUTER JOIN prefixes p USING (id_prefixe) 
        LEFT OUTER JOIN lecturesLieux ll ON f.origineNormee=ll.lectureLieu 
      WHERE f.id_fait IN (${faitsGroup.id_faits})`);

      // factoids
      const factoidIds: Set<string> = new Set();
      // factoidTypes = grades
      const gradesFaits = faits.filter((f: Fait) => f.id_grade);
      if (gradesFaits.length === 0) {
        console.log(faits);
        throw new Error("no grades ?");
      }

      const contextFaits = faits.filter((f: Fait) => !f.id_grade && f.type !== "alias");

      for (const fait of gradesFaits) {
        // build a factoid from a group of faits
        const idFactoid = fait.id_fait.toString();
        factoidIds.add(idFactoid);

        const dates: DateISO[] = fait.dateNormee
          ? [generateDateValue(fait.dateNormee.toString(), fait.dateNormee.toString())]
          : [];

        for (const op of factoid(
          { id: idFactoid, description: fait.commentaire },
          dates,
          [`grade|${fait.id_grade.toString()}`], // type
          fait.id_ref, // source id
          {
            certainty: 0.5,
            //TODO: translate page format=PPPCA; PPP = num. page/folio; C = 0 (p), 1 (folio recto), 2 (folio verso); A = 0, 1 (note marginale), 2 (volet adjoint)
            ...(fait.page.toString() !== ""
              ? {}
              : {
                  page: fait.page.toString(),
                  permalink: `http://repertorium.projets.univ-poitiers.fr/voir-numerisation.php?id_ref=${
                    fait.id_ref
                  }&page=${fait.page.toString()}`,
                }),
          }, // source details
        )) {
          yield op;
        }

        // Secondary role persons from contextual facts
        // contextual information are identical to all factoid of the group
        for (const contextFait of contextFaits) {
          let role: RoleType | null = null;
          switch (contextFait.type) {
            case "sign":
              role = "signum";
              break;
            case "dom":
              role = "dominus";
              break;
            default:
              throw new Error(`unkown contextFait type ${contextFait.type}`);
          }
          for (const operation of factoidParticipation(idFactoid, contextFait.id_agent2.toString(), role)) {
            yield operation;
          }
        }
        // TODO: add institution

        // end LOOP on Factoids
      }

      // add acting person in Factoid

      // in a group of fait the acting person is always the same
      // the person can be already created in person table
      let actingIdPerson = gradesFaits[0].id_agent1 && gradesFaits[0].id_agent1.toString();
      // or we have to create a new object
      if (!actingIdPerson) {
        // TODO: isolate and export the id generator
        actingIdPerson = sha1(pick(faits[0], ["id_fait", "nom", "abrege", "suffixe", "origineNormee"]));
        yield addNodeOperation<Pick<PhysicalPerson, "id" | "name" | "description">>("PhysicalPerson", {
          id: actingIdPerson,
          name: faits[0].nom,
        });
      }

      // Acting person participation
      for (const factoidId of factoidIds) {
        for (const operation of factoidParticipation(factoidId, actingIdPerson, "studium")) {
          yield operation;
        }
      }

      // add person aliases
      const aliases = faits.filter((f: Fait) => f.type === "alias");
      for (const alias of aliases) {
        yield addNodeOperation<Pick<PersonName, "id" | "name">>("PersonName", { id: alias.nom, name: alias.nom });
        yield addEdgeOperation(
          "IS_NAMED",
          { type: "PhysicalPerson", id: actingIdPerson },
          { type: "PersonName", id: alias.nom },
          {},
        );
      }

      // ORIGINE
      // TODO CREATE lieu if id_lieu is missing
      if (gradesFaits[0].id_lieu) {
        const originFactoidId = `${actingIdPerson}-${gradesFaits[0].id_lieu}`;
        yield addNodeOperation<{ id: string }>("Factoid", { id: originFactoidId });
        yield addEdgeOperation(
          "REFERS_TO",
          { id: originFactoidId, type: "Factoid" },
          { id: gradesFaits[0].id_ref.toString(), type: "Source" },
          {
            //TODO: format=PPPCA; PPP = num. page/folio; C = 0 (p), 1 (folio recto), 2 (folio verso); A = 0, 1 (note marginale), 2 (volet adjoint)
            page: gradesFaits[0].page.toString(),
            permalink: `http://repertorium.projets.univ-poitiers.fr/voir-numerisation.php?id_ref=${
              gradesFaits[0].id_ref
            }&page=${gradesFaits[0].page.toString()}`,
          },
        );
        yield addEdgeOperation(
          "HAS_FACTOID_TYPE",
          { id: originFactoidId, type: "Factoid" },
          { id: "origine", type: "FactoidType" },
          {},
        );
        for (const operation of factoidParticipation(originFactoidId, actingIdPerson)) {
          yield operation;
        }
        // should we date origin factoid?
        yield addEdgeOperation(
          "TOOK_PLACE_AT",
          { id: originFactoidId, type: "Factoid" },
          { id: gradesFaits[0].id_lieu.toString(), type: "Place" },
          {},
        );
      }
    }
  } catch (e) {
    console.log(e);
  }
}
