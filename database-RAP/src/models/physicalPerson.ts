import { Pool } from "mariadb";
import { FactoidType, PersonName, PhysicalPerson } from "src/graphql/generated/graphql";
import { addNodeOperation, addEdgeOperation, generateDateValue } from "./daphne";
import { factoid } from "./factoid";
import { rapSource } from "./importSource";

export function* alias(personId: string, aliases: string[]) {
  for (const alias of aliases) {
    yield addNodeOperation<Pick<PersonName, "id" | "name">>("PersonName", { id: alias, name: alias });
    yield addEdgeOperation("IS_NAMED", { type: "PhysicalPerson", id: personId }, { type: "PersonName", id: alias }, {});
  }
}

function* birhtDeathFactoid(
  date: string,
  dateQuant: string,
  birthDeath: "naissance" | "décès",
  personId: string,
  personName: string,
) {
  let startDate: string | undefined = date;
  let endDate: string | undefined = date;
  const dateAsDate = new Date(date);
  switch (dateQuant) {
    case "ca.": {
      // TODO: validate this quantitative interpretation of circa
      startDate = "" + (dateAsDate.getFullYear() - 5);
      endDate = "" + (dateAsDate.getFullYear() + 5);
      break;
    }
    case "<": {
      startDate = undefined;
      break;
    }
    case ">": {
      endDate = undefined;
      break;
    }
    // Default => "=" => we use the date as is
  }
  const dateValue = generateDateValue(startDate, endDate);

  return factoid(
    {
      id: `${personId}|${birthDeath}|${dateValue.id}`,
      description: `${dateQuant} ${date}: ${birthDeath} de ${personName}`,
    },
    [dateValue],
    [birthDeath],
  );
}

export async function* personnesToPhysicalPerson(pool: Pool) {
  const conn = await pool.getConnection();
  await conn.query("USE repertorium");

  yield addNodeOperation<Pick<FactoidType, "id" | "name" | "description">>("FactoidType", {
    id: "origine",
    name: "origine",
    description: "origine d'une personne",
  });
  yield addNodeOperation<Pick<FactoidType, "id" | "name" | "description">>("FactoidType", {
    id: "naissance",
    name: "naissance",
    description: "naissance d'une personne",
  });
  yield addNodeOperation<Pick<FactoidType, "id" | "name" | "description">>("FactoidType", {
    id: "décès",
    name: "décès",
    description: "décès d'une personne",
  });

  for await (const personne of conn.queryStream(`
    SELECT *
    FROM personnes p 
    LEFT JOIN (SELECT GROUP_CONCAT(id_ref) as ref_ids, id_personne  FROM auteurs GROUP BY id_personne) ref USING (id_personne)
    LEFT OUTER JOIN prefixes USING (id_prefixe);`)) {
    const personName = `${personne.prenom} ${personne.nom}`;
    const personId = personne.id_personne.toString();
    yield addNodeOperation<Pick<PhysicalPerson, "id" | "name">>(
      "PhysicalPerson",
      {
        id: personId,
        name: personName,
        //TODO: firstname lastname ?
      },
      rapSource(personId, `http://repertorium.projets.univ-poitiers.fr/voir-personne.php?id_personne=${personId}`),
    );
    // prefix sufix...
    const aliases = [];

    if (personne.suffixe && personne.suffixe !== "") {
      aliases.push(personne.suffixe);
      aliases.push(`${personName}, ${personne.suffixe}`);
    }
    if (personne.prefix) {
      aliases.push(`${personne.prefix} ${personName}`);
    }
    if (personne.abrege) {
      aliases.push(`${personne.abrege} ${personName}`);
    }
    if (aliases.length > 0) for (const op of alias(personne.id_personne.toString(), aliases)) yield op;

    // BIRTH
    if (personne.naissance) {
      for (const op of birhtDeathFactoid(
        personne.naissance,
        personne.naissanceQuant,
        "naissance",
        personne.id_personne.toString(),
        personName,
      )) {
        yield op;
      }
    }
    // DEATH
    if (personne.deces) {
      for (const op of birhtDeathFactoid(
        personne.deces,
        personne.decesQuant,
        "décès",
        personne.id_personne.toString(),
        personName,
      )) {
        yield op;
      }
    }
    // origine: origine is handled from faits table in factoid. Inded origine in personne is not linked to lieu table??
  }
  // TODO auteurs
  for await (const auteur of conn.queryStream(`
    SELECT *
    FROM auteurs;`)) {
    addEdgeOperation(
      "ISSUED_BY",
      { id: auteur.id_personne.toString(), type: "PhysicalPerson" },
      { id: auteur.id_ref.toString(), type: "Source" },
      {},
    );
  }
}
