import { DateValue } from "src/graphql/generated/graphql";
import { ImportAddRel, ImportMergeNode, ImportProperties, ImportSource } from "./import";
import { rapSource } from "./importSource";

export type NodeType =
  | "Source"
  | "Factoid"
  | "SourceType"
  | "Edition"
  | "FactoidType"
  | "PhysicalPerson"
  | "PersonName"
  | "FactoidPersonParticipate"
  | "Role"
  | "Rank"
  | "Person"
  | "DateValue"
  | "Place";

type MergeNode<P> = ImportMergeNode & {
  type: NodeType;
  properties: { id?: string } & P;
};

export const addNodeOperation = <P>(
  type: NodeType,
  properties: { id: string } & P,
  importSource?: ImportSource,
): MergeNode<P> => {
  return {
    op: "merge_node",
    type,
    id: properties.id,
    properties,
    dataSource: importSource || rapSource(properties.id),
  };
};

const generateDateId = (startIso?: string, endIso?: string) => {
  if (!startIso && !endIso) throw new Error("invalid empty date");
  return `${startIso || ""}${endIso ? `|${endIso}` : ""}`;
};

const checkIsoDate = (isoDate: string): boolean => {
  try {
    new Date(isoDate);
    return true;
  } catch (e) {
    throw new Error(`Invalid ISO date`);
  }
};

export const generateDateValue = (startIso?: string, endIso?: string): DateISO => {
  if (!startIso && !endIso) throw new Error("invalid empty date");

  if (startIso) checkIsoDate(startIso);
  if (endIso) checkIsoDate(endIso);
  return {
    id: generateDateId(startIso, endIso),
    startIso,
    endIso,
  };
};

export type EdgeType =
  | "HAS_SOURCE_TYPE"
  | "PART_OF"
  | "PUBLISHED_AS"
  | "IS_NAMED"
  | "HAS_FACTOID_TYPE"
  | "OCCURED_AT"
  | "TOOK_PLACE_AT"
  | "PARTICIPATE"
  | "REFERS_TO"
  | "WITH_ROLE"
  | "ISSUED_BY";

interface NodeIdentifier {
  id: string;
  type: NodeType;
}

type AddEdge = ImportAddRel;

export const addEdgeOperation = <P extends ImportProperties>(
  type: EdgeType,
  source: NodeIdentifier,
  target: NodeIdentifier,
  properties: P,
): AddEdge => {
  return { op: "add_rel", type, source, target, properties };
};

export type DateISO = Pick<DateValue, "startIso" | "endIso" | "id">;

export interface ExportIndices {
  dateIds: Set<string>;
}
