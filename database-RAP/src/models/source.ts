import { Connection } from "mariadb";
import { Edition, Source, SourceType } from "../graphql/generated/graphql";
import { addEdgeOperation, addNodeOperation, DateISO, ExportIndices, generateDateValue } from "./daphne";
import { rapSource } from "./importSource";

const referenceTypeEnum = ["book", "chapter", "article", "phdthesis", "register", "unpublished", "test"] as const;

export interface ReferenceType {
  id_ref: string; // designation abregee du document
  type: typeof referenceTypeEnum[number]; // type de document, format bibtex
  titre: string;
  titreLivre?: string; // titre d''ouvrage ou de revue contenant cette reference
  annee?: string; // annee de publication, de soutenance, de redaction
  serie?: string;
  publisher?: string;
  lieu?: string; // lieu de publication, de soutenance, de redaction...
  pagination: string; // pagination, localisation dans livre, foliotage
  note: string; // commentaires
  p_location?: string; // localisation
  p_n_inv?: string; // numero inventaire
  p_photo?: string; // numerisation
  p_copy?: string; // copyright
  p_path?: string; // illustration
}

export type SourceProperties = Pick<Source, "id" | "name" | "description" | "language" | "reputation">;
export type EditionProperties = Pick<Edition, "id" | "title" | "notes" | "place" | "editor" | "collection" | "pages">;

const referenceToSource = (ref: ReferenceType): SourceProperties => {
  return {
    id: ref.id_ref,
    name: ref.titre,
    description: ref.note,
  };
};

const editionDates = (ref: ReferenceType): DateISO | null => {
  let dates: string[] | undefined = undefined;
  switch (ref.annee) {
    case "1891 [1892]":
      dates = ["1891", "1892"];
      break;
    case "oct. 2001":
      dates = ["2001-10"];
      break;
    case undefined:
      break;
    default:
      dates = ref.annee.split("-");
  }
  return dates ? generateDateValue(dates[0], dates[1] || dates[0]) : null;
};

const referenceToEdition = (ref: ReferenceType): EditionProperties => {
  return {
    id: ref.id_ref,
    title: ref.titre,
    notes: ref.note,
    place: ref.lieu,
    collection: ref.serie,
    editor: ref.publisher,
    pages: ref.pagination,
  };
};

export async function* referencesToSources(conn: Connection, indices: ExportIndices) {
  // first create source types
  for (const i in referenceTypeEnum) {
    const refType = referenceTypeEnum[i];
    yield addNodeOperation<Pick<SourceType, "id" | "name">>("SourceType", { id: refType, name: refType });
  }
  await conn.query("USE repertorium");
  const references = conn.queryStream("SELECT * from `references`;");
  console.log(`tranforming references`);
  const allTitreLivre = new Set<string>();

  try {
    for await (const ref of references) {
      yield addNodeOperation<SourceProperties>(
        "Source",
        referenceToSource(ref),
        rapSource(
          ref.id_ref,
          `http://repertorium.projets.univ-poitiers.fr/voir-reference.php?id_ref=${encodeURIComponent(ref.id_ref)}`,
        ),
      );
      yield addEdgeOperation(
        "HAS_SOURCE_TYPE",
        { id: ref.id_ref, type: "Source" },
        { id: ref.type, type: "SourceType" },
        {},
      );
      if (ref.titreLivre) {
        if (!allTitreLivre.has(ref.titreLivre)) {
          allTitreLivre.add(ref.titreLivre);
          yield addNodeOperation<SourceProperties>("Source", {
            id: ref.titreLivre,
            name: ref.titreLivre,
          });
        }
        yield addEdgeOperation(
          "PART_OF",
          { id: ref.id_ref, type: "Source" },
          { id: ref.titreLivre, type: "Source" },
          {},
        );
      }
      const edition = referenceToEdition(ref);
      yield addNodeOperation<EditionProperties>("Edition", edition);
      const editionDate = editionDates(ref);
      if (editionDate) {
        if (!indices.dateIds.has(editionDate.id)) {
          indices.dateIds.add(editionDate.id);
          yield addNodeOperation<DateISO>("DateValue", editionDate);
        }
        yield addEdgeOperation(
          "OCCURED_AT",
          { id: edition.id, type: "Edition" },
          { id: editionDate?.id, type: "DateValue" },
          {},
        );
      }
      yield addEdgeOperation(
        "PUBLISHED_AS",
        { id: ref.id_ref, type: "Source" },
        { id: edition.id, type: "Edition" },
        {},
      );
    }
  } catch (e) {
    console.log(e);
  }
}
