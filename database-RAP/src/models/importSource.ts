import { ImportSource } from "./import";

export const rapSource = (id: string, permalink?: string): ImportSource => ({
  name: "Repertorium academicum pictaviense",
  originalId: id,
  permalink,
});
