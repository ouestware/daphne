import { Pool } from "mariadb";
import { Place } from "src/graphql/generated/graphql";
import { addEdgeOperation, addNodeOperation } from "./daphne";
import { rapSource } from "./importSource";

export async function* lieuToPlace(pool: Pool) {
  const conn = await pool.getConnection();
  await conn.query("USE repertorium");
  const partOfs: { from: string; to: string }[] = [];
  for await (const lieu of conn.queryStream(
    `SELECT *, GROUP_CONCAT(distinct ll.lectureLieu SEPARATOR "#|#") as alternativeNames  
      from lieux l 
      left outer join lecturesLieux ll using (id_lieu)
      GROUP BY l.id_lieu;`,
  )) {
    yield addNodeOperation<Pick<Place, "id" | "name" | "alternativeNames">>(
      "Place",
      {
        id: lieu.id_lieu.toString(),
        name: lieu.nom,
        alternativeNames: lieu.alternativeNames
          ? lieu.alternativeNames.split("#|#").filter((n: string) => n !== lieu.nom && n !== "")
          : undefined,
      },
      rapSource(
        lieu.id_lieu.toString(),
        `http://repertorium.projets.univ-poitiers.fr/voir-lieux.php?q=${lieu.id_lieu.toString()}`,
      ),
    );
    // part of
    if (lieu.conteneur) partOfs.push({ from: lieu.id_lieu.toString(), to: lieu.conteneur.toString() });
  }

  // TODO geoname

  for (const partOf of partOfs)
    yield addEdgeOperation("PART_OF", { id: partOf.from, type: "Place" }, { id: partOf.to, type: "Place" }, {});
}
