// Used to defined the data source
export type ImportSource = { name: string; originalId?: string; permalink?: string };

type ImportNodeCommon = {
  id: string;
  type: string;
  dataSource: ImportSource;
};

type ImportRelCommon = {
  type: string;
  source: Omit<ImportNodeCommon, "dataSource">;
  target: Omit<ImportNodeCommon, "dataSource">;
};
export type ImportProperties = { [key: string]: unknown };

export type ImportAddNode = { op: "add_node"; properties: ImportProperties } & ImportNodeCommon;
export type ImportMergeNode = { op: "merge_node"; properties: ImportProperties } & ImportNodeCommon;
export type ImportReplaceNode = { op: "replace_node"; properties: ImportProperties } & ImportNodeCommon;
export type ImportRemoveNode = { op: "remove_node" } & ImportNodeCommon;
export type ImportAddRel = { op: "add_rel"; properties: ImportProperties } & ImportRelCommon;
export type ImportMergeRel = { op: "merge_rel"; properties: ImportProperties } & ImportRelCommon;
export type ImportReplaceRel = {
  op: "replace_rel";
  properties: Omit<ImportProperties, "DataSource">;
} & ImportRelCommon;
export type ImportRemoveRel = { op: "remove_rel" } & ImportRelCommon;

export type ImportLine =
  | ImportAddNode
  | ImportMergeNode
  | ImportReplaceNode
  | ImportRemoveNode
  | ImportAddRel
  | ImportMergeRel
  | ImportReplaceRel
  | ImportRemoveRel;

export type ImportOperationType = ImportLine["op"];

export const importOperations = [
  "add_node",
  "merge_node",
  "replace_node",
  "remove_node",
  "add_rel",
  "merge_rel",
  "replace_rel",
  "remove_rel",
];
