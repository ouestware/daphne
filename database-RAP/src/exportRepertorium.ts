import { createPool } from "mariadb";
import { config } from "dotenv";
import { stringify } from "ndjson";
import { referencesToSources } from "./models/source";

import { exit } from "process";
import { createWriteStream } from "fs";
import * as stream from "stream";
import * as util from "util";
import { faitsToFactoids } from "./models/factoid";
import { personnesToPhysicalPerson } from "./models/physicalPerson";
import { lieuToPlace } from "./models/place";

const pipeline = util.promisify(stream.pipeline);

// load .env
config();

async function* rapToDaphneOperations() {
  //create connection to mariadb
  const pool = createPool({
    host: "localhost",
    port: +(process.env.MARIADB_PORT || "3306"),
    user: "root",
    password: process.env.MYSQL_ROOT_PASSWORD,
    trace: true,
    dateStrings: true,
  });

  // INDICES
  const dateIds = new Set<string>();

  // SOURCES
  console.log("streaming sources");
  const sources = referencesToSources(await pool.getConnection(), { dateIds });
  for await (const s of sources) {
    yield s;
  }
  // PEOPLE
  console.log("streaming physicalPersons (béta)");
  // TODO: transformation is minimal to be done
  for await (const p of personnesToPhysicalPerson(pool)) {
    yield p;
  }
  // places
  console.log("streaming places (béta)");
  for await (const p of lieuToPlace(pool)) {
    yield p;
  }

  // FACTOIDS

  console.log("streaming factoids");
  for await (const factoidOperation of faitsToFactoids(pool)) {
    yield factoidOperation;
  }
  console.log("streaming operations ends");
}

const exportRepertorium = async () => {
  // transform + output stream
  const ndJsonStream = stringify();
  const outputStream = createWriteStream("./rap.ndjson", {
    flags: "w",
    encoding: "utf-8",
  });

  const operationsStream = stream.Readable.from(rapToDaphneOperations());

  await pipeline(operationsStream, ndJsonStream, outputStream);
};

exportRepertorium()
  .catch((error) => {
    console.log(error);
    exit(1);
  })
  .then(() => {
    exit(0);
  });
