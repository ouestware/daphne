export const CorpusInline = `
  fragment CorpusInline on Corpus {
    id
    name
  }
`;

export const findCorpusesQuery = `
  query findCorpuses($where: CorpusWhere) {
    corpuses(where: $where) {
      id
      name
    }
  }
`;

export const deleteCorpusMutation = `
  mutation deleteCorpus($id: ID!) {
    deleteCorpus(id: $id)
  }
`;

export const createCorpusMutation = `
  mutation createCorpus($name: String!, $description: String) {
    createCorpus(name: $name, description: $description) {
      id
      name
    }
  }
`;
