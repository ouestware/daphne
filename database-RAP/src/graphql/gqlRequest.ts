import axios from "axios";
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";

export const gql =
  <V, D>(query: string) =>
  (variables: V): Promise<D | null> =>
    gqlRequest<V, D>({ query, variables });

export interface GQLQuery<V> {
  query: string;
  variables: V;
}

export const gqlRequest = async <V, D>(query: GQLQuery<V>): Promise<D | null> => {
  const request = await axios.post<{ data: D }>("http://localhost:4000/graphql", query, {
    responseType: "json",
  });
  if (request) return request.data.data;
  else return null;
};
