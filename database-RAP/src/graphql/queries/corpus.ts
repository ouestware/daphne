import { createCorpusMutation, deleteCorpusMutation, findCorpusesQuery } from "../corpus.gql";
import { Corpus } from "../generated/graphql";
import { gql } from "../gqlRequest";

export const listCorpus = gql<{ where: Partial<Corpus> }, { corpuses: Pick<Corpus, "id" | "name">[] }>(
  findCorpusesQuery,
);

export const deleteCorpus = gql<{ id: string }, boolean>(deleteCorpusMutation);

export const createCorpus = gql<{ name: string; description?: string }, { createCorpus: { id: string } }>(
  createCorpusMutation,
);
