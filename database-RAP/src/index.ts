import { createCorpus, deleteCorpus, listCorpus } from "./graphql/queries/corpus";
import { every } from "lodash";
import { exit } from "process";

export const importRAP = async () => {
  console.log("starting RAP import... ");

  console.log("Looking for existing RAP corpus");
  const rapCorpuses = await listCorpus({ where: { name: "RAP" } });
  if (rapCorpuses) {
    const existingCorpus = rapCorpuses.corpuses.filter((c) => c.name === "RAP");
    if (existingCorpus) {
      console.log(`deleting ${existingCorpus.length} existing Corpus ${JSON.stringify(existingCorpus)}`);
      const deleted = await Promise.all(existingCorpus.map((c) => deleteCorpus({ id: c.id })));
      if (every(deleted)) console.log(`deleted`);
    }
  }
  //create corpus
  const RapCorpusId = await createCorpus({ name: "RAP", description: "Poitier tout ça..." });
  if (RapCorpusId) console.log(`new corpus created ${RapCorpusId.createCorpus.id}`);
};

try {
  importRAP().then(() => {
    console.log("done");
    exit(0);
  });
} catch (error) {
  console.error(error);
}
