# Repertorium academicum pictaviense

Scripts which imports data from Repertorium academicum pictaviense project into a Daphné instance.

http://repertorium.projets.univ-poitiers.fr/documentation/registre.php

## load dump

- place dump into data
- make sure to remove the foreign keys to users

```sql
-- ADD CONSTRAINT `faits_ibfk_5` FOREIGN KEY (`modif_uid`) REFERENCES `utilisateurs` (`uid`) ON DELETE NO ACTION ON UPDATE CASCADE,
```

- start docker
- load dump

```
 docker exec -i daphne-rap-db-1 sh -c 'exec mysql -u root --password=$MYSQL_ROOT_PASSWORD' < ./data/2022-10-12-RAP.sql
```

## modelisation

![MySQL database schema](./RAP_mysql_schema.png)

> user table has been isolated as user data are not included in the dump

### Personnes

From `Personnes` create:

- `Person`
- `Physical Person`
- `Name` (add prefix and suffix ?)

Factoids:

- birth
- death
- origin

No certainty but precision on birth death dates.

### Faits

La table `Faits` liste les inscriptions transcriptes des registres de l'université. On y trouve:

- diplômes obtenus type ="GRAD"
- personne certifiant le fait type= "SIGN"
- alias de nom
- (ré)inscription type ="MAT" "INC"
- continue d'atudier type = "STUD" ??
- maître type = "DOM"
- (a précédemment étudié) jusqu'à type = "EXITUNIV"

Factoid naissance
