import { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
  schema: "http://localhost:4000/graphql",
  generates: {
    "./src/graphql/generated/": {
      plugins: [],
      preset: "client",
    },
  },
  watchConfig: {
    usePolling: true,
    interval: 1000,
  },
  debug: true,
  verbose: true,
};

export default config;
