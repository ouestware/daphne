The Daphne[^1] platform developed in the framework of a national project (ANR) is a tool allowing the historian to query online the facts (factoids) present in a set of prosopographical databases previously loaded.

It is based on a rich model of accurate data storage integrating different dimensions of uncertainty, designed to integrate any prosopographical databases at the cost of writing a dedicated import process.

With the help of this platform, prosopographers can explore the various properties of factoids while considering their uncertainty using hyperlinks and a faceted search engine.

It also allows a visual and progressive exploration of selected data in the form of manipulable, storable and expandable node-link graphs.

The platform has been designed to foster an investigative process allowing scholars to confront their hypotheses, increase or decrease the uncertainty of stored facts, or even create new ones through an annotation system. All modifications are stored in a separate layer than source data to allow future update. Daphne acts as an investigative engine leaving the data ground-truth to the original database editors.

So far Daphne is used on the Repertorium academicum pictaviense[^2] and Studium Parisiense[^3] databases.

[^1]: http://daphne.huma-num.fr/
[^2]: http://repertorium.projets.univ-poitiers.fr/
[^3]: http://studium.univ-paris1.fr/
