import { program } from "commander";
import { createWriteStream } from "fs";
import * as stream from "stream";
import * as util from "util";
import { stringify } from "ndjson";
import { MultiDirectedGraph } from "graphology";

const pipeline = util.promisify(stream.pipeline);

import config from "./utils/config";
import getDaphneGraph from "./utils/get-daphne-graph";
import { addEdgeOperation, addNodeOperation, EdgeType, NodeType } from "./utils/daphne";
import { omit } from "lodash";

program.name("build-ndjson");
program.description("A script to index Studium prosopographies in Daphné");
program.parse();

function getNodeType(id: string): NodeType {
  return id.split("::")[0] as NodeType;
}

async function* getDaphneOperations(graph: MultiDirectedGraph, log = console.log) {
  log("Start writing node operations");
  let i = 0;
  for (const { node, attributes } of graph.nodeEntries()) {
    yield addNodeOperation(getNodeType(node), { id: node, ...attributes });

    i++;
    if (!(i % 1000)) console.log(`  -> ${i} / ${graph.order} nodes written`);
  }

  i = 0;
  for (const {
    source,
    target,
    attributes: { type, ...rest },
  } of graph.edgeEntries()) {
    yield addEdgeOperation(
      type as EdgeType,
      { id: source, type: getNodeType(source) },
      { id: target, type: getNodeType(target) },
      rest,
    );

    i++;
    if (!(i % 1000)) console.log(`  -> ${i} / ${graph.size} edges written`);
  }
}

async function init() {
  let step = 0;

  const graph = await getDaphneGraph(config, (...attrs: unknown[]) => console.log(`${++step}.`, ...attrs));

  console.log(`${++step}. Start writing .ndjson file`);
  const ndJsonStream = stringify();
  const d = new Date();
  const outputStream = createWriteStream(`./studium-${d.getFullYear()}${d.getMonth() + 1}${d.getDate()}.ndjson`, {
    flags: "w",
    encoding: "utf-8",
  });

  const operationsStream = stream.Readable.from(
    getDaphneOperations(graph, (...attrs: unknown[]) => console.log(`${++step}.`, ...attrs)),
  );
  await pipeline(operationsStream, ndJsonStream, outputStream);

  console.log("All good!");
}

init().catch((error) => {
  throw error;
});
