import { sortBy } from "lodash";
import { program } from "commander";
import { intersectionSize } from "mnemonist/set";

import config from "./utils/config";
import getLiteGraph, { PROSOPOGRAPHY_TYPE } from "./utils/get-lite-graph";

program.name("extract-similar-pairs");
program.description("A script to find most similar prosopographies pairs from Studium");
program.parse();

const MIN_COINCIDENCES = 10;
const RESULTS = 20;

async function init() {
  let step = 0;

  const graph = await getLiteGraph(config, (...attrs: unknown[]) => console.log(`${++step}.`, ...attrs));

  console.log(`${++step}. Index graph candidates`);
  const candidatesIndex: string[] = [];
  graph.forEachNode((node, attrs) => {
    if (attrs.type === PROSOPOGRAPHY_TYPE && graph.neighbors(node).length > MIN_COINCIDENCES)
      candidatesIndex.push(node);
  });

  console.log(`${++step}. Find pairs`);
  const neighborsSetIndices = candidatesIndex.reduce(
    (iter, p) => ({
      ...iter,
      [p]: new Set(graph.neighbors(p)),
    }),
    {},
  ) as Record<string, Set<string>>;
  const results: { p1: string; p2: string; coincidences: number }[] = [];
  const length = candidatesIndex.length;
  for (let i = 0; i < length; i++) {
    for (let j = i + 1; j < length; j++) {
      const p1 = candidatesIndex[i];
      const p2 = candidatesIndex[j];

      const coincidences = intersectionSize(neighborsSetIndices[p1], neighborsSetIndices[p2]);

      if (coincidences > MIN_COINCIDENCES) {
        results.push({
          p1,
          p2,
          coincidences,
        });
      }
    }
  }

  console.log(`${++step}. Results count:`, results.length);

  const log = sortBy(results, (res) => -res.coincidences).slice(0, RESULTS);
  console.log(`${++step}. Top ${log.length} results:`);
  log.forEach((res) =>
    console.log(
      `  -> ${res.p1} (${graph.getNodeAttribute(res.p1, "name")}) - ${res.p2} (${graph.getNodeAttribute(
        res.p2,
        "name",
      )}) - ${res.coincidences}`,
    ),
  );

  console.log("All good!");
}

init().catch((error) => {
  throw error;
});
