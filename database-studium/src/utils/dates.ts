import assert from "assert";

export const isValidDate = (date: string) => {
  try {
    const isoRegexp = /^\d{4}(-\d{2})?(-\d{2})?$/;
    assert(isoRegexp.test(date));
    new Date(date);
    return true;
  } catch {
    return false;
  }
};
