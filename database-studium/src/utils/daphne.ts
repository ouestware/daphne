export type NodeType =
  | "Source"
  | "Factoid"
  | "SourceType"
  | "Edition"
  | "FactoidType"
  | "PhysicalPerson"
  | "PersonName"
  | "FactoidPersonParticipate"
  | "Role"
  | "Rank"
  | "Person"
  | "DateInstant"
  | "Place";

interface AddNode<P> {
  op: "add_node";
  type: NodeType;
  id?: string;
  properties: { id?: string } & P;
}

export const addNodeOperation = <P>(type: NodeType, properties: { id?: string } & P): AddNode<P> => {
  return { op: "add_node", type, id: properties.id, properties };
};

export type EdgeType =
  | "HAS_SOURCE_TYPE"
  | "PART_OF"
  | "PUBLISHED_AS"
  | "IS_NAMED"
  | "HAS_FACTOID_TYPE"
  | "OCCURED_AT"
  | "TOOK_PLACE_AT"
  | "PARTICIPATE"
  | "REFERS_TO"
  | "WITH_ROLE";

interface NodeIdentifier {
  id: string;
  type: NodeType;
}

interface AddEdge<P> {
  op: "add_rel";
  type: EdgeType;
  source: NodeIdentifier;
  target: NodeIdentifier;
  properties: P;
}

export const addEdgeOperation = <P>(
  type: EdgeType,
  source: NodeIdentifier,
  target: NodeIdentifier,
  properties: P,
): AddEdge<P> => {
  return { op: "add_rel", type, source, target, properties };
};

export interface DateInstantAlt {
  id?: string;
  startValue: string; //ISO format
  endValue?: string; //ISO format
}
