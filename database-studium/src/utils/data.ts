/**
 * Dates:
 * ******
 */
import { isArray, omit } from "lodash";

export interface SimpleDateDefinition {
  type: "SIMPLE";
  date: number;
}
export interface IntervalDefinition {
  type: "INTERVAL";
  startDate: SimpleDateDefinition | null;
  endDate: SimpleDateDefinition | null;
}
export type DateDefinition = SimpleDateDefinition | IntervalDefinition;

/**
 * Factoids:
 * *********
 */
export interface Factoid {
  value: string;
  reference?: string[];
  comment?: string[];
  meta: {
    names?: string[];
    dates?: DateDefinition[];
    places?: string[];
    titles?: string[];
    institutions?: string[];
    isComment?: boolean;
    isLink?: boolean;
  };
}
export type FactoidNode = Factoid | Factoid[] | FactoidNodesCollection;
export interface FactoidNodesCollection extends Record<string, FactoidNode> {}
export const ITEM_TYPES: (keyof Factoid["meta"])[] = ["names", "places", "titles", "institutions"];

/**
 * Prosopographies:
 * ****************
 */
export type Prosopography = {
  reference: string;
  title: string;
  link: string;
  raw: string[];
  extras: {
    activityMediane: number;
  };
  identity?: {
    name?: Factoid[];
    nameVariant?: Factoid[];
    shortDescription?: Factoid[];
    datesOfActivity?: Factoid[];
    gender?: Factoid[];
    status?: Factoid[];
  };
  bibliography?: {
    workSources?: Factoid[];
    workReferences?: Factoid[];
    bookReferences?: Factoid[];
    otherBases?: Factoid[];
  }
} & FactoidNodesCollection;

function getNodeFactoids(node: FactoidNode, path: string[]): { path: string[]; factoid: Factoid }[] {
  if (isArray(node)) return node.map((factoid) => ({ path, factoid }));
  if (typeof node.value === "string") return [{ factoid: node as Factoid, path }];

  const collection = node as FactoidNodesCollection;
  return Object.keys(collection).flatMap((key) => getNodeFactoids(collection[key], path.concat(key)));
}
export function getFactoids(doc: Prosopography): { path: string[]; factoid: Factoid }[] {
  const nodes = omit(doc, "reference", "title", "link", "raw", "extras") as FactoidNodesCollection;
  try {
    return getNodeFactoids(nodes, []);
  } catch (e) {
    console.log("Problematic doc:", doc);
    throw e;
  }
}
