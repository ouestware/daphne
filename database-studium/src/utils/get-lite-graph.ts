import { MongoClient } from "mongodb";
import { MultiDirectedGraph } from "graphology";

import CONFIG from "./config";
import { getFactoids, ITEM_TYPES, Prosopography } from "./data";

export const PROSOPOGRAPHY_TYPE = "Prosopography";
export const FINGERPRINT_TYPE = "Fingerprint";

function echap(s: string | undefined): string {
  return (s || "").replace(/'/g, "\\'");
}

export default async function getLiteGraph(config = CONFIG, log = console.log): Promise<MultiDirectedGraph> {
  log(`Initialize mongo session`);
  const mongoClient = new MongoClient(config.mongo.uri);
  const db = mongoClient.db(config.mongo.db);
  const collection = db.collection<Prosopography>("prosopography");

  const graph = new MultiDirectedGraph();

  log(`Index data`);
  let indexedDocs = 0;
  const docs = await collection.find();
  const docsCount = await collection.countDocuments();
  let doc;
  while ((doc = await docs.next())) {
    const docID = echap(doc._id.toString());
    const factoids = getFactoids(doc);

    graph.addNode(docID, { name: doc.title || docID, type: PROSOPOGRAPHY_TYPE });

    let items: { name: string; type: string; path: string; value: string }[] = [];
    for (const { factoid, path } of factoids) {
      const pathString = path.join(" > ");
      items = items.concat(
        ITEM_TYPES.flatMap((type) =>
          ((factoid.meta[type] || []) as string[]).map((name) => ({
            name,
            type,
            path: pathString,
            value: factoid.value,
          })),
        ),
      );
    }

    items.forEach((item) => {
      graph.mergeNode(item.name, {
        name: item.name || docID,
        type: FINGERPRINT_TYPE,
        fingerprintType: item.type,
      });
      graph.addEdge(docID, item.name, { path: item.path, value: item.value });
    });

    indexedDocs++;
    if (indexedDocs % 1000 === 0) console.log(`  -> ${indexedDocs} / ${docsCount} docs indexed`);
  }

  log(`Closing mongo connection`);
  await mongoClient.close();

  return graph;
}
