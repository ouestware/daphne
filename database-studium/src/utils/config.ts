import { env } from "process";

export default {
  mongo: {
    uri: "mongodb://localhost:27017",
    db: "studium",
  },
  neo4j: {
    uri: `neo4j://localhost:${env.NEO4J_ADMIN_PORT}`,
    boltUri: `neo4j://localhost:${env.NEO4J_BOLT_PORT}`,
    username: env.NEO4J_LOGIN || "neo4j",
    password: env.NEO4J_PASSWORD || "adminadmin",
  },
};
