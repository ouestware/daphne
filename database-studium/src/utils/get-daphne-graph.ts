import { MultiDirectedGraph } from "graphology";
import { MongoClient } from "mongodb";
import slugify from "slugify";

import CONFIG from "./config";
import { Factoid, getFactoids, Prosopography } from "./data";
import { isValidDate } from "./dates";

function clean(s: string): string {
  return s
    .replace(/^[\t \n]+/g, "")
    .replace(/[\t \n]+$/g, "")
    .replace(/( ;|%|\.)+$/g, "");
}

export default async function getDaphneGraph(config = CONFIG, log = console.log): Promise<MultiDirectedGraph> {
  log(`Initialize mongo session`);
  const mongoClient = new MongoClient(config.mongo.uri);
  const db = mongoClient.db(config.mongo.db);
  const collection = db.collection<Prosopography>("prosopography");

  const graph = new MultiDirectedGraph();
  const personNamesIndex: Record<string, string[]> = {};

  log(`Index constant nodes`);
  const titleTypeId = "ObjectType::Titre";
  graph.mergeNode(titleTypeId, {
    name: "Titre",
    description: "Titre, tel que décrit dans Studium",
  });

  log(`Index persons`);
  let indexedDocs = 0;
  let docs = await collection.find();
  const docsCount = await collection.countDocuments();
  let prosopography;
  while ((prosopography = await docs.next())) {
    // Create PhysicalPerson node:
    const personId = `PhysicalPerson::${slugify("" + prosopography.reference)}`;
    const description = (prosopography.identity?.shortDescription || [])
      .map((factoid: Factoid) => factoid.value)
      .join(", ");
    // Check other names:
    const otherNames = (prosopography.identity?.nameVariant || []).flatMap(
      (factoid: Factoid) => factoid.meta.names || [],
    );
    const name = prosopography.title || (otherNames.length > 0 ? otherNames[0] : "");
    graph.mergeNode(personId, {
      name,
      description: description,
    });
    personNamesIndex[name] = (personNamesIndex[name] || []).concat(personId);

    for (const name of otherNames) {
      const nameId = `PersonName::${slugify(name)}`;
      personNamesIndex[name] = (personNamesIndex[name] || []).concat(personId);
      graph.mergeNode(nameId, {
        name,
      });
      graph.mergeEdge(personId, nameId, {
        type: "IS_NAMED",
      });
    }

    // Check sources:
    const bibliography: Record<string, Factoid[]> = prosopography.bibliography || {};
    for (const sourceType in bibliography) {
      const sources = bibliography[sourceType];
      const sourceTypeId = `SourceType::${slugify(sourceType)}`;
      graph.mergeNode(sourceTypeId, {
        name: sourceType,
      });
      sources.forEach((rawSource) => {
        const source = clean(rawSource.value);
        const sourceId = `Source::${slugify(source)}`;
        graph.mergeNode(sourceId, {
          name: source,
        });
        graph.mergeEdge(sourceId, sourceTypeId, {
          type: "HAS_SOURCE_TYPE",
        });
        graph.mergeEdge(personId, sourceId, {
          type: "ISSUED_BY",
        });
      });
    }

    indexedDocs++;
    if (indexedDocs % 1000 === 0) console.log(`  -> ${indexedDocs} / ${docsCount} docs indexed`);
  }

  log(`Index factoids`);
  indexedDocs = 0;
  docs = await collection.find();
  while ((prosopography = await docs.next())) {
    // Create PhysicalPerson node:
    const personId = `PhysicalPerson::${slugify("" + prosopography.reference)}`;

    const factoids = getFactoids(prosopography).filter(
      (factoid) =>
        !(
          factoid.path[0] === "identity" &&
          (factoid.path[1] === "shortDescription" || factoid.path[1] === "nameVariant" || factoid.path[1] === "name")
        ) && factoid.path[0] !== "bibliography",
    );
    factoids.forEach(({ factoid, path }, i) => {
      const factoidType = path.join("-");
      const factoidId = `Factoid::${personId}::${factoidType}-${i}`;
      const factoidTypeId = `FactoidType::${factoidType}`;

      // Register factoid:
      graph.mergeNode(factoidId, {
        originalText: factoid.value,
        ...(factoid.comment?.length ? { description: factoid.comment.join(", ") } : {}),
      });

      // Register factoid type:
      graph.mergeNode(factoidTypeId, {
        name: path.join(" > "),
      });
      graph.mergeEdge(factoidId, factoidTypeId, {
        type: "HAS_FACTOID_TYPE",
      });

      // Link factoid to this person:
      const factoidPersonParticipateId = `FactoidPersonParticipate::${factoidId}::${personId}`;
      graph.mergeNode(factoidPersonParticipateId, { certainty: 1 });
      graph.mergeEdge(personId, factoidPersonParticipateId, { type: "PARTICIPATE" });
      graph.mergeEdge(factoidPersonParticipateId, factoidId, { type: "PARTICIPATE" });

      // Look for other persons:
      factoid.meta?.names?.forEach((name) => {
        const persons = personNamesIndex[name] || [];
        persons.forEach((otherPersonId) => {
          const factoidPersonParticipateId = `FactoidPersonParticipate::${factoidId}::${otherPersonId}`;
          const certainty = 1 / persons.length;

          graph.mergeNode(factoidPersonParticipateId, { certainty });
          graph.mergeEdge(otherPersonId, factoidPersonParticipateId, { type: "PARTICIPATE" });
          graph.mergeEdge(factoidPersonParticipateId, factoidId, { type: "PARTICIPATE" });

          graph.mergeEdge(personId, otherPersonId, { type: "CONNECTED_TO", certainty });
        });
      });

      // Link factoid to places:
      factoid.meta?.places?.forEach((rawPlace) => {
        const place = clean(rawPlace);
        if (!place) return;

        const placeId = `Place::${slugify(place)}`;
        graph.mergeNode(placeId, { name: place });
        graph.mergeEdge(factoidId, placeId, { type: "TOOK_PLACE_AT" });
      });

      // Link factoid to sources:
      factoid.reference?.forEach((rawRef) => {
        const ref = clean(rawRef);
        if (!ref) return;

        const sourceId = `Source::${slugify(ref)}`;
        graph.mergeNode(sourceId, { name: ref });
        graph.mergeEdge(factoidId, sourceId, { type: "REFERS_TO" });
      });

      // Link factoid to institutions (as moral entities):
      factoid.meta?.institutions?.forEach((rawInstitution) => {
        const institution = clean(rawInstitution);
        if (!institution) return;

        const institutionId = `MoralEntity::${slugify(institution)}`;
        const factoidInstitutionParticipateId = `FactoidPersonParticipate::${factoidId}::${institutionId}`;

        graph.mergeNode(institutionId, { name: institution });

        graph.mergeNode(factoidPersonParticipateId);
        graph.mergeEdge(institutionId, factoidInstitutionParticipateId, { type: "PARTICIPATE" });
        graph.mergeEdge(factoidInstitutionParticipateId, factoidId, { type: "PARTICIPATE" });

        graph.mergeEdge(personId, institutionId, { type: "CONNECTED_TO" });
      });

      // Link factoid to titles (as objects):
      factoid.meta?.titles?.forEach((rawTitle) => {
        const title = clean(rawTitle);
        const titleId = `Object::Titre::${slugify(title)}`;

        graph.mergeNode(titleId, {
          name: title,
        });
        graph.mergeEdge(titleId, titleTypeId, { type: "HAS_OBJECT_TYPE" });
        graph.mergeEdge(factoidId, titleId, { type: "IMPACTS" });
      });

      // Link factoid to dates:
      factoid.meta?.dates?.forEach((date) => {
        if (date.type === "SIMPLE") {
          const dateType = isValidDate("" + date.date) ? "DateValue" : "DateIncorrect";
          const dateId = `${dateType}::${date.date}`;
          graph.mergeNode(dateId, { startIso: "" + date.date, endIso: "" + date.date });
          graph.mergeEdge(factoidId, dateId, { type: "OCCURED_AT" });
        } else if (date.type === "INTERVAL") {
          const startDate = date.startDate?.date ? date.startDate.date + "" : undefined;
          const endDate = date.endDate?.date ? date.endDate.date + "" : undefined;

          const dateType =
            (!startDate && !endDate) || (startDate && !isValidDate(startDate)) || (endDate && !isValidDate(endDate))
              ? "DateIncorrect"
              : "DateValue";
          const dateId = `${dateType}::${startDate}::${endDate}`;
          graph.mergeNode(dateId, { startIso: startDate, endIso: endDate });
          graph.mergeEdge(factoidId, dateId, { type: "OCCURED_AT" });
        }
      });
    });

    indexedDocs++;
    if (indexedDocs % 1000 === 0) console.log(`  -> ${indexedDocs} / ${docsCount} docs searched for factoids`);
  }

  log(`Closing mongo connection`);
  await mongoClient.close();

  return graph;
}
