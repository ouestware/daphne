 import { extname } from 'path';
import { mapValues } from 'lodash';
import { readFile } from 'fs/promises';
import { deserialize, DeserializeOptions } from 'bson';

function _getNextObjectSize(buffer: Buffer) {
  return buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
}

/**
 * Helps sync reading a raw arbitrary BSON file:
 */
export function deserializeBSON<T>(buffer: Buffer, options: DeserializeOptions = {}): T[] {
  let _buffer = buffer;
  let _result = [];

  while (_buffer.length > 0) {
    let nextSize = _getNextObjectSize(_buffer);
    if (_buffer.length < nextSize) {
      throw new Error('Corrupted BSON file: the last object is incomplete.');
    } else if (_buffer[nextSize - 1] !== 0) {
      throw new Error(
        `Corrupted BSON file: the ${_result.length + 1}-th object does not end with 0.`
      );
    }

    let obj = deserialize(_buffer, {
      ...options,
      promoteBuffers: false,
      allowObjectSmallerThanBufferSize: true,
    });
    // _result.push(obj as T);
    _result.push(mapValues(obj, (v) => (v instanceof Buffer ? v.toString() : v)) as T);
    _buffer = _buffer.slice(nextSize);
  }

  return _result;
}

/**
 * Helps retrieve the content of any JSON or BSON file:
 */
export async function readDataFile<T = unknown>(path: string): Promise<T[]> {
  const ext = extname(path).toLowerCase();

  switch (extname(path).toLowerCase()) {
    case '.json': {
      const buffer = await readFile(path);
      const data = JSON.parse(buffer.toString());

      return data as T[];
    }
    case '.bson': {
      const buffer = await readFile(path);
      const data = deserializeBSON(buffer, {
        promoteValues: true,
        raw: true,
      });

      return data as T[];
    }
    default:
      throw new Error(`readDataFile: File extension "${ext}" not recognized.`);
  }
}
