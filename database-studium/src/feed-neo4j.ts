import { chunk } from "lodash";
import neo4j from "neo4j-driver";
import { program } from "commander";

import config from "./utils/config";
import getLiteGraph, { FINGERPRINT_TYPE, PROSOPOGRAPHY_TYPE } from "./utils/get-lite-graph";

program.name("feed-neo4j");
program.description("A script to index Studium prosopographies in Neo4j");
program.parse();

async function init() {
  let step = 0;

  const graph = await getLiteGraph(config, (...attrs: unknown[]) => console.log(`${++step}.`, ...attrs));

  console.log(`${++step}. Initialize Neo4j session`);
  const neo4JDriver = neo4j.driver(
    config.neo4j.boltUri,
    neo4j.auth.basic(config.neo4j.username, config.neo4j.password),
  );
  const neo4JSession = neo4JDriver.session();

  console.log(`${++step}. Clear existing Neo4j data`);
  await neo4JSession.run("MATCH (a) DETACH DELETE a");

  await neo4JSession.run(
    "CREATE CONSTRAINT prosopography_id IF NOT EXISTS FOR (n:Prosopography) REQUIRE n.id IS UNIQUE",
  );
  await neo4JSession.run(
    `CREATE CONSTRAINT fingerprint_name IF NOT EXISTS FOR (n:FingerPrint) REQUIRE n.name IS UNIQUE`,
  );

  const prosopographies: string[] = [];
  const fingerprints: string[] = [];
  graph.forEachNode((node, attrs) => {
    if (attrs.type === PROSOPOGRAPHY_TYPE) prosopographies.push(node);
    if (attrs.type === FINGERPRINT_TYPE) fingerprints.push(node);
  });

  console.log(`${++step}. Index fingerprints into Neo4j`);
  let indexedDocs = 0;
  const CHUNK_SIZE = 100;
  const fingerprintsChunks = chunk(fingerprints, CHUNK_SIZE);
  for (let i = 0; i < fingerprintsChunks.length; i++) {
    const ids = fingerprintsChunks[i];
    const items = ids.map((id) => ({
      id,
      name: graph.getNodeAttribute(id, "name"),
      type: graph.getNodeAttribute(id, "fingerprintType"),
    }));
    await neo4JSession.run(`UNWIND $items as item CREATE (n:FingerPrint { name: item.name }) SET n.type = item.type`, {
      items,
    });

    indexedDocs += CHUNK_SIZE;
    if (indexedDocs % 1000 === 0) console.log(`  -> ${indexedDocs} / ${fingerprints.length} fingerprints indexed`);
  }

  console.log(`${++step}. Index prosopographies into Neo4j`);
  indexedDocs = 0;
  const prosopographiesChunks = chunk(prosopographies, CHUNK_SIZE);
  for (let i = 0; i < prosopographiesChunks.length; i++) {
    const ids = prosopographiesChunks[i];
    const items = ids.map((id) => ({ id, name: graph.getNodeAttribute(id, "name") }));
    await neo4JSession.run(`UNWIND $items as item CREATE (n:Prosopography { name: item.name, id: item.id })`, {
      items,
    });

    for (let j = 0; j < ids.length; j++) {
      const prosopographyID = ids[j];
      const items = graph.edges(prosopographyID).map((id) => ({
        target: graph.target(id),
        ...graph.getEdgeAttributes(id),
      }));
      await neo4JSession.run(
        `MATCH (p:Prosopography { id: $prosopographyID })
         UNWIND $items AS item
           MATCH (i:FingerPrint { name: item.target })
             CREATE (p)-[r:HasFingerPrint { path: item.path, value: item.value }]->(i)`,
        {
          prosopographyID,
          items,
        },
      );
    }

    indexedDocs += CHUNK_SIZE;
    if (indexedDocs % 1000 === 0)
      console.log(`  -> ${indexedDocs} / ${prosopographies.length} prosopographies indexed`);
  }

  console.log(`${++step}. Closing Neo4j connection`);
  await neo4JDriver.close();

  console.log("All good!");
}

init().catch((error) => {
  throw error;
});
