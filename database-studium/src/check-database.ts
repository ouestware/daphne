import { MongoClient } from 'mongodb';
import neo4j from 'neo4j-driver';

import config from './utils/config';

async function init() {
  let step = 0;
  const mongoClient = new MongoClient(config.mongo.uri);
  const db = mongoClient.db(config.mongo.db);

  // Ping MongoDB:
  console.log(`${++step}. MongoDB database "${config.mongo.db}" exists`);
  await db.command({ ping: 1 });

  // Log elements counts in each collection:
  console.log(`${++step}. Listing collections`);
  const collections = await db.collections();
  for (const collection of collections) {
    if (collection.collectionName === 'logs') continue;

    const count = await collection.countDocuments();
    console.log(`  -> ${count} documents in collection ${collection.collectionName}`);
  }

  console.log(`${++step}. Closing MongoDB connection`);
  await mongoClient.close();

  // Ping Neo4J
  const neo4JDriver = neo4j.driver(
    config.neo4j.boltUri,
    neo4j.auth.basic(config.neo4j.username, config.neo4j.password)
  );
  const neo4JSession = neo4JDriver.session();

  console.log(`${++step}. Pinging Neo4J server`);
  await neo4JSession.run('SHOW DATABASE neo4j');

  console.log(`${++step}. Closing Neo4J connection`);
  await neo4JDriver.close();

  console.log('All good!');
}

init().catch((error) => {
  throw error;
});
