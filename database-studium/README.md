# Daphné - Studium database import scripts

## Setup

1. Download a dump of the Studium database
2. Copy everything (.json and .bson files) into the `./dump` folder
3. Install Node dependencies by running `npm install`
4. Run the Docker stack by running

```bash
cd ./docker
docker compose up -d --build
```

5. Import the dump in the Mongo container by running:

```bash
docker exec -it studium-mongo bash
studium-mongo> mongorestore -d studium /data/dump
```

At this point, you can explore the database using [Mongo Express](https://github.com/mongo-express/mongo-express) by opening http://localhost:8081 in your browser, and then select the `studium` database.

You can also run `npm run check-database` to see if everything went well.

## Using Neo4J

You can index the dataset into Neo4J by running `npm run feed-neo4j`. This script is quite long, but once it's done, you can explore the database on Neo4J as well by opening http://localhost:7474 in your browser.

### Useful queries

- Find coincidences between two prosopographies:

```cypher
MATCH (p1:Prosopography { name: $p1 })-[e1]->(f:FingerPrint)<-[e2]-(p2:Prosopography { name: $p2 })
    RETURN p1, e1, f, e2, p2;
```

- Fetch ego-centered graph of a prosopography:

```cypher
MATCH (p:Prosopography { name: $p })-[e1]->(f:FingerPrint)<-[e2]-(p2:Prosopography)
    WHERE apoc.node.degree(f) < 20
    WITH p, e1, f, e2, p2, count(f) as coincidences
    RETURN p, e1, f, e2, p2, coincidences
    ORDER BY coincidences DESC;
```

### Neo4j Aura

You can index everything on a free server on [Neo4j Aura](https://neo4j.com/cloud/aura-free/). This will allow you to use [Neo4j Bloom](https://neo4j.com/product/bloom/) to explore your data.

The file `./bloom-scene-sample.json` is an example scene file, with some rendering settings and two parameterized queries, that you can import from Bloom, to start exploring your data.

If you use it, you can try the two following queries:

- `Show neighbours of JOHANNES Juvenalis des Ursins`
- `Find coincidences between JACOBUS Jouvenel des Ursins and JOHANNES Juvenalis des Ursins`

## Exporting corpus to Daphne

To generate the `./studium-2022XXXX.ndjson` file that will allow you to import the Studium dataset into Daphne, just run the `npm run build-ndjson` command while a proper Mongo runs with the dump loaded.
