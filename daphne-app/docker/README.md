The Docker stack is composed of 36 containers:

- **elasticsearch** – Search
- **neo4j** – Graph database
- **project** – React front-end application & node graphql server

## Container philosophy

All the code is mounted on containers with a volume (ie. it's not part of the `Dockerfile` with a _ADD_ or a _COPY_). So code modification are directly available in containers. This is usefull for development, but also in production when you need to update the stack. You just have to `git pull` and restart the stack, without rebuilding all the containers.

You need to rebuild containers when there is a change on `Dockerfile`.

## Docker compose philosophy

- `.env` – the environment variables such as ports, version, path, users ...
- `docker-compose.yml` – the Docker compose file where all the stack is defined
- `docker-compose.override.yml` – the override to Docker compose file, which contains only port bindings.

In development, this setup provides direct access to the stack tools (like the Neo4j browser).

In production, to avoid security breach, we prefer to have only one port open on the stack,
and it's the one for the web application.

## Installation and Running

```sh
cd docker
docker compose up
```

If you don't want to expose all the server's port

```sh
docker compose -p daphne -f docker-compose.yml  up
```

Then, open `http://localhost`
