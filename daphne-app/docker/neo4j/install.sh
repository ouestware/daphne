#!/bin/bash
set -eou pipefail

#
# Installing neo4j-elasticsearch plugin if needed
#
PLUGIN_FILE=neo4j-elasticsearch-${ES_PLUGIN_VERSION}-all.jar
DOWNLOAD_URL="https://repo1.maven.org/maven2/com/ouestware/neo4j-elasticsearch/${ES_PLUGIN_VERSION}/${PLUGIN_FILE}"
cd ${NEO4J_HOME}/plugins

if [ ! -f "${NEO4J_HOME}/plugins/${PLUGIN_FILE}" ]; then
  echo "Installing plugin ${PLUGIN_FILE}"
  rm -f neo4j-elasticsearch*.jar
  wget ${DOWNLOAD_URL}
fi
