#!/bin/bash
cd /project

echo "(i) Npm version is $(npm -v)"
echo "(i) Node version is $(node -v)"

set -o allexport
source /.env
set +o allexport

echo
echo " ~"
echo " ~ Install dependencies"
echo " ~"
echo
# Take ownership of the node_module
mkdir -p ./node_modules
sudo chown -R docker:docker ./node_modules
npm install

echo
echo " ~"
echo " ~ Build project"
echo " ~"
echo
npm run build

echo
echo " ~"
echo " ~ Start the web server"
echo " ~"
echo
sudo nginx -c /etc/nginx/nginx.conf

echo
echo " ~"
echo " ~ Wait for neo4j"
echo " ~"
echo
attempt_counter=0
max_attempts=60
# Waiting until Neo4j is not available
until $(curl --output /dev/null --silent --head --fail http://neo4j:7474); do
  if [ ${attempt_counter} -eq ${max_attempts} ];then
    echo "Max attempts reached"
    exit 1
  fi
  printf '.'
  attempt_counter=$(($attempt_counter+1))
  sleep 1
done


mode=${1:-${MODE:-dev}};
case $mode in
  dev)
    echo
    echo " ~"
    echo " ~ Start project for dev"
    echo " ~"
    echo
    npm start
    ;;
  prod)
      echo
      echo " ~"
      echo " ~ Start project for prod"
      echo " ~"
      echo
      npm run start:prod
      ;;
  *)
    echo -e "Unexpected mode ${mode^}"
    ;;
esac
