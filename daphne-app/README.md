# Project Daphné

## Project structure

The root of the project is composed of two folder :

- docker : All file needed to run the stack with docker
- project : The code of the application

### Docker

Inside the folder, just run `docker compose -p daphne up`
When the stack is ready, just open your browser on this url : http://localhost

### Project

It's a javascript mono-repository, that contains two modules :

- server : the code for backend server which is an express with graphql
- client : a next.js application

## For developers

### The stack

The primary database is Neo4j (the source of truth), and some data are auto-replicated to elasticsearch for fulltext capabilities.
This synchronisation is done via a plugin directly in Neo4j.

The server is an express server with graphql, which is done via _neo4j/graphql_ library.

Client is an next.js application with typescript, used in SSR mode.

### Typescript

We use typescript for the project (client & server), so types are important  
and we try to have one source of truth for types in the project : the graphql schema

Thanks to neo4j/graphql library, the database model is driven by the graphql schema. Then we use the schema to generate types for :

- json schema of the graphql items
- server resolvers : https://the-guild.dev/graphql/codegen/plugins/typescript/typescript-resolvers
- client queries and fragment : https://the-guild.dev/graphql/codegen/docs/guides/react-vue

On both project, you can run the command `npm run generate` to generate all the typescript type.

NOTE: On the server, the ts generation is directly included in the script `build` & `start`. On client, you have to call it manually `npm run generate -- --watch`, and the server must be running

### Reset ES indices

First you need to be logged as an admin on the graphql endpoint.

```
mutation($password: String!) {
   jwt: userLogin(username: "admin", password: $password)
}
```

This mutation gives you a JWT that you must provide in an `Authorization` header like that :

```
Authorization: Bearer ${JWT}
```

Then you can call those mutation :

```
mutation { adminElasticCreateIndices(recreate:true) }
mutation { adminElasticIndexData }
```

## Inquiry and data import

Before to import your data in daphné, you need to create an inquiry.
For that you need to call the following mutation:

```
mutation($name: String!, $password: String!, $passwordConfirmation: String!) {
  inquiryCreate(name: $name, password: $password, passwordConfirmation: $passwordConfirmation) {
    id
    name
  }
}
```

NOTE: password will be used to create a user for the inquiry where it's name will be the id of the inquiry.

When it's done, you can call the mutation `inquiryBatchImportLocalFile` which takes :

- an inquiry ID
- a file

Ex:

```
mutation {
  inquiryBatchImportLocalFile(id: "rap", filePath: "rap.ndjson") {
    success
    errors
    warnings
  }
}
```

File is a [ndjson](http://ndjson.org/), so :

- each line is a valid JSON value
- line separator is `\n`
- encoding is UTF-8

Example:

```
{ "op": "add_node", type:"Person", id:"1", properties:{}}
{ "op": "replace_node", type:"Person", id:"1", properties:{}}
{ "op": "remove_node", type:"Person", id:"1"}
{ "op": "add_rel", type: "LINKED_TO", source:{id:"1", type:"Person"}, target:{id:"2", type:"Person"}, properties:{}}
{ "op": "replace_rel", type: "LINKED_TO", source:{id:"1", type:"Person"}, target:{id:"2", type:"Person"}, properties:{}}
{ "op": "remove_rel", type: "LINKED_TO", source:{id:"1", type:"Person"}, target:{id:"2", type:"Person"}, properties:{}}
```

NOTE:

- all node in the database must have an `id` field which is a string
- between two nodes, you can't have more than one relationship of the same type (and the same direction)

## Annexe

### Database schema

#### Hyper edges

- connectedTo : (person) - [certainty + source] -> (person)
- named : (person) - [certainty + source] -> (name)
- isLocated : (place) - [certainty + date] -> (Zone)
- partOf : (zone) - [certainty + date] -> (Zone)

#### Check loops (ie explicit edges that are implicit):

- factoid -> source | factoid -> type | type -> source

#### Question

- `person -> connectedTo -> person` should it be a Factoid ?
