export type LogLevel = "error" | "warning" | "info" | "debug" | "off";

interface ConfigLog {
  console_level: LogLevel;
  file_level: LogLevel;
  file_maxsize: string;
  file_retention: string;
  file_path: string;
}

interface ConfigNeo4j {
  url: string;
  login: string;
  password: string;
  database?: string;
  options?: { disableLosslessIntegers?: boolean; fetchSize?: number };
}

interface ConfigES {
  // List of ES hosts
  nodes: Array<string>;
  // Sniffing : discover cluster member at startup ?
  sniffOnStart: boolean;
  // Sniffing interval
  sniffInterval: number;
}

export interface Config {
  port: number;
  baseUrl: string;
  secret: string;
  error_with_stack: boolean;
  ssl: boolean;
  maxUploadSize: number;
  maxBodySize: string;
  defaultAdminPassword: string;
  import: {
    path: string;
    maxError: number;
    batchSize: number;
  };
  logs: ConfigLog;
  neo4j: ConfigNeo4j;
  elastic: ConfigES;
}
