import * as path from "path";
import { toNumber } from "lodash";
import { Config, LogLevel } from "./type";

const baseUrl = process.env.BASE_URL || "http://127.0.0.1";
export const config: Config = {
  port: process.env.BACKEND_PORT ? Number(process.env.BACKEND_PORT) : 4000,
  secret: process.env.BACKEND_SECRET || "azertyuiop",
  defaultAdminPassword: process.env.DEFAULT_ADMIN_PASSWORD || "_Daphn3!_",
  error_with_stack: process.env.ERROR_WITH_STACK ? true : false,
  baseUrl,
  ssl: baseUrl.startsWith("https"),
  maxUploadSize: 10000000,
  maxBodySize: "50mb",
  import: {
    path: process.env.FILES_STORAGE || path.join(__dirname, "./"),
    batchSize: 1000,
    maxError: process.env.IMPORT_MAX_ERROR ? toNumber(process.env.IMPORT_MAX_ERROR) : 50,
  },
  logs: {
    console_level: (process.env.LOG_CONSOLE_LEVEL || "debug") as LogLevel,
    file_level: (process.env.LOG_FILE_LEVEL || "off") as LogLevel,
    file_maxsize: "200m",
    file_retention: "7d",
    file_path: "./",
  },
  neo4j: {
    url: process.env.NEO4J_URL || "neo4j://localhost:7687",
    login: process.env.NEO4J_LOGIN || "neo4j",
    password: process.env.NEO4J_PASSWORD || "admin",
    options: {
      disableLosslessIntegers: true,
      fetchSize: 10000,
    },
  },
  elastic: {
    nodes: [process.env.ELASTIC_URL || "http://localhost:9200"],
    sniffOnStart: true,
    sniffInterval: 60000,
  },
};
