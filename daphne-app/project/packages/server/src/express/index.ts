import express, { Express } from "express";
import bodyParser from "body-parser";
import http, { Server } from "http";

import { initGraphql } from "../graphql";
import { config } from "../config";
import { getLogger } from "../service/technical/logger";
import { errorFilter } from "./error";

// Logger
const log = getLogger("Express");

export async function initExpress(): Promise<void> {
  // Create expressjs app
  const app: Express = express();
  app.set("trust proxy", true);
  app.use((req: express.Request, res: express.Response, next: express.NextFunction): void => {
    // hack to bridge secure & proxySecure for client-sessions
    // @see https://github.com/mozilla/node-client-sessions/issues/101
    if (req.secure) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (req.connection as any).proxySecure = true;
    }
    bodyParser.json({ limit: config.maxBodySize })(req, res, next);
  });

  // Create a server
  const server: Server = http.createServer(app);

  // Register GraphQL
  initGraphql(app, server);

  // Register error filter
  app.use(errorFilter);

  // Start the server
  server.listen({ port: config.port });
  log.info(`Server ready at http://localhost:${config.port}`);
}
