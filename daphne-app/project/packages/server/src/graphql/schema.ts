import { omit, pick, isNil, map, groupBy, keyBy } from "lodash";
import * as path from "path";
import { readFileSync } from "node:fs";
import { Container } from "typescript-ioc";
import { GraphQLResolveInfo, OperationTypeNode } from "graphql";
import { GraphQLUpload } from "graphql-upload-minimal";
import { delegateToSchema } from "@graphql-tools/delegate";
import { ExtractField } from "@graphql-tools/wrap";

import { config } from "../config";
import { FileSystem } from "../service/technical/filesystem";
import { Errors } from "../service/technical/error";
import { ElasticSearch } from "../service/technical/elasticsearch";
import { InquiryService } from "../service/business/inquiry";
import { ImportService } from "../service/business/import";
import { UserService } from "../service/business/user";
import { AclService } from "../service/business/acl";
import { AnnotationService } from "../service/business/annotation";
import { mergeReports } from "../utils/report";
import { Annotation, Factoid, FilterType, PhysicalPerson, Place, Resolvers } from "./generated/types";
import { GraphQlContext } from "./index";

export const typeDefs = readFileSync(path.resolve(__dirname, "./schema.graphql"), "utf8")
  .replace("scalar DateTime", "")
  .replace("scalar LocalDateTime", "")
  .replace("scalar LocalTime", "")
  .replace("scalar Date", "")
  .replace("scalar Time", "")
  .replace("scalar Duration", "")
  .replaceAll("@@ELASTIC_URL@@", config.elastic.nodes[0]);

const inquiry = Container.get(InquiryService);
const fileSystem = Container.get(FileSystem);
const elastic = Container.get(ElasticSearch);
const user = Container.get(UserService);
const acl = Container.get(AclService);
const importer = Container.get(ImportService);
const annotation = Container.get(AnnotationService);

const PEOPLE_INDEX = "people";
const FACTOIDS_INDEX = "factoids";
const PLACES_INDEX = "places";
const ROLES_INDEX = "roles";
const FACTOID_TYPES_INDEX = "factoid_types";

async function getPeople(context: GraphQlContext, info: GraphQLResolveInfo, ids: string[]) {
  return await delegateToSchema({
    schema: info.schema,
    operation: "query" as OperationTypeNode,
    fieldName: "_getPhysicalPersons",
    context: {
      ...context,
      cypherParams: { ids },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as Record<string, any>,
    info: info,
    transforms: [
      new ExtractField({
        from: ["_getPhysicalPersons", "results"],
        to: ["_getPhysicalPersons"],
      }),
    ],
  });
}
async function getPlaces(context: GraphQlContext, info: GraphQLResolveInfo, ids: string[]) {
  return await delegateToSchema({
    schema: info.schema,
    operation: "query" as OperationTypeNode,
    fieldName: "_getPlaces",
    context: {
      ...context,
      cypherParams: { ids },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as Record<string, any>,
    info: info,
    transforms: [
      new ExtractField({
        from: ["_getPlaces", "results"],
        to: ["_getPlaces"],
      }),
    ],
  });
}
async function getRoles(context: GraphQlContext, info: GraphQLResolveInfo, ids: string[]) {
  return await delegateToSchema({
    schema: info.schema,
    operation: "query" as OperationTypeNode,
    fieldName: "_getRoles",
    context: {
      ...context,
      cypherParams: { ids },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as Record<string, any>,
    info: info,
    transforms: [
      new ExtractField({
        from: ["_getRoles", "results"],
        to: ["_getRoles"],
      }),
    ],
  });
}
async function getFactoidTypes(context: GraphQlContext, info: GraphQLResolveInfo, ids: string[]) {
  return await delegateToSchema({
    schema: info.schema,
    operation: "query" as OperationTypeNode,
    fieldName: "_getFactoidTypes",
    context: {
      ...context,
      cypherParams: { ids },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as Record<string, any>,
    info: info,
    transforms: [
      new ExtractField({
        from: ["_getFactoidTypes", "results"],
        to: ["_getFactoidTypes"],
      }),
    ],
  });
}
async function getFactoids(context: GraphQlContext, info: GraphQLResolveInfo, ids: string[]) {
  return await delegateToSchema({
    schema: info.schema,
    operation: "query" as OperationTypeNode,
    fieldName: "_getFactoids",
    context: {
      ...context,
      cypherParams: { ids },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as Record<string, any>,
    info: info,
    transforms: [
      new ExtractField({
        from: ["_getFactoids", "results"],
        to: ["_getFactoids"],
      }),
    ],
  });
}

export const resolvers: Resolvers<GraphQlContext> = {
  Upload: GraphQLUpload,
  Person: {
    __resolveType(obj, _contextValue, _info) {
      // on union, neo4j add a `__resolveType` field
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return obj["__typename"] || (obj as any)["__resolveType"] || "PhysicalPerson";
    },
  },
  GraphSearchHit: {
    __resolveType(obj, _contextValue, _info) {
      // on union, neo4j add a `__resolveType` field
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return obj["__typename"] || (obj as any)["__resolveType"] || "Factoid";
    },
  },

  Query: {
    whoami: async (_root, _params, context, info) => {
      if (context.auth.jwt) {
        // Calling _getUsers query with the good cypherParams
        context.cypherParams = { ids: [context.auth.jwt.id] };
        const users = (await delegateToSchema({
          schema: info.schema,
          operation: "query" as OperationTypeNode,
          fieldName: "_getUsers",
          context: context,
          info: info,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        })) as Array<any>;

        if (users && users.length > 0) return users[0];
      }
      throw Errors.unauthorized();
    },
    physicalPersonsSearch: async (_root, params, context, info) => {
      const esResult = await elastic.search(
        PEOPLE_INDEX,
        params.search,
        isNil(params.skip) ? 0 : params.skip,
        isNil(params.limit) ? 0 : params.limit,
        params.aggregations || [],
        [{ field: "inquiryId", values: [params.inquiryId] }, ...(params.filters || [])],
        params.sort,
      );

      // Calling _getPhysicalPerson query with the good cypherParams
      const people = await getPeople(context, info, map(esResult.results, "id"));

      return {
        ...pick(esResult, ["total", "aggregations"]),
        results: people,
      };
    },
    placesSearch: async (_root, params, context, info) => {
      const esResult = await elastic.search(
        PLACES_INDEX,
        params.search,
        isNil(params.skip) ? 0 : params.skip,
        isNil(params.limit) ? 0 : params.limit,
        params.aggregations || [],
        [{ field: "inquiryId", values: [params.inquiryId] }, ...(params.filters || [])],
        params.sort,
      );

      // Calling _getPlaces query with the good cypherParams
      const places = await getPlaces(context, info, map(esResult.results, "id"));

      return {
        ...pick(esResult, ["total", "aggregations"]),
        results: places,
      };
    },
    rolesSearch: async (_root, params, context, info) => {
      const esResult = await elastic.search(
        ROLES_INDEX,
        params.search,
        isNil(params.skip) ? 0 : params.skip,
        isNil(params.limit) ? 0 : params.limit,
        [],
        [{ field: "inquiryId", type: FilterType.Terms, values: [params.inquiryId] }],
        params.sort,
      );

      // Calling _getPlaces query with the good cypherParams
      const roles = await getRoles(context, info, map(esResult.results, "id"));

      return {
        ...pick(esResult, ["total"]),
        results: roles,
      };
    },
    factoidTypesSearch: async (_root, params, context, info) => {
      const esResult = await elastic.search(
        FACTOID_TYPES_INDEX,
        params.search,
        isNil(params.skip) ? 0 : params.skip,
        isNil(params.limit) ? 0 : params.limit,
        [],
        [{ field: "inquiryId", type: FilterType.Terms, values: [params.inquiryId] }],
        params.sort,
      );
      // Calling _getPlaces query with the good cypherParams
      const types = await getFactoidTypes(context, info, map(esResult.results, "id"));

      return {
        ...pick(esResult, ["total"]),
        results: types,
      };
    },
    factoidsSearch: async (_root, params, context, info) => {
      const esResult = await elastic.search(
        FACTOIDS_INDEX,
        params.search,
        isNil(params.skip) ? 0 : params.skip,
        isNil(params.limit) ? 0 : params.limit,
        params.aggregations || [],
        [{ field: "inquiryId", values: [params.inquiryId] }, ...(params.filters || [])],
        params.sort,
      );

      // Calling _getPlaces query with the good cypherParams
      const factoids = await getFactoids(context, info, map(esResult.results, "id"));

      return {
        ...pick(esResult, ["total", "aggregations"]),
        results: factoids,
      };
    },
    graphSearch: async (_root, params, context, info) => {
      const esResult = await elastic.search(
        [PEOPLE_INDEX, PLACES_INDEX, FACTOIDS_INDEX].join(","),
        params.search,
        isNil(params.skip) ? 0 : params.skip,
        isNil(params.limit) ? 0 : params.limit,
        [],
        [{ field: "inquiryId", values: [params.inquiryId] }],
      );

      const groupedEsResult = groupBy(esResult.results, "index");
      const neo4jResults: Record<string, Record<string, Factoid | PhysicalPerson | Place>> = {
        [PEOPLE_INDEX]: keyBy(await getPeople(context, info, map(groupedEsResult[PEOPLE_INDEX], "id")), "id"),
        [FACTOIDS_INDEX]: keyBy(await getFactoids(context, info, map(groupedEsResult[FACTOIDS_INDEX], "id")), "id"),
        [PLACES_INDEX]: keyBy(await getPlaces(context, info, map(groupedEsResult[PLACES_INDEX], "id")), "id"),
      };

      return {
        ...pick(esResult, ["total"]),
        results: esResult.results
          .map(({ id, index }) => (neo4jResults[index as keyof typeof neo4jResults] || {})[id])
          .filter((res) => !isNil(res)),
      };
    },
    inquiryAnnotations: async (_root, params, _context, _info) => {
      return annotation.getAnnotations(params.id);
    },
  },
  Mutation: {
    userLogin: async (_root, params, _context, _info) => {
      const curr = await user.login(params.username, params.password);
      const token = await user.generateToken(curr.id);
      return token;
    },
    userChangePassword: async (_root, params, context, _info) => {
      if (!context.auth.jwt) throw Errors.unauthorized();
      // the user to change (default is connected user)
      let userId = context.auth.jwt.id;

      // only admin can change the username of a user
      if (params.username && params.username !== context.auth.jwt.username) {
        await acl.checkIsAdmin(context);
        const result = await user.findByUsername(params.username);
        if (!result) throw Errors.notFound(`User ${params.username} not found`);
        userId = result.id;
      }
      await user.changePassword({ id: userId }, params.password);
      return true;
    },
    inquiryCreate: async (_root, params, context, info) => {
      // only admin can create a inquiry
      await acl.checkIsAdmin(context);

      // Create the inquiry
      const created = await inquiry.create(omit(params, ["password"]), params.password);

      // Calling inquiries query with the good params
      const result = await delegateToSchema({
        schema: info.schema,
        operation: "query" as OperationTypeNode,
        fieldName: "inquiries",
        args: { where: { id: created.id } },
        context: context,
        info: info,
      });

      return result[0];
    },
    inquiryUpdate: async (_root, params, context, info) => {
      await acl.checkIsInquiryWriter(context, params.id);

      // update the password if needed
      if (params.password) {
        await acl.checkIsAdmin(context);
        await user.changePassword({ username: params.id }, params.password);
      }

      // update the inquiry
      const updated = await inquiry.update(params.id, omit(params, ["id"]));

      // Calling inquiry query with the good params
      const result = await delegateToSchema({
        schema: info.schema,
        operation: "query" as OperationTypeNode,
        fieldName: "inquiries",
        args: { where: { id: updated.id } },
        context: context,
        info: info,
      });

      return result[0];
    },
    inquiryDelete: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);
      return await inquiry.delete(params.id);
    },
    inquiryFlush: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);
      await inquiry.flush(params.id);
      return true;
    },
    inquiryImportFile: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);

      const file = await params.file;
      const filePath = await fileSystem.saveTemp(file, `inquiry-${params.id}`);
      const importReport = await importer.import(params.id, filePath, params.flush);

      // if inquiry has been flushed, we replay the annotations
      if (params.flush) {
        const annotReport = await annotation.applyAnnotations(params.id);
        return mergeReports(importReport, annotReport);
      }

      return importReport;
    },
    inquiryBatchImportFile: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);

      const file = await params.file;
      const filePath = await fileSystem.saveTemp(file, `inquiry-${params.id}`);
      const importReport = await importer.batchImport(params.id, filePath, params.flush, params.batchSize, params.skip);
      // if inquiry has been flushed, we replay the annotations
      if (params.flush) {
        const annotReport = await annotation.applyAnnotations(params.id);
        return mergeReports(importReport, annotReport);
      }

      return importReport;
    },
    inquiryImportLocalFile: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);

      const filePath = path.join(config.import.path, params.filePath);
      const importReport = await importer.import(params.id, filePath, params.flush);

      // if inquiry has been flushed, we replay the annotations
      if (params.flush) {
        const annotReport = await annotation.applyAnnotations(params.id);
        return mergeReports(importReport, annotReport);
      }

      return importReport;
    },
    inquiryBatchImportLocalFile: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);

      const filePath = path.join(config.import.path, params.filePath);
      const importReport = await importer.batchImport(params.id, filePath, params.flush, params.batchSize, params.skip);

      // if inquiry has been flushed, we replay the annotations
      if (params.flush) {
        const annotReport = await annotation.applyAnnotations(params.id);
        return mergeReports(importReport, annotReport);
      }

      return importReport;
    },
    inquiryAnnotationCreate: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);
      await annotation.createAnnotation(params.id, {
        ...params,
        inquiryId: params.id,
      } as Omit<Annotation, "id" | "createdAt" | "updatedAt">);
      return true;
    },
    inquiryAnnotationReplay: async (_root, params, context) => {
      await acl.checkIsInquiryWriter(context, params.id);
      return await annotation.applyAnnotations(params.id);
    },
    adminElasticCreateIndices: async (_root, params, context) => {
      await acl.checkIsAdmin(context);
      return elastic.createIndices(params.recreate);
    },
    adminElasticIndexData: async (_root, _params, context) => {
      await acl.checkIsAdmin(context);
      return elastic.indexData();
    },
  },
};
