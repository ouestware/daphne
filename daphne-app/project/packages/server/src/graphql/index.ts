import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import {
  ApolloServerPluginLandingPageLocalDefault,
  ApolloServerPluginLandingPageProductionDefault,
} from "@apollo/server/plugin/landingPage/default";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { Express } from "express";
import { json } from "body-parser";
import cors from "cors";
import * as neo4j from "neo4j-driver";
import { Neo4jGraphQL } from "@neo4j/graphql";
import { Neo4jGraphQLAuthJWTPlugin } from "@neo4j/graphql-plugin-auth";
import { Server } from "http";
import { graphqlUploadExpress } from "graphql-upload-minimal";
import { graphqlSync, getIntrospectionQuery, IntrospectionQuery } from "graphql";
import { fromIntrospectionQuery } from "graphql-2-json-schema";

import { getLogger } from "../service/technical/logger";
import { config } from "../config";
import { User } from "../types/user";
import { typeDefs, resolvers } from "./schema";

const log = getLogger("GraphQl");

export type GraphQlContext = {
  driver: neo4j.Driver;
  cypherParams: { [key: string]: unknown };
  auth: {
    isAuthenticated: boolean;
    jwt?: User & { iat: number };
  };
};

export async function initGraphql(app: Express, httpServer: Server): Promise<void> {
  // Neo4j driver
  const driver = neo4j.driver(config.neo4j.url, neo4j.auth.basic(config.neo4j.login, config.neo4j.password));

  // Build the neo4j graphql schema
  const neoSchema = new Neo4jGraphQL({
    typeDefs,
    resolvers,
    driver,
    plugins: {
      auth: new Neo4jGraphQLAuthJWTPlugin({
        secret: config.secret,
      }),
    },
  });
  const schema = await neoSchema.getSchema();
  await neoSchema.assertIndexesAndConstraints({ options: { create: true } });

  // Build & start the Apollo server
  const server = new ApolloServer<GraphQlContext>({
    schema,
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      process.env.NODE_ENV === "production"
        ? ApolloServerPluginLandingPageProductionDefault()
        : ApolloServerPluginLandingPageLocalDefault(),
    ],
    logger: log,
  });
  await server.start();

  // Add the server to express
  app.use(
    "/graphql",
    cors<cors.CorsRequest>(),
    json(),
    expressMiddleware(server, {
      context: async ({ req }) => ({ driver, req, cypherParams: {}, auth: { isAuthenticated: false } }),
    }),
  );

  // Register the middleware for the upload
  app.use(graphqlUploadExpress({ maxFileSize: config.maxUploadSize, maxFiles: 10 }));

  // Compute json schema of the graphql and expose it
  const introspection = graphqlSync({ schema, source: getIntrospectionQuery() });
  const jsonSchema = fromIntrospectionQuery(introspection.data as unknown as IntrospectionQuery);
  app.get("/graphql-json-schema", (_req, res) => res.send(jsonSchema));
}
