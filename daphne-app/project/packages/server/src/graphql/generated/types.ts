import { FileUpload } from '../models';
import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T;
export type InputMaybe<T> = T;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: Date;
  DateTime: Date;
  Duration: String;
  JSON: any;
  LocalDateTime: Date;
  LocalTime: number;
  Time: number;
  Upload: FileUpload;
};

export enum AggregationType {
  DateHistogramPerYear = 'date_histogram_per_year',
  Terms = 'terms'
}

/** Annotation */
export type Annotation = {
  __typename?: 'Annotation';
  author: Scalars['String'];
  comment?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  patch: Array<Scalars['JSON']>;
  updatedAt: Scalars['DateTime'];
};

/**
 * Relationship property for certainty
 * -----------------------------------
 * The value must be between 0 and 1
 */
export type Certainty = {
  certainty: Scalars['Float'];
};

/**
 * Relationship property for certainty & type
 * ------------------------------------------
 * certainty value must be between 0 and 1
 */
export type CertaintyAndType = {
  certainty: Scalars['Float'];
  type: Scalars['String'];
};

/** DataSource */
export type DataSource = {
  __typename?: 'DataSource';
  id: Scalars['ID'];
  name: Scalars['String'];
};

/**
 * Date
 * --------------
 * Dates can be approximative. Dates can alos be intervals.
 * This implementation encodes both approximations and intervals the same way to ease queries
 * A Date is expressed as a period into the gregorian calendar.
 * It has a start and end date.
 * We keep the original date as ISO string to keep track of the time granularity of extreme dates
 */
export type DateValue = {
  __typename?: 'DateValue';
  dataSources: Array<DataSource>;
  end?: Maybe<Scalars['Date']>;
  endIso?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  start?: Maybe<Scalars['Date']>;
  startIso?: Maybe<Scalars['String']>;
};

/** ------ */
export type Domain = {
  __typename?: 'Domain';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

/**
 * Edition
 * -------
 */
export type Edition = {
  __typename?: 'Edition';
  collection?: Maybe<Scalars['String']>;
  dataSources: Array<DataSource>;
  dates: Array<DateValue>;
  editor?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  notes?: Maybe<Scalars['String']>;
  pages?: Maybe<Scalars['String']>;
  place?: Maybe<Scalars['String']>;
  sources: Array<Source>;
  title?: Maybe<Scalars['String']>;
};

/**
 * Factoid
 * -------
 */
export type Factoid = {
  __typename?: 'Factoid';
  annotations: Array<Annotation>;
  certainty: Scalars['Float'];
  dataSources: Array<DataSource>;
  dates: Array<DateValue>;
  description?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Duration']>;
  id: Scalars['ID'];
  impacts: Array<Object>;
  inquiry: Inquiry;
  linkedTo: Array<Factoid>;
  originalText?: Maybe<Scalars['String']>;
  persons: Array<FactoidPersonParticipate>;
  places: Array<Place>;
  sources: Array<Source>;
  types: Array<FactoidType>;
};

/**
 * Relationship property for (Factoid)-[IMPACTS]->(Object)
 * -------------------------------------------------------
 * certainty value must be between 0 and 1
 */
export type FactoidImpactObject = {
  certainty: Scalars['Float'];
  quantity?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
};

/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipate = {
  __typename?: 'FactoidPersonParticipate';
  annotations: Array<Annotation>;
  certainty?: Maybe<Scalars['Float']>;
  dataSources: Array<DataSource>;
  factoid: Factoid;
  id: Scalars['ID'];
  person: Person;
  ranks: Array<Rank>;
  roles: Array<Role>;
};

/**
 * Relationship property for (Factoid)-[REFERS_TO]->(Source)
 * -------------------------------------------------------
 * certainty value must be between 0 and 1
 */
export type FactoidRefersToSource = {
  certainty?: Maybe<Scalars['Float']>;
  page?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidType = {
  __typename?: 'FactoidType';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  sources: Array<Source>;
};

export enum FilterType {
  Range = 'range',
  Terms = 'terms'
}

export type GraphSearchHit = Factoid | MoralEntity | PhysicalPerson | Place;

export type GraphSearchResult = {
  __typename?: 'GraphSearchResult';
  results: Array<GraphSearchHit>;
  total: Scalars['Int'];
};

/**
 * Group
 * -----
 */
export type Group = {
  __typename?: 'Group';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  partOf: Array<Group>;
};

/** Relationship property for object that has been imported from a data source */
export type ImportedFrom = {
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type Inquiry = {
  __typename?: 'Inquiry';
  annotations: Array<Annotation>;
  createdAt: Scalars['DateTime'];
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

/**
 * MoralEntity
 * -----------
 */
export type MoralEntity = {
  __typename?: 'MoralEntity';
  annotations: Array<Annotation>;
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  domains: Array<Domain>;
  id: Scalars['ID'];
  inquiry: Inquiry;
  name: Scalars['String'];
  partOf: Array<MoralEntity>;
};

export type Mutation = {
  __typename?: 'Mutation';
  /**
   * Admin task : create (or recreate) elasticsearch indices
   * Returns true if an index has been created, false otherwise.
   */
  adminElasticCreateIndices: Scalars['Boolean'];
  /**
   * Admin task : Re-index neo4j data into elastic.
   * Returns the number of document indexed
   */
  adminElasticIndexData: Scalars['Int'];
  /** Inquiry: Create an annotation on a node */
  inquiryAnnotationCreate?: Maybe<Scalars['Boolean']>;
  /** Inquiry: Replay all the annotation of the inquiry */
  inquiryAnnotationReplay: Report;
  /** Inquiry: data import as multiple non-reversible transactions (best for full corpus reload) */
  inquiryBatchImportFile: Report;
  /** Inquiry: data import with local file as multiple non-reversible transactions (best for full corpus reload) */
  inquiryBatchImportLocalFile: Report;
  /**
   * Inquiry: create a new inquiry
   * Only admin can create an inquiry
   */
  inquiryCreate?: Maybe<Inquiry>;
  /** Inquiry: delete a inquiry */
  inquiryDelete?: Maybe<Scalars['Boolean']>;
  /**
   * Inquiry: flush a inquiry
   * ------------------------
   */
  inquiryFlush?: Maybe<Scalars['Boolean']>;
  /** Inquiry: data import as a single reversible transaction (best for updates) */
  inquiryImportFile: Report;
  /** Inquiry: data import with local file as a single reversible transaction (best for updates) */
  inquiryImportLocalFile: Report;
  /**
   * Inquiry: update a inquiry
   * Only admin can update the password of an inquiry
   */
  inquiryUpdate?: Maybe<Inquiry>;
  /**
   * Change password for the connected user.
   * Only admin can specify a username
   */
  userChangePassword?: Maybe<Scalars['Boolean']>;
  /**
   * Check the login / password, and returns a JWT that contains the user's information.
   * This JWT must be passed in the authorization header as a bearer token, to this graphql endpoint if you want to be authenticated
   */
  userLogin?: Maybe<Scalars['String']>;
};


export type MutationAdminElasticCreateIndicesArgs = {
  recreate?: InputMaybe<Scalars['Boolean']>;
};


export type MutationInquiryAnnotationCreateArgs = {
  author: Scalars['String'];
  comment: Scalars['String'];
  id: Scalars['ID'];
  patch: Array<PatchInput>;
};


export type MutationInquiryAnnotationReplayArgs = {
  id: Scalars['ID'];
};


export type MutationInquiryBatchImportFileArgs = {
  batchSize?: InputMaybe<Scalars['Int']>;
  file: Scalars['Upload'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  skip?: InputMaybe<Scalars['Int']>;
};


export type MutationInquiryBatchImportLocalFileArgs = {
  batchSize?: InputMaybe<Scalars['Int']>;
  filePath: Scalars['String'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  skip?: InputMaybe<Scalars['Int']>;
};


export type MutationInquiryCreateArgs = {
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  password: Scalars['String'];
};


export type MutationInquiryDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationInquiryFlushArgs = {
  id: Scalars['ID'];
};


export type MutationInquiryImportFileArgs = {
  file: Scalars['Upload'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
};


export type MutationInquiryImportLocalFileArgs = {
  filePath: Scalars['String'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
};


export type MutationInquiryUpdateArgs = {
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
};


export type MutationUserChangePasswordArgs = {
  password: Scalars['String'];
  username?: InputMaybe<Scalars['String']>;
};


export type MutationUserLoginArgs = {
  password: Scalars['String'];
  username: Scalars['ID'];
};

/**
 * Object
 * ------
 */
export type Object = {
  __typename?: 'Object';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  types: Array<ObjectType>;
};

/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectType = {
  __typename?: 'ObjectType';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  partOf: Array<ObjectType>;
};

export type PatchInput = {
  id?: InputMaybe<Scalars['String']>;
  op: Scalars['String'];
  properties?: InputMaybe<Scalars['JSON']>;
  source?: InputMaybe<PatchNodeReferenceInput>;
  target?: InputMaybe<PatchNodeReferenceInput>;
  type: Scalars['String'];
};

export type PatchNodeReferenceInput = {
  id: Scalars['String'];
  type: Scalars['String'];
};

/**
 * Person
 * ------
 * A person can be a PhysicalPerson or a MoralEntity
 * See below for the PhysicalPerson & MoralEntity definitions
 */
export type Person = MoralEntity | PhysicalPerson;

/**
 * PersonName
 * ----------
 */
export type PersonName = {
  __typename?: 'PersonName';
  dataSources: Array<DataSource>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPerson = {
  __typename?: 'PhysicalPerson';
  annotations: Array<Annotation>;
  connectedTo: Array<PhysicalPerson>;
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  groups: Array<Group>;
  id: Scalars['ID'];
  inquiry: Inquiry;
  name: Scalars['String'];
  otherNames: Array<PersonName>;
  participatesTo: Array<FactoidPersonParticipate>;
  seeAlso: Array<PhysicalPerson>;
  sources: Array<Source>;
};

/**
 * Place
 * -----
 */
export type Place = {
  __typename?: 'Place';
  alternativeNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  annotations: Array<Annotation>;
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  factoids: Array<Factoid>;
  id: Scalars['ID'];
  inquiry: Inquiry;
  locatedIn: Array<PlaceZoneOverTime>;
  name: Scalars['String'];
};

/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTime = {
  __typename?: 'PlaceZoneOverTime';
  certainty: Scalars['Float'];
  dataSources: Array<DataSource>;
  date: DateValue;
  place: Place;
  zone: Zone;
};

export type Query = {
  __typename?: 'Query';
  /** DON'T use this method, it's only available internally */
  _getFactoidTypes: Array<FactoidType>;
  /** DON'T use this method, it's only available internally */
  _getFactoids: Array<Factoid>;
  /** DON'T use this method, it's only available internally */
  _getPhysicalPersons: Array<PhysicalPerson>;
  /** DON'T use this method, it's only available internally */
  _getPlaces: Array<Place>;
  /** DON'T use this method, it's only available internally */
  _getRoles: Array<Role>;
  /** DON'T use this method, it's only available internally */
  _getUsers: Array<User>;
  /** Search a factoid type */
  factoidTypesSearch: SearchFactoidTypesResult;
  /** Search a factoid */
  factoidsSearch: SearchFactoidsResult;
  /** Search for various types at the same time */
  graphSearch: GraphSearchResult;
  /** Inquiry: get all annotations of the inquiry */
  inquiryAnnotations: Array<Annotation>;
  /** Search a PhysicalPerson */
  physicalPersonsSearch: SearchPhysicalPersonsResult;
  /** Search a place */
  placesSearch: SearchPlacesResult;
  /** Search a role */
  rolesSearch: SearchRolesResult;
  /** Return the connected user */
  whoami: User;
};


export type QueryFactoidTypesSearchArgs = {
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryFactoidsSearchArgs = {
  aggregations?: InputMaybe<Array<SearchAggregationInput>>;
  filters?: InputMaybe<Array<SearchFilterInput>>;
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryGraphSearchArgs = {
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryInquiryAnnotationsArgs = {
  id: Scalars['ID'];
};


export type QueryPhysicalPersonsSearchArgs = {
  aggregations?: InputMaybe<Array<SearchAggregationInput>>;
  filters?: InputMaybe<Array<SearchFilterInput>>;
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryPlacesSearchArgs = {
  aggregations?: InputMaybe<Array<SearchAggregationInput>>;
  filters?: InputMaybe<Array<SearchFilterInput>>;
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryRolesSearchArgs = {
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};

/**
 * Rank
 * ----
 */
export type Rank = {
  __typename?: 'Rank';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

/**
 * A report generated by the server
 * --------------------------------
 * This type is used mainly for data import
 */
export type Report = {
  __typename?: 'Report';
  errors: Array<Maybe<Scalars['String']>>;
  success: Scalars['Boolean'];
  warnings: Array<Maybe<Scalars['String']>>;
};

/**
 * Role
 * ----
 */
export type Role = {
  __typename?: 'Role';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type SearchAggregation = {
  __typename?: 'SearchAggregation';
  name: Scalars['String'];
  total?: Maybe<Scalars['Int']>;
  values: Array<SearchAggregationItem>;
};

export type SearchAggregationInput = {
  field: Scalars['String'];
  id: Scalars['String'];
  include?: InputMaybe<Scalars['String']>;
  nested?: InputMaybe<Scalars['String']>;
  orderByAlpha?: InputMaybe<Scalars['Boolean']>;
  type: AggregationType;
};

export type SearchAggregationItem = {
  __typename?: 'SearchAggregationItem';
  count: Scalars['Int'];
  key: Scalars['String'];
};

export type SearchFactoidTypesResult = {
  __typename?: 'SearchFactoidTypesResult';
  results: Array<FactoidType>;
  total: Scalars['Int'];
};

export type SearchFactoidsResult = {
  __typename?: 'SearchFactoidsResult';
  aggregations: Array<SearchAggregation>;
  results: Array<Factoid>;
  total: Scalars['Int'];
};

export type SearchFilterInput = {
  field: Scalars['String'];
  type?: InputMaybe<FilterType>;
  values: Array<Scalars['JSON']>;
};

export type SearchPhysicalPersonsResult = {
  __typename?: 'SearchPhysicalPersonsResult';
  aggregations: Array<SearchAggregation>;
  results: Array<PhysicalPerson>;
  total: Scalars['Int'];
};

export type SearchPlacesResult = {
  __typename?: 'SearchPlacesResult';
  aggregations: Array<SearchAggregation>;
  results: Array<Place>;
  total: Scalars['Int'];
};

export type SearchRolesResult = {
  __typename?: 'SearchRolesResult';
  results: Array<Role>;
  total: Scalars['Int'];
};

export type SearchSortInput = {
  field: Scalars['String'];
  order: SortOrder;
};

export enum SortOrder {
  Asc = 'ASC',
  Desc = 'DESC'
}

/**
 * Source
 * ------
 */
export type Source = {
  __typename?: 'Source';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  editions: Array<Edition>;
  id: Scalars['ID'];
  inquiry: Inquiry;
  issuing: Array<PhysicalPerson>;
  language?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  partOf: Array<SourceType>;
  refering: Array<Factoid>;
  reputation?: Maybe<Scalars['Float']>;
  types: Array<SourceType>;
};

/**
 * SourceType
 * ----------
 */
export type SourceType = {
  __typename?: 'SourceType';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  partOf: Array<SourceType>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
  inquiries: Array<Inquiry>;
  isAdmin: Scalars['Boolean'];
  username: Scalars['String'];
};

/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type Zone = {
  __typename?: 'Zone';
  dataSources: Array<DataSource>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  partOf: Array<ZonePartOfOverTime>;
};

/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTime = {
  __typename?: 'ZonePartOfOverTime';
  certainty: Scalars['Float'];
  dataSources: Array<DataSource>;
  date: DateValue;
  partOf: Zone;
  zone: Zone;
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  AggregationType: AggregationType;
  Annotation: ResolverTypeWrapper<Annotation>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Certainty: never;
  CertaintyAndType: never;
  DataSource: ResolverTypeWrapper<DataSource>;
  Date: ResolverTypeWrapper<Scalars['Date']>;
  DateTime: ResolverTypeWrapper<Scalars['DateTime']>;
  DateValue: ResolverTypeWrapper<DateValue>;
  Domain: ResolverTypeWrapper<Domain>;
  Duration: ResolverTypeWrapper<Scalars['Duration']>;
  Edition: ResolverTypeWrapper<Edition>;
  Factoid: ResolverTypeWrapper<Factoid>;
  FactoidImpactObject: never;
  FactoidPersonParticipate: ResolverTypeWrapper<Omit<FactoidPersonParticipate, 'person'> & { person: ResolversTypes['Person'] }>;
  FactoidRefersToSource: never;
  FactoidType: ResolverTypeWrapper<FactoidType>;
  FilterType: FilterType;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  GraphSearchHit: ResolversTypes['Factoid'] | ResolversTypes['MoralEntity'] | ResolversTypes['PhysicalPerson'] | ResolversTypes['Place'];
  GraphSearchResult: ResolverTypeWrapper<Omit<GraphSearchResult, 'results'> & { results: Array<ResolversTypes['GraphSearchHit']> }>;
  Group: ResolverTypeWrapper<Group>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  ImportedFrom: never;
  Inquiry: ResolverTypeWrapper<Inquiry>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  JSON: ResolverTypeWrapper<Scalars['JSON']>;
  LocalDateTime: ResolverTypeWrapper<Scalars['LocalDateTime']>;
  LocalTime: ResolverTypeWrapper<Scalars['LocalTime']>;
  MoralEntity: ResolverTypeWrapper<MoralEntity>;
  Mutation: ResolverTypeWrapper<{}>;
  Object: ResolverTypeWrapper<Object>;
  ObjectType: ResolverTypeWrapper<ObjectType>;
  PatchInput: PatchInput;
  PatchNodeReferenceInput: PatchNodeReferenceInput;
  Person: ResolversTypes['MoralEntity'] | ResolversTypes['PhysicalPerson'];
  PersonName: ResolverTypeWrapper<PersonName>;
  PhysicalPerson: ResolverTypeWrapper<PhysicalPerson>;
  Place: ResolverTypeWrapper<Place>;
  PlaceZoneOverTime: ResolverTypeWrapper<PlaceZoneOverTime>;
  Query: ResolverTypeWrapper<{}>;
  Rank: ResolverTypeWrapper<Rank>;
  Report: ResolverTypeWrapper<Report>;
  Role: ResolverTypeWrapper<Role>;
  SearchAggregation: ResolverTypeWrapper<SearchAggregation>;
  SearchAggregationInput: SearchAggregationInput;
  SearchAggregationItem: ResolverTypeWrapper<SearchAggregationItem>;
  SearchFactoidTypesResult: ResolverTypeWrapper<SearchFactoidTypesResult>;
  SearchFactoidsResult: ResolverTypeWrapper<SearchFactoidsResult>;
  SearchFilterInput: SearchFilterInput;
  SearchPhysicalPersonsResult: ResolverTypeWrapper<SearchPhysicalPersonsResult>;
  SearchPlacesResult: ResolverTypeWrapper<SearchPlacesResult>;
  SearchRolesResult: ResolverTypeWrapper<SearchRolesResult>;
  SearchSortInput: SearchSortInput;
  SortOrder: SortOrder;
  Source: ResolverTypeWrapper<Source>;
  SourceType: ResolverTypeWrapper<SourceType>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Time: ResolverTypeWrapper<Scalars['Time']>;
  Upload: ResolverTypeWrapper<FileUpload>;
  User: ResolverTypeWrapper<User>;
  Zone: ResolverTypeWrapper<Zone>;
  ZonePartOfOverTime: ResolverTypeWrapper<ZonePartOfOverTime>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Annotation: Annotation;
  Boolean: Scalars['Boolean'];
  Certainty: never;
  CertaintyAndType: never;
  DataSource: DataSource;
  Date: Scalars['Date'];
  DateTime: Scalars['DateTime'];
  DateValue: DateValue;
  Domain: Domain;
  Duration: Scalars['Duration'];
  Edition: Edition;
  Factoid: Factoid;
  FactoidImpactObject: never;
  FactoidPersonParticipate: Omit<FactoidPersonParticipate, 'person'> & { person: ResolversParentTypes['Person'] };
  FactoidRefersToSource: never;
  FactoidType: FactoidType;
  Float: Scalars['Float'];
  GraphSearchHit: ResolversParentTypes['Factoid'] | ResolversParentTypes['MoralEntity'] | ResolversParentTypes['PhysicalPerson'] | ResolversParentTypes['Place'];
  GraphSearchResult: Omit<GraphSearchResult, 'results'> & { results: Array<ResolversParentTypes['GraphSearchHit']> };
  Group: Group;
  ID: Scalars['ID'];
  ImportedFrom: never;
  Inquiry: Inquiry;
  Int: Scalars['Int'];
  JSON: Scalars['JSON'];
  LocalDateTime: Scalars['LocalDateTime'];
  LocalTime: Scalars['LocalTime'];
  MoralEntity: MoralEntity;
  Mutation: {};
  Object: Object;
  ObjectType: ObjectType;
  PatchInput: PatchInput;
  PatchNodeReferenceInput: PatchNodeReferenceInput;
  Person: ResolversParentTypes['MoralEntity'] | ResolversParentTypes['PhysicalPerson'];
  PersonName: PersonName;
  PhysicalPerson: PhysicalPerson;
  Place: Place;
  PlaceZoneOverTime: PlaceZoneOverTime;
  Query: {};
  Rank: Rank;
  Report: Report;
  Role: Role;
  SearchAggregation: SearchAggregation;
  SearchAggregationInput: SearchAggregationInput;
  SearchAggregationItem: SearchAggregationItem;
  SearchFactoidTypesResult: SearchFactoidTypesResult;
  SearchFactoidsResult: SearchFactoidsResult;
  SearchFilterInput: SearchFilterInput;
  SearchPhysicalPersonsResult: SearchPhysicalPersonsResult;
  SearchPlacesResult: SearchPlacesResult;
  SearchRolesResult: SearchRolesResult;
  SearchSortInput: SearchSortInput;
  Source: Source;
  SourceType: SourceType;
  String: Scalars['String'];
  Time: Scalars['Time'];
  Upload: FileUpload;
  User: User;
  Zone: Zone;
  ZonePartOfOverTime: ZonePartOfOverTime;
};

export type AnnotationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Annotation'] = ResolversParentTypes['Annotation']> = {
  author?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  comment?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  createdAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  patch?: Resolver<Array<ResolversTypes['JSON']>, ParentType, ContextType>;
  updatedAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CertaintyResolvers<ContextType = any, ParentType extends ResolversParentTypes['Certainty'] = ResolversParentTypes['Certainty']> = {
  __resolveType: TypeResolveFn<null, ParentType, ContextType>;
  certainty?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
};

export type CertaintyAndTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['CertaintyAndType'] = ResolversParentTypes['CertaintyAndType']> = {
  __resolveType: TypeResolveFn<null, ParentType, ContextType>;
  certainty?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  type?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
};

export type DataSourceResolvers<ContextType = any, ParentType extends ResolversParentTypes['DataSource'] = ResolversParentTypes['DataSource']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface DateScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Date'], any> {
  name: 'Date';
}

export interface DateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['DateTime'], any> {
  name: 'DateTime';
}

export type DateValueResolvers<ContextType = any, ParentType extends ResolversParentTypes['DateValue'] = ResolversParentTypes['DateValue']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>;
  endIso?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>;
  startIso?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DomainResolvers<ContextType = any, ParentType extends ResolversParentTypes['Domain'] = ResolversParentTypes['Domain']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface DurationScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Duration'], any> {
  name: 'Duration';
}

export type EditionResolvers<ContextType = any, ParentType extends ResolversParentTypes['Edition'] = ResolversParentTypes['Edition']> = {
  collection?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  dates?: Resolver<Array<ResolversTypes['DateValue']>, ParentType, ContextType>;
  editor?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  notes?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  pages?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  place?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  sources?: Resolver<Array<ResolversTypes['Source']>, ParentType, ContextType>;
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type FactoidResolvers<ContextType = any, ParentType extends ResolversParentTypes['Factoid'] = ResolversParentTypes['Factoid']> = {
  annotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType>;
  certainty?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  dates?: Resolver<Array<ResolversTypes['DateValue']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  duration?: Resolver<Maybe<ResolversTypes['Duration']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  impacts?: Resolver<Array<ResolversTypes['Object']>, ParentType, ContextType>;
  inquiry?: Resolver<ResolversTypes['Inquiry'], ParentType, ContextType>;
  linkedTo?: Resolver<Array<ResolversTypes['Factoid']>, ParentType, ContextType>;
  originalText?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  persons?: Resolver<Array<ResolversTypes['FactoidPersonParticipate']>, ParentType, ContextType>;
  places?: Resolver<Array<ResolversTypes['Place']>, ParentType, ContextType>;
  sources?: Resolver<Array<ResolversTypes['Source']>, ParentType, ContextType>;
  types?: Resolver<Array<ResolversTypes['FactoidType']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type FactoidImpactObjectResolvers<ContextType = any, ParentType extends ResolversParentTypes['FactoidImpactObject'] = ResolversParentTypes['FactoidImpactObject']> = {
  __resolveType: TypeResolveFn<null, ParentType, ContextType>;
  certainty?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  quantity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
};

export type FactoidPersonParticipateResolvers<ContextType = any, ParentType extends ResolversParentTypes['FactoidPersonParticipate'] = ResolversParentTypes['FactoidPersonParticipate']> = {
  annotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType>;
  certainty?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  factoid?: Resolver<ResolversTypes['Factoid'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  person?: Resolver<ResolversTypes['Person'], ParentType, ContextType>;
  ranks?: Resolver<Array<ResolversTypes['Rank']>, ParentType, ContextType>;
  roles?: Resolver<Array<ResolversTypes['Role']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type FactoidRefersToSourceResolvers<ContextType = any, ParentType extends ResolversParentTypes['FactoidRefersToSource'] = ResolversParentTypes['FactoidRefersToSource']> = {
  __resolveType: TypeResolveFn<null, ParentType, ContextType>;
  certainty?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  page?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  permalink?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
};

export type FactoidTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['FactoidType'] = ResolversParentTypes['FactoidType']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  sources?: Resolver<Array<ResolversTypes['Source']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GraphSearchHitResolvers<ContextType = any, ParentType extends ResolversParentTypes['GraphSearchHit'] = ResolversParentTypes['GraphSearchHit']> = {
  __resolveType: TypeResolveFn<'Factoid' | 'MoralEntity' | 'PhysicalPerson' | 'Place', ParentType, ContextType>;
};

export type GraphSearchResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['GraphSearchResult'] = ResolversParentTypes['GraphSearchResult']> = {
  results?: Resolver<Array<ResolversTypes['GraphSearchHit']>, ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GroupResolvers<ContextType = any, ParentType extends ResolversParentTypes['Group'] = ResolversParentTypes['Group']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  partOf?: Resolver<Array<ResolversTypes['Group']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ImportedFromResolvers<ContextType = any, ParentType extends ResolversParentTypes['ImportedFrom'] = ResolversParentTypes['ImportedFrom']> = {
  __resolveType: TypeResolveFn<null, ParentType, ContextType>;
  originalId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  permalink?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
};

export type InquiryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Inquiry'] = ResolversParentTypes['Inquiry']> = {
  annotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType>;
  createdAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface JsonScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export interface LocalDateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['LocalDateTime'], any> {
  name: 'LocalDateTime';
}

export interface LocalTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['LocalTime'], any> {
  name: 'LocalTime';
}

export type MoralEntityResolvers<ContextType = any, ParentType extends ResolversParentTypes['MoralEntity'] = ResolversParentTypes['MoralEntity']> = {
  annotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  domains?: Resolver<Array<ResolversTypes['Domain']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  inquiry?: Resolver<ResolversTypes['Inquiry'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  partOf?: Resolver<Array<ResolversTypes['MoralEntity']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  adminElasticCreateIndices?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationAdminElasticCreateIndicesArgs, 'recreate'>>;
  adminElasticIndexData?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  inquiryAnnotationCreate?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType, RequireFields<MutationInquiryAnnotationCreateArgs, 'author' | 'comment' | 'id' | 'patch'>>;
  inquiryAnnotationReplay?: Resolver<ResolversTypes['Report'], ParentType, ContextType, RequireFields<MutationInquiryAnnotationReplayArgs, 'id'>>;
  inquiryBatchImportFile?: Resolver<ResolversTypes['Report'], ParentType, ContextType, RequireFields<MutationInquiryBatchImportFileArgs, 'file' | 'id'>>;
  inquiryBatchImportLocalFile?: Resolver<ResolversTypes['Report'], ParentType, ContextType, RequireFields<MutationInquiryBatchImportLocalFileArgs, 'filePath' | 'id'>>;
  inquiryCreate?: Resolver<Maybe<ResolversTypes['Inquiry']>, ParentType, ContextType, RequireFields<MutationInquiryCreateArgs, 'name' | 'password'>>;
  inquiryDelete?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType, RequireFields<MutationInquiryDeleteArgs, 'id'>>;
  inquiryFlush?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType, RequireFields<MutationInquiryFlushArgs, 'id'>>;
  inquiryImportFile?: Resolver<ResolversTypes['Report'], ParentType, ContextType, RequireFields<MutationInquiryImportFileArgs, 'file' | 'id'>>;
  inquiryImportLocalFile?: Resolver<ResolversTypes['Report'], ParentType, ContextType, RequireFields<MutationInquiryImportLocalFileArgs, 'filePath' | 'id'>>;
  inquiryUpdate?: Resolver<Maybe<ResolversTypes['Inquiry']>, ParentType, ContextType, RequireFields<MutationInquiryUpdateArgs, 'id'>>;
  userChangePassword?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType, RequireFields<MutationUserChangePasswordArgs, 'password'>>;
  userLogin?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType, RequireFields<MutationUserLoginArgs, 'password' | 'username'>>;
};

export type ObjectResolvers<ContextType = any, ParentType extends ResolversParentTypes['Object'] = ResolversParentTypes['Object']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  types?: Resolver<Array<ResolversTypes['ObjectType']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ObjectTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['ObjectType'] = ResolversParentTypes['ObjectType']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  partOf?: Resolver<Array<ResolversTypes['ObjectType']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type PersonResolvers<ContextType = any, ParentType extends ResolversParentTypes['Person'] = ResolversParentTypes['Person']> = {
  __resolveType: TypeResolveFn<'MoralEntity' | 'PhysicalPerson', ParentType, ContextType>;
};

export type PersonNameResolvers<ContextType = any, ParentType extends ResolversParentTypes['PersonName'] = ResolversParentTypes['PersonName']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type PhysicalPersonResolvers<ContextType = any, ParentType extends ResolversParentTypes['PhysicalPerson'] = ResolversParentTypes['PhysicalPerson']> = {
  annotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType>;
  connectedTo?: Resolver<Array<ResolversTypes['PhysicalPerson']>, ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  groups?: Resolver<Array<ResolversTypes['Group']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  inquiry?: Resolver<ResolversTypes['Inquiry'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  otherNames?: Resolver<Array<ResolversTypes['PersonName']>, ParentType, ContextType>;
  participatesTo?: Resolver<Array<ResolversTypes['FactoidPersonParticipate']>, ParentType, ContextType>;
  seeAlso?: Resolver<Array<ResolversTypes['PhysicalPerson']>, ParentType, ContextType>;
  sources?: Resolver<Array<ResolversTypes['Source']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type PlaceResolvers<ContextType = any, ParentType extends ResolversParentTypes['Place'] = ResolversParentTypes['Place']> = {
  alternativeNames?: Resolver<Maybe<Array<Maybe<ResolversTypes['String']>>>, ParentType, ContextType>;
  annotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  factoids?: Resolver<Array<ResolversTypes['Factoid']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  inquiry?: Resolver<ResolversTypes['Inquiry'], ParentType, ContextType>;
  locatedIn?: Resolver<Array<ResolversTypes['PlaceZoneOverTime']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type PlaceZoneOverTimeResolvers<ContextType = any, ParentType extends ResolversParentTypes['PlaceZoneOverTime'] = ResolversParentTypes['PlaceZoneOverTime']> = {
  certainty?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  date?: Resolver<ResolversTypes['DateValue'], ParentType, ContextType>;
  place?: Resolver<ResolversTypes['Place'], ParentType, ContextType>;
  zone?: Resolver<ResolversTypes['Zone'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  _getFactoidTypes?: Resolver<Array<ResolversTypes['FactoidType']>, ParentType, ContextType>;
  _getFactoids?: Resolver<Array<ResolversTypes['Factoid']>, ParentType, ContextType>;
  _getPhysicalPersons?: Resolver<Array<ResolversTypes['PhysicalPerson']>, ParentType, ContextType>;
  _getPlaces?: Resolver<Array<ResolversTypes['Place']>, ParentType, ContextType>;
  _getRoles?: Resolver<Array<ResolversTypes['Role']>, ParentType, ContextType>;
  _getUsers?: Resolver<Array<ResolversTypes['User']>, ParentType, ContextType>;
  factoidTypesSearch?: Resolver<ResolversTypes['SearchFactoidTypesResult'], ParentType, ContextType, RequireFields<QueryFactoidTypesSearchArgs, 'inquiryId' | 'limit' | 'search' | 'skip'>>;
  factoidsSearch?: Resolver<ResolversTypes['SearchFactoidsResult'], ParentType, ContextType, RequireFields<QueryFactoidsSearchArgs, 'inquiryId' | 'limit' | 'search' | 'skip'>>;
  graphSearch?: Resolver<ResolversTypes['GraphSearchResult'], ParentType, ContextType, RequireFields<QueryGraphSearchArgs, 'inquiryId' | 'limit' | 'search' | 'skip'>>;
  inquiryAnnotations?: Resolver<Array<ResolversTypes['Annotation']>, ParentType, ContextType, RequireFields<QueryInquiryAnnotationsArgs, 'id'>>;
  physicalPersonsSearch?: Resolver<ResolversTypes['SearchPhysicalPersonsResult'], ParentType, ContextType, RequireFields<QueryPhysicalPersonsSearchArgs, 'inquiryId' | 'limit' | 'search' | 'skip'>>;
  placesSearch?: Resolver<ResolversTypes['SearchPlacesResult'], ParentType, ContextType, RequireFields<QueryPlacesSearchArgs, 'inquiryId' | 'limit' | 'search' | 'skip'>>;
  rolesSearch?: Resolver<ResolversTypes['SearchRolesResult'], ParentType, ContextType, RequireFields<QueryRolesSearchArgs, 'inquiryId' | 'limit' | 'search' | 'skip'>>;
  whoami?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
};

export type RankResolvers<ContextType = any, ParentType extends ResolversParentTypes['Rank'] = ResolversParentTypes['Rank']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ReportResolvers<ContextType = any, ParentType extends ResolversParentTypes['Report'] = ResolversParentTypes['Report']> = {
  errors?: Resolver<Array<Maybe<ResolversTypes['String']>>, ParentType, ContextType>;
  success?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  warnings?: Resolver<Array<Maybe<ResolversTypes['String']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type RoleResolvers<ContextType = any, ParentType extends ResolversParentTypes['Role'] = ResolversParentTypes['Role']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchAggregationResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchAggregation'] = ResolversParentTypes['SearchAggregation']> = {
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  total?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  values?: Resolver<Array<ResolversTypes['SearchAggregationItem']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchAggregationItemResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchAggregationItem'] = ResolversParentTypes['SearchAggregationItem']> = {
  count?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  key?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchFactoidTypesResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchFactoidTypesResult'] = ResolversParentTypes['SearchFactoidTypesResult']> = {
  results?: Resolver<Array<ResolversTypes['FactoidType']>, ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchFactoidsResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchFactoidsResult'] = ResolversParentTypes['SearchFactoidsResult']> = {
  aggregations?: Resolver<Array<ResolversTypes['SearchAggregation']>, ParentType, ContextType>;
  results?: Resolver<Array<ResolversTypes['Factoid']>, ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchPhysicalPersonsResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchPhysicalPersonsResult'] = ResolversParentTypes['SearchPhysicalPersonsResult']> = {
  aggregations?: Resolver<Array<ResolversTypes['SearchAggregation']>, ParentType, ContextType>;
  results?: Resolver<Array<ResolversTypes['PhysicalPerson']>, ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchPlacesResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchPlacesResult'] = ResolversParentTypes['SearchPlacesResult']> = {
  aggregations?: Resolver<Array<ResolversTypes['SearchAggregation']>, ParentType, ContextType>;
  results?: Resolver<Array<ResolversTypes['Place']>, ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SearchRolesResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['SearchRolesResult'] = ResolversParentTypes['SearchRolesResult']> = {
  results?: Resolver<Array<ResolversTypes['Role']>, ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SourceResolvers<ContextType = any, ParentType extends ResolversParentTypes['Source'] = ResolversParentTypes['Source']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  editions?: Resolver<Array<ResolversTypes['Edition']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  inquiry?: Resolver<ResolversTypes['Inquiry'], ParentType, ContextType>;
  issuing?: Resolver<Array<ResolversTypes['PhysicalPerson']>, ParentType, ContextType>;
  language?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  partOf?: Resolver<Array<ResolversTypes['SourceType']>, ParentType, ContextType>;
  refering?: Resolver<Array<ResolversTypes['Factoid']>, ParentType, ContextType>;
  reputation?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  types?: Resolver<Array<ResolversTypes['SourceType']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SourceTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['SourceType'] = ResolversParentTypes['SourceType']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  partOf?: Resolver<Array<ResolversTypes['SourceType']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface TimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Time'], any> {
  name: 'Time';
}

export interface UploadScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Upload'], any> {
  name: 'Upload';
}

export type UserResolvers<ContextType = any, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  inquiries?: Resolver<Array<ResolversTypes['Inquiry']>, ParentType, ContextType>;
  isAdmin?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  username?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ZoneResolvers<ContextType = any, ParentType extends ResolversParentTypes['Zone'] = ResolversParentTypes['Zone']> = {
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  partOf?: Resolver<Array<ResolversTypes['ZonePartOfOverTime']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ZonePartOfOverTimeResolvers<ContextType = any, ParentType extends ResolversParentTypes['ZonePartOfOverTime'] = ResolversParentTypes['ZonePartOfOverTime']> = {
  certainty?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  dataSources?: Resolver<Array<ResolversTypes['DataSource']>, ParentType, ContextType>;
  date?: Resolver<ResolversTypes['DateValue'], ParentType, ContextType>;
  partOf?: Resolver<ResolversTypes['Zone'], ParentType, ContextType>;
  zone?: Resolver<ResolversTypes['Zone'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type Resolvers<ContextType = any> = {
  Annotation?: AnnotationResolvers<ContextType>;
  Certainty?: CertaintyResolvers<ContextType>;
  CertaintyAndType?: CertaintyAndTypeResolvers<ContextType>;
  DataSource?: DataSourceResolvers<ContextType>;
  Date?: GraphQLScalarType;
  DateTime?: GraphQLScalarType;
  DateValue?: DateValueResolvers<ContextType>;
  Domain?: DomainResolvers<ContextType>;
  Duration?: GraphQLScalarType;
  Edition?: EditionResolvers<ContextType>;
  Factoid?: FactoidResolvers<ContextType>;
  FactoidImpactObject?: FactoidImpactObjectResolvers<ContextType>;
  FactoidPersonParticipate?: FactoidPersonParticipateResolvers<ContextType>;
  FactoidRefersToSource?: FactoidRefersToSourceResolvers<ContextType>;
  FactoidType?: FactoidTypeResolvers<ContextType>;
  GraphSearchHit?: GraphSearchHitResolvers<ContextType>;
  GraphSearchResult?: GraphSearchResultResolvers<ContextType>;
  Group?: GroupResolvers<ContextType>;
  ImportedFrom?: ImportedFromResolvers<ContextType>;
  Inquiry?: InquiryResolvers<ContextType>;
  JSON?: GraphQLScalarType;
  LocalDateTime?: GraphQLScalarType;
  LocalTime?: GraphQLScalarType;
  MoralEntity?: MoralEntityResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Object?: ObjectResolvers<ContextType>;
  ObjectType?: ObjectTypeResolvers<ContextType>;
  Person?: PersonResolvers<ContextType>;
  PersonName?: PersonNameResolvers<ContextType>;
  PhysicalPerson?: PhysicalPersonResolvers<ContextType>;
  Place?: PlaceResolvers<ContextType>;
  PlaceZoneOverTime?: PlaceZoneOverTimeResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Rank?: RankResolvers<ContextType>;
  Report?: ReportResolvers<ContextType>;
  Role?: RoleResolvers<ContextType>;
  SearchAggregation?: SearchAggregationResolvers<ContextType>;
  SearchAggregationItem?: SearchAggregationItemResolvers<ContextType>;
  SearchFactoidTypesResult?: SearchFactoidTypesResultResolvers<ContextType>;
  SearchFactoidsResult?: SearchFactoidsResultResolvers<ContextType>;
  SearchPhysicalPersonsResult?: SearchPhysicalPersonsResultResolvers<ContextType>;
  SearchPlacesResult?: SearchPlacesResultResolvers<ContextType>;
  SearchRolesResult?: SearchRolesResultResolvers<ContextType>;
  Source?: SourceResolvers<ContextType>;
  SourceType?: SourceTypeResolvers<ContextType>;
  Time?: GraphQLScalarType;
  Upload?: GraphQLScalarType;
  User?: UserResolvers<ContextType>;
  Zone?: ZoneResolvers<ContextType>;
  ZonePartOfOverTime?: ZonePartOfOverTimeResolvers<ContextType>;
};

