/**
 * Given a string, it returns a slugify version of it.
 */
export function slugify(value: string): string {
  return value
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/[\s]/g, "-")
    .replace(/[!"#$%&'()*+,./:;<=>?@[\]^_`{|}~]/g, "");
}
