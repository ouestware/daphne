import { Report } from "../graphql/generated/types";

export function getEmptyReport(): Report {
  return {
    success: false,
    errors: [],
    warnings: [],
  };
}

export function mergeReports(...reports: Array<Report>): Report {
  const report = getEmptyReport();
  report.errors = reports.flatMap((r) => r.errors);
  report.warnings = reports.flatMap((r) => r.warnings);
  report.success = reports.map((r) => r.success).every((b) => b === true);
  return report;
}
