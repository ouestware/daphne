import assert from "assert";
import { sortBy } from "lodash";
import neo4j, { Date as Neo4jDate } from "neo4j-driver";

export const transformIsoDate = (
  isoDate?: string,
): { start: Neo4jDate<number>; end: Neo4jDate<number> } | undefined => {
  if (!isoDate) return undefined;
  try {
    const isoRegexp = /^\d{4}(-\d{2})?(-\d{2})?$/;
    assert(isoRegexp.test(isoDate));
    const date = new Date(isoDate);
    let endDate = date;
    switch (isoDate.length) {
      case 4: //year only
        endDate = new Date(`${isoDate}-12-31T23:59:59Z`);
        break;
      case 7: {
        const [year, month] = isoDate.split("-");
        // day 0 is the last day of the previous month
        // month beeing 0-indexed using the 1-indexed one from ISO date jump to next month
        // 12 month is well understood as first month of next year
        endDate = new Date(`${year}-${month}-${new Date(+year, +month, 0).getDate()}T23:59:59Z`);
        break;
      }
      case 10: {
        endDate = new Date(`${isoDate}T23:59:59Z`);
        break;
      }
    }
    const [start, end] = sortBy([date, endDate]);
    return { start: neo4j.types.Date.fromStandardDate(start), end: neo4j.types.Date.fromStandardDate(end) };
  } catch (e) {
    throw new Error(`Invalid Iso date format ${isoDate}`);
  }
};
