import { Container } from "typescript-ioc";

import { ElasticSearch } from "./service/technical/elasticsearch";
import { UserService } from "./service/business/user";
import { initExpress } from "./express";

// Starting express
initExpress();

if (process.env.NODE_END !== "production") {
  // Init ES indices (if needed)
  const elastic = Container.get(ElasticSearch);
  elastic.createIndices(false);
}

// Init user
const user = Container.get(UserService);
user.init();
