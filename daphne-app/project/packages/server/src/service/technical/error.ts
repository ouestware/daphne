import { GraphQLError } from "graphql";

export class BusinessError extends GraphQLError {
  constructor(message?: string) {
    super(message || "", { extensions: { code: "BUSINESS_ERROR" } });
  }
}

export class TechnicalError extends GraphQLError {
  constructor(message?: string) {
    super(message || "", { extensions: { code: "INTERNAL_SERVER_ERROR" } });
  }
}

export class NotFoundError extends GraphQLError {
  constructor(message?: string) {
    super(message || "", { extensions: { code: "NOT_FOUND" } });
  }
}

export class ForbiddenError extends GraphQLError {
  constructor(message?: string) {
    super(message || "", { extensions: { code: "FORBIDDEN" } });
  }
}

export class NotImplementedError extends GraphQLError {
  constructor(message?: string) {
    super(message || "", { extensions: { code: "NOT_IMPLEMENTED" } });
  }
}

export class UnauthorizedError extends GraphQLError {
  constructor(message?: string) {
    super(message || "", { extensions: { code: "UNAUTHORIZED" } });
  }
}

export function computeErrorMessage(error?: unknown): string {
  if (!error) return "";
  let message = `${error}`;
  if (error instanceof Error && error.message) {
    message = error.message;
  }
  return message;
}

export const Errors = {
  technical: (error?: unknown) => new TechnicalError(computeErrorMessage(error)),
  business: (error?: unknown) => new BusinessError(computeErrorMessage(error)),
  notFound: (error?: unknown) => new NotFoundError(computeErrorMessage(error)),
  forbidden: (error?: unknown) => new ForbiddenError(computeErrorMessage(error)),
  unauthorized: (error?: unknown) => new UnauthorizedError(computeErrorMessage(error)),
  notImplemented: (error?: unknown) => new NotImplementedError(computeErrorMessage(error)),
};

/**
 * Function that handle error in typescript for catch clauses.
 * If error is already a custom one, we just throw it, otherwise
 * it is casted to a technical one.
 */
export function handleError(e: unknown): Error {
  if (
    e instanceof TechnicalError ||
    e instanceof BusinessError ||
    e instanceof NotFoundError ||
    e instanceof ForbiddenError ||
    e instanceof NotImplementedError
  ) {
    return e;
  }
  return Errors.technical(e);
}
