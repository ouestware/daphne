import { Singleton } from "typescript-ioc";
import * as fs from "fs";
import * as path from "path";
import * as os from "os";
import { finished } from "stream";
import { promisify } from "util";
import { FileUpload } from "graphql-upload-minimal";

import { getLogger, Logger } from "./logger";

const finishedAsync = promisify(finished);

@Singleton
export class FileSystem {
  /**
   * Logger of the service
   */
  private log: Logger = getLogger("FileSystem");

  /**
   * Default constructor
   */
  constructor() {
    this.log.debug("Creating FileSystem service");
  }

  /**
   * Save a file upload into the specified path.
   *
   * @param file The file upload to save
   * @param prefix The path prefix where to store the file
   * @returns the absolute path of the image (relative to the / on the FS)
   */
  async saveTemp(file: FileUpload, prefix: string): Promise<string> {
    const stream = file.createReadStream();
    // Create the directory if needed
    const dirPath = path.join(os.tmpdir(), prefix);
    await fs.promises.mkdir(dirPath, { recursive: true });

    // Write the file
    const filePath = path.join(dirPath, file.filename);
    const out = fs.createWriteStream(filePath);
    stream.pipe(out);
    await finishedAsync(out);

    // return the absolute path
    return filePath;
  }

  /**
   * Remove a deprecated file upload from its url.
   *
   * @param filePath The file path
   */
  async remove(filePath: string): Promise<void> {
    if (fs.existsSync(filePath)) await fs.promises.rm(filePath);
  }
}
