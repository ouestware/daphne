import { Inject, Singleton } from "typescript-ioc";
import { Client, estypes } from "@elastic/elasticsearch";
import { isNumber, head } from "lodash";

import { SearchAggregationInput, SearchSortInput, SearchFilterInput } from "../../../graphql/generated/types";
import { Aggregation } from "../../../types";
import { config } from "../../../config";
import { getLogger, Logger } from "../logger";
import { Neo4j } from "../neo4j";
import { Errors } from "../error";
import { indices, IndexConfig } from "./indices";

/**
 * ElasticSearch service
 */
@Singleton
export class ElasticSearch {
  /**
   * logger
   */
  private log: Logger = getLogger("ElasticSearch");

  /**
   * Neo4j service
   */
  private neo4j: Neo4j;

  /**
   * Nodejs es client
   */
  client: Client;

  /**
   * Default constructor
   */
  constructor(@Inject neo4j: Neo4j) {
    this.neo4j = neo4j;
    this.client = new Client(config.elastic);
    this.log.info("Creating Elastic service");
  }

  /**
   * Create indices if they not exist.
   */
  async createIndices(recreate = false): Promise<boolean> {
    let hasCreation = false;

    await Promise.all(
      indices.map(async (index: IndexConfig) => {
        // Check if the index already exists
        const result = await this.client.indices.exists({
          index: index.name,
        });

        // Delete the indices if needed
        if (result && recreate) {
          this.log.debug(`Delete index ${index.name}`);
          await this.client.indices.delete({ index: index.name });
        }

        if ((result && recreate) || !result) {
          hasCreation = true;
          // Creating the index
          this.log.debug(`Creating index ${index.name}`);
          await this.client.indices.create({
            index: index.name,
            body: index.config,
          });
        }
      }),
    );

    return hasCreation;
  }

  /**
   * Import data from neo4j.
   */
  async indexData(): Promise<number> {
    const result = await this.neo4j.getFirstResultQuery<number>(
      "CALL elasticsearch.indexAll({ batchSize:5000, async:false }) YIELD numberOfIndexedDocument RETURN numberOfIndexedDocument AS result",
      {},
    );
    this.log.info(`${result} document have been indexed`);
    return result || 0;
  }

  search(
    index: string,
    query: string,
    skip: number,
    limit: number,
    aggregations: Array<SearchAggregationInput>,
    filters: Array<SearchFilterInput>,
    sort?: SearchSortInput,
  ): Promise<{
    total: number;
    results: { id: string; index: string }[];
    aggregations: Array<Aggregation>;
  }> {
    const body: estypes.SearchRequest = {
      index,
      from: skip,
      size: limit,
      track_total_hits: true,
      query: {
        bool: {
          must: [
            {
              simple_query_string: {
                default_operator: "AND",
                fields: ["_search"],
                query: `${query}*`,
              },
            },
            ...(filters || []).map((filter) => {
              switch (filter.type) {
                case "range":
                  if (filter.values.length !== 2)
                    throw Errors.business(
                      `Range query for ${filter.field} requires 2 values : reading ${JSON.stringify(filter.values)}`,
                    );
                  return {
                    range: {
                      [filter.field]: {
                        ...(filter.values[0] !== "" ? { gte: filter.values[0] } : {}),
                        ...(filter.values[1] !== "" ? { lte: filter.values[1] } : {}),
                      },
                    },
                  };
                default:
                  return {
                    terms: {
                      [filter.field]: filter.values,
                    },
                  };
              }
            }),
          ],
        },
      },
    };
    if (aggregations) {
      const aggs: estypes.SearchRequest["aggs"] = {};
      aggregations.forEach((aggregation) => {
        if (aggregation.nested) {
          aggs[aggregation.id] = {
            nested: {
              path: aggregation.nested,
            },
            aggs: {
              field: {
                ...this.buildAggregation(
                  aggregation.type,
                  `${aggregation.nested}.${aggregation.field}`,
                  aggregation.include,
                  aggregation.orderByAlpha,
                ),
              },
              // should be done only for terms ??
              fieldTotal: {
                ...this.buildAggregation("cardinality", `${aggregation.nested}.${aggregation.field}`),
              },
            },
          };
        } else {
          aggs[aggregation.id] = {
            ...this.buildAggregation(
              aggregation.type,
              aggregation.field,
              aggregation.include,
              aggregation.orderByAlpha,
            ),
          };
          aggs[`${aggregation.id}Total`] = {
            ...this.buildAggregation("cardinality", aggregation.field),
          };
        }
      });
      body.aggs = aggs;
    }
    if (sort && sort.field !== "_score") {
      body.sort = {
        [sort.field]: sort.order as string,
      } as estypes.Sort;
    }
    console.log(JSON.stringify(body));
    return this.doSearch(body);
  }

  /**
   * Do an ES search query
   */
  private async doSearch(body: estypes.SearchRequest): Promise<{
    total: number;
    results: { id: string; index: string }[];
    aggregations: Array<Aggregation>;
  }> {
    const result = await this.client.search<{ id: string }>(body);
    let total = 0;
    if (result.hits.total) {
      if (isNumber(result.hits.total)) total = result.hits.total;
      else total = result.hits.total?.value;
    }

    const aggregations: Array<Aggregation> = [];

    if (result.aggregations) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const rsAggregation = (result.aggregations || {}) as Record<string, any>;
      Object.keys(rsAggregation).forEach((name) => {
        const aggregation = rsAggregation[name];

        // in case of agg on root field
        if (aggregation.buckets) {
          aggregations.push({
            name,
            total: rsAggregation[`${name}Total`] ? rsAggregation[`${name}Total`].value : undefined,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            values: aggregation.buckets.map((b: any) => ({ key: b.key, count: b.doc_count })),
          });
        } else {
          // in case of agg on nested field
          const subName = head(
            Object.keys(aggregation).filter((k) => !["doc_count", "value"].includes(k) && !k.endsWith("Total")),
          );
          const subAgg = subName ? aggregation[subName] : undefined;
          if (subAgg) {
            aggregations.push({
              name: `${name}`,
              total: aggregation[`${subName}Total`] ? aggregation[`${subName}Total`].value : undefined,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              values: subAgg.buckets.map((b: any) => ({ key: b.key, count: b.doc_count })),
            });
          }
        }
      });
    }

    return {
      total,
      aggregations,
      results: result.hits.hits
        .filter((hit) => hit._source?.id)
        .map((hit) => ({ id: hit._source?.id || "", index: hit._index })),
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private buildAggregation(type: string, field: string, include?: string, orderByAlpha?: boolean): any {
    const computedInclude = include
      ? `.*${Array.from(include)
          .map((char) => `[${char.toLowerCase()}${char.toUpperCase()}]`)
          .join("")}.*`
      : undefined;
    switch (type) {
      case "date_histogram_per_year":
        return {
          date_histogram: {
            field,
            include: computedInclude,
            fixed_interval: "365d",
          },
        };
      case "cardinality":
        return {
          cardinality: {
            field,
            include: computedInclude,
          },
        };
      default:
        return {
          terms: {
            field,
            include: computedInclude,
            size: 5,
            order: orderByAlpha ? { ["_key"]: "asc" } : { ["_count"]: "desc" },
          },
        };
    }
  }
}
