const settings = {
  analysis: {
    filter: {
      filter_stop: {
        type: "stop",
      },
      custom_word_delimiter: {
        type: "word_delimiter",
        split_on_numerics: false,
      },
    },
    analyzer: {
      IndexAnalyzer: {
        filter: ["lowercase", "asciifolding", "custom_word_delimiter", "filter_stop"],
        type: "custom",
        tokenizer: "whitespace",
      },
      SearchAnalyzer: {
        filter: ["lowercase", "asciifolding", "custom_word_delimiter", "filter_stop"],
        type: "custom",
        tokenizer: "whitespace",
      },
    },
  },
};

const commonMapping = {
  id: {
    type: "keyword",
    copy_to: ["_search"],
  },
  description: {
    type: "text",
    store: false,
    analyzer: "IndexAnalyzer",
    search_analyzer: "SearchAnalyzer",
    copy_to: ["_search"],
    fields: {
      raw: {
        type: "keyword",
        ignore_above: 256,
      },
    },
  },
  inquiryId: {
    type: "keyword",
    store: false,
  },
  dataSources: {
    type: "nested",
    include_in_parent: true,
    properties: {
      id: { type: "keyword" },
      name: {
        type: "text",
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        copy_to: ["_search"],
        fields: {
          raw: {
            type: "keyword",
            ignore_above: 256,
          },
        },
      },
    },
  },
  _search: {
    type: "text",
    store: false,
    analyzer: "IndexAnalyzer",
    search_analyzer: "SearchAnalyzer",
  },
};

const factoidsIndexConfig = {
  settings,
  mappings: {
    properties: {
      ...commonMapping,
      types: {
        type: "keyword",
        store: false,
        copy_to: ["_search"],
      },
      certainty: {
        type: "float",
      },
      places: {
        type: "nested",
        include_in_parent: true,
        properties: {
          id: { type: "keyword" },
          name: {
            type: "text",
            analyzer: "IndexAnalyzer",
            search_analyzer: "SearchAnalyzer",
            copy_to: ["_search"],
            fields: {
              raw: {
                type: "keyword",
                ignore_above: 256,
              },
            },
          },
        },
      },
      people: {
        type: "nested",
        include_in_parent: true,
        properties: {
          id: { type: "keyword" },
          name: {
            type: "text",
            analyzer: "IndexAnalyzer",
            search_analyzer: "SearchAnalyzer",
            copy_to: ["_search"],
            fields: {
              raw: {
                type: "keyword",
                ignore_above: 256,
              },
            },
          },
        },
      },
      institutions: {
        type: "nested",
        include_in_parent: true,
        properties: {
          id: { type: "keyword" },
          name: {
            type: "text",
            analyzer: "IndexAnalyzer",
            search_analyzer: "SearchAnalyzer",
            copy_to: ["_search"],
            fields: {
              raw: {
                type: "keyword",
                ignore_above: 256,
              },
            },
          },
        },
      },
      dates: {
        type: "date",
      },
    },
  },
};

const peopleIndexConfig = {
  settings,
  mappings: {
    properties: {
      ...commonMapping,
      name: {
        type: "text",
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        copy_to: ["_search"],
        fields: {
          raw: {
            type: "keyword",
            ignore_above: 256,
          },
        },
      },
      otherNames: {
        type: "keyword",
        store: false,
        copy_to: ["_search"],
      },
      roles: {
        type: "keyword",
        store: false,
        copy_to: ["_search"],
      },
    },
  },
};

const placesIndexConfig = {
  settings,
  mappings: {
    properties: {
      ...commonMapping,
      name: {
        type: "text",
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        copy_to: ["_search"],
        fields: {
          raw: {
            type: "keyword",
            ignore_above: 256,
          },
        },
      },
      alternativeNames: {
        type: "text",
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        copy_to: ["_search"],
        fields: {
          raw: {
            type: "keyword",
            ignore_above: 256,
          },
        },
      },
    },
  },
};

const rolesIndexConfig = {
  settings,
  mappings: {
    properties: {
      ...commonMapping,
      name: {
        type: "text",
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        copy_to: ["_search"],
        fields: {
          raw: {
            type: "keyword",
            ignore_above: 256,
          },
        },
      },
    },
  },
};

export type IndexConfig = { name: string; config: Record<string, unknown> };
export const indices: Array<IndexConfig> = [
  { name: "factoids", config: factoidsIndexConfig },
  { name: "people", config: peopleIndexConfig },
  { name: "places", config: placesIndexConfig },
  { name: "roles", config: rolesIndexConfig },
  { name: "factoid_types", config: rolesIndexConfig },
];
