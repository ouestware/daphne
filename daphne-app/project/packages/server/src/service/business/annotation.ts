import { Singleton, Inject } from "typescript-ioc";
import { v4 as uuid } from "uuid";
import { Transaction } from "neo4j-driver";

import { Annotation, Report } from "../../graphql/generated/types";
import { ImportLine, ImportNodeRef } from "../../types";
import { getEmptyReport, mergeReports } from "../../utils/report";
import { Neo4j } from "../technical/neo4j";
import { getLogger, Logger } from "../technical/logger";
import { handleError, Errors } from "../technical/error";
import { InquiryService } from "./inquiry";
import { ImportService } from "./import";

/**
 * Service that handles inquiries.
 */
@Singleton
export class AnnotationService {
  /**
   * Logger
   */
  private log: Logger = getLogger("Annotation");

  /**
   * Neo4j service
   */
  private neo4j: Neo4j;

  /**
   * Inquiry service
   */
  private inquiry: InquiryService;

  /**
   * Import service
   */
  private importer: ImportService;

  /**
   * Default constructor
   */
  constructor(@Inject neo4j: Neo4j, @Inject inquiry: InquiryService, @Inject importer: ImportService) {
    this.neo4j = neo4j;
    this.inquiry = inquiry;
    this.importer = importer;
    this.log.info("Creating Inquiry service");
  }

  /**
   * Create and apply the annotation in the database.
   *
   * @param inquiryId The inquiry on which the annotation applies
   * @param annotation The annotation to save & apply
   */
  async createAnnotation(
    inquiryId: string,
    annotation: Omit<Annotation, "id" | "createdAt" | "updatedAt">,
  ): Promise<void> {
    const session = this.neo4j.getWriteSession();
    const tx = session.beginTransaction();
    try {
      // check that the inquiry exists
      await this.inquiry.checkInquiryExist(inquiryId);

      // save the annotation in the database
      const result = await this.neo4j.getTxFirstResultQuery<Annotation>(
        tx,
        `MATCH (i:Inquiry { id: $inquiryId })
         WITH i
            CREATE (a:Annotation {
              id: $annotation.id,
              author: $annotation.author,
              comment: $annotation.comment,
              patch: $annotation.patch,
              createdAt: datetime(),
              updatedAt: datetime()
            })
            CREATE (i)-[:HAS_ANNOTATION]->(a)
          RETURN {
            id: a.id,
            author: a.author,
            comment: a.comment,
            patch: apoc.convert.fromJsonList(a.patch),
            createdAt: a.createdAt,
            updatedAt: a.updatedAt
          } as result
          ORDER BY a.createdAT ASC
        `,
        {
          inquiryId,
          annotation: {
            ...annotation,
            id: uuid(),
            patch: JSON.stringify(annotation.patch),
            dataSource: {
              name: "Annotations",
            },
          },
        },
      );

      if (!result)
        throw Errors.business(
          "Annotation has not been created. Please check that the node type & ID are correct and exists in the inquiry",
        );

      // Apply the annotation
      await this.applyAnnotationInTx(inquiryId, tx, result);
      await tx.commit();
    } catch (e) {
      await tx.rollback();
      throw handleError(e);
    } finally {
      await session.close();
    }
  }

  /**
   * Apply the given annotation in the database with the provided transaction.
   *
   * @param inquiryId The inquiry on which we want to apply the annotation
   * @param tx The neo4j transaction
   * @param annotation The annotation to apply
   * @returns A report
   */
  async applyAnnotationInTx(inquiryId: string, tx: Transaction, annotation: Annotation): Promise<Report> {
    let indexPatch = 0;
    const report = getEmptyReport();
    for (const line of annotation.patch) {
      // check that the patch is valid
      const preparedLine = ImportService.prepareImportLine(line);
      // apply the annotation
      await this.importer.runImportDataOperation(tx, preparedLine, inquiryId, report, indexPatch);
      // link the annotation
      await this.annotationImpactedLinks(tx, annotation);
      indexPatch++;
    }
    return report;
  }

  /**
   * Get all annotation of the specified inquiry.
   *
   * @param inquiryId The inquiry ID
   */
  async getAnnotations(inquiryId: string): Promise<Array<Annotation>> {
    try {
      await this.inquiry.checkInquiryExist(inquiryId);
      const annotations = await this.neo4j.getResultQuery<Annotation>(
        `
      MATCH (a:Annotation { inquiryId: $inquiryId })
      RETURN {
        id: a.id,
        author: a.author,
        comment: a.comment,
        patch: apoc.convert.fromJsonList(a.patch),
        createdAt: a.createdAt,
        updatedAt: a.updatedAt
      } as result
      ORDER BY a.createdAT ASC
    `,
        { inquiryId },
      );
      return annotations;
    } catch (e) {
      throw handleError(e);
    }
  }

  /**
   * Replay all the annotations of the specified inquiry.
   *
   * @param inquiryId The inquiry ID
   * @returns a report
   */
  async applyAnnotations(inquiryId: string): Promise<Report> {
    const annotations = await this.getAnnotations(inquiryId);
    let report = getEmptyReport();
    const session = this.neo4j.getWriteSession();
    const tx = session.beginTransaction();
    try {
      for (const annotation of annotations) {
        const result = await this.applyAnnotationInTx(inquiryId, tx, annotation);
        report = mergeReports(report, result);
      }
      await tx.commit();
      return report;
    } catch (e) {
      await tx.rollback();
      throw handleError(e);
    } finally {
      await session.close();
    }
  }

  async annotationImpactedLinks(tx: Transaction, annotation: Annotation): Promise<void> {
    const impactedNodes: Array<ImportNodeRef> = (annotation.patch as ImportLine[]).flatMap((line) => {
      if ("source" in line) return [line.source, line.target];
      return [{ id: line.id, type: line.type }];
    });
    for (const nodeRef of impactedNodes) {
      await this.neo4j.getTxFirstResultQuery(
        tx,
        `
        MATCH (a:Annotation { id: $annotation.id })<-[:HAS_ANNOTATION]-(i:Inquiry)
        MATCH (n:\`${nodeRef.type}\` { id: i.id + "-" + $id })
        MERGE (a)-[:IMPACTS]->(n)
        RETURN count(*) as result
      `,
        { annotation, id: nodeRef.id },
      );
    }
  }
}
