import { Singleton, Inject } from "typescript-ioc";

import { getLogger, Logger } from "../technical/logger";
import { Errors } from "../technical/error";
import { Neo4j } from "../technical/neo4j";
import { GraphQlContext } from "../../graphql/index";

/**
 * Service that handles ACL.
 */
@Singleton
export class AclService {
  /**
   * Logger
   */
  private log: Logger = getLogger("ACL");

  /**
   * user service
   */
  private neo4j: Neo4j;

  /**
   * Default constructor
   */
  constructor(@Inject neo4j: Neo4j) {
    this.neo4j = neo4j;
    this.log.info("Creating acl service");
  }

  async checkIsAdmin(context: GraphQlContext): Promise<void> {
    // only admin can create a corpus
    if (!context.auth.jwt) throw Errors.unauthorized();
    if (!context.auth.jwt.isAdmin) throw Errors.forbidden();
  }

  async checkIsInquiryWriter(context: GraphQlContext, inquiryId: string): Promise<void> {
    if (!context.auth.jwt) throw Errors.unauthorized();
    if (!context.auth.jwt.isAdmin) {
      const result = await this.neo4j.getFirstResultQuery(
        `MATCH (u:User { id: $userId})-[:HAS_ACCESS_TO]->(i:Inquiry { id: $inquiryId })
        RETURN true AS result`,
        { userId: context.auth.jwt.id, inquiryId },
      );
      if (!result) throw Errors.forbidden();
    }
  }
}
