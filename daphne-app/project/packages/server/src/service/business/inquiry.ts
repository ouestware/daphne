import { Transaction } from "neo4j-driver";
import { Singleton, Inject } from "typescript-ioc";

import { config } from "../../config";
import { Neo4j } from "../technical/neo4j";
import { getLogger, Logger } from "../technical/logger";
import { Errors, handleError } from "../technical/error";
import { UserService } from "./user";
import { Inquiry } from "../../graphql/generated/types";
import { slugify } from "../../utils/strings";

/**
 * Service that handles inquiries.
 */
@Singleton
export class InquiryService {
  /**
   * Logger
   */
  private log: Logger = getLogger("Inquiry");

  /**
   * Neo4j service
   */
  private neo4j: Neo4j;

  /**
   * User service
   */
  private user: UserService;

  /**
   * Default constructor
   */
  constructor(@Inject neo4j: Neo4j, @Inject user: UserService) {
    this.neo4j = neo4j;
    this.user = user;
    this.log.info("Creating Inquiry service");
  }

  /**
   * Check if inquiry exist
   */
  async checkInquiryExist(id: string): Promise<void> {
    const result = await this.neo4j.getFirstResultQuery<Inquiry>(
      `MATCH (i:Inquiry { id: $id }) RETURN true as result`,
      { id },
    );
    if (!result) throw Errors.notFound(`Inquiry ${id} can't be found`);
  }

  /**
   * find inquery by its id
   */
  async findById(id: string): Promise<Inquiry | null> {
    const result = await this.neo4j.getFirstResultQuery<Inquiry>(
      `MATCH (i:Inquiry { id: $id }) RETURN properties(i) as result`,
      { id },
    );
    return result;
  }

  /**
   * Create an inquiry.
   */
  async create(
    data: Omit<Inquiry, "id" | "dataSources" | "createdAt" | "updatedAt" | "annotations">,
    password: string,
  ): Promise<Inquiry> {
    // we check that if an inquiry (and its related user) already exists in the db
    // if its is the case we add the suffix _X
    let id = slugify(data.name);
    let isFree = (await this.findById(id)) === null && (await this.user.findByUsername(id)) === null;
    let count = 1;
    while (!isFree) {
      id = `${slugify(data.name)}_${count}`;
      isFree = isFree = (await this.findById(id)) === null && (await this.user.findByUsername(id)) === null;
      count++;
    }

    try {
      await this.user.create(id, password);
      const result = await this.neo4j.getFirstResultQuery<Inquiry>(
        `MATCH (u:User {username: $id}) WITH u
         CREATE (i:Inquiry { id: $id, createdAt: datetime(), updatedAt: datetime()})
         CREATE (u)-[:HAS_ACCESS_TO]->(i)
         SET i += $data
         RETURN properties(i) as result`,
        { id, data },
      );
      if (!result) throw Errors.technical("Result of inquiry creation can't be null");
      return result;
    } catch (e) {
      throw handleError(e);
    }
  }

  /**
   * Update an inquiry.
   */
  async update(id: string, data: Partial<Inquiry>): Promise<Inquiry> {
    try {
      const result = await this.neo4j.getFirstResultQuery<Inquiry>(
        `MATCH (i:Inquiry {id: $id})
         SET i += $data
         RETURN properties(i) as result`,
        { id, data },
      );
      if (!result) throw Errors.notFound(`Inquiry ${id} can't be found`);
      return result;
    } catch (e) {
      throw handleError(e);
    }
  }

  /**
   * Delete all data of the inquiry, but keep the inquiry and annotations
   * This operation is done in a single transaction, so if the inquiry is heavy,
   * it can leads to a memory error.
   */
  async flushInTx(id: string, tx: Transaction): Promise<void> {
    await this.neo4j.getTxFirstResultQuery(
      tx,
      `MATCH (i:Inquiry { id: $id })-[:USES_DATA_SOURCE]->(ds:DataSource)<-[:IMPORTED_FROM]-(n)
       DETACH DELETE ds
       DETACH DELETE n
       RETURN count(*) AS result`,
      { id },
    );
  }

  /**
   * Delete all data of the inquiry, but keep the inquiry and its annotations
   * /!\ This function does mutliple transaction.
   */
  async flush(id: string): Promise<void> {
    await this.checkInquiryExist(id);
    try {
      await this.neo4j.getFirstResultQuery(
        `CALL apoc.periodic.commit(
          "MATCH (i:Inquiry { id: $id })-[:USES_DATA_SOURCE]->(ds:DataSource)<-[:IMPORTED_FROM]-(n)
           WITH n LIMIT ${config.import.batchSize}
           DETACH DELETE n
           RETURN count(*) AS result",
           { id: $id }
         ) YIELD batches 
         RETURN batches as result`,
        { id },
      );
      await this.neo4j.getFirstResultQuery(
          `MATCH (i:Inquiry { id: $id })-[:USES_DATA_SOURCE]->(ds:DataSource)
           DETACH DELETE ds
           RETURN count(*) as result`,
        { id },
      );
    } catch (e) {
      throw handleError(e);
    }
  }

  /**
   * Delete an inquiry and all its relative data.
   */
  async delete(id: string): Promise<boolean> {
    await this.checkInquiryExist(id);

    const session = this.neo4j.getWriteSession();
    const tx = session.beginTransaction();
    try {
      this.flushInTx(id, tx);
      const result = await this.neo4j.getTxFirstResultQuery<Inquiry>(
        tx,
        `MATCH (i:Inquiry {id: $id}) DETACH DELETE i RETURN true AS result`,
        { id },
      );
      if (!result) throw Errors.notFound(`Inquiry ${id} can't be found`);
      await tx.commit();
      return true;
    } catch (e) {
      await tx.rollback();
      throw handleError(e);
    } finally {
      await session.close();
    }
  }
}
