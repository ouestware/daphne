import {
  clone,
  every,
  isArray,
  isBoolean,
  isDate,
  isNil,
  isNull,
  isNumber,
  isObject,
  isString,
  omit,
  toPairs,
} from "lodash";
import { parse } from "ndjson";
import { Transaction } from "neo4j-driver";
import { createReadStream } from "node:fs";
import { Inject, Singleton } from "typescript-ioc";

import { config } from "../../config";
import { Report } from "../../graphql/generated/types";
import { ImportLine, importOperations, schema } from "../../types";
import { transformIsoDate } from "../../utils/dates";
import { getEmptyReport } from "../../utils/report";
import { InquiryService } from "../business/inquiry";
import { Logger, getLogger } from "../technical/logger";
import { Neo4j } from "../technical/neo4j";

/**
 * Service that handles data imports.
 */
@Singleton
export class ImportService {
  /**
   * Logger
   */
  private log: Logger = getLogger("import");

  /**
   * user service
   */
  private inquiry: InquiryService;

  /**
   * Neo4j service
   */
  private neo4j: Neo4j;

  /**
   * Default constructor
   */
  constructor(@Inject inquiry: InquiryService, @Inject neo4j: Neo4j) {
    this.inquiry = inquiry;
    this.neo4j = neo4j;
    this.log.info("Creating import service");
  }

  /**
   * Import data in the Inquiry as a single reversible transaction
   * See this for async in data event : https://stackoverflow.com/questions/50307139/how-to-asynchronously-createreadstream-in-node-js-with-async-await
   */
  async import(id: string, filePath: string, flush = false): Promise<Report> {
    await this.inquiry.checkInquiryExist(id);

    const report = getEmptyReport();

    const session = this.neo4j.getWriteSession();
    const tx = session.beginTransaction();

    if (flush) {
      this.log.debug("Flushing data...");
      await this.inquiry.flushInTx(id, tx);
      this.log.debug("Flush done");
    }

    return new Promise((resolve) => {
      let index = 0;
      const stream = createReadStream(filePath, {
        flags: "r",
        encoding: "utf8",
        highWaterMark: 1,
      });
      const parsedStream = stream.pipe(parse());
      parsedStream
        .on("data", async (obj: unknown) => {
          index++;
          this.log.debug("Stream data", index);
          if (!tx.isOpen()) {
            this.log.debug(`Transaction is closed`);
            parsedStream.emit("error", new Error("Transaction has been closed"));
          } else {
            try {
              parsedStream.pause();
              const data = ImportService.prepareImportLine(obj);
              if (data) {
                await this.runImportDataOperation(tx, data as ImportLine, id, report, index);
              }
            } catch (e) {
              report.errors.push(`Error at ${index}: ${e instanceof Error ? e.message : JSON.stringify(e)}`);
            } finally {
              parsedStream.resume();
            }
            if (report.errors.length === config.import.maxError) {
              this.log.debug(`reached ${config.import.maxError} errors, aborting...`);
              parsedStream.emit("error", new Error(`Reached ${config.import.maxError} errors, aborting...`));
            }
          }
        })
        .on("error", async (e) => {
          this.log.error(`Stream error : ${e.message}`);
          report.success = false;
          report.errors.push(e.message);
          if (tx.isOpen()) {
            await tx.rollback();
            await tx.close();
          }
          this.log.error(`Import error`, report);
          resolve(report);
        })
        // like a finally
        .on("end", async () => {
          this.log.debug("Closing stream");
          if (report.errors.length === 0) {
            report.success = true;
            await tx.commit();
            this.log.info(`Import success`, report);
          } else {
            // rollback
            this.log.debug("rollback");
            await tx.rollback();
            this.log.error(`Import error`, report);
          }
          this.log.debug("closing session");
          await tx.close();

          // return the report
          resolve(report);
        });
    });
  }

  /**
   * Import data in the Inquiry as multiple transactions, and parallelizing operations.
   * See this for async in data event : https://stackoverflow.com/questions/50307139/how-to-asynchronously-createreadstream-in-node-js-with-async-await
   * The `batchSize` attribute specifies the number of maximum parallel queries this function can initiate in Neo4j.
   */
  async batchImport(
    id: string,
    filePath: string,
    flush = false,
    batchSize = config.import.batchSize,
    skip = 0,
  ): Promise<Report> {
    const report: Report = {
      success: false,
      errors: [],
      warnings: [],
    };

    // check Inquiry existence
    await this.inquiry.checkInquiryExist(id);

    // flush inquiry if needed
    if (flush) this.inquiry.flush(id);

    const session = this.neo4j.getWriteSession();
    return new Promise((resolve) => {
      let parsed = 0;
      let indexed = 0;
      let buffer: unknown[] = [];

      const stream = createReadStream(filePath, {
        flags: "r",
        encoding: "utf8",
        highWaterMark: batchSize * 2,
      });
      const parsedStream = stream.pipe(parse());

      const runOperation = async (tx: Transaction, obj: unknown) => {
        try {
          indexed++;
          if (!(indexed % 10000)) this.log.info(`Indexed ${indexed} rows`);

          const data = ImportService.prepareImportLine(obj);
          if (data) {
            await this.runImportDataOperation(tx, data as ImportLine, id, report, indexed);
          }
        } catch (e) {
          report.errors.push(`Error at ${indexed}: ${e instanceof Error ? e.message : JSON.stringify(e)}`);
        }

        if (report.errors.length === config.import.maxError) {
          this.log.debug(`reached ${config.import.maxError} errors, aborting...`);
          parsedStream.emit("error", new Error(`Reached ${config.import.maxError} errors, aborting...`));
        }
      };

      const runOperations = async (tx: Transaction, operations: unknown[]) => {
        return Promise.all(operations.map((op) => runOperation(tx, op)));
        // for (const op of operations) {
        //   await runOperation(tx, op);
        // }
      };

      let streamIndex = 0;
      parsedStream
        .on("data", async (obj: unknown) => {
          streamIndex += 1;
          if (!(streamIndex % 1000)) this.log.debug("Stream data", streamIndex);

          if (skip && parsed++ < skip) return;

          buffer.push(obj);
          if (buffer.length >= batchSize) {
            parsedStream.pause();

            const tx = session.beginTransaction();
            await runOperations(tx, buffer);
            if (!tx.isOpen()) {
              parsedStream.emit("error", new Error(`Transaction has been closed.`));
            } else {
              await tx.commit();
              buffer = [];
              parsedStream.resume();
            }
          }
        })
        .on("error", async (e) => {
          this.log.error(`Stream error : ${e.message}`);
          report.success = false;
          report.errors.push(e.message);

          this.log.error("Import error", report);
          resolve(report);
        })
        // like a finally
        .on("end", async () => {
          this.log.debug("Closing stream");
          if (report.errors.length === 0) {
            report.success = true;
            this.log.info("Import success", report);
          } else {
            this.log.error("Import error", report);
            // rollback
            this.log.debug("rollback");
          }
          this.log.debug("closing session");

          resolve(report);
        });
    });
  }

  /***
   * Check property value scalar type
   */
  public static isPropertyValueValid(value: unknown): boolean {
    if (isArray(value)) {
      if (value.length === 0) return true;
      else {
        const getValueType = (value: unknown): string =>
          (isNull(value) && "null") ||
          (isBoolean(value) && "boolean") ||
          (isNumber(value) && "number") ||
          (isDate(value) && "date") ||
          (isString(value) && "string") ||
          "invalid";
        // first element type
        const firstValueType = getValueType(value[0]);
        // check all element types are valid and the same as the first one
        return firstValueType !== "invalid" && every(value.slice(1), (v) => getValueType(v) === firstValueType);
      }
    } else return isNull(value) || isBoolean(value) || isNumber(value) || isDate(value) || isString(value);
  }

  /**
   * Check if the object receive by the json stream is a valid and prepare special variables like dates for Neo4j.
   * if obj is valid, it is casted as a ImportLine, otherwise the function returns null and also fill the report accordly.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static prepareImportLine(obj: any): ImportLine {
    // Must have the common prop `op`
    if (isNil(obj["op"])) {
      throw new Error(`JSON ${JSON.stringify(obj)} has no 'op' property`);
    }

    // Operation must be part of the allowed import operations
    if (!importOperations.includes(obj["op"])) {
      throw new Error(`Operation ${obj["op"]} is not valid`);
    }

    // Checking if data source is present and valid
    if (!isNil(obj["dataSource"])) {
      const dataSource = obj["dataSource"];
      if (!isObject(dataSource)) throw new Error(`Properties ${obj["dataSource"]} must be a JSON object`);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      if (isNil((dataSource as any).name)) throw new Error(`Properties name is mandatory in data source`);
    }

    // Checking if properties fields are valid
    if (!isNil(obj["properties"])) {
      const properties = obj["properties"];
      if (!isObject(properties)) throw new Error(`Properties ${obj["properties"]} must be a JSON object`);

      const propsError = toPairs(properties)
        .map((pair) => (!this.isPropertyValueValid(pair[1]) ? pair[0] : null))
        .filter((e) => e !== null);
      if (propsError.length > 0)
        throw new Error(`Properties ${propsError.join(", ")} must be of type null | boolean | number | string | date`);
    }

    const operation = obj.op;
    switch (operation) {
      case "add_node":
      case "merge_node":
      case "replace_node":
      case "remove_node":
        if (isNil(obj["id"])) {
          throw new Error(`Operation ${operation} has missing property 'id'`);
        }
        if (isNil(obj["type"])) {
          throw new Error(`Operation ${operation} has missing property 'type'`);
        }
        if (!Object.keys(schema.nodes).includes(`${obj["type"]}`)) {
          throw new Error(`Operation ${operation} has an unknown 'type'`);
        }
        if (operation !== "remove_node" && isNil(obj["properties"])) {
          throw new Error(`Operation ${operation} has missing property 'properties'`);
        }

        // On creation/replace we MUST provide mandatory fields
        if (
          (operation === "add_node" || operation === "replace_node") &&
          (
            !(schema.nodes[obj["type"]] as Array<string>).every((field) => !isNil(obj["properties"][field])) 
            ||
            //sepcial case for DateValue
            (obj["type"] === "DateValue" && !obj["properties"]["startIso"] && !obj["properties"]["endIso"]))
        ) {
          throw new Error(`Operation ${operation} has missing required properties in 'properties'`);
        }
        break;
      case "add_rel":
      case "merge_rel":
      case "replace_rel":
      case "remove_rel":
        if (isNil(obj["type"])) {
          throw new Error(`Operation ${operation} has missing property 'type'`);
        }
        if (isNil(obj["source"]["type"])) {
          throw new Error(`Operation ${operation} has missing property 'type' in 'source'`);
        }
        if (!Object.keys(schema.nodes).includes(`${obj["source"]["type"]}`)) {
          throw new Error(`Operation ${operation} has an unknown 'type' in 'source'`);
        }
        if (isNil(obj["source"])) {
          throw new Error(`Operation ${operation} has missing property 'source'`);
        }
        if (isNil(obj["target"])) {
          throw new Error(`Operation ${operation} has missing property 'target'`);
        }
        if (isNil(obj["target"]["type"])) {
          throw new Error(`Operation ${operation} has missing property 'type' in 'target'`);
        }
        if (!Object.keys(schema.nodes).includes(`${obj["target"]["type"]}`)) {
          throw new Error(`Operation ${operation} has an unknown 'type' in 'target'`);
        }
        if (operation !== "remove_rel" && isNil(obj["properties"])) {
          throw new Error(`Operation ${operation} has missing property 'properties'`);
        }

        // check schema
        if (!Object.keys(schema.edges).includes(`${obj["type"]}`)) {
          throw new Error(`Operation ${operation} has an unknown 'type'`);
        }
        // On creation/replace we MUST provide mandatory fields
        if (
          (operation === "add_rel" || operation === "replace_rel") &&
          !schema.edges[obj["type"]].every((field) => isNil(obj["properties"][field]))
        ) {
          throw new Error(`Operation ${operation} has missing required properties in 'properties'`);
        }
        break;
    }

    // Format date
    if (obj["type"] === "DateValue") {
      if (!isNil(obj["properties"].startIso)) {
        const start = transformIsoDate(obj["properties"].startIso);
        obj["properties"] = {
          ...obj["properties"],
          start: start ? start.start : undefined,
        };
      }
      if (!isNil(obj["properties"].endIso)) {
        const end = transformIsoDate(obj["properties"].endIso);
        obj["properties"] = {
          ...obj["properties"],
          end: end ? end.end : undefined,
        };
      }
    }
    return obj as ImportLine;
  }

  private async checkNodeExistance(
    tx: Transaction,
    inquiryId: string,
    nodeType: string,
    nodeId: string,
  ): Promise<boolean> {
    const checkExistance = await this.neo4j.getTxFirstResultQuery<number>(
      tx,
      `MATCH (n:\`${nodeType}\` { id: $inquiryId + "-" + $nodeId }) RETURN count(*) AS result`,
      { nodeType, nodeId, inquiryId },
    );
    return checkExistance !== null && checkExistance > 0 ? true : false;
  }

  /**
   * Import the importLine in the database.
   * /!\ Parameter report mutates
   *
   * NOTE: Object ids are composed like that "$inquiryId-$objectId
   * This allows to import the same file into an other inquiry.
   * More over it helps us to know if an object is part of an inquiry or not
   */
  public async runImportDataOperation(
    tx: Transaction,
    data: ImportLine,
    inquiryId: string,
    report: Report,
    index: number,
  ): Promise<void> {
    let query: string | null = null;
    let dataParam = clone(data);
    try {
      switch (data.op) {
        case "add_node": {
          const nodeCreationExist = await this.checkNodeExistance(tx, inquiryId, data.type, data.id);
          if (nodeCreationExist) throw new Error(`Can't add node ${data.type} with id ${data.id}, node already exists`);
          query = `
            MATCH (i:Inquiry { id: $inquiryId })
            MERGE (i)-[:USES_DATA_SOURCE]->(ds:DataSource { id: $inquiryId + "-" + coalesce( $data.dataSource.name, "unknown") })
            SET ds.name = coalesce( $data.dataSource.name, "unknown")
            WITH ds
              CREATE (n:\`${data.type}\` { id: $inquiryId + "-" + $data.id })
              SET n += $data.properties

              CREATE (n)-[:IMPORTED_FROM { originalId: $data.dataSource.originalId, permalink: $data.dataSource.permalink }]->(ds)

              RETURN 1 AS result`;
          dataParam = { ...data, properties: omit(data.properties, ["id"]) };
          break;
        }
        case "merge_node": {
          query = `
            MATCH (i:Inquiry { id: $inquiryId })
            MERGE (i)-[:USES_DATA_SOURCE]->(ds:DataSource { id: $inquiryId + "-" + coalesce( $data.dataSource.name, "unknown") })
            SET ds.name = coalesce( $data.dataSource.name, "unknown")
            WITH ds
              MERGE (n:\`${data.type}\` { id: $inquiryId + "-" + $data.id })
              SET n += $data.properties
              MERGE (n)-[r:IMPORTED_FROM]->(ds)
              SET r.originalId = coalesce($data.dataSource.originalId,  r.originalId),
                  r.permalink = coalesce($data.dataSource.permalink, r.permalink)
              RETURN 1 AS result`;
          dataParam = { ...data, properties: omit(data.properties, ["id"]) };
          break;
        }
        case "replace_node": {
          const replaceNodeExist = await this.checkNodeExistance(tx, inquiryId, data.type, data.id);
          if (!replaceNodeExist)
            throw new Error(`Can't replace node ${data.type} with id ${data.id}, node doesn't exist`);
          query = `
            MATCH (n:\`${data.type}\` { id: $inquiryId + "-" + $data.id })
            SET n += $data.properties
            RETURN 1 AS result`;
          dataParam = { ...data, properties: omit(data.properties, ["id"]) };
          break;
        }
        case "remove_node": {
          query = `
            MATCH (n:\`${data.type}\` {id: $inquiryId + "-" + $data.id })
            DETACH DELETE n
            RETURN 1 AS result`;
          break;
        }
        case "merge_rel":
        case "add_rel": {
          const addRelStartExist = await this.checkNodeExistance(tx, inquiryId, data.source.type, data.source.id);
          const addRelEndExist = await this.checkNodeExistance(tx, inquiryId, data.target.type, data.target.id);
          if (!addRelStartExist || !addRelEndExist)
            throw new Error(
              `Can't add relationship ${data.type}, ${!addRelStartExist ? "start" : "end"} node is missing`,
            );
          query = `
            MATCH (s:\`${data.source.type}\` {id: $inquiryId + "-" + $data.source.id})
            MATCH (t:\`${data.target.type}\` {id: $inquiryId + "-" + $data.target.id})
            MERGE (s)-[r:\`${data.type}\`]->(t)
              ON CREATE SET r += $data.properties
            RETURN 1 AS result`;
          break;
        }
        case "replace_rel": {
          const replaceRelStartExist = await this.checkNodeExistance(tx, inquiryId, data.source.type, data.source.id);
          const replaceRelEndExist = await this.checkNodeExistance(tx, inquiryId, data.target.type, data.target.id);
          if (!replaceRelStartExist || !replaceRelEndExist)
            throw new Error(
              `Can't add relationship ${data.type}, ${!replaceRelStartExist ? "start" : "end"} node is missing`,
            );
          query = `
            MATCH (s:\`${data.source.type}\` {id: $inquiryId + "-" + $data.source.id})
            MATCH (t:\`${data.target.type}\` {id: $inquiryId + "-" + $data.target.id})
            MATCH (s)-[r:\`${data.type}\`]->(t)
            SET r += $data.properties
            RETURN 1 AS result`;
          break;
        }
        case "remove_rel":
          query = `
            MATCH (s:\`${data.source.type}\` {id: $inquiryId + "-" + $data.source.id})
            MATCH (t:\`${data.target.type}\` {id: $inquiryId + "-" + $data.target.id})
            MATCH (s)-[r:\`${data.type}\`]->(t)
            DELETE r
            RETURN 1 AS result`;
          break;
      }
      if (!query)
        report.errors.push(`Operation ${data.op} at position ${index} doesn't exists. Line has been skipped.`);

      const result = await this.neo4j.getTxFirstResultQuery<number>(tx, query, {
        data: dataParam,
        inquiryId,
      });
      if (!result)
        report.warnings.push(
          `Line ${JSON.stringify(
            data,
          )} at position ${index} did no modification in database. You should check that nodes/edges exists.`,
        );
    } catch (e) {
      this.log.error(`Line ${JSON.stringify(data)} at position ${index} failed`, e);
      report.errors.push(
        `Line ${JSON.stringify(data)} at position ${index} failed due to error: ${(e as Error).message}`,
      );
    }
  }
}
