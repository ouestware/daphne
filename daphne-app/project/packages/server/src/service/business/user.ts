import { v4 as uuid } from "uuid";
import { Singleton, Inject } from "typescript-ioc";
import bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import { passwordStrength, defaultOptions, DiversityType } from "check-password-strength";

import { config } from "../../config";
import { Neo4j } from "../technical/neo4j";
import { getLogger, Logger } from "../technical/logger";
import { Errors } from "../technical/error";
import { User, CheckPasswordResult, PasswordRule } from "../../types/user";
import { keys } from "lodash";

const CYPHER_USER_RETURN = `
{
  id: n.id,
  username: n.username,
  isAdmin: coalesce(n.isAdmin, false)
}`;

export const PASSWORD_RULES: Array<PasswordRule> = [
  { name: "length", description: "Must contain at least 8 characters" },
  { name: "lowercase", description: "Must contain at least 1 lowercase character" },
  { name: "uppercase", description: "Must contain at least 1 uppercase character" },
  { name: "number", description: "Must contain at least 1 numeric character" },
  { name: "symbol", description: "Must contain at least 1 special character (!@#$%^&*)" },
];

/**
 * Service that handles users.
 */
@Singleton
export class UserService {
  /**
   * Logger
   */
  private log: Logger = getLogger("User");

  /**
   * Neo4j service
   */
  private neo4j: Neo4j;

  /**
   * Default constructor
   */
  constructor(@Inject neo4j: Neo4j) {
    this.neo4j = neo4j;
    this.log.info("Creating user service");
  }

  async init(): Promise<void> {
    const user = await this.findByUsername("admin");
    if (!user) {
      this.log.info("Creating admin user with default password. Please change it");
      await this.create("admin", config.defaultAdminPassword, true);
    }
  }

  /**
   * Log a user with its username or email / password
   *
   * @param username The username of the user
   * @param password The password of the user
   * @returns The authenticated user object otherwise an exception
   */
  async login(username: string, password: string): Promise<User> {
    this.log.debug(`Try to log user ${username}`);
    const result = await this.neo4j.getFirstResultQuery<{
      id: string;
      password: string;
    }>(
      `MATCH (n:User) WHERE n.username = $username
       RETURN  {
         id: n.id,
         password: n.password
       } AS result`,
      { username },
    );

    if (result != null) {
      if (await this.verifyPassword(password, result.password)) {
        const user = await this.findById(result.id);
        if (user != null) {
          return user;
        }
      }
    }
    throw Errors.business("The login and password you entered does not match, please try again");
  }

  /**
   * Create a new user.
   *
   * @param username login of the user
   * @param password password of the user
   * @param params Additionnals attributs for the user
   * @param isAdmin If true, we create an admin account
   * @returns The created user.
   * @throws An exception if user can't be created (or for a technical issue)
   */
  async create(username: string, password: string, isAdmin = false): Promise<User> {
    // Check password
    const passwordValidity = this.checkPassword(password);
    if (!passwordValidity.isValid)
      throw Errors.business(
        `Password is too weak, missing rules ${passwordValidity.missingRules.map((e) => e.name).join(",")}`,
      );
    const hashedPassword = await bcrypt.hash(password, 10);

    // Check username validity
    if (!this.checkUsername(username))
      throw Errors.business("Username should not contains spaces or special charaters");
    const countUsername = await this.neo4j.getFirstResultQuery<number>(
      `MATCH (u:User) WHERE u.username=$username RETURN count(*) AS result`,
      { username },
    );
    if (countUsername !== 0) throw Errors.business("Username already exists");

    // create the id of the user
    const id = uuid();

    // Finally, create the user
    const user = await this.neo4j.getFirstResultQuery<User>(
      ` CREATE (n:User {
            id: $id,
            username: $username,
            password: $password,
            isAdmin: $isAdmin,
            createdAt: datetime(),
            updatedAt: datetime()
        })
         RETURN  ${CYPHER_USER_RETURN} AS result`,
      { id, username, password: hashedPassword, isAdmin },
    );
    if (user === null) throw Errors.technical("Failed to create user");
    return user;
  }

  /**
   * Change the password of the user.
   *
   * @param userId
   * @param password
   * @param confirmation
   * @throw Exception if password is too weak or if the confirmation doesn't match or if the user is a social user.
   */
  async changePassword(userIdentity: { id?: string } | { username?: string }, password: string): Promise<void> {
    // Check password validity
    const passwordValidity = this.checkPassword(password);
    if (!passwordValidity.isValid)
      throw Errors.business(
        `Password is too weak, missing rules ${passwordValidity.missingRules.map((e) => e.name).join(",")}`,
      );

    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await this.neo4j.getFirstResultQuery<User>(
      `MATCH (n:User { ${keys(userIdentity)[0]}:$${keys(userIdentity)[0]} })
        SET n.password = $password,
            n.updatedAt = datetime()
        RETURN  ${CYPHER_USER_RETURN} AS result`,
      { ...userIdentity, password: hashedPassword },
    );

    if (!user) throw Errors.business("User has not been found.");
  }

  /**
   * Find a user in the database by its id.
   *
   * @param id The id of the user
   * @returns The user object otherwise an exception
   */
  async findById(id: string): Promise<User | null> {
    this.log.debug(`Find user ${id}`);
    const user = await this.neo4j.getFirstResultQuery<User>(
      `MATCH (n:User { id: $id })
       RETURN ${CYPHER_USER_RETURN} AS result`,
      { id },
    );
    if (!user) return null;
    return user;
  }

  /**
   * Find a user in the database by its id.
   *
   * @param id The id of the user
   * @returns The user object otherwise an exception
   */
  async findByUsername(username: string): Promise<User | null> {
    this.log.debug(`Find user ${username}`);
    const user = await this.neo4j.getFirstResultQuery<User>(
      `MATCH (n:User { username: $username })
       RETURN ${CYPHER_USER_RETURN} AS result`,
      { username },
    );
    if (!user) return null;
    return user;
  }

  /**
   * Generate a token (a JWT) for the specified user.
   *
   * @param id The user id
   * @returns A JWT token
   */
  generateToken(id: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.findById(id)
        .then((user) => {
          if (user) {
            jwt.sign(user, config.secret, (err, token) => {
              if (err) reject(Errors.technical("Failed to sign the JWT token"));
              resolve(token as string);
            });
          } else {
            reject(new Error("User not found"));
          }
        })
        .catch((e: Error) => reject(e));
    });
  }

  /**
   * Verify and decode a JWT.
   *
   * @param token The JWT to verify/decode
   * @returns The decoded value of the JWT token that is a user
   */
  verifyToken(token: string): Promise<User> {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) reject(Errors.technical(err));
        resolve(decoded as User);
      });
    });
  }

  /**
   * Compare a clear password with its encrypted version.
   *
   * @param password The clear password to check
   * @param hash The encrypted password
   * @returns <code>true</code> if passwords are the same, <code>false</code> otherwise
   */
  private verifyPassword(password: string, hash: string): Promise<boolean> {
    return new Promise((resolve) => {
      bcrypt.compare(password, hash, (_error, same: boolean) => {
        if (same) return resolve(true);
        return resolve(false);
      });
    });
  }

  private checkUsername(username: string): boolean {
    return !/[ `!@#$%^&*()+=[\]{};':"\\|,<>/?~]/.test(username);
  }

  private checkPassword(password: string): CheckPasswordResult {
    const strength = passwordStrength(password, defaultOptions);
    return {
      isValid: strength.id >= 2,
      strength: strength.value,
      missingRules: PASSWORD_RULES.filter((rule: PasswordRule) => {
        if (rule.name === "length") return strength.length < 8;
        else return !strength.contains.includes(rule.name as DiversityType);
      }),
    };
  }
}
