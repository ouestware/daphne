export type Aggregation = {
  name: string;
  total?: number;
  values: Array<{ key: string; count: number }>;
};
