export type User = {
  id: string;
  username: string;
  isAdmin: boolean;
};

export interface CheckPasswordResult {
  isValid: boolean;
  strength: string; // ie Too weak, Weak, Medium, Strong
  missingRules: Array<PasswordRule>; // lowercase, uppercase, symbol, number, length
}

export interface PasswordRule {
  name: string;
  description: string;
}
