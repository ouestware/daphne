// Used to defined the data source
type ImportSource = { name: string; originalId?: string; permalink?: string };

export type ImportNodeRef = {
  id: string;
  type: string;
};
export type ImportNodeCommon = ImportNodeRef & { dataSource: ImportSource };

type ImportRelRef = {
  type: string;
  source: ImportNodeRef;
  target: ImportNodeRef;
};
type ImportProperties = { [key: string]: unknown };

type ImportAddNode = { op: "add_node"; properties: ImportProperties } & ImportNodeCommon;
type ImportMergeNode = { op: "merge_node"; properties: ImportProperties } & ImportNodeCommon;
type ImportReplaceNode = { op: "replace_node"; properties: ImportProperties } & ImportNodeCommon;
type ImportRemoveNode = { op: "remove_node" } & ImportNodeCommon;
type ImportAddRel = { op: "add_rel"; properties: ImportProperties } & ImportRelRef;
type ImportMergeRel = { op: "merge_rel"; properties: ImportProperties } & ImportRelRef;
type ImportReplaceRel = { op: "replace_rel"; properties: ImportProperties } & ImportRelRef;
type ImportRemoveRel = { op: "remove_rel" } & ImportRelRef;

export type ImportLine =
  | ImportAddNode
  | ImportMergeNode
  | ImportReplaceNode
  | ImportRemoveNode
  | ImportAddRel
  | ImportMergeRel
  | ImportReplaceRel
  | ImportRemoveRel;

export type ImportOperationType = ImportLine["op"];

export const importOperations = [
  "add_node",
  "merge_node",
  "replace_node",
  "remove_node",
  "add_rel",
  "merge_rel",
  "replace_rel",
  "remove_rel",
];

/**
 * List all available node type and list fields that are mandatory
 */
export const schema: { nodes: { [key: string]: Array<string> }; edges: { [key: string]: Array<string> } } = {
  nodes: {
    DataSource: ["name"],
    DateValue: [], //["startIso", "endIso"], we need one of the two so we need a custom check
    Rank: ["name"],
    Role: ["name"],
    FactoidType: ["name"],
    FactoidPersonParticipate: [],
    Factoid: [],
    ObjectType: ["name"],
    Object: ["name"],
    Domain: ["name"],
    MoralEntity: ["name"],
    Group: ["name"],
    PersonName: ["name"],
    PhysicalPerson: ["name"],
    Zone: [],
    Place: ["name"],
    SourceType: ["name"],
    Source: ["name"],
    Edition: ["name"],
    DateIncorrect: [],
    Person: ["name"],
  },
  edges: {
    HAS_SOURCE_TYPE: [],
    OCCURED_AT: [],
    PUBLISHED_AS: [],
    PART_OF: [],
    IS_NAMED: [],
    REFERS_TO: [],
    HAS_FACTOID_TYPE: [],
    PARTICIPATE: [],
    WITH_ROLE: [],
    TOOK_PLACE_AT: [],
    ISSUED_BY: [],
    CONNECTED_TO: [],
    HAS_OBJECT_TYPE: [],
    IMPACTS: [],
  },
};
