import type { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
  schema: "./src/graphql/schema.graphql",
  generates: {
    "./src/graphql/generated/types.ts": {
      plugins: ["typescript", "typescript-resolvers"],
      config: {
        defaultScalarType: "unknown",
        maybeValue: "T",
        strictScalars: true,
        mappers: {
          Upload: "../models#FileUpload",
        },
        scalars: {
          Date: "Date",
          DateTime: "Date",
          JSON: "any",
          LocalDateTime: "Date",
          Time: "number",
          LocalTime: "number",
          Duration: "String",
          Upload: "../models#FileUpload",
        },
        useIndexSignature: false,
      },
    },
  },
};

export default config;
