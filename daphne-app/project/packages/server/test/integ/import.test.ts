import * as assert from "assert";
import { Container } from "typescript-ioc";
import { faker } from "@faker-js/faker";
import * as path from "path";

import { InquiryService } from "../../src/service/business/inquiry";
import { ImportService } from "../../src/service/business/import";

const inquiry = Container.get(InquiryService);
const importer = Container.get(ImportService);

describe("Testing import", () => {
  it("Import small should work", async () => {
    const myInquiry = await inquiry.create(
      {
        name: faker.commerce.product(),
        description: faker.commerce.productDescription(),
      },
      "_Daphn3!_",
    );
    const filePath = path.join(__dirname, "../resources/import-small.txt");
    const result = await importer.import(myInquiry.id, filePath);

    assert.equal(result.success, true);
  });
});
