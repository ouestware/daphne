import { FC, PropsWithChildren, useMemo } from "react";
import { AppProps } from "next/app";
import {
  ApolloProvider as AP,
  ApolloClient,
  HttpLink,
  InMemoryCache,
  from,
  NormalizedCacheObject,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import merge from "deepmerge";
import isEqual from "lodash/isEqual";

import config from "../../config";
import { useAppContext } from "../context";

export const APOLLO_STATE_PROP_NAME = "__APOLLO_STATE__";

let apolloClient: ApolloClient<NormalizedCacheObject>;

function getApolloLinks(jwt?: string) {
  // Http link of apollo with the auth header
  const httpLink = new HttpLink({
    uri: typeof window === "undefined" ? config.graphql.server_url : config.graphql.uri,
    headers: jwt ? { authorization: `Bearer ${jwt}` } : {},
  });

  // Display errors in the console
  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.forEach(({ message, locations, path }) =>
        console.error(`[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(locations)}, Path: ${path}`),
      );
    if (networkError) console.log(`[Network error]: ${networkError}`);
  });

  return from([errorLink, httpLink]);
}

function createApolloClient(jwt?: string): ApolloClient<NormalizedCacheObject> {
  return new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: getApolloLinks(jwt),
    cache: new InMemoryCache({
      possibleTypes: {
        GraphSearchHit: ["PhysicalPerson", "MoralEntity", "Place", "Factoid"],
      },
    }),
    queryDeduplication: true,
  });
}

export function initializeApollo(
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  opts: { state: any; jwt?: string } = { state: null },
): ApolloClient<NormalizedCacheObject> {
  const _apolloClient = apolloClient ?? createApolloClient(opts.jwt);

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (opts.state) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the initialState from getStaticProps/getServerSideProps in the existing cache
    const data = merge(existingCache, opts.state, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) => sourceArray.every((s) => !isEqual(d, s))),
      ],
    });

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }

  // if a jwt is present, we re-set the link
  if (opts.jwt) {
    _apolloClient.setLink(getApolloLinks(opts.jwt));
  }

  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;

  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export function addApolloState(client: ApolloClient<NormalizedCacheObject>, pageProps: AppProps["pageProps"]) {
  if (pageProps?.props) {
    pageProps.props[APOLLO_STATE_PROP_NAME] = client.cache.extract();
  }
  return pageProps;
}

export function useApollo(pageProps: AppProps["pageProps"]) {
  const state = pageProps[APOLLO_STATE_PROP_NAME];
  const [{ auth }] = useAppContext();

  const store = useMemo(() => initializeApollo({ state, jwt: auth.jwt }), [state, auth.jwt]);
  return store;
}

export const ApolloProvider: FC<PropsWithChildren<{ pageProps: AppProps["pageProps"] }>> = ({
  children,
  pageProps,
}) => {
  const apolloClient = useApollo(pageProps);
  return <AP client={apolloClient}>{children}</AP>;
};
