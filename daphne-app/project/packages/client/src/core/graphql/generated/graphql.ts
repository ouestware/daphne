/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date, represented as a 'yyyy-mm-dd' string */
  Date: any;
  /** A date and time, represented as an ISO-8601 string */
  DateTime: any;
  /** A duration, represented as an ISO 8601 duration string */
  Duration: any;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export enum AggregationType {
  DateHistogramPerYear = 'date_histogram_per_year',
  Terms = 'terms'
}

/** Annotation */
export type Annotation = {
  __typename?: 'Annotation';
  author: Scalars['String'];
  comment?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  patch: Array<Scalars['JSON']>;
  updatedAt: Scalars['DateTime'];
};

export type AnnotationAggregateSelection = {
  __typename?: 'AnnotationAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  count: Scalars['Int'];
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type AnnotationEdge = {
  __typename?: 'AnnotationEdge';
  cursor: Scalars['String'];
  node: Annotation;
};

export type AnnotationOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more AnnotationSort objects to sort Annotations by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<AnnotationSort>>;
};

/** Fields to sort Annotations by. The order in which sorts are applied is not guaranteed when specifying many fields in one AnnotationSort object. */
export type AnnotationSort = {
  author?: InputMaybe<SortDirection>;
  comment?: InputMaybe<SortDirection>;
  createdAt?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  updatedAt?: InputMaybe<SortDirection>;
};

export type AnnotationWhere = {
  AND?: InputMaybe<Array<AnnotationWhere>>;
  NOT?: InputMaybe<AnnotationWhere>;
  OR?: InputMaybe<Array<AnnotationWhere>>;
  author?: InputMaybe<Scalars['String']>;
  author_CONTAINS?: InputMaybe<Scalars['String']>;
  author_ENDS_WITH?: InputMaybe<Scalars['String']>;
  author_IN?: InputMaybe<Array<Scalars['String']>>;
  author_STARTS_WITH?: InputMaybe<Scalars['String']>;
  comment?: InputMaybe<Scalars['String']>;
  comment_CONTAINS?: InputMaybe<Scalars['String']>;
  comment_ENDS_WITH?: InputMaybe<Scalars['String']>;
  comment_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  comment_STARTS_WITH?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  createdAt_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_IN?: InputMaybe<Array<Scalars['DateTime']>>;
  createdAt_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_LTE?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  updatedAt_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_IN?: InputMaybe<Array<Scalars['DateTime']>>;
  updatedAt_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type AnnotationsConnection = {
  __typename?: 'AnnotationsConnection';
  edges: Array<AnnotationEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type Certainty = {
  certainty: Scalars['Float'];
};

export type CertaintyAndType = {
  certainty: Scalars['Float'];
  type: Scalars['String'];
};

export type CertaintyAndTypeSort = {
  certainty?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
};

export type CertaintyAndTypeWhere = {
  AND?: InputMaybe<Array<CertaintyAndTypeWhere>>;
  NOT?: InputMaybe<CertaintyAndTypeWhere>;
  OR?: InputMaybe<Array<CertaintyAndTypeWhere>>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<Scalars['Float']>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
  type?: InputMaybe<Scalars['String']>;
  type_CONTAINS?: InputMaybe<Scalars['String']>;
  type_ENDS_WITH?: InputMaybe<Scalars['String']>;
  type_IN?: InputMaybe<Array<Scalars['String']>>;
  type_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type CertaintySort = {
  certainty?: InputMaybe<SortDirection>;
};

export type CertaintyWhere = {
  AND?: InputMaybe<Array<CertaintyWhere>>;
  NOT?: InputMaybe<CertaintyWhere>;
  OR?: InputMaybe<Array<CertaintyWhere>>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<Scalars['Float']>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
};

/** DataSource */
export type DataSource = {
  __typename?: 'DataSource';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type DataSourceAggregateSelection = {
  __typename?: 'DataSourceAggregateSelection';
  count: Scalars['Int'];
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type DataSourceEdge = {
  __typename?: 'DataSourceEdge';
  cursor: Scalars['String'];
  node: DataSource;
};

export type DataSourceOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more DataSourceSort objects to sort DataSources by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<DataSourceSort>>;
};

/** Fields to sort DataSources by. The order in which sorts are applied is not guaranteed when specifying many fields in one DataSourceSort object. */
export type DataSourceSort = {
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type DataSourceWhere = {
  AND?: InputMaybe<Array<DataSourceWhere>>;
  NOT?: InputMaybe<DataSourceWhere>;
  OR?: InputMaybe<Array<DataSourceWhere>>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type DataSourcesConnection = {
  __typename?: 'DataSourcesConnection';
  edges: Array<DataSourceEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type DateTimeAggregateSelectionNonNullable = {
  __typename?: 'DateTimeAggregateSelectionNonNullable';
  max: Scalars['DateTime'];
  min: Scalars['DateTime'];
};

/**
 * Date
 * --------------
 * Dates can be approximative. Dates can alos be intervals.
 * This implementation encodes both approximations and intervals the same way to ease queries
 * A Date is expressed as a period into the gregorian calendar.
 * It has a start and end date.
 * We keep the original date as ISO string to keep track of the time granularity of extreme dates
 */
export type DateValue = {
  __typename?: 'DateValue';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<DateValueDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: DateValueDataSourcesConnection;
  end?: Maybe<Scalars['Date']>;
  endIso?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  start?: Maybe<Scalars['Date']>;
  startIso?: Maybe<Scalars['String']>;
};


/**
 * Date
 * --------------
 * Dates can be approximative. Dates can alos be intervals.
 * This implementation encodes both approximations and intervals the same way to ease queries
 * A Date is expressed as a period into the gregorian calendar.
 * It has a start and end date.
 * We keep the original date as ISO string to keep track of the time granularity of extreme dates
 */
export type DateValueDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Date
 * --------------
 * Dates can be approximative. Dates can alos be intervals.
 * This implementation encodes both approximations and intervals the same way to ease queries
 * A Date is expressed as a period into the gregorian calendar.
 * It has a start and end date.
 * We keep the original date as ISO string to keep track of the time granularity of extreme dates
 */
export type DateValueDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Date
 * --------------
 * Dates can be approximative. Dates can alos be intervals.
 * This implementation encodes both approximations and intervals the same way to ease queries
 * A Date is expressed as a period into the gregorian calendar.
 * It has a start and end date.
 * We keep the original date as ISO string to keep track of the time granularity of extreme dates
 */
export type DateValueDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<DateValueDataSourcesConnectionSort>>;
  where?: InputMaybe<DateValueDataSourcesConnectionWhere>;
};

export type DateValueAggregateSelection = {
  __typename?: 'DateValueAggregateSelection';
  count: Scalars['Int'];
  endIso: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  startIso: StringAggregateSelectionNullable;
};

export type DateValueDataSourceDataSourcesAggregationSelection = {
  __typename?: 'DateValueDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<DateValueDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<DateValueDataSourceDataSourcesNodeAggregateSelection>;
};

export type DateValueDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'DateValueDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type DateValueDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'DateValueDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type DateValueDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<DateValueDataSourcesAggregateInput>>;
  NOT?: InputMaybe<DateValueDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<DateValueDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<DateValueDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<DateValueDataSourcesNodeAggregationWhereInput>;
};

export type DateValueDataSourcesConnection = {
  __typename?: 'DateValueDataSourcesConnection';
  edges: Array<DateValueDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type DateValueDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type DateValueDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<DateValueDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<DateValueDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<DateValueDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type DateValueDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<DateValueDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<DateValueDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<DateValueDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type DateValueDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<DateValueDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<DateValueDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<DateValueDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type DateValueDataSourcesRelationship = ImportedFrom & {
  __typename?: 'DateValueDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type DateValueEdge = {
  __typename?: 'DateValueEdge';
  cursor: Scalars['String'];
  node: DateValue;
};

export type DateValueOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more DateValueSort objects to sort DateValues by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<DateValueSort>>;
};

/** Fields to sort DateValues by. The order in which sorts are applied is not guaranteed when specifying many fields in one DateValueSort object. */
export type DateValueSort = {
  end?: InputMaybe<SortDirection>;
  endIso?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  start?: InputMaybe<SortDirection>;
  startIso?: InputMaybe<SortDirection>;
};

export type DateValueWhere = {
  AND?: InputMaybe<Array<DateValueWhere>>;
  NOT?: InputMaybe<DateValueWhere>;
  OR?: InputMaybe<Array<DateValueWhere>>;
  dataSourcesAggregate?: InputMaybe<DateValueDataSourcesAggregateInput>;
  /** Return DateValues where all of the related DateValueDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<DateValueDataSourcesConnectionWhere>;
  /** Return DateValues where none of the related DateValueDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<DateValueDataSourcesConnectionWhere>;
  /** Return DateValues where one of the related DateValueDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<DateValueDataSourcesConnectionWhere>;
  /** Return DateValues where some of the related DateValueDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<DateValueDataSourcesConnectionWhere>;
  /** Return DateValues where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return DateValues where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return DateValues where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return DateValues where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  end?: InputMaybe<Scalars['Date']>;
  endIso?: InputMaybe<Scalars['String']>;
  endIso_CONTAINS?: InputMaybe<Scalars['String']>;
  endIso_ENDS_WITH?: InputMaybe<Scalars['String']>;
  endIso_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  endIso_STARTS_WITH?: InputMaybe<Scalars['String']>;
  end_GT?: InputMaybe<Scalars['Date']>;
  end_GTE?: InputMaybe<Scalars['Date']>;
  end_IN?: InputMaybe<Array<InputMaybe<Scalars['Date']>>>;
  end_LT?: InputMaybe<Scalars['Date']>;
  end_LTE?: InputMaybe<Scalars['Date']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  start?: InputMaybe<Scalars['Date']>;
  startIso?: InputMaybe<Scalars['String']>;
  startIso_CONTAINS?: InputMaybe<Scalars['String']>;
  startIso_ENDS_WITH?: InputMaybe<Scalars['String']>;
  startIso_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startIso_STARTS_WITH?: InputMaybe<Scalars['String']>;
  start_GT?: InputMaybe<Scalars['Date']>;
  start_GTE?: InputMaybe<Scalars['Date']>;
  start_IN?: InputMaybe<Array<InputMaybe<Scalars['Date']>>>;
  start_LT?: InputMaybe<Scalars['Date']>;
  start_LTE?: InputMaybe<Scalars['Date']>;
};

export type DateValuesConnection = {
  __typename?: 'DateValuesConnection';
  edges: Array<DateValueEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/** ------ */
export type Domain = {
  __typename?: 'Domain';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<DomainDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: DomainDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};


/** ------ */
export type DomainDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/** ------ */
export type DomainDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/** ------ */
export type DomainDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<DomainDataSourcesConnectionSort>>;
  where?: InputMaybe<DomainDataSourcesConnectionWhere>;
};

export type DomainAggregateSelection = {
  __typename?: 'DomainAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type DomainDataSourceDataSourcesAggregationSelection = {
  __typename?: 'DomainDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<DomainDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<DomainDataSourceDataSourcesNodeAggregateSelection>;
};

export type DomainDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'DomainDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type DomainDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'DomainDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type DomainDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<DomainDataSourcesAggregateInput>>;
  NOT?: InputMaybe<DomainDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<DomainDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<DomainDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<DomainDataSourcesNodeAggregationWhereInput>;
};

export type DomainDataSourcesConnection = {
  __typename?: 'DomainDataSourcesConnection';
  edges: Array<DomainDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type DomainDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type DomainDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<DomainDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<DomainDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<DomainDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type DomainDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<DomainDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<DomainDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<DomainDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type DomainDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<DomainDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<DomainDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<DomainDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type DomainDataSourcesRelationship = ImportedFrom & {
  __typename?: 'DomainDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type DomainEdge = {
  __typename?: 'DomainEdge';
  cursor: Scalars['String'];
  node: Domain;
};

export type DomainOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more DomainSort objects to sort Domains by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<DomainSort>>;
};

/** Fields to sort Domains by. The order in which sorts are applied is not guaranteed when specifying many fields in one DomainSort object. */
export type DomainSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type DomainWhere = {
  AND?: InputMaybe<Array<DomainWhere>>;
  NOT?: InputMaybe<DomainWhere>;
  OR?: InputMaybe<Array<DomainWhere>>;
  dataSourcesAggregate?: InputMaybe<DomainDataSourcesAggregateInput>;
  /** Return Domains where all of the related DomainDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<DomainDataSourcesConnectionWhere>;
  /** Return Domains where none of the related DomainDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<DomainDataSourcesConnectionWhere>;
  /** Return Domains where one of the related DomainDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<DomainDataSourcesConnectionWhere>;
  /** Return Domains where some of the related DomainDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<DomainDataSourcesConnectionWhere>;
  /** Return Domains where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Domains where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Domains where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Domains where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type DomainsConnection = {
  __typename?: 'DomainsConnection';
  edges: Array<DomainEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type DurationAggregateSelectionNullable = {
  __typename?: 'DurationAggregateSelectionNullable';
  max?: Maybe<Scalars['Duration']>;
  min?: Maybe<Scalars['Duration']>;
};

/**
 * Edition
 * -------
 */
export type Edition = {
  __typename?: 'Edition';
  collection?: Maybe<Scalars['String']>;
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<EditionDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: EditionDataSourcesConnection;
  dates: Array<DateValue>;
  datesAggregate?: Maybe<EditionDateValueDatesAggregationSelection>;
  datesConnection: EditionDatesConnection;
  editor?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  notes?: Maybe<Scalars['String']>;
  pages?: Maybe<Scalars['String']>;
  place?: Maybe<Scalars['String']>;
  sources: Array<Source>;
  sourcesAggregate?: Maybe<EditionSourceSourcesAggregationSelection>;
  sourcesConnection: EditionSourcesConnection;
  title?: Maybe<Scalars['String']>;
};


/**
 * Edition
 * -------
 */
export type EditionDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<EditionDataSourcesConnectionSort>>;
  where?: InputMaybe<EditionDataSourcesConnectionWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionDatesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DateValueOptions>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionDatesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionDatesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<EditionDatesConnectionSort>>;
  where?: InputMaybe<EditionDatesConnectionWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceOptions>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * Edition
 * -------
 */
export type EditionSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<EditionSourcesConnectionSort>>;
  where?: InputMaybe<EditionSourcesConnectionWhere>;
};

export type EditionAggregateSelection = {
  __typename?: 'EditionAggregateSelection';
  collection: StringAggregateSelectionNullable;
  count: Scalars['Int'];
  editor: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  notes: StringAggregateSelectionNullable;
  pages: StringAggregateSelectionNullable;
  place: StringAggregateSelectionNullable;
  title: StringAggregateSelectionNullable;
};

export type EditionDataSourceDataSourcesAggregationSelection = {
  __typename?: 'EditionDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<EditionDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<EditionDataSourceDataSourcesNodeAggregateSelection>;
};

export type EditionDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'EditionDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type EditionDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'EditionDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type EditionDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<EditionDataSourcesAggregateInput>>;
  NOT?: InputMaybe<EditionDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<EditionDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<EditionDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<EditionDataSourcesNodeAggregationWhereInput>;
};

export type EditionDataSourcesConnection = {
  __typename?: 'EditionDataSourcesConnection';
  edges: Array<EditionDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type EditionDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type EditionDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<EditionDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<EditionDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<EditionDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type EditionDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<EditionDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<EditionDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<EditionDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type EditionDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<EditionDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<EditionDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<EditionDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type EditionDataSourcesRelationship = ImportedFrom & {
  __typename?: 'EditionDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type EditionDateValueDatesAggregationSelection = {
  __typename?: 'EditionDateValueDatesAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<EditionDateValueDatesNodeAggregateSelection>;
};

export type EditionDateValueDatesNodeAggregateSelection = {
  __typename?: 'EditionDateValueDatesNodeAggregateSelection';
  endIso: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  startIso: StringAggregateSelectionNullable;
};

export type EditionDatesAggregateInput = {
  AND?: InputMaybe<Array<EditionDatesAggregateInput>>;
  NOT?: InputMaybe<EditionDatesAggregateInput>;
  OR?: InputMaybe<Array<EditionDatesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<EditionDatesNodeAggregationWhereInput>;
};

export type EditionDatesConnection = {
  __typename?: 'EditionDatesConnection';
  edges: Array<EditionDatesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type EditionDatesConnectionSort = {
  node?: InputMaybe<DateValueSort>;
};

export type EditionDatesConnectionWhere = {
  AND?: InputMaybe<Array<EditionDatesConnectionWhere>>;
  NOT?: InputMaybe<EditionDatesConnectionWhere>;
  OR?: InputMaybe<Array<EditionDatesConnectionWhere>>;
  node?: InputMaybe<DateValueWhere>;
};

export type EditionDatesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<EditionDatesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<EditionDatesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<EditionDatesNodeAggregationWhereInput>>;
  endIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  endIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  startIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type EditionDatesRelationship = {
  __typename?: 'EditionDatesRelationship';
  cursor: Scalars['String'];
  node: DateValue;
};

export type EditionEdge = {
  __typename?: 'EditionEdge';
  cursor: Scalars['String'];
  node: Edition;
};

export type EditionOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more EditionSort objects to sort Editions by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<EditionSort>>;
};

/** Fields to sort Editions by. The order in which sorts are applied is not guaranteed when specifying many fields in one EditionSort object. */
export type EditionSort = {
  collection?: InputMaybe<SortDirection>;
  editor?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  notes?: InputMaybe<SortDirection>;
  pages?: InputMaybe<SortDirection>;
  place?: InputMaybe<SortDirection>;
  title?: InputMaybe<SortDirection>;
};

export type EditionSourceSourcesAggregationSelection = {
  __typename?: 'EditionSourceSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<EditionSourceSourcesEdgeAggregateSelection>;
  node?: Maybe<EditionSourceSourcesNodeAggregateSelection>;
};

export type EditionSourceSourcesEdgeAggregateSelection = {
  __typename?: 'EditionSourceSourcesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type EditionSourceSourcesNodeAggregateSelection = {
  __typename?: 'EditionSourceSourcesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  language: StringAggregateSelectionNullable;
  name: StringAggregateSelectionNonNullable;
  reputation: FloatAggregateSelectionNullable;
};

export type EditionSourcesAggregateInput = {
  AND?: InputMaybe<Array<EditionSourcesAggregateInput>>;
  NOT?: InputMaybe<EditionSourcesAggregateInput>;
  OR?: InputMaybe<Array<EditionSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<EditionSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<EditionSourcesNodeAggregationWhereInput>;
};

export type EditionSourcesConnection = {
  __typename?: 'EditionSourcesConnection';
  edges: Array<EditionSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type EditionSourcesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<SourceSort>;
};

export type EditionSourcesConnectionWhere = {
  AND?: InputMaybe<Array<EditionSourcesConnectionWhere>>;
  NOT?: InputMaybe<EditionSourcesConnectionWhere>;
  OR?: InputMaybe<Array<EditionSourcesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<SourceWhere>;
};

export type EditionSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<EditionSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<EditionSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<EditionSourcesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type EditionSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<EditionSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<EditionSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<EditionSourcesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  language_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  reputation_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type EditionSourcesRelationship = Certainty & {
  __typename?: 'EditionSourcesRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Source;
};

export type EditionWhere = {
  AND?: InputMaybe<Array<EditionWhere>>;
  NOT?: InputMaybe<EditionWhere>;
  OR?: InputMaybe<Array<EditionWhere>>;
  collection?: InputMaybe<Scalars['String']>;
  collection_CONTAINS?: InputMaybe<Scalars['String']>;
  collection_ENDS_WITH?: InputMaybe<Scalars['String']>;
  collection_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  collection_STARTS_WITH?: InputMaybe<Scalars['String']>;
  dataSourcesAggregate?: InputMaybe<EditionDataSourcesAggregateInput>;
  /** Return Editions where all of the related EditionDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<EditionDataSourcesConnectionWhere>;
  /** Return Editions where none of the related EditionDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<EditionDataSourcesConnectionWhere>;
  /** Return Editions where one of the related EditionDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<EditionDataSourcesConnectionWhere>;
  /** Return Editions where some of the related EditionDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<EditionDataSourcesConnectionWhere>;
  /** Return Editions where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Editions where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Editions where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Editions where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  datesAggregate?: InputMaybe<EditionDatesAggregateInput>;
  /** Return Editions where all of the related EditionDatesConnections match this filter */
  datesConnection_ALL?: InputMaybe<EditionDatesConnectionWhere>;
  /** Return Editions where none of the related EditionDatesConnections match this filter */
  datesConnection_NONE?: InputMaybe<EditionDatesConnectionWhere>;
  /** Return Editions where one of the related EditionDatesConnections match this filter */
  datesConnection_SINGLE?: InputMaybe<EditionDatesConnectionWhere>;
  /** Return Editions where some of the related EditionDatesConnections match this filter */
  datesConnection_SOME?: InputMaybe<EditionDatesConnectionWhere>;
  /** Return Editions where all of the related DateValues match this filter */
  dates_ALL?: InputMaybe<DateValueWhere>;
  /** Return Editions where none of the related DateValues match this filter */
  dates_NONE?: InputMaybe<DateValueWhere>;
  /** Return Editions where one of the related DateValues match this filter */
  dates_SINGLE?: InputMaybe<DateValueWhere>;
  /** Return Editions where some of the related DateValues match this filter */
  dates_SOME?: InputMaybe<DateValueWhere>;
  editor?: InputMaybe<Scalars['String']>;
  editor_CONTAINS?: InputMaybe<Scalars['String']>;
  editor_ENDS_WITH?: InputMaybe<Scalars['String']>;
  editor_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  editor_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  notes?: InputMaybe<Scalars['String']>;
  notes_CONTAINS?: InputMaybe<Scalars['String']>;
  notes_ENDS_WITH?: InputMaybe<Scalars['String']>;
  notes_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  notes_STARTS_WITH?: InputMaybe<Scalars['String']>;
  pages?: InputMaybe<Scalars['String']>;
  pages_CONTAINS?: InputMaybe<Scalars['String']>;
  pages_ENDS_WITH?: InputMaybe<Scalars['String']>;
  pages_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  pages_STARTS_WITH?: InputMaybe<Scalars['String']>;
  place?: InputMaybe<Scalars['String']>;
  place_CONTAINS?: InputMaybe<Scalars['String']>;
  place_ENDS_WITH?: InputMaybe<Scalars['String']>;
  place_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  place_STARTS_WITH?: InputMaybe<Scalars['String']>;
  sourcesAggregate?: InputMaybe<EditionSourcesAggregateInput>;
  /** Return Editions where all of the related EditionSourcesConnections match this filter */
  sourcesConnection_ALL?: InputMaybe<EditionSourcesConnectionWhere>;
  /** Return Editions where none of the related EditionSourcesConnections match this filter */
  sourcesConnection_NONE?: InputMaybe<EditionSourcesConnectionWhere>;
  /** Return Editions where one of the related EditionSourcesConnections match this filter */
  sourcesConnection_SINGLE?: InputMaybe<EditionSourcesConnectionWhere>;
  /** Return Editions where some of the related EditionSourcesConnections match this filter */
  sourcesConnection_SOME?: InputMaybe<EditionSourcesConnectionWhere>;
  /** Return Editions where all of the related Sources match this filter */
  sources_ALL?: InputMaybe<SourceWhere>;
  /** Return Editions where none of the related Sources match this filter */
  sources_NONE?: InputMaybe<SourceWhere>;
  /** Return Editions where one of the related Sources match this filter */
  sources_SINGLE?: InputMaybe<SourceWhere>;
  /** Return Editions where some of the related Sources match this filter */
  sources_SOME?: InputMaybe<SourceWhere>;
  title?: InputMaybe<Scalars['String']>;
  title_CONTAINS?: InputMaybe<Scalars['String']>;
  title_ENDS_WITH?: InputMaybe<Scalars['String']>;
  title_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  title_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type EditionsConnection = {
  __typename?: 'EditionsConnection';
  edges: Array<EditionEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/**
 * Factoid
 * -------
 */
export type Factoid = {
  __typename?: 'Factoid';
  annotations: Array<Annotation>;
  annotationsAggregate?: Maybe<FactoidAnnotationAnnotationsAggregationSelection>;
  annotationsConnection: FactoidAnnotationsConnection;
  certainty: Scalars['Float'];
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<FactoidDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: FactoidDataSourcesConnection;
  dates: Array<DateValue>;
  datesAggregate?: Maybe<FactoidDateValueDatesAggregationSelection>;
  datesConnection: FactoidDatesConnection;
  description?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Duration']>;
  id: Scalars['ID'];
  impacts: Array<Object>;
  impactsAggregate?: Maybe<FactoidObjectImpactsAggregationSelection>;
  impactsConnection: FactoidImpactsConnection;
  inquiry: Inquiry;
  linkedTo: Array<Factoid>;
  linkedToAggregate?: Maybe<FactoidFactoidLinkedToAggregationSelection>;
  linkedToConnection: FactoidLinkedToConnection;
  originalText?: Maybe<Scalars['String']>;
  persons: Array<FactoidPersonParticipate>;
  personsAggregate?: Maybe<FactoidFactoidPersonParticipatePersonsAggregationSelection>;
  personsConnection: FactoidPersonsConnection;
  places: Array<Place>;
  placesAggregate?: Maybe<FactoidPlacePlacesAggregationSelection>;
  placesConnection: FactoidPlacesConnection;
  sources: Array<Source>;
  sourcesAggregate?: Maybe<FactoidSourceSourcesAggregationSelection>;
  sourcesConnection: FactoidSourcesConnection;
  types: Array<FactoidType>;
  typesAggregate?: Maybe<FactoidFactoidTypeTypesAggregationSelection>;
  typesConnection: FactoidTypesConnection;
};


/**
 * Factoid
 * -------
 */
export type FactoidAnnotationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidAnnotationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidAnnotationsConnectionSort>>;
  where?: InputMaybe<FactoidAnnotationsConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidDataSourcesConnectionSort>>;
  where?: InputMaybe<FactoidDataSourcesConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidDatesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DateValueOptions>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidDatesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidDatesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidDatesConnectionSort>>;
  where?: InputMaybe<FactoidDatesConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidImpactsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ObjectOptions>;
  where?: InputMaybe<ObjectWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidImpactsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ObjectWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidImpactsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidImpactsConnectionSort>>;
  where?: InputMaybe<FactoidImpactsConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidLinkedToArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidOptions>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidLinkedToAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidLinkedToConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidLinkedToConnectionSort>>;
  where?: InputMaybe<FactoidLinkedToConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidPersonsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidPersonParticipateOptions>;
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidPersonsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidPersonsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPersonsConnectionSort>>;
  where?: InputMaybe<FactoidPersonsConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidPlacesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<PlaceOptions>;
  where?: InputMaybe<PlaceWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidPlacesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<PlaceWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidPlacesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPlacesConnectionSort>>;
  where?: InputMaybe<FactoidPlacesConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceOptions>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidSourcesConnectionSort>>;
  where?: InputMaybe<FactoidSourcesConnectionWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidTypesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidTypeOptions>;
  where?: InputMaybe<FactoidTypeWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidTypesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidTypeWhere>;
};


/**
 * Factoid
 * -------
 */
export type FactoidTypesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidTypesConnectionSort>>;
  where?: InputMaybe<FactoidTypesConnectionWhere>;
};

export type FactoidAggregateSelection = {
  __typename?: 'FactoidAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  duration: DurationAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  originalText: StringAggregateSelectionNullable;
};

export type FactoidAnnotationAnnotationsAggregationSelection = {
  __typename?: 'FactoidAnnotationAnnotationsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidAnnotationAnnotationsNodeAggregateSelection>;
};

export type FactoidAnnotationAnnotationsNodeAggregateSelection = {
  __typename?: 'FactoidAnnotationAnnotationsNodeAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type FactoidAnnotationsAggregateInput = {
  AND?: InputMaybe<Array<FactoidAnnotationsAggregateInput>>;
  NOT?: InputMaybe<FactoidAnnotationsAggregateInput>;
  OR?: InputMaybe<Array<FactoidAnnotationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidAnnotationsNodeAggregationWhereInput>;
};

export type FactoidAnnotationsConnection = {
  __typename?: 'FactoidAnnotationsConnection';
  edges: Array<FactoidAnnotationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidAnnotationsConnectionSort = {
  node?: InputMaybe<AnnotationSort>;
};

export type FactoidAnnotationsConnectionWhere = {
  AND?: InputMaybe<Array<FactoidAnnotationsConnectionWhere>>;
  NOT?: InputMaybe<FactoidAnnotationsConnectionWhere>;
  OR?: InputMaybe<Array<FactoidAnnotationsConnectionWhere>>;
  node?: InputMaybe<AnnotationWhere>;
};

export type FactoidAnnotationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidAnnotationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidAnnotationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidAnnotationsNodeAggregationWhereInput>>;
  author_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  author_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  comment_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  createdAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type FactoidAnnotationsRelationship = {
  __typename?: 'FactoidAnnotationsRelationship';
  cursor: Scalars['String'];
  node: Annotation;
};

export type FactoidDataSourceDataSourcesAggregationSelection = {
  __typename?: 'FactoidDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<FactoidDataSourceDataSourcesNodeAggregateSelection>;
};

export type FactoidDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'FactoidDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type FactoidDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'FactoidDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<FactoidDataSourcesAggregateInput>>;
  NOT?: InputMaybe<FactoidDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<FactoidDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidDataSourcesNodeAggregationWhereInput>;
};

export type FactoidDataSourcesConnection = {
  __typename?: 'FactoidDataSourcesConnection';
  edges: Array<FactoidDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type FactoidDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<FactoidDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type FactoidDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidDataSourcesRelationship = ImportedFrom & {
  __typename?: 'FactoidDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type FactoidDateValueDatesAggregationSelection = {
  __typename?: 'FactoidDateValueDatesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidDateValueDatesEdgeAggregateSelection>;
  node?: Maybe<FactoidDateValueDatesNodeAggregateSelection>;
};

export type FactoidDateValueDatesEdgeAggregateSelection = {
  __typename?: 'FactoidDateValueDatesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type FactoidDateValueDatesNodeAggregateSelection = {
  __typename?: 'FactoidDateValueDatesNodeAggregateSelection';
  endIso: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  startIso: StringAggregateSelectionNullable;
};

export type FactoidDatesAggregateInput = {
  AND?: InputMaybe<Array<FactoidDatesAggregateInput>>;
  NOT?: InputMaybe<FactoidDatesAggregateInput>;
  OR?: InputMaybe<Array<FactoidDatesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidDatesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidDatesNodeAggregationWhereInput>;
};

export type FactoidDatesConnection = {
  __typename?: 'FactoidDatesConnection';
  edges: Array<FactoidDatesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidDatesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<DateValueSort>;
};

export type FactoidDatesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidDatesConnectionWhere>>;
  NOT?: InputMaybe<FactoidDatesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidDatesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<DateValueWhere>;
};

export type FactoidDatesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidDatesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidDatesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidDatesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type FactoidDatesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidDatesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidDatesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidDatesNodeAggregationWhereInput>>;
  endIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  endIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  startIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidDatesRelationship = Certainty & {
  __typename?: 'FactoidDatesRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: DateValue;
};

export type FactoidEdge = {
  __typename?: 'FactoidEdge';
  cursor: Scalars['String'];
  node: Factoid;
};

export type FactoidFactoidLinkedToAggregationSelection = {
  __typename?: 'FactoidFactoidLinkedToAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidFactoidLinkedToEdgeAggregateSelection>;
  node?: Maybe<FactoidFactoidLinkedToNodeAggregateSelection>;
};

export type FactoidFactoidLinkedToEdgeAggregateSelection = {
  __typename?: 'FactoidFactoidLinkedToEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
  type: StringAggregateSelectionNonNullable;
};

export type FactoidFactoidLinkedToNodeAggregateSelection = {
  __typename?: 'FactoidFactoidLinkedToNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  duration: DurationAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  originalText: StringAggregateSelectionNullable;
};

export type FactoidFactoidPersonParticipatePersonsAggregationSelection = {
  __typename?: 'FactoidFactoidPersonParticipatePersonsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidFactoidPersonParticipatePersonsNodeAggregateSelection>;
};

export type FactoidFactoidPersonParticipatePersonsNodeAggregateSelection = {
  __typename?: 'FactoidFactoidPersonParticipatePersonsNodeAggregateSelection';
  certainty: FloatAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
};

export type FactoidFactoidTypeTypesAggregationSelection = {
  __typename?: 'FactoidFactoidTypeTypesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidFactoidTypeTypesEdgeAggregateSelection>;
  node?: Maybe<FactoidFactoidTypeTypesNodeAggregateSelection>;
};

export type FactoidFactoidTypeTypesEdgeAggregateSelection = {
  __typename?: 'FactoidFactoidTypeTypesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type FactoidFactoidTypeTypesNodeAggregateSelection = {
  __typename?: 'FactoidFactoidTypeTypesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidImpactObject = {
  certainty: Scalars['Float'];
  quantity?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
};

export type FactoidImpactObjectSort = {
  certainty?: InputMaybe<SortDirection>;
  quantity?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
};

export type FactoidImpactObjectWhere = {
  AND?: InputMaybe<Array<FactoidImpactObjectWhere>>;
  NOT?: InputMaybe<FactoidImpactObjectWhere>;
  OR?: InputMaybe<Array<FactoidImpactObjectWhere>>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<Scalars['Float']>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
  quantity?: InputMaybe<Scalars['Int']>;
  quantity_GT?: InputMaybe<Scalars['Int']>;
  quantity_GTE?: InputMaybe<Scalars['Int']>;
  quantity_IN?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  quantity_LT?: InputMaybe<Scalars['Int']>;
  quantity_LTE?: InputMaybe<Scalars['Int']>;
  type?: InputMaybe<Scalars['String']>;
  type_CONTAINS?: InputMaybe<Scalars['String']>;
  type_ENDS_WITH?: InputMaybe<Scalars['String']>;
  type_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  type_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type FactoidImpactsAggregateInput = {
  AND?: InputMaybe<Array<FactoidImpactsAggregateInput>>;
  NOT?: InputMaybe<FactoidImpactsAggregateInput>;
  OR?: InputMaybe<Array<FactoidImpactsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidImpactsEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidImpactsNodeAggregationWhereInput>;
};

export type FactoidImpactsConnection = {
  __typename?: 'FactoidImpactsConnection';
  edges: Array<FactoidImpactsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidImpactsConnectionSort = {
  edge?: InputMaybe<FactoidImpactObjectSort>;
  node?: InputMaybe<ObjectSort>;
};

export type FactoidImpactsConnectionWhere = {
  AND?: InputMaybe<Array<FactoidImpactsConnectionWhere>>;
  NOT?: InputMaybe<FactoidImpactsConnectionWhere>;
  OR?: InputMaybe<Array<FactoidImpactsConnectionWhere>>;
  edge?: InputMaybe<FactoidImpactObjectWhere>;
  node?: InputMaybe<ObjectWhere>;
};

export type FactoidImpactsEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidImpactsEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidImpactsEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidImpactsEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
  quantity_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  quantity_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  quantity_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  quantity_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  quantity_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  quantity_MAX_EQUAL?: InputMaybe<Scalars['Int']>;
  quantity_MAX_GT?: InputMaybe<Scalars['Int']>;
  quantity_MAX_GTE?: InputMaybe<Scalars['Int']>;
  quantity_MAX_LT?: InputMaybe<Scalars['Int']>;
  quantity_MAX_LTE?: InputMaybe<Scalars['Int']>;
  quantity_MIN_EQUAL?: InputMaybe<Scalars['Int']>;
  quantity_MIN_GT?: InputMaybe<Scalars['Int']>;
  quantity_MIN_GTE?: InputMaybe<Scalars['Int']>;
  quantity_MIN_LT?: InputMaybe<Scalars['Int']>;
  quantity_MIN_LTE?: InputMaybe<Scalars['Int']>;
  quantity_SUM_EQUAL?: InputMaybe<Scalars['Int']>;
  quantity_SUM_GT?: InputMaybe<Scalars['Int']>;
  quantity_SUM_GTE?: InputMaybe<Scalars['Int']>;
  quantity_SUM_LT?: InputMaybe<Scalars['Int']>;
  quantity_SUM_LTE?: InputMaybe<Scalars['Int']>;
  type_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  type_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidImpactsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidImpactsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidImpactsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidImpactsNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidImpactsRelationship = FactoidImpactObject & {
  __typename?: 'FactoidImpactsRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Object;
  quantity?: Maybe<Scalars['Int']>;
  type?: Maybe<Scalars['String']>;
};

export type FactoidLinkedToAggregateInput = {
  AND?: InputMaybe<Array<FactoidLinkedToAggregateInput>>;
  NOT?: InputMaybe<FactoidLinkedToAggregateInput>;
  OR?: InputMaybe<Array<FactoidLinkedToAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidLinkedToEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidLinkedToNodeAggregationWhereInput>;
};

export type FactoidLinkedToConnection = {
  __typename?: 'FactoidLinkedToConnection';
  edges: Array<FactoidLinkedToRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidLinkedToConnectionSort = {
  edge?: InputMaybe<CertaintyAndTypeSort>;
  node?: InputMaybe<FactoidSort>;
};

export type FactoidLinkedToConnectionWhere = {
  AND?: InputMaybe<Array<FactoidLinkedToConnectionWhere>>;
  NOT?: InputMaybe<FactoidLinkedToConnectionWhere>;
  OR?: InputMaybe<Array<FactoidLinkedToConnectionWhere>>;
  edge?: InputMaybe<CertaintyAndTypeWhere>;
  node?: InputMaybe<FactoidWhere>;
};

export type FactoidLinkedToEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidLinkedToEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidLinkedToEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidLinkedToEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  type_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidLinkedToNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidLinkedToNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidLinkedToNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidLinkedToNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  duration_AVERAGE_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GTE?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LTE?: InputMaybe<Scalars['Duration']>;
  originalText_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalText_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidLinkedToRelationship = CertaintyAndType & {
  __typename?: 'FactoidLinkedToRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Factoid;
  type: Scalars['String'];
};

export type FactoidObjectImpactsAggregationSelection = {
  __typename?: 'FactoidObjectImpactsAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidObjectImpactsEdgeAggregateSelection>;
  node?: Maybe<FactoidObjectImpactsNodeAggregateSelection>;
};

export type FactoidObjectImpactsEdgeAggregateSelection = {
  __typename?: 'FactoidObjectImpactsEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
  quantity: IntAggregateSelectionNullable;
  type: StringAggregateSelectionNullable;
};

export type FactoidObjectImpactsNodeAggregateSelection = {
  __typename?: 'FactoidObjectImpactsNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more FactoidSort objects to sort Factoids by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<FactoidSort>>;
};

/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipate = {
  __typename?: 'FactoidPersonParticipate';
  annotations: Array<Annotation>;
  annotationsAggregate?: Maybe<FactoidPersonParticipateAnnotationAnnotationsAggregationSelection>;
  annotationsConnection: FactoidPersonParticipateAnnotationsConnection;
  certainty?: Maybe<Scalars['Float']>;
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<FactoidPersonParticipateDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: FactoidPersonParticipateDataSourcesConnection;
  factoid: Factoid;
  factoidAggregate?: Maybe<FactoidPersonParticipateFactoidFactoidAggregationSelection>;
  factoidConnection: FactoidPersonParticipateFactoidConnection;
  id: Scalars['ID'];
  person: Person;
  personConnection: FactoidPersonParticipatePersonConnection;
  ranks: Array<Rank>;
  ranksAggregate?: Maybe<FactoidPersonParticipateRankRanksAggregationSelection>;
  ranksConnection: FactoidPersonParticipateRanksConnection;
  roles: Array<Role>;
  rolesAggregate?: Maybe<FactoidPersonParticipateRoleRolesAggregationSelection>;
  rolesConnection: FactoidPersonParticipateRolesConnection;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateAnnotationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateAnnotationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPersonParticipateAnnotationsConnectionSort>>;
  where?: InputMaybe<FactoidPersonParticipateAnnotationsConnectionWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPersonParticipateDataSourcesConnectionSort>>;
  where?: InputMaybe<FactoidPersonParticipateDataSourcesConnectionWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateFactoidArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidOptions>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateFactoidAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateFactoidConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPersonParticipateFactoidConnectionSort>>;
  where?: InputMaybe<FactoidPersonParticipateFactoidConnectionWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipatePersonArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<QueryOptions>;
  where?: InputMaybe<PersonWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipatePersonConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FactoidPersonParticipatePersonConnectionWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateRanksArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<RankOptions>;
  where?: InputMaybe<RankWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateRanksAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<RankWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateRanksConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPersonParticipateRanksConnectionSort>>;
  where?: InputMaybe<FactoidPersonParticipateRanksConnectionWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateRolesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<RoleOptions>;
  where?: InputMaybe<RoleWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateRolesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<RoleWhere>;
};


/**
 * TODO: Check if it can be done directly on the relationship type
 * Do we need a fixed list of rank & role ?
 */
export type FactoidPersonParticipateRolesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidPersonParticipateRolesConnectionSort>>;
  where?: InputMaybe<FactoidPersonParticipateRolesConnectionWhere>;
};

export type FactoidPersonParticipateAggregateSelection = {
  __typename?: 'FactoidPersonParticipateAggregateSelection';
  certainty: FloatAggregateSelectionNullable;
  count: Scalars['Int'];
  id: IdAggregateSelectionNonNullable;
};

export type FactoidPersonParticipateAnnotationAnnotationsAggregationSelection = {
  __typename?: 'FactoidPersonParticipateAnnotationAnnotationsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidPersonParticipateAnnotationAnnotationsNodeAggregateSelection>;
};

export type FactoidPersonParticipateAnnotationAnnotationsNodeAggregateSelection = {
  __typename?: 'FactoidPersonParticipateAnnotationAnnotationsNodeAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type FactoidPersonParticipateAnnotationsAggregateInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateAnnotationsAggregateInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateAnnotationsAggregateInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateAnnotationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidPersonParticipateAnnotationsNodeAggregationWhereInput>;
};

export type FactoidPersonParticipateAnnotationsConnection = {
  __typename?: 'FactoidPersonParticipateAnnotationsConnection';
  edges: Array<FactoidPersonParticipateAnnotationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonParticipateAnnotationsConnectionSort = {
  node?: InputMaybe<AnnotationSort>;
};

export type FactoidPersonParticipateAnnotationsConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipateAnnotationsConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipateAnnotationsConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipateAnnotationsConnectionWhere>>;
  node?: InputMaybe<AnnotationWhere>;
};

export type FactoidPersonParticipateAnnotationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateAnnotationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateAnnotationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateAnnotationsNodeAggregationWhereInput>>;
  author_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  author_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  comment_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  createdAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type FactoidPersonParticipateAnnotationsRelationship = {
  __typename?: 'FactoidPersonParticipateAnnotationsRelationship';
  cursor: Scalars['String'];
  node: Annotation;
};

export type FactoidPersonParticipateDataSourceDataSourcesAggregationSelection = {
  __typename?: 'FactoidPersonParticipateDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidPersonParticipateDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<FactoidPersonParticipateDataSourceDataSourcesNodeAggregateSelection>;
};

export type FactoidPersonParticipateDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'FactoidPersonParticipateDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type FactoidPersonParticipateDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'FactoidPersonParticipateDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidPersonParticipateDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateDataSourcesAggregateInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidPersonParticipateDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidPersonParticipateDataSourcesNodeAggregationWhereInput>;
};

export type FactoidPersonParticipateDataSourcesConnection = {
  __typename?: 'FactoidPersonParticipateDataSourcesConnection';
  edges: Array<FactoidPersonParticipateDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonParticipateDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type FactoidPersonParticipateDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipateDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipateDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipateDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type FactoidPersonParticipateDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidPersonParticipateDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidPersonParticipateDataSourcesRelationship = ImportedFrom & {
  __typename?: 'FactoidPersonParticipateDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type FactoidPersonParticipateEdge = {
  __typename?: 'FactoidPersonParticipateEdge';
  cursor: Scalars['String'];
  node: FactoidPersonParticipate;
};

export type FactoidPersonParticipateFactoidAggregateInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateFactoidAggregateInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateFactoidAggregateInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateFactoidAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidPersonParticipateFactoidNodeAggregationWhereInput>;
};

export type FactoidPersonParticipateFactoidConnection = {
  __typename?: 'FactoidPersonParticipateFactoidConnection';
  edges: Array<FactoidPersonParticipateFactoidRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonParticipateFactoidConnectionSort = {
  node?: InputMaybe<FactoidSort>;
};

export type FactoidPersonParticipateFactoidConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipateFactoidConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipateFactoidConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipateFactoidConnectionWhere>>;
  node?: InputMaybe<FactoidWhere>;
};

export type FactoidPersonParticipateFactoidFactoidAggregationSelection = {
  __typename?: 'FactoidPersonParticipateFactoidFactoidAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidPersonParticipateFactoidFactoidNodeAggregateSelection>;
};

export type FactoidPersonParticipateFactoidFactoidNodeAggregateSelection = {
  __typename?: 'FactoidPersonParticipateFactoidFactoidNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  duration: DurationAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  originalText: StringAggregateSelectionNullable;
};

export type FactoidPersonParticipateFactoidNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateFactoidNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateFactoidNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateFactoidNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  duration_AVERAGE_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GTE?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LTE?: InputMaybe<Scalars['Duration']>;
  originalText_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalText_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidPersonParticipateFactoidRelationship = {
  __typename?: 'FactoidPersonParticipateFactoidRelationship';
  cursor: Scalars['String'];
  node: Factoid;
};

export type FactoidPersonParticipateOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more FactoidPersonParticipateSort objects to sort FactoidPersonParticipates by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<FactoidPersonParticipateSort>>;
};

export type FactoidPersonParticipatePersonConnection = {
  __typename?: 'FactoidPersonParticipatePersonConnection';
  edges: Array<FactoidPersonParticipatePersonRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonParticipatePersonConnectionWhere = {
  MoralEntity?: InputMaybe<FactoidPersonParticipatePersonMoralEntityConnectionWhere>;
  PhysicalPerson?: InputMaybe<FactoidPersonParticipatePersonPhysicalPersonConnectionWhere>;
};

export type FactoidPersonParticipatePersonMoralEntityConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipatePersonMoralEntityConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipatePersonMoralEntityConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipatePersonMoralEntityConnectionWhere>>;
  node?: InputMaybe<MoralEntityWhere>;
};

export type FactoidPersonParticipatePersonPhysicalPersonConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipatePersonPhysicalPersonConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipatePersonPhysicalPersonConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipatePersonPhysicalPersonConnectionWhere>>;
  node?: InputMaybe<PhysicalPersonWhere>;
};

export type FactoidPersonParticipatePersonRelationship = {
  __typename?: 'FactoidPersonParticipatePersonRelationship';
  cursor: Scalars['String'];
  node: Person;
};

export type FactoidPersonParticipateRankRanksAggregationSelection = {
  __typename?: 'FactoidPersonParticipateRankRanksAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidPersonParticipateRankRanksNodeAggregateSelection>;
};

export type FactoidPersonParticipateRankRanksNodeAggregateSelection = {
  __typename?: 'FactoidPersonParticipateRankRanksNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidPersonParticipateRanksAggregateInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateRanksAggregateInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateRanksAggregateInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateRanksAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidPersonParticipateRanksNodeAggregationWhereInput>;
};

export type FactoidPersonParticipateRanksConnection = {
  __typename?: 'FactoidPersonParticipateRanksConnection';
  edges: Array<FactoidPersonParticipateRanksRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonParticipateRanksConnectionSort = {
  node?: InputMaybe<RankSort>;
};

export type FactoidPersonParticipateRanksConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipateRanksConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipateRanksConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipateRanksConnectionWhere>>;
  node?: InputMaybe<RankWhere>;
};

export type FactoidPersonParticipateRanksNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateRanksNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateRanksNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateRanksNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidPersonParticipateRanksRelationship = {
  __typename?: 'FactoidPersonParticipateRanksRelationship';
  cursor: Scalars['String'];
  node: Rank;
};

export type FactoidPersonParticipateRoleRolesAggregationSelection = {
  __typename?: 'FactoidPersonParticipateRoleRolesAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidPersonParticipateRoleRolesNodeAggregateSelection>;
};

export type FactoidPersonParticipateRoleRolesNodeAggregateSelection = {
  __typename?: 'FactoidPersonParticipateRoleRolesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidPersonParticipateRolesAggregateInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateRolesAggregateInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateRolesAggregateInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateRolesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidPersonParticipateRolesNodeAggregationWhereInput>;
};

export type FactoidPersonParticipateRolesConnection = {
  __typename?: 'FactoidPersonParticipateRolesConnection';
  edges: Array<FactoidPersonParticipateRolesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonParticipateRolesConnectionSort = {
  node?: InputMaybe<RoleSort>;
};

export type FactoidPersonParticipateRolesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipateRolesConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipateRolesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipateRolesConnectionWhere>>;
  node?: InputMaybe<RoleWhere>;
};

export type FactoidPersonParticipateRolesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonParticipateRolesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonParticipateRolesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonParticipateRolesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidPersonParticipateRolesRelationship = {
  __typename?: 'FactoidPersonParticipateRolesRelationship';
  cursor: Scalars['String'];
  node: Role;
};

/** Fields to sort FactoidPersonParticipates by. The order in which sorts are applied is not guaranteed when specifying many fields in one FactoidPersonParticipateSort object. */
export type FactoidPersonParticipateSort = {
  certainty?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
};

export type FactoidPersonParticipateWhere = {
  AND?: InputMaybe<Array<FactoidPersonParticipateWhere>>;
  NOT?: InputMaybe<FactoidPersonParticipateWhere>;
  OR?: InputMaybe<Array<FactoidPersonParticipateWhere>>;
  annotationsAggregate?: InputMaybe<FactoidPersonParticipateAnnotationsAggregateInput>;
  /** Return FactoidPersonParticipates where all of the related FactoidPersonParticipateAnnotationsConnections match this filter */
  annotationsConnection_ALL?: InputMaybe<FactoidPersonParticipateAnnotationsConnectionWhere>;
  /** Return FactoidPersonParticipates where none of the related FactoidPersonParticipateAnnotationsConnections match this filter */
  annotationsConnection_NONE?: InputMaybe<FactoidPersonParticipateAnnotationsConnectionWhere>;
  /** Return FactoidPersonParticipates where one of the related FactoidPersonParticipateAnnotationsConnections match this filter */
  annotationsConnection_SINGLE?: InputMaybe<FactoidPersonParticipateAnnotationsConnectionWhere>;
  /** Return FactoidPersonParticipates where some of the related FactoidPersonParticipateAnnotationsConnections match this filter */
  annotationsConnection_SOME?: InputMaybe<FactoidPersonParticipateAnnotationsConnectionWhere>;
  /** Return FactoidPersonParticipates where all of the related Annotations match this filter */
  annotations_ALL?: InputMaybe<AnnotationWhere>;
  /** Return FactoidPersonParticipates where none of the related Annotations match this filter */
  annotations_NONE?: InputMaybe<AnnotationWhere>;
  /** Return FactoidPersonParticipates where one of the related Annotations match this filter */
  annotations_SINGLE?: InputMaybe<AnnotationWhere>;
  /** Return FactoidPersonParticipates where some of the related Annotations match this filter */
  annotations_SOME?: InputMaybe<AnnotationWhere>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
  dataSourcesAggregate?: InputMaybe<FactoidPersonParticipateDataSourcesAggregateInput>;
  /** Return FactoidPersonParticipates where all of the related FactoidPersonParticipateDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<FactoidPersonParticipateDataSourcesConnectionWhere>;
  /** Return FactoidPersonParticipates where none of the related FactoidPersonParticipateDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<FactoidPersonParticipateDataSourcesConnectionWhere>;
  /** Return FactoidPersonParticipates where one of the related FactoidPersonParticipateDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<FactoidPersonParticipateDataSourcesConnectionWhere>;
  /** Return FactoidPersonParticipates where some of the related FactoidPersonParticipateDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<FactoidPersonParticipateDataSourcesConnectionWhere>;
  /** Return FactoidPersonParticipates where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return FactoidPersonParticipates where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return FactoidPersonParticipates where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return FactoidPersonParticipates where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  factoid?: InputMaybe<FactoidWhere>;
  factoidAggregate?: InputMaybe<FactoidPersonParticipateFactoidAggregateInput>;
  factoidConnection?: InputMaybe<FactoidPersonParticipateFactoidConnectionWhere>;
  factoidConnection_NOT?: InputMaybe<FactoidPersonParticipateFactoidConnectionWhere>;
  factoid_NOT?: InputMaybe<FactoidWhere>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  personConnection?: InputMaybe<FactoidPersonParticipatePersonConnectionWhere>;
  personConnection_NOT?: InputMaybe<FactoidPersonParticipatePersonConnectionWhere>;
  ranksAggregate?: InputMaybe<FactoidPersonParticipateRanksAggregateInput>;
  /** Return FactoidPersonParticipates where all of the related FactoidPersonParticipateRanksConnections match this filter */
  ranksConnection_ALL?: InputMaybe<FactoidPersonParticipateRanksConnectionWhere>;
  /** Return FactoidPersonParticipates where none of the related FactoidPersonParticipateRanksConnections match this filter */
  ranksConnection_NONE?: InputMaybe<FactoidPersonParticipateRanksConnectionWhere>;
  /** Return FactoidPersonParticipates where one of the related FactoidPersonParticipateRanksConnections match this filter */
  ranksConnection_SINGLE?: InputMaybe<FactoidPersonParticipateRanksConnectionWhere>;
  /** Return FactoidPersonParticipates where some of the related FactoidPersonParticipateRanksConnections match this filter */
  ranksConnection_SOME?: InputMaybe<FactoidPersonParticipateRanksConnectionWhere>;
  /** Return FactoidPersonParticipates where all of the related Ranks match this filter */
  ranks_ALL?: InputMaybe<RankWhere>;
  /** Return FactoidPersonParticipates where none of the related Ranks match this filter */
  ranks_NONE?: InputMaybe<RankWhere>;
  /** Return FactoidPersonParticipates where one of the related Ranks match this filter */
  ranks_SINGLE?: InputMaybe<RankWhere>;
  /** Return FactoidPersonParticipates where some of the related Ranks match this filter */
  ranks_SOME?: InputMaybe<RankWhere>;
  rolesAggregate?: InputMaybe<FactoidPersonParticipateRolesAggregateInput>;
  /** Return FactoidPersonParticipates where all of the related FactoidPersonParticipateRolesConnections match this filter */
  rolesConnection_ALL?: InputMaybe<FactoidPersonParticipateRolesConnectionWhere>;
  /** Return FactoidPersonParticipates where none of the related FactoidPersonParticipateRolesConnections match this filter */
  rolesConnection_NONE?: InputMaybe<FactoidPersonParticipateRolesConnectionWhere>;
  /** Return FactoidPersonParticipates where one of the related FactoidPersonParticipateRolesConnections match this filter */
  rolesConnection_SINGLE?: InputMaybe<FactoidPersonParticipateRolesConnectionWhere>;
  /** Return FactoidPersonParticipates where some of the related FactoidPersonParticipateRolesConnections match this filter */
  rolesConnection_SOME?: InputMaybe<FactoidPersonParticipateRolesConnectionWhere>;
  /** Return FactoidPersonParticipates where all of the related Roles match this filter */
  roles_ALL?: InputMaybe<RoleWhere>;
  /** Return FactoidPersonParticipates where none of the related Roles match this filter */
  roles_NONE?: InputMaybe<RoleWhere>;
  /** Return FactoidPersonParticipates where one of the related Roles match this filter */
  roles_SINGLE?: InputMaybe<RoleWhere>;
  /** Return FactoidPersonParticipates where some of the related Roles match this filter */
  roles_SOME?: InputMaybe<RoleWhere>;
};

export type FactoidPersonParticipatesConnection = {
  __typename?: 'FactoidPersonParticipatesConnection';
  edges: Array<FactoidPersonParticipateEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonsAggregateInput = {
  AND?: InputMaybe<Array<FactoidPersonsAggregateInput>>;
  NOT?: InputMaybe<FactoidPersonsAggregateInput>;
  OR?: InputMaybe<Array<FactoidPersonsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidPersonsNodeAggregationWhereInput>;
};

export type FactoidPersonsConnection = {
  __typename?: 'FactoidPersonsConnection';
  edges: Array<FactoidPersonsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPersonsConnectionSort = {
  node?: InputMaybe<FactoidPersonParticipateSort>;
};

export type FactoidPersonsConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPersonsConnectionWhere>>;
  NOT?: InputMaybe<FactoidPersonsConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPersonsConnectionWhere>>;
  node?: InputMaybe<FactoidPersonParticipateWhere>;
};

export type FactoidPersonsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPersonsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPersonsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPersonsNodeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type FactoidPersonsRelationship = {
  __typename?: 'FactoidPersonsRelationship';
  cursor: Scalars['String'];
  node: FactoidPersonParticipate;
};

export type FactoidPlacePlacesAggregationSelection = {
  __typename?: 'FactoidPlacePlacesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidPlacePlacesEdgeAggregateSelection>;
  node?: Maybe<FactoidPlacePlacesNodeAggregateSelection>;
};

export type FactoidPlacePlacesEdgeAggregateSelection = {
  __typename?: 'FactoidPlacePlacesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type FactoidPlacePlacesNodeAggregateSelection = {
  __typename?: 'FactoidPlacePlacesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidPlacesAggregateInput = {
  AND?: InputMaybe<Array<FactoidPlacesAggregateInput>>;
  NOT?: InputMaybe<FactoidPlacesAggregateInput>;
  OR?: InputMaybe<Array<FactoidPlacesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidPlacesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidPlacesNodeAggregationWhereInput>;
};

export type FactoidPlacesConnection = {
  __typename?: 'FactoidPlacesConnection';
  edges: Array<FactoidPlacesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidPlacesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<PlaceSort>;
};

export type FactoidPlacesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidPlacesConnectionWhere>>;
  NOT?: InputMaybe<FactoidPlacesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidPlacesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<PlaceWhere>;
};

export type FactoidPlacesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPlacesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPlacesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPlacesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type FactoidPlacesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidPlacesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidPlacesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidPlacesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidPlacesRelationship = Certainty & {
  __typename?: 'FactoidPlacesRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Place;
};

export type FactoidRefersToSource = {
  certainty?: Maybe<Scalars['Float']>;
  page?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type FactoidRefersToSourceSort = {
  certainty?: InputMaybe<SortDirection>;
  page?: InputMaybe<SortDirection>;
  permalink?: InputMaybe<SortDirection>;
};

export type FactoidRefersToSourceWhere = {
  AND?: InputMaybe<Array<FactoidRefersToSourceWhere>>;
  NOT?: InputMaybe<FactoidRefersToSourceWhere>;
  OR?: InputMaybe<Array<FactoidRefersToSourceWhere>>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
  page?: InputMaybe<Scalars['String']>;
  page_CONTAINS?: InputMaybe<Scalars['String']>;
  page_ENDS_WITH?: InputMaybe<Scalars['String']>;
  page_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  page_STARTS_WITH?: InputMaybe<Scalars['String']>;
  permalink?: InputMaybe<Scalars['String']>;
  permalink_CONTAINS?: InputMaybe<Scalars['String']>;
  permalink_ENDS_WITH?: InputMaybe<Scalars['String']>;
  permalink_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  permalink_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

/** Fields to sort Factoids by. The order in which sorts are applied is not guaranteed when specifying many fields in one FactoidSort object. */
export type FactoidSort = {
  certainty?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  duration?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  originalText?: InputMaybe<SortDirection>;
};

export type FactoidSourceSourcesAggregationSelection = {
  __typename?: 'FactoidSourceSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidSourceSourcesEdgeAggregateSelection>;
  node?: Maybe<FactoidSourceSourcesNodeAggregateSelection>;
};

export type FactoidSourceSourcesEdgeAggregateSelection = {
  __typename?: 'FactoidSourceSourcesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNullable;
  page: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type FactoidSourceSourcesNodeAggregateSelection = {
  __typename?: 'FactoidSourceSourcesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  language: StringAggregateSelectionNullable;
  name: StringAggregateSelectionNonNullable;
  reputation: FloatAggregateSelectionNullable;
};

export type FactoidSourcesAggregateInput = {
  AND?: InputMaybe<Array<FactoidSourcesAggregateInput>>;
  NOT?: InputMaybe<FactoidSourcesAggregateInput>;
  OR?: InputMaybe<Array<FactoidSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidSourcesNodeAggregationWhereInput>;
};

export type FactoidSourcesConnection = {
  __typename?: 'FactoidSourcesConnection';
  edges: Array<FactoidSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidSourcesConnectionSort = {
  edge?: InputMaybe<FactoidRefersToSourceSort>;
  node?: InputMaybe<SourceSort>;
};

export type FactoidSourcesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidSourcesConnectionWhere>>;
  NOT?: InputMaybe<FactoidSourcesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidSourcesConnectionWhere>>;
  edge?: InputMaybe<FactoidRefersToSourceWhere>;
  node?: InputMaybe<SourceWhere>;
};

export type FactoidSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidSourcesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  page_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidSourcesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  language_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  reputation_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type FactoidSourcesRelationship = FactoidRefersToSource & {
  __typename?: 'FactoidSourcesRelationship';
  certainty?: Maybe<Scalars['Float']>;
  cursor: Scalars['String'];
  node: Source;
  page?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidType = {
  __typename?: 'FactoidType';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<FactoidTypeDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: FactoidTypeDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  sources: Array<Source>;
  sourcesAggregate?: Maybe<FactoidTypeSourceSourcesAggregationSelection>;
  sourcesConnection: FactoidTypeSourcesConnection;
};


/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidTypeDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidTypeDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidTypeDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidTypeDataSourcesConnectionSort>>;
  where?: InputMaybe<FactoidTypeDataSourcesConnectionWhere>;
};


/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidTypeSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceOptions>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidTypeSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * Factoid Type
 * -------
 * This define the type of a factoid, like for example 'contributed to', 'teach at', ...
 */
export type FactoidTypeSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<FactoidTypeSourcesConnectionSort>>;
  where?: InputMaybe<FactoidTypeSourcesConnectionWhere>;
};

export type FactoidTypeAggregateSelection = {
  __typename?: 'FactoidTypeAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidTypeDataSourceDataSourcesAggregationSelection = {
  __typename?: 'FactoidTypeDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<FactoidTypeDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<FactoidTypeDataSourceDataSourcesNodeAggregateSelection>;
};

export type FactoidTypeDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'FactoidTypeDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type FactoidTypeDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'FactoidTypeDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type FactoidTypeDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<FactoidTypeDataSourcesAggregateInput>>;
  NOT?: InputMaybe<FactoidTypeDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<FactoidTypeDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidTypeDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidTypeDataSourcesNodeAggregationWhereInput>;
};

export type FactoidTypeDataSourcesConnection = {
  __typename?: 'FactoidTypeDataSourcesConnection';
  edges: Array<FactoidTypeDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidTypeDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type FactoidTypeDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidTypeDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<FactoidTypeDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidTypeDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type FactoidTypeDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidTypeDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidTypeDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidTypeDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidTypeDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidTypeDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidTypeDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidTypeDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidTypeDataSourcesRelationship = ImportedFrom & {
  __typename?: 'FactoidTypeDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type FactoidTypeEdge = {
  __typename?: 'FactoidTypeEdge';
  cursor: Scalars['String'];
  node: FactoidType;
};

export type FactoidTypeOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more FactoidTypeSort objects to sort FactoidTypes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<FactoidTypeSort>>;
};

/** Fields to sort FactoidTypes by. The order in which sorts are applied is not guaranteed when specifying many fields in one FactoidTypeSort object. */
export type FactoidTypeSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type FactoidTypeSourceSourcesAggregationSelection = {
  __typename?: 'FactoidTypeSourceSourcesAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<FactoidTypeSourceSourcesNodeAggregateSelection>;
};

export type FactoidTypeSourceSourcesNodeAggregateSelection = {
  __typename?: 'FactoidTypeSourceSourcesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  language: StringAggregateSelectionNullable;
  name: StringAggregateSelectionNonNullable;
  reputation: FloatAggregateSelectionNullable;
};

export type FactoidTypeSourcesAggregateInput = {
  AND?: InputMaybe<Array<FactoidTypeSourcesAggregateInput>>;
  NOT?: InputMaybe<FactoidTypeSourcesAggregateInput>;
  OR?: InputMaybe<Array<FactoidTypeSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<FactoidTypeSourcesNodeAggregationWhereInput>;
};

export type FactoidTypeSourcesConnection = {
  __typename?: 'FactoidTypeSourcesConnection';
  edges: Array<FactoidTypeSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidTypeSourcesConnectionSort = {
  node?: InputMaybe<SourceSort>;
};

export type FactoidTypeSourcesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidTypeSourcesConnectionWhere>>;
  NOT?: InputMaybe<FactoidTypeSourcesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidTypeSourcesConnectionWhere>>;
  node?: InputMaybe<SourceWhere>;
};

export type FactoidTypeSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidTypeSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidTypeSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidTypeSourcesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  language_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  reputation_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type FactoidTypeSourcesRelationship = {
  __typename?: 'FactoidTypeSourcesRelationship';
  cursor: Scalars['String'];
  node: Source;
};

export type FactoidTypeWhere = {
  AND?: InputMaybe<Array<FactoidTypeWhere>>;
  NOT?: InputMaybe<FactoidTypeWhere>;
  OR?: InputMaybe<Array<FactoidTypeWhere>>;
  dataSourcesAggregate?: InputMaybe<FactoidTypeDataSourcesAggregateInput>;
  /** Return FactoidTypes where all of the related FactoidTypeDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<FactoidTypeDataSourcesConnectionWhere>;
  /** Return FactoidTypes where none of the related FactoidTypeDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<FactoidTypeDataSourcesConnectionWhere>;
  /** Return FactoidTypes where one of the related FactoidTypeDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<FactoidTypeDataSourcesConnectionWhere>;
  /** Return FactoidTypes where some of the related FactoidTypeDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<FactoidTypeDataSourcesConnectionWhere>;
  /** Return FactoidTypes where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return FactoidTypes where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return FactoidTypes where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return FactoidTypes where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  sourcesAggregate?: InputMaybe<FactoidTypeSourcesAggregateInput>;
  /** Return FactoidTypes where all of the related FactoidTypeSourcesConnections match this filter */
  sourcesConnection_ALL?: InputMaybe<FactoidTypeSourcesConnectionWhere>;
  /** Return FactoidTypes where none of the related FactoidTypeSourcesConnections match this filter */
  sourcesConnection_NONE?: InputMaybe<FactoidTypeSourcesConnectionWhere>;
  /** Return FactoidTypes where one of the related FactoidTypeSourcesConnections match this filter */
  sourcesConnection_SINGLE?: InputMaybe<FactoidTypeSourcesConnectionWhere>;
  /** Return FactoidTypes where some of the related FactoidTypeSourcesConnections match this filter */
  sourcesConnection_SOME?: InputMaybe<FactoidTypeSourcesConnectionWhere>;
  /** Return FactoidTypes where all of the related Sources match this filter */
  sources_ALL?: InputMaybe<SourceWhere>;
  /** Return FactoidTypes where none of the related Sources match this filter */
  sources_NONE?: InputMaybe<SourceWhere>;
  /** Return FactoidTypes where one of the related Sources match this filter */
  sources_SINGLE?: InputMaybe<SourceWhere>;
  /** Return FactoidTypes where some of the related Sources match this filter */
  sources_SOME?: InputMaybe<SourceWhere>;
};

export type FactoidTypesAggregateInput = {
  AND?: InputMaybe<Array<FactoidTypesAggregateInput>>;
  NOT?: InputMaybe<FactoidTypesAggregateInput>;
  OR?: InputMaybe<Array<FactoidTypesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<FactoidTypesEdgeAggregationWhereInput>;
  node?: InputMaybe<FactoidTypesNodeAggregationWhereInput>;
};

export type FactoidTypesConnection = {
  __typename?: 'FactoidTypesConnection';
  edges: Array<FactoidTypeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type FactoidTypesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<FactoidTypeSort>;
};

export type FactoidTypesConnectionWhere = {
  AND?: InputMaybe<Array<FactoidTypesConnectionWhere>>;
  NOT?: InputMaybe<FactoidTypesConnectionWhere>;
  OR?: InputMaybe<Array<FactoidTypesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<FactoidTypeWhere>;
};

export type FactoidTypesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidTypesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidTypesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidTypesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type FactoidTypesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<FactoidTypesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<FactoidTypesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<FactoidTypesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type FactoidWhere = {
  AND?: InputMaybe<Array<FactoidWhere>>;
  NOT?: InputMaybe<FactoidWhere>;
  OR?: InputMaybe<Array<FactoidWhere>>;
  annotationsAggregate?: InputMaybe<FactoidAnnotationsAggregateInput>;
  /** Return Factoids where all of the related FactoidAnnotationsConnections match this filter */
  annotationsConnection_ALL?: InputMaybe<FactoidAnnotationsConnectionWhere>;
  /** Return Factoids where none of the related FactoidAnnotationsConnections match this filter */
  annotationsConnection_NONE?: InputMaybe<FactoidAnnotationsConnectionWhere>;
  /** Return Factoids where one of the related FactoidAnnotationsConnections match this filter */
  annotationsConnection_SINGLE?: InputMaybe<FactoidAnnotationsConnectionWhere>;
  /** Return Factoids where some of the related FactoidAnnotationsConnections match this filter */
  annotationsConnection_SOME?: InputMaybe<FactoidAnnotationsConnectionWhere>;
  /** Return Factoids where all of the related Annotations match this filter */
  annotations_ALL?: InputMaybe<AnnotationWhere>;
  /** Return Factoids where none of the related Annotations match this filter */
  annotations_NONE?: InputMaybe<AnnotationWhere>;
  /** Return Factoids where one of the related Annotations match this filter */
  annotations_SINGLE?: InputMaybe<AnnotationWhere>;
  /** Return Factoids where some of the related Annotations match this filter */
  annotations_SOME?: InputMaybe<AnnotationWhere>;
  dataSourcesAggregate?: InputMaybe<FactoidDataSourcesAggregateInput>;
  /** Return Factoids where all of the related FactoidDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<FactoidDataSourcesConnectionWhere>;
  /** Return Factoids where none of the related FactoidDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<FactoidDataSourcesConnectionWhere>;
  /** Return Factoids where one of the related FactoidDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<FactoidDataSourcesConnectionWhere>;
  /** Return Factoids where some of the related FactoidDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<FactoidDataSourcesConnectionWhere>;
  /** Return Factoids where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Factoids where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Factoids where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Factoids where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  datesAggregate?: InputMaybe<FactoidDatesAggregateInput>;
  /** Return Factoids where all of the related FactoidDatesConnections match this filter */
  datesConnection_ALL?: InputMaybe<FactoidDatesConnectionWhere>;
  /** Return Factoids where none of the related FactoidDatesConnections match this filter */
  datesConnection_NONE?: InputMaybe<FactoidDatesConnectionWhere>;
  /** Return Factoids where one of the related FactoidDatesConnections match this filter */
  datesConnection_SINGLE?: InputMaybe<FactoidDatesConnectionWhere>;
  /** Return Factoids where some of the related FactoidDatesConnections match this filter */
  datesConnection_SOME?: InputMaybe<FactoidDatesConnectionWhere>;
  /** Return Factoids where all of the related DateValues match this filter */
  dates_ALL?: InputMaybe<DateValueWhere>;
  /** Return Factoids where none of the related DateValues match this filter */
  dates_NONE?: InputMaybe<DateValueWhere>;
  /** Return Factoids where one of the related DateValues match this filter */
  dates_SINGLE?: InputMaybe<DateValueWhere>;
  /** Return Factoids where some of the related DateValues match this filter */
  dates_SOME?: InputMaybe<DateValueWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  duration?: InputMaybe<Scalars['Duration']>;
  duration_GT?: InputMaybe<Scalars['Duration']>;
  duration_GTE?: InputMaybe<Scalars['Duration']>;
  duration_IN?: InputMaybe<Array<InputMaybe<Scalars['Duration']>>>;
  duration_LT?: InputMaybe<Scalars['Duration']>;
  duration_LTE?: InputMaybe<Scalars['Duration']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  impactsAggregate?: InputMaybe<FactoidImpactsAggregateInput>;
  /** Return Factoids where all of the related FactoidImpactsConnections match this filter */
  impactsConnection_ALL?: InputMaybe<FactoidImpactsConnectionWhere>;
  /** Return Factoids where none of the related FactoidImpactsConnections match this filter */
  impactsConnection_NONE?: InputMaybe<FactoidImpactsConnectionWhere>;
  /** Return Factoids where one of the related FactoidImpactsConnections match this filter */
  impactsConnection_SINGLE?: InputMaybe<FactoidImpactsConnectionWhere>;
  /** Return Factoids where some of the related FactoidImpactsConnections match this filter */
  impactsConnection_SOME?: InputMaybe<FactoidImpactsConnectionWhere>;
  /** Return Factoids where all of the related Objects match this filter */
  impacts_ALL?: InputMaybe<ObjectWhere>;
  /** Return Factoids where none of the related Objects match this filter */
  impacts_NONE?: InputMaybe<ObjectWhere>;
  /** Return Factoids where one of the related Objects match this filter */
  impacts_SINGLE?: InputMaybe<ObjectWhere>;
  /** Return Factoids where some of the related Objects match this filter */
  impacts_SOME?: InputMaybe<ObjectWhere>;
  linkedToAggregate?: InputMaybe<FactoidLinkedToAggregateInput>;
  /** Return Factoids where all of the related FactoidLinkedToConnections match this filter */
  linkedToConnection_ALL?: InputMaybe<FactoidLinkedToConnectionWhere>;
  /** Return Factoids where none of the related FactoidLinkedToConnections match this filter */
  linkedToConnection_NONE?: InputMaybe<FactoidLinkedToConnectionWhere>;
  /** Return Factoids where one of the related FactoidLinkedToConnections match this filter */
  linkedToConnection_SINGLE?: InputMaybe<FactoidLinkedToConnectionWhere>;
  /** Return Factoids where some of the related FactoidLinkedToConnections match this filter */
  linkedToConnection_SOME?: InputMaybe<FactoidLinkedToConnectionWhere>;
  /** Return Factoids where all of the related Factoids match this filter */
  linkedTo_ALL?: InputMaybe<FactoidWhere>;
  /** Return Factoids where none of the related Factoids match this filter */
  linkedTo_NONE?: InputMaybe<FactoidWhere>;
  /** Return Factoids where one of the related Factoids match this filter */
  linkedTo_SINGLE?: InputMaybe<FactoidWhere>;
  /** Return Factoids where some of the related Factoids match this filter */
  linkedTo_SOME?: InputMaybe<FactoidWhere>;
  originalText?: InputMaybe<Scalars['String']>;
  originalText_CONTAINS?: InputMaybe<Scalars['String']>;
  originalText_ENDS_WITH?: InputMaybe<Scalars['String']>;
  originalText_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  originalText_STARTS_WITH?: InputMaybe<Scalars['String']>;
  personsAggregate?: InputMaybe<FactoidPersonsAggregateInput>;
  /** Return Factoids where all of the related FactoidPersonsConnections match this filter */
  personsConnection_ALL?: InputMaybe<FactoidPersonsConnectionWhere>;
  /** Return Factoids where none of the related FactoidPersonsConnections match this filter */
  personsConnection_NONE?: InputMaybe<FactoidPersonsConnectionWhere>;
  /** Return Factoids where one of the related FactoidPersonsConnections match this filter */
  personsConnection_SINGLE?: InputMaybe<FactoidPersonsConnectionWhere>;
  /** Return Factoids where some of the related FactoidPersonsConnections match this filter */
  personsConnection_SOME?: InputMaybe<FactoidPersonsConnectionWhere>;
  /** Return Factoids where all of the related FactoidPersonParticipates match this filter */
  persons_ALL?: InputMaybe<FactoidPersonParticipateWhere>;
  /** Return Factoids where none of the related FactoidPersonParticipates match this filter */
  persons_NONE?: InputMaybe<FactoidPersonParticipateWhere>;
  /** Return Factoids where one of the related FactoidPersonParticipates match this filter */
  persons_SINGLE?: InputMaybe<FactoidPersonParticipateWhere>;
  /** Return Factoids where some of the related FactoidPersonParticipates match this filter */
  persons_SOME?: InputMaybe<FactoidPersonParticipateWhere>;
  placesAggregate?: InputMaybe<FactoidPlacesAggregateInput>;
  /** Return Factoids where all of the related FactoidPlacesConnections match this filter */
  placesConnection_ALL?: InputMaybe<FactoidPlacesConnectionWhere>;
  /** Return Factoids where none of the related FactoidPlacesConnections match this filter */
  placesConnection_NONE?: InputMaybe<FactoidPlacesConnectionWhere>;
  /** Return Factoids where one of the related FactoidPlacesConnections match this filter */
  placesConnection_SINGLE?: InputMaybe<FactoidPlacesConnectionWhere>;
  /** Return Factoids where some of the related FactoidPlacesConnections match this filter */
  placesConnection_SOME?: InputMaybe<FactoidPlacesConnectionWhere>;
  /** Return Factoids where all of the related Places match this filter */
  places_ALL?: InputMaybe<PlaceWhere>;
  /** Return Factoids where none of the related Places match this filter */
  places_NONE?: InputMaybe<PlaceWhere>;
  /** Return Factoids where one of the related Places match this filter */
  places_SINGLE?: InputMaybe<PlaceWhere>;
  /** Return Factoids where some of the related Places match this filter */
  places_SOME?: InputMaybe<PlaceWhere>;
  sourcesAggregate?: InputMaybe<FactoidSourcesAggregateInput>;
  /** Return Factoids where all of the related FactoidSourcesConnections match this filter */
  sourcesConnection_ALL?: InputMaybe<FactoidSourcesConnectionWhere>;
  /** Return Factoids where none of the related FactoidSourcesConnections match this filter */
  sourcesConnection_NONE?: InputMaybe<FactoidSourcesConnectionWhere>;
  /** Return Factoids where one of the related FactoidSourcesConnections match this filter */
  sourcesConnection_SINGLE?: InputMaybe<FactoidSourcesConnectionWhere>;
  /** Return Factoids where some of the related FactoidSourcesConnections match this filter */
  sourcesConnection_SOME?: InputMaybe<FactoidSourcesConnectionWhere>;
  /** Return Factoids where all of the related Sources match this filter */
  sources_ALL?: InputMaybe<SourceWhere>;
  /** Return Factoids where none of the related Sources match this filter */
  sources_NONE?: InputMaybe<SourceWhere>;
  /** Return Factoids where one of the related Sources match this filter */
  sources_SINGLE?: InputMaybe<SourceWhere>;
  /** Return Factoids where some of the related Sources match this filter */
  sources_SOME?: InputMaybe<SourceWhere>;
  typesAggregate?: InputMaybe<FactoidTypesAggregateInput>;
  /** Return Factoids where all of the related FactoidTypesConnections match this filter */
  typesConnection_ALL?: InputMaybe<FactoidTypesConnectionWhere>;
  /** Return Factoids where none of the related FactoidTypesConnections match this filter */
  typesConnection_NONE?: InputMaybe<FactoidTypesConnectionWhere>;
  /** Return Factoids where one of the related FactoidTypesConnections match this filter */
  typesConnection_SINGLE?: InputMaybe<FactoidTypesConnectionWhere>;
  /** Return Factoids where some of the related FactoidTypesConnections match this filter */
  typesConnection_SOME?: InputMaybe<FactoidTypesConnectionWhere>;
  /** Return Factoids where all of the related FactoidTypes match this filter */
  types_ALL?: InputMaybe<FactoidTypeWhere>;
  /** Return Factoids where none of the related FactoidTypes match this filter */
  types_NONE?: InputMaybe<FactoidTypeWhere>;
  /** Return Factoids where one of the related FactoidTypes match this filter */
  types_SINGLE?: InputMaybe<FactoidTypeWhere>;
  /** Return Factoids where some of the related FactoidTypes match this filter */
  types_SOME?: InputMaybe<FactoidTypeWhere>;
};

export type FactoidsConnection = {
  __typename?: 'FactoidsConnection';
  edges: Array<FactoidEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export enum FilterType {
  Range = 'range',
  Terms = 'terms'
}

export type FloatAggregateSelectionNonNullable = {
  __typename?: 'FloatAggregateSelectionNonNullable';
  average: Scalars['Float'];
  max: Scalars['Float'];
  min: Scalars['Float'];
  sum: Scalars['Float'];
};

export type FloatAggregateSelectionNullable = {
  __typename?: 'FloatAggregateSelectionNullable';
  average?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Float']>;
  min?: Maybe<Scalars['Float']>;
  sum?: Maybe<Scalars['Float']>;
};

export type GraphSearchHit = Factoid | MoralEntity | PhysicalPerson | Place;

export type GraphSearchResult = {
  __typename?: 'GraphSearchResult';
  results: Array<GraphSearchHit>;
  total: Scalars['Int'];
};

/**
 * Group
 * -----
 */
export type Group = {
  __typename?: 'Group';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<GroupDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: GroupDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  partOf: Array<Group>;
  partOfAggregate?: Maybe<GroupGroupPartOfAggregationSelection>;
  partOfConnection: GroupPartOfConnection;
};


/**
 * Group
 * -----
 */
export type GroupDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Group
 * -----
 */
export type GroupDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Group
 * -----
 */
export type GroupDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<GroupDataSourcesConnectionSort>>;
  where?: InputMaybe<GroupDataSourcesConnectionWhere>;
};


/**
 * Group
 * -----
 */
export type GroupPartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<GroupOptions>;
  where?: InputMaybe<GroupWhere>;
};


/**
 * Group
 * -----
 */
export type GroupPartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<GroupWhere>;
};


/**
 * Group
 * -----
 */
export type GroupPartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<GroupPartOfConnectionSort>>;
  where?: InputMaybe<GroupPartOfConnectionWhere>;
};

export type GroupAggregateSelection = {
  __typename?: 'GroupAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type GroupDataSourceDataSourcesAggregationSelection = {
  __typename?: 'GroupDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<GroupDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<GroupDataSourceDataSourcesNodeAggregateSelection>;
};

export type GroupDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'GroupDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type GroupDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'GroupDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type GroupDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<GroupDataSourcesAggregateInput>>;
  NOT?: InputMaybe<GroupDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<GroupDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<GroupDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<GroupDataSourcesNodeAggregationWhereInput>;
};

export type GroupDataSourcesConnection = {
  __typename?: 'GroupDataSourcesConnection';
  edges: Array<GroupDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type GroupDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type GroupDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<GroupDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<GroupDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<GroupDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type GroupDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<GroupDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<GroupDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<GroupDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type GroupDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GroupDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GroupDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GroupDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type GroupDataSourcesRelationship = ImportedFrom & {
  __typename?: 'GroupDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type GroupEdge = {
  __typename?: 'GroupEdge';
  cursor: Scalars['String'];
  node: Group;
};

export type GroupGroupPartOfAggregationSelection = {
  __typename?: 'GroupGroupPartOfAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<GroupGroupPartOfEdgeAggregateSelection>;
  node?: Maybe<GroupGroupPartOfNodeAggregateSelection>;
};

export type GroupGroupPartOfEdgeAggregateSelection = {
  __typename?: 'GroupGroupPartOfEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type GroupGroupPartOfNodeAggregateSelection = {
  __typename?: 'GroupGroupPartOfNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type GroupOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more GroupSort objects to sort Groups by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<GroupSort>>;
};

export type GroupPartOfAggregateInput = {
  AND?: InputMaybe<Array<GroupPartOfAggregateInput>>;
  NOT?: InputMaybe<GroupPartOfAggregateInput>;
  OR?: InputMaybe<Array<GroupPartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<GroupPartOfEdgeAggregationWhereInput>;
  node?: InputMaybe<GroupPartOfNodeAggregationWhereInput>;
};

export type GroupPartOfConnection = {
  __typename?: 'GroupPartOfConnection';
  edges: Array<GroupPartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type GroupPartOfConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<GroupSort>;
};

export type GroupPartOfConnectionWhere = {
  AND?: InputMaybe<Array<GroupPartOfConnectionWhere>>;
  NOT?: InputMaybe<GroupPartOfConnectionWhere>;
  OR?: InputMaybe<Array<GroupPartOfConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<GroupWhere>;
};

export type GroupPartOfEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<GroupPartOfEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<GroupPartOfEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<GroupPartOfEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type GroupPartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GroupPartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GroupPartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GroupPartOfNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type GroupPartOfRelationship = Certainty & {
  __typename?: 'GroupPartOfRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Group;
};

/** Fields to sort Groups by. The order in which sorts are applied is not guaranteed when specifying many fields in one GroupSort object. */
export type GroupSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type GroupWhere = {
  AND?: InputMaybe<Array<GroupWhere>>;
  NOT?: InputMaybe<GroupWhere>;
  OR?: InputMaybe<Array<GroupWhere>>;
  dataSourcesAggregate?: InputMaybe<GroupDataSourcesAggregateInput>;
  /** Return Groups where all of the related GroupDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<GroupDataSourcesConnectionWhere>;
  /** Return Groups where none of the related GroupDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<GroupDataSourcesConnectionWhere>;
  /** Return Groups where one of the related GroupDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<GroupDataSourcesConnectionWhere>;
  /** Return Groups where some of the related GroupDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<GroupDataSourcesConnectionWhere>;
  /** Return Groups where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Groups where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Groups where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Groups where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  partOfAggregate?: InputMaybe<GroupPartOfAggregateInput>;
  /** Return Groups where all of the related GroupPartOfConnections match this filter */
  partOfConnection_ALL?: InputMaybe<GroupPartOfConnectionWhere>;
  /** Return Groups where none of the related GroupPartOfConnections match this filter */
  partOfConnection_NONE?: InputMaybe<GroupPartOfConnectionWhere>;
  /** Return Groups where one of the related GroupPartOfConnections match this filter */
  partOfConnection_SINGLE?: InputMaybe<GroupPartOfConnectionWhere>;
  /** Return Groups where some of the related GroupPartOfConnections match this filter */
  partOfConnection_SOME?: InputMaybe<GroupPartOfConnectionWhere>;
  /** Return Groups where all of the related Groups match this filter */
  partOf_ALL?: InputMaybe<GroupWhere>;
  /** Return Groups where none of the related Groups match this filter */
  partOf_NONE?: InputMaybe<GroupWhere>;
  /** Return Groups where one of the related Groups match this filter */
  partOf_SINGLE?: InputMaybe<GroupWhere>;
  /** Return Groups where some of the related Groups match this filter */
  partOf_SOME?: InputMaybe<GroupWhere>;
};

export type GroupsConnection = {
  __typename?: 'GroupsConnection';
  edges: Array<GroupEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type IdAggregateSelectionNonNullable = {
  __typename?: 'IDAggregateSelectionNonNullable';
  longest: Scalars['ID'];
  shortest: Scalars['ID'];
};

export type ImportedFrom = {
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type ImportedFromSort = {
  originalId?: InputMaybe<SortDirection>;
  permalink?: InputMaybe<SortDirection>;
};

export type ImportedFromWhere = {
  AND?: InputMaybe<Array<ImportedFromWhere>>;
  NOT?: InputMaybe<ImportedFromWhere>;
  OR?: InputMaybe<Array<ImportedFromWhere>>;
  originalId?: InputMaybe<Scalars['String']>;
  originalId_CONTAINS?: InputMaybe<Scalars['String']>;
  originalId_ENDS_WITH?: InputMaybe<Scalars['String']>;
  originalId_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  originalId_STARTS_WITH?: InputMaybe<Scalars['String']>;
  permalink?: InputMaybe<Scalars['String']>;
  permalink_CONTAINS?: InputMaybe<Scalars['String']>;
  permalink_ENDS_WITH?: InputMaybe<Scalars['String']>;
  permalink_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  permalink_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type InquiriesConnection = {
  __typename?: 'InquiriesConnection';
  edges: Array<InquiryEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type Inquiry = {
  __typename?: 'Inquiry';
  annotations: Array<Annotation>;
  annotationsAggregate?: Maybe<InquiryAnnotationAnnotationsAggregationSelection>;
  annotationsConnection: InquiryAnnotationsConnection;
  createdAt: Scalars['DateTime'];
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<InquiryDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: InquiryDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};


/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type InquiryAnnotationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type InquiryAnnotationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type InquiryAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InquiryAnnotationsConnectionSort>>;
  where?: InputMaybe<InquiryAnnotationsConnectionWhere>;
};


/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type InquiryDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type InquiryDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Inquiry
 * -------
 * Daphné is used to create inquiries on data sources
 */
export type InquiryDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InquiryDataSourcesConnectionSort>>;
  where?: InputMaybe<InquiryDataSourcesConnectionWhere>;
};

export type InquiryAggregateSelection = {
  __typename?: 'InquiryAggregateSelection';
  count: Scalars['Int'];
  createdAt: DateTimeAggregateSelectionNonNullable;
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type InquiryAnnotationAnnotationsAggregationSelection = {
  __typename?: 'InquiryAnnotationAnnotationsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<InquiryAnnotationAnnotationsNodeAggregateSelection>;
};

export type InquiryAnnotationAnnotationsNodeAggregateSelection = {
  __typename?: 'InquiryAnnotationAnnotationsNodeAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type InquiryAnnotationsAggregateInput = {
  AND?: InputMaybe<Array<InquiryAnnotationsAggregateInput>>;
  NOT?: InputMaybe<InquiryAnnotationsAggregateInput>;
  OR?: InputMaybe<Array<InquiryAnnotationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<InquiryAnnotationsNodeAggregationWhereInput>;
};

export type InquiryAnnotationsConnection = {
  __typename?: 'InquiryAnnotationsConnection';
  edges: Array<InquiryAnnotationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type InquiryAnnotationsConnectionSort = {
  node?: InputMaybe<AnnotationSort>;
};

export type InquiryAnnotationsConnectionWhere = {
  AND?: InputMaybe<Array<InquiryAnnotationsConnectionWhere>>;
  NOT?: InputMaybe<InquiryAnnotationsConnectionWhere>;
  OR?: InputMaybe<Array<InquiryAnnotationsConnectionWhere>>;
  node?: InputMaybe<AnnotationWhere>;
};

export type InquiryAnnotationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<InquiryAnnotationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<InquiryAnnotationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<InquiryAnnotationsNodeAggregationWhereInput>>;
  author_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  author_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  comment_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  createdAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type InquiryAnnotationsRelationship = {
  __typename?: 'InquiryAnnotationsRelationship';
  cursor: Scalars['String'];
  node: Annotation;
};

export type InquiryDataSourceDataSourcesAggregationSelection = {
  __typename?: 'InquiryDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<InquiryDataSourceDataSourcesNodeAggregateSelection>;
};

export type InquiryDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'InquiryDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type InquiryDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<InquiryDataSourcesAggregateInput>>;
  NOT?: InputMaybe<InquiryDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<InquiryDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<InquiryDataSourcesNodeAggregationWhereInput>;
};

export type InquiryDataSourcesConnection = {
  __typename?: 'InquiryDataSourcesConnection';
  edges: Array<InquiryDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type InquiryDataSourcesConnectionSort = {
  node?: InputMaybe<DataSourceSort>;
};

export type InquiryDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<InquiryDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<InquiryDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<InquiryDataSourcesConnectionWhere>>;
  node?: InputMaybe<DataSourceWhere>;
};

export type InquiryDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<InquiryDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<InquiryDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<InquiryDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type InquiryDataSourcesRelationship = {
  __typename?: 'InquiryDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
};

export type InquiryEdge = {
  __typename?: 'InquiryEdge';
  cursor: Scalars['String'];
  node: Inquiry;
};

export type InquiryOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more InquirySort objects to sort Inquiries by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<InquirySort>>;
};

/** Fields to sort Inquiries by. The order in which sorts are applied is not guaranteed when specifying many fields in one InquirySort object. */
export type InquirySort = {
  createdAt?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type InquiryWhere = {
  AND?: InputMaybe<Array<InquiryWhere>>;
  NOT?: InputMaybe<InquiryWhere>;
  OR?: InputMaybe<Array<InquiryWhere>>;
  annotationsAggregate?: InputMaybe<InquiryAnnotationsAggregateInput>;
  /** Return Inquiries where all of the related InquiryAnnotationsConnections match this filter */
  annotationsConnection_ALL?: InputMaybe<InquiryAnnotationsConnectionWhere>;
  /** Return Inquiries where none of the related InquiryAnnotationsConnections match this filter */
  annotationsConnection_NONE?: InputMaybe<InquiryAnnotationsConnectionWhere>;
  /** Return Inquiries where one of the related InquiryAnnotationsConnections match this filter */
  annotationsConnection_SINGLE?: InputMaybe<InquiryAnnotationsConnectionWhere>;
  /** Return Inquiries where some of the related InquiryAnnotationsConnections match this filter */
  annotationsConnection_SOME?: InputMaybe<InquiryAnnotationsConnectionWhere>;
  /** Return Inquiries where all of the related Annotations match this filter */
  annotations_ALL?: InputMaybe<AnnotationWhere>;
  /** Return Inquiries where none of the related Annotations match this filter */
  annotations_NONE?: InputMaybe<AnnotationWhere>;
  /** Return Inquiries where one of the related Annotations match this filter */
  annotations_SINGLE?: InputMaybe<AnnotationWhere>;
  /** Return Inquiries where some of the related Annotations match this filter */
  annotations_SOME?: InputMaybe<AnnotationWhere>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  createdAt_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_IN?: InputMaybe<Array<Scalars['DateTime']>>;
  createdAt_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_LTE?: InputMaybe<Scalars['DateTime']>;
  dataSourcesAggregate?: InputMaybe<InquiryDataSourcesAggregateInput>;
  /** Return Inquiries where all of the related InquiryDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<InquiryDataSourcesConnectionWhere>;
  /** Return Inquiries where none of the related InquiryDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<InquiryDataSourcesConnectionWhere>;
  /** Return Inquiries where one of the related InquiryDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<InquiryDataSourcesConnectionWhere>;
  /** Return Inquiries where some of the related InquiryDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<InquiryDataSourcesConnectionWhere>;
  /** Return Inquiries where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Inquiries where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Inquiries where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Inquiries where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type IntAggregateSelectionNullable = {
  __typename?: 'IntAggregateSelectionNullable';
  average?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Int']>;
  min?: Maybe<Scalars['Int']>;
  sum?: Maybe<Scalars['Int']>;
};

export type MoralEntitiesConnection = {
  __typename?: 'MoralEntitiesConnection';
  edges: Array<MoralEntityEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/**
 * MoralEntity
 * -----------
 */
export type MoralEntity = {
  __typename?: 'MoralEntity';
  annotations: Array<Annotation>;
  annotationsAggregate?: Maybe<MoralEntityAnnotationAnnotationsAggregationSelection>;
  annotationsConnection: MoralEntityAnnotationsConnection;
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<MoralEntityDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: MoralEntityDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  domains: Array<Domain>;
  domainsAggregate?: Maybe<MoralEntityDomainDomainsAggregationSelection>;
  domainsConnection: MoralEntityDomainsConnection;
  id: Scalars['ID'];
  inquiry: Inquiry;
  name: Scalars['String'];
  partOf: Array<MoralEntity>;
  partOfAggregate?: Maybe<MoralEntityMoralEntityPartOfAggregationSelection>;
  partOfConnection: MoralEntityPartOfConnection;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityAnnotationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityAnnotationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<MoralEntityAnnotationsConnectionSort>>;
  where?: InputMaybe<MoralEntityAnnotationsConnectionWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<MoralEntityDataSourcesConnectionSort>>;
  where?: InputMaybe<MoralEntityDataSourcesConnectionWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityDomainsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DomainOptions>;
  where?: InputMaybe<DomainWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityDomainsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DomainWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityDomainsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<MoralEntityDomainsConnectionSort>>;
  where?: InputMaybe<MoralEntityDomainsConnectionWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityPartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<MoralEntityOptions>;
  where?: InputMaybe<MoralEntityWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityPartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<MoralEntityWhere>;
};


/**
 * MoralEntity
 * -----------
 */
export type MoralEntityPartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<MoralEntityPartOfConnectionSort>>;
  where?: InputMaybe<MoralEntityPartOfConnectionWhere>;
};

export type MoralEntityAggregateSelection = {
  __typename?: 'MoralEntityAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type MoralEntityAnnotationAnnotationsAggregationSelection = {
  __typename?: 'MoralEntityAnnotationAnnotationsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<MoralEntityAnnotationAnnotationsNodeAggregateSelection>;
};

export type MoralEntityAnnotationAnnotationsNodeAggregateSelection = {
  __typename?: 'MoralEntityAnnotationAnnotationsNodeAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type MoralEntityAnnotationsAggregateInput = {
  AND?: InputMaybe<Array<MoralEntityAnnotationsAggregateInput>>;
  NOT?: InputMaybe<MoralEntityAnnotationsAggregateInput>;
  OR?: InputMaybe<Array<MoralEntityAnnotationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<MoralEntityAnnotationsNodeAggregationWhereInput>;
};

export type MoralEntityAnnotationsConnection = {
  __typename?: 'MoralEntityAnnotationsConnection';
  edges: Array<MoralEntityAnnotationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type MoralEntityAnnotationsConnectionSort = {
  node?: InputMaybe<AnnotationSort>;
};

export type MoralEntityAnnotationsConnectionWhere = {
  AND?: InputMaybe<Array<MoralEntityAnnotationsConnectionWhere>>;
  NOT?: InputMaybe<MoralEntityAnnotationsConnectionWhere>;
  OR?: InputMaybe<Array<MoralEntityAnnotationsConnectionWhere>>;
  node?: InputMaybe<AnnotationWhere>;
};

export type MoralEntityAnnotationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityAnnotationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityAnnotationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityAnnotationsNodeAggregationWhereInput>>;
  author_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  author_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  comment_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  createdAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type MoralEntityAnnotationsRelationship = {
  __typename?: 'MoralEntityAnnotationsRelationship';
  cursor: Scalars['String'];
  node: Annotation;
};

export type MoralEntityDataSourceDataSourcesAggregationSelection = {
  __typename?: 'MoralEntityDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<MoralEntityDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<MoralEntityDataSourceDataSourcesNodeAggregateSelection>;
};

export type MoralEntityDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'MoralEntityDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type MoralEntityDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'MoralEntityDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type MoralEntityDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<MoralEntityDataSourcesAggregateInput>>;
  NOT?: InputMaybe<MoralEntityDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<MoralEntityDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<MoralEntityDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<MoralEntityDataSourcesNodeAggregationWhereInput>;
};

export type MoralEntityDataSourcesConnection = {
  __typename?: 'MoralEntityDataSourcesConnection';
  edges: Array<MoralEntityDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type MoralEntityDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type MoralEntityDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<MoralEntityDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<MoralEntityDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<MoralEntityDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type MoralEntityDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type MoralEntityDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type MoralEntityDataSourcesRelationship = ImportedFrom & {
  __typename?: 'MoralEntityDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type MoralEntityDomainDomainsAggregationSelection = {
  __typename?: 'MoralEntityDomainDomainsAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<MoralEntityDomainDomainsEdgeAggregateSelection>;
  node?: Maybe<MoralEntityDomainDomainsNodeAggregateSelection>;
};

export type MoralEntityDomainDomainsEdgeAggregateSelection = {
  __typename?: 'MoralEntityDomainDomainsEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type MoralEntityDomainDomainsNodeAggregateSelection = {
  __typename?: 'MoralEntityDomainDomainsNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type MoralEntityDomainsAggregateInput = {
  AND?: InputMaybe<Array<MoralEntityDomainsAggregateInput>>;
  NOT?: InputMaybe<MoralEntityDomainsAggregateInput>;
  OR?: InputMaybe<Array<MoralEntityDomainsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<MoralEntityDomainsEdgeAggregationWhereInput>;
  node?: InputMaybe<MoralEntityDomainsNodeAggregationWhereInput>;
};

export type MoralEntityDomainsConnection = {
  __typename?: 'MoralEntityDomainsConnection';
  edges: Array<MoralEntityDomainsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type MoralEntityDomainsConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<DomainSort>;
};

export type MoralEntityDomainsConnectionWhere = {
  AND?: InputMaybe<Array<MoralEntityDomainsConnectionWhere>>;
  NOT?: InputMaybe<MoralEntityDomainsConnectionWhere>;
  OR?: InputMaybe<Array<MoralEntityDomainsConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<DomainWhere>;
};

export type MoralEntityDomainsEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityDomainsEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityDomainsEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityDomainsEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type MoralEntityDomainsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityDomainsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityDomainsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityDomainsNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type MoralEntityDomainsRelationship = Certainty & {
  __typename?: 'MoralEntityDomainsRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Domain;
};

export type MoralEntityEdge = {
  __typename?: 'MoralEntityEdge';
  cursor: Scalars['String'];
  node: MoralEntity;
};

export type MoralEntityMoralEntityPartOfAggregationSelection = {
  __typename?: 'MoralEntityMoralEntityPartOfAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<MoralEntityMoralEntityPartOfEdgeAggregateSelection>;
  node?: Maybe<MoralEntityMoralEntityPartOfNodeAggregateSelection>;
};

export type MoralEntityMoralEntityPartOfEdgeAggregateSelection = {
  __typename?: 'MoralEntityMoralEntityPartOfEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type MoralEntityMoralEntityPartOfNodeAggregateSelection = {
  __typename?: 'MoralEntityMoralEntityPartOfNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type MoralEntityOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more MoralEntitySort objects to sort MoralEntities by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<MoralEntitySort>>;
};

export type MoralEntityPartOfAggregateInput = {
  AND?: InputMaybe<Array<MoralEntityPartOfAggregateInput>>;
  NOT?: InputMaybe<MoralEntityPartOfAggregateInput>;
  OR?: InputMaybe<Array<MoralEntityPartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<MoralEntityPartOfEdgeAggregationWhereInput>;
  node?: InputMaybe<MoralEntityPartOfNodeAggregationWhereInput>;
};

export type MoralEntityPartOfConnection = {
  __typename?: 'MoralEntityPartOfConnection';
  edges: Array<MoralEntityPartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type MoralEntityPartOfConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<MoralEntitySort>;
};

export type MoralEntityPartOfConnectionWhere = {
  AND?: InputMaybe<Array<MoralEntityPartOfConnectionWhere>>;
  NOT?: InputMaybe<MoralEntityPartOfConnectionWhere>;
  OR?: InputMaybe<Array<MoralEntityPartOfConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<MoralEntityWhere>;
};

export type MoralEntityPartOfEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityPartOfEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityPartOfEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityPartOfEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type MoralEntityPartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<MoralEntityPartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<MoralEntityPartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<MoralEntityPartOfNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type MoralEntityPartOfRelationship = Certainty & {
  __typename?: 'MoralEntityPartOfRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: MoralEntity;
};

/** Fields to sort MoralEntities by. The order in which sorts are applied is not guaranteed when specifying many fields in one MoralEntitySort object. */
export type MoralEntitySort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type MoralEntityWhere = {
  AND?: InputMaybe<Array<MoralEntityWhere>>;
  NOT?: InputMaybe<MoralEntityWhere>;
  OR?: InputMaybe<Array<MoralEntityWhere>>;
  annotationsAggregate?: InputMaybe<MoralEntityAnnotationsAggregateInput>;
  /** Return MoralEntities where all of the related MoralEntityAnnotationsConnections match this filter */
  annotationsConnection_ALL?: InputMaybe<MoralEntityAnnotationsConnectionWhere>;
  /** Return MoralEntities where none of the related MoralEntityAnnotationsConnections match this filter */
  annotationsConnection_NONE?: InputMaybe<MoralEntityAnnotationsConnectionWhere>;
  /** Return MoralEntities where one of the related MoralEntityAnnotationsConnections match this filter */
  annotationsConnection_SINGLE?: InputMaybe<MoralEntityAnnotationsConnectionWhere>;
  /** Return MoralEntities where some of the related MoralEntityAnnotationsConnections match this filter */
  annotationsConnection_SOME?: InputMaybe<MoralEntityAnnotationsConnectionWhere>;
  /** Return MoralEntities where all of the related Annotations match this filter */
  annotations_ALL?: InputMaybe<AnnotationWhere>;
  /** Return MoralEntities where none of the related Annotations match this filter */
  annotations_NONE?: InputMaybe<AnnotationWhere>;
  /** Return MoralEntities where one of the related Annotations match this filter */
  annotations_SINGLE?: InputMaybe<AnnotationWhere>;
  /** Return MoralEntities where some of the related Annotations match this filter */
  annotations_SOME?: InputMaybe<AnnotationWhere>;
  dataSourcesAggregate?: InputMaybe<MoralEntityDataSourcesAggregateInput>;
  /** Return MoralEntities where all of the related MoralEntityDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<MoralEntityDataSourcesConnectionWhere>;
  /** Return MoralEntities where none of the related MoralEntityDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<MoralEntityDataSourcesConnectionWhere>;
  /** Return MoralEntities where one of the related MoralEntityDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<MoralEntityDataSourcesConnectionWhere>;
  /** Return MoralEntities where some of the related MoralEntityDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<MoralEntityDataSourcesConnectionWhere>;
  /** Return MoralEntities where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return MoralEntities where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return MoralEntities where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return MoralEntities where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  domainsAggregate?: InputMaybe<MoralEntityDomainsAggregateInput>;
  /** Return MoralEntities where all of the related MoralEntityDomainsConnections match this filter */
  domainsConnection_ALL?: InputMaybe<MoralEntityDomainsConnectionWhere>;
  /** Return MoralEntities where none of the related MoralEntityDomainsConnections match this filter */
  domainsConnection_NONE?: InputMaybe<MoralEntityDomainsConnectionWhere>;
  /** Return MoralEntities where one of the related MoralEntityDomainsConnections match this filter */
  domainsConnection_SINGLE?: InputMaybe<MoralEntityDomainsConnectionWhere>;
  /** Return MoralEntities where some of the related MoralEntityDomainsConnections match this filter */
  domainsConnection_SOME?: InputMaybe<MoralEntityDomainsConnectionWhere>;
  /** Return MoralEntities where all of the related Domains match this filter */
  domains_ALL?: InputMaybe<DomainWhere>;
  /** Return MoralEntities where none of the related Domains match this filter */
  domains_NONE?: InputMaybe<DomainWhere>;
  /** Return MoralEntities where one of the related Domains match this filter */
  domains_SINGLE?: InputMaybe<DomainWhere>;
  /** Return MoralEntities where some of the related Domains match this filter */
  domains_SOME?: InputMaybe<DomainWhere>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  partOfAggregate?: InputMaybe<MoralEntityPartOfAggregateInput>;
  /** Return MoralEntities where all of the related MoralEntityPartOfConnections match this filter */
  partOfConnection_ALL?: InputMaybe<MoralEntityPartOfConnectionWhere>;
  /** Return MoralEntities where none of the related MoralEntityPartOfConnections match this filter */
  partOfConnection_NONE?: InputMaybe<MoralEntityPartOfConnectionWhere>;
  /** Return MoralEntities where one of the related MoralEntityPartOfConnections match this filter */
  partOfConnection_SINGLE?: InputMaybe<MoralEntityPartOfConnectionWhere>;
  /** Return MoralEntities where some of the related MoralEntityPartOfConnections match this filter */
  partOfConnection_SOME?: InputMaybe<MoralEntityPartOfConnectionWhere>;
  /** Return MoralEntities where all of the related MoralEntities match this filter */
  partOf_ALL?: InputMaybe<MoralEntityWhere>;
  /** Return MoralEntities where none of the related MoralEntities match this filter */
  partOf_NONE?: InputMaybe<MoralEntityWhere>;
  /** Return MoralEntities where one of the related MoralEntities match this filter */
  partOf_SINGLE?: InputMaybe<MoralEntityWhere>;
  /** Return MoralEntities where some of the related MoralEntities match this filter */
  partOf_SOME?: InputMaybe<MoralEntityWhere>;
};

export type Mutation = {
  __typename?: 'Mutation';
  /**
   * Admin task : create (or recreate) elasticsearch indices
   * Returns true if an index has been created, false otherwise.
   */
  adminElasticCreateIndices: Scalars['Boolean'];
  /**
   * Admin task : Re-index neo4j data into elastic.
   * Returns the number of document indexed
   */
  adminElasticIndexData: Scalars['Int'];
  /** Inquiry: Create an annotation on a node */
  inquiryAnnotationCreate?: Maybe<Scalars['Boolean']>;
  /** Inquiry: Replay all the annotation of the inquiry */
  inquiryAnnotationReplay: Report;
  /** Inquiry: data import as multiple non-reversible transactions (best for full corpus reload) */
  inquiryBatchImportFile: Report;
  /** Inquiry: data import with local file as multiple non-reversible transactions (best for full corpus reload) */
  inquiryBatchImportLocalFile: Report;
  /**
   * Inquiry: create a new inquiry
   * Only admin can create an inquiry
   */
  inquiryCreate?: Maybe<Inquiry>;
  /** Inquiry: delete a inquiry */
  inquiryDelete?: Maybe<Scalars['Boolean']>;
  /**
   * Inquiry: flush a inquiry
   * ------------------------
   */
  inquiryFlush?: Maybe<Scalars['Boolean']>;
  /** Inquiry: data import as a single reversible transaction (best for updates) */
  inquiryImportFile: Report;
  /** Inquiry: data import with local file as a single reversible transaction (best for updates) */
  inquiryImportLocalFile: Report;
  /**
   * Inquiry: update a inquiry
   * Only admin can update the password of an inquiry
   */
  inquiryUpdate?: Maybe<Inquiry>;
  /**
   * Change password for the connected user.
   * Only admin can specify a username
   */
  userChangePassword?: Maybe<Scalars['Boolean']>;
  /**
   * Check the login / password, and returns a JWT that contains the user's information.
   * This JWT must be passed in the authorization header as a bearer token, to this graphql endpoint if you want to be authenticated
   */
  userLogin?: Maybe<Scalars['String']>;
};


export type MutationAdminElasticCreateIndicesArgs = {
  recreate?: InputMaybe<Scalars['Boolean']>;
};


export type MutationInquiryAnnotationCreateArgs = {
  author: Scalars['String'];
  comment: Scalars['String'];
  id: Scalars['ID'];
  patch: Array<PatchInput>;
};


export type MutationInquiryAnnotationReplayArgs = {
  id: Scalars['ID'];
};


export type MutationInquiryBatchImportFileArgs = {
  batchSize?: InputMaybe<Scalars['Int']>;
  file: Scalars['Upload'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  skip?: InputMaybe<Scalars['Int']>;
};


export type MutationInquiryBatchImportLocalFileArgs = {
  batchSize?: InputMaybe<Scalars['Int']>;
  filePath: Scalars['String'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  skip?: InputMaybe<Scalars['Int']>;
};


export type MutationInquiryCreateArgs = {
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  password: Scalars['String'];
};


export type MutationInquiryDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationInquiryFlushArgs = {
  id: Scalars['ID'];
};


export type MutationInquiryImportFileArgs = {
  file: Scalars['Upload'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
};


export type MutationInquiryImportLocalFileArgs = {
  filePath: Scalars['String'];
  flush?: InputMaybe<Scalars['Boolean']>;
  id: Scalars['ID'];
};


export type MutationInquiryUpdateArgs = {
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
};


export type MutationUserChangePasswordArgs = {
  password: Scalars['String'];
  username?: InputMaybe<Scalars['String']>;
};


export type MutationUserLoginArgs = {
  password: Scalars['String'];
  username: Scalars['ID'];
};

/**
 * Object
 * ------
 */
export type Object = {
  __typename?: 'Object';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<ObjectDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: ObjectDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  types: Array<ObjectType>;
  typesAggregate?: Maybe<ObjectObjectTypeTypesAggregationSelection>;
  typesConnection: ObjectTypesConnection;
};


/**
 * Object
 * ------
 */
export type ObjectDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Object
 * ------
 */
export type ObjectDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Object
 * ------
 */
export type ObjectDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ObjectDataSourcesConnectionSort>>;
  where?: InputMaybe<ObjectDataSourcesConnectionWhere>;
};


/**
 * Object
 * ------
 */
export type ObjectTypesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ObjectTypeOptions>;
  where?: InputMaybe<ObjectTypeWhere>;
};


/**
 * Object
 * ------
 */
export type ObjectTypesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ObjectTypeWhere>;
};


/**
 * Object
 * ------
 */
export type ObjectTypesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ObjectTypesConnectionSort>>;
  where?: InputMaybe<ObjectTypesConnectionWhere>;
};

export type ObjectAggregateSelection = {
  __typename?: 'ObjectAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ObjectDataSourceDataSourcesAggregationSelection = {
  __typename?: 'ObjectDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<ObjectDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<ObjectDataSourceDataSourcesNodeAggregateSelection>;
};

export type ObjectDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'ObjectDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type ObjectDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'ObjectDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ObjectDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<ObjectDataSourcesAggregateInput>>;
  NOT?: InputMaybe<ObjectDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<ObjectDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<ObjectDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<ObjectDataSourcesNodeAggregationWhereInput>;
};

export type ObjectDataSourcesConnection = {
  __typename?: 'ObjectDataSourcesConnection';
  edges: Array<ObjectDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ObjectDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type ObjectDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<ObjectDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<ObjectDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<ObjectDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type ObjectDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ObjectDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ObjectDataSourcesRelationship = ImportedFrom & {
  __typename?: 'ObjectDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type ObjectEdge = {
  __typename?: 'ObjectEdge';
  cursor: Scalars['String'];
  node: Object;
};

export type ObjectObjectTypeTypesAggregationSelection = {
  __typename?: 'ObjectObjectTypeTypesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<ObjectObjectTypeTypesEdgeAggregateSelection>;
  node?: Maybe<ObjectObjectTypeTypesNodeAggregateSelection>;
};

export type ObjectObjectTypeTypesEdgeAggregateSelection = {
  __typename?: 'ObjectObjectTypeTypesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type ObjectObjectTypeTypesNodeAggregateSelection = {
  __typename?: 'ObjectObjectTypeTypesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ObjectOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more ObjectSort objects to sort Objects by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ObjectSort>>;
};

/** Fields to sort Objects by. The order in which sorts are applied is not guaranteed when specifying many fields in one ObjectSort object. */
export type ObjectSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectType = {
  __typename?: 'ObjectType';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<ObjectTypeDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: ObjectTypeDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  partOf: Array<ObjectType>;
  partOfAggregate?: Maybe<ObjectTypeObjectTypePartOfAggregationSelection>;
  partOfConnection: ObjectTypePartOfConnection;
};


/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectTypeDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectTypeDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectTypeDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ObjectTypeDataSourcesConnectionSort>>;
  where?: InputMaybe<ObjectTypeDataSourcesConnectionWhere>;
};


/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectTypePartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ObjectTypeOptions>;
  where?: InputMaybe<ObjectTypeWhere>;
};


/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectTypePartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ObjectTypeWhere>;
};


/**
 * Object Type
 * -------
 * A list of type for an Object, like 'book', ...
 */
export type ObjectTypePartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ObjectTypePartOfConnectionSort>>;
  where?: InputMaybe<ObjectTypePartOfConnectionWhere>;
};

export type ObjectTypeAggregateSelection = {
  __typename?: 'ObjectTypeAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ObjectTypeDataSourceDataSourcesAggregationSelection = {
  __typename?: 'ObjectTypeDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<ObjectTypeDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<ObjectTypeDataSourceDataSourcesNodeAggregateSelection>;
};

export type ObjectTypeDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'ObjectTypeDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type ObjectTypeDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'ObjectTypeDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ObjectTypeDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<ObjectTypeDataSourcesAggregateInput>>;
  NOT?: InputMaybe<ObjectTypeDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<ObjectTypeDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<ObjectTypeDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<ObjectTypeDataSourcesNodeAggregationWhereInput>;
};

export type ObjectTypeDataSourcesConnection = {
  __typename?: 'ObjectTypeDataSourcesConnection';
  edges: Array<ObjectTypeDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ObjectTypeDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type ObjectTypeDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<ObjectTypeDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<ObjectTypeDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<ObjectTypeDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type ObjectTypeDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectTypeDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectTypeDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectTypeDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ObjectTypeDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectTypeDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectTypeDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectTypeDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ObjectTypeDataSourcesRelationship = ImportedFrom & {
  __typename?: 'ObjectTypeDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type ObjectTypeEdge = {
  __typename?: 'ObjectTypeEdge';
  cursor: Scalars['String'];
  node: ObjectType;
};

export type ObjectTypeObjectTypePartOfAggregationSelection = {
  __typename?: 'ObjectTypeObjectTypePartOfAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<ObjectTypeObjectTypePartOfEdgeAggregateSelection>;
  node?: Maybe<ObjectTypeObjectTypePartOfNodeAggregateSelection>;
};

export type ObjectTypeObjectTypePartOfEdgeAggregateSelection = {
  __typename?: 'ObjectTypeObjectTypePartOfEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type ObjectTypeObjectTypePartOfNodeAggregateSelection = {
  __typename?: 'ObjectTypeObjectTypePartOfNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ObjectTypeOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more ObjectTypeSort objects to sort ObjectTypes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ObjectTypeSort>>;
};

export type ObjectTypePartOfAggregateInput = {
  AND?: InputMaybe<Array<ObjectTypePartOfAggregateInput>>;
  NOT?: InputMaybe<ObjectTypePartOfAggregateInput>;
  OR?: InputMaybe<Array<ObjectTypePartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<ObjectTypePartOfEdgeAggregationWhereInput>;
  node?: InputMaybe<ObjectTypePartOfNodeAggregationWhereInput>;
};

export type ObjectTypePartOfConnection = {
  __typename?: 'ObjectTypePartOfConnection';
  edges: Array<ObjectTypePartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ObjectTypePartOfConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<ObjectTypeSort>;
};

export type ObjectTypePartOfConnectionWhere = {
  AND?: InputMaybe<Array<ObjectTypePartOfConnectionWhere>>;
  NOT?: InputMaybe<ObjectTypePartOfConnectionWhere>;
  OR?: InputMaybe<Array<ObjectTypePartOfConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<ObjectTypeWhere>;
};

export type ObjectTypePartOfEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectTypePartOfEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectTypePartOfEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectTypePartOfEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type ObjectTypePartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectTypePartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectTypePartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectTypePartOfNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ObjectTypePartOfRelationship = Certainty & {
  __typename?: 'ObjectTypePartOfRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: ObjectType;
};

/** Fields to sort ObjectTypes by. The order in which sorts are applied is not guaranteed when specifying many fields in one ObjectTypeSort object. */
export type ObjectTypeSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type ObjectTypeWhere = {
  AND?: InputMaybe<Array<ObjectTypeWhere>>;
  NOT?: InputMaybe<ObjectTypeWhere>;
  OR?: InputMaybe<Array<ObjectTypeWhere>>;
  dataSourcesAggregate?: InputMaybe<ObjectTypeDataSourcesAggregateInput>;
  /** Return ObjectTypes where all of the related ObjectTypeDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<ObjectTypeDataSourcesConnectionWhere>;
  /** Return ObjectTypes where none of the related ObjectTypeDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<ObjectTypeDataSourcesConnectionWhere>;
  /** Return ObjectTypes where one of the related ObjectTypeDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<ObjectTypeDataSourcesConnectionWhere>;
  /** Return ObjectTypes where some of the related ObjectTypeDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<ObjectTypeDataSourcesConnectionWhere>;
  /** Return ObjectTypes where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return ObjectTypes where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return ObjectTypes where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return ObjectTypes where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  partOfAggregate?: InputMaybe<ObjectTypePartOfAggregateInput>;
  /** Return ObjectTypes where all of the related ObjectTypePartOfConnections match this filter */
  partOfConnection_ALL?: InputMaybe<ObjectTypePartOfConnectionWhere>;
  /** Return ObjectTypes where none of the related ObjectTypePartOfConnections match this filter */
  partOfConnection_NONE?: InputMaybe<ObjectTypePartOfConnectionWhere>;
  /** Return ObjectTypes where one of the related ObjectTypePartOfConnections match this filter */
  partOfConnection_SINGLE?: InputMaybe<ObjectTypePartOfConnectionWhere>;
  /** Return ObjectTypes where some of the related ObjectTypePartOfConnections match this filter */
  partOfConnection_SOME?: InputMaybe<ObjectTypePartOfConnectionWhere>;
  /** Return ObjectTypes where all of the related ObjectTypes match this filter */
  partOf_ALL?: InputMaybe<ObjectTypeWhere>;
  /** Return ObjectTypes where none of the related ObjectTypes match this filter */
  partOf_NONE?: InputMaybe<ObjectTypeWhere>;
  /** Return ObjectTypes where one of the related ObjectTypes match this filter */
  partOf_SINGLE?: InputMaybe<ObjectTypeWhere>;
  /** Return ObjectTypes where some of the related ObjectTypes match this filter */
  partOf_SOME?: InputMaybe<ObjectTypeWhere>;
};

export type ObjectTypesAggregateInput = {
  AND?: InputMaybe<Array<ObjectTypesAggregateInput>>;
  NOT?: InputMaybe<ObjectTypesAggregateInput>;
  OR?: InputMaybe<Array<ObjectTypesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<ObjectTypesEdgeAggregationWhereInput>;
  node?: InputMaybe<ObjectTypesNodeAggregationWhereInput>;
};

export type ObjectTypesConnection = {
  __typename?: 'ObjectTypesConnection';
  edges: Array<ObjectTypeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ObjectTypesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<ObjectTypeSort>;
};

export type ObjectTypesConnectionWhere = {
  AND?: InputMaybe<Array<ObjectTypesConnectionWhere>>;
  NOT?: InputMaybe<ObjectTypesConnectionWhere>;
  OR?: InputMaybe<Array<ObjectTypesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<ObjectTypeWhere>;
};

export type ObjectTypesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectTypesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectTypesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectTypesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type ObjectTypesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ObjectTypesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ObjectTypesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ObjectTypesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ObjectWhere = {
  AND?: InputMaybe<Array<ObjectWhere>>;
  NOT?: InputMaybe<ObjectWhere>;
  OR?: InputMaybe<Array<ObjectWhere>>;
  dataSourcesAggregate?: InputMaybe<ObjectDataSourcesAggregateInput>;
  /** Return Objects where all of the related ObjectDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<ObjectDataSourcesConnectionWhere>;
  /** Return Objects where none of the related ObjectDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<ObjectDataSourcesConnectionWhere>;
  /** Return Objects where one of the related ObjectDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<ObjectDataSourcesConnectionWhere>;
  /** Return Objects where some of the related ObjectDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<ObjectDataSourcesConnectionWhere>;
  /** Return Objects where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Objects where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Objects where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Objects where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  typesAggregate?: InputMaybe<ObjectTypesAggregateInput>;
  /** Return Objects where all of the related ObjectTypesConnections match this filter */
  typesConnection_ALL?: InputMaybe<ObjectTypesConnectionWhere>;
  /** Return Objects where none of the related ObjectTypesConnections match this filter */
  typesConnection_NONE?: InputMaybe<ObjectTypesConnectionWhere>;
  /** Return Objects where one of the related ObjectTypesConnections match this filter */
  typesConnection_SINGLE?: InputMaybe<ObjectTypesConnectionWhere>;
  /** Return Objects where some of the related ObjectTypesConnections match this filter */
  typesConnection_SOME?: InputMaybe<ObjectTypesConnectionWhere>;
  /** Return Objects where all of the related ObjectTypes match this filter */
  types_ALL?: InputMaybe<ObjectTypeWhere>;
  /** Return Objects where none of the related ObjectTypes match this filter */
  types_NONE?: InputMaybe<ObjectTypeWhere>;
  /** Return Objects where one of the related ObjectTypes match this filter */
  types_SINGLE?: InputMaybe<ObjectTypeWhere>;
  /** Return Objects where some of the related ObjectTypes match this filter */
  types_SOME?: InputMaybe<ObjectTypeWhere>;
};

export type ObjectsConnection = {
  __typename?: 'ObjectsConnection';
  edges: Array<ObjectEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/** Pagination information (Relay) */
export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type PatchInput = {
  id?: InputMaybe<Scalars['String']>;
  op: Scalars['String'];
  properties?: InputMaybe<Scalars['JSON']>;
  source?: InputMaybe<PatchNodeReferenceInput>;
  target?: InputMaybe<PatchNodeReferenceInput>;
  type: Scalars['String'];
};

export type PatchNodeReferenceInput = {
  id: Scalars['String'];
  type: Scalars['String'];
};

/**
 * Person
 * ------
 * A person can be a PhysicalPerson or a MoralEntity
 * See below for the PhysicalPerson & MoralEntity definitions
 */
export type Person = MoralEntity | PhysicalPerson;

/**
 * PersonName
 * ----------
 */
export type PersonName = {
  __typename?: 'PersonName';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<PersonNameDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: PersonNameDataSourcesConnection;
  id: Scalars['ID'];
  name: Scalars['String'];
};


/**
 * PersonName
 * ----------
 */
export type PersonNameDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * PersonName
 * ----------
 */
export type PersonNameDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * PersonName
 * ----------
 */
export type PersonNameDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PersonNameDataSourcesConnectionSort>>;
  where?: InputMaybe<PersonNameDataSourcesConnectionWhere>;
};

export type PersonNameAggregateSelection = {
  __typename?: 'PersonNameAggregateSelection';
  count: Scalars['Int'];
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PersonNameDataSourceDataSourcesAggregationSelection = {
  __typename?: 'PersonNameDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PersonNameDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<PersonNameDataSourceDataSourcesNodeAggregateSelection>;
};

export type PersonNameDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'PersonNameDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type PersonNameDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'PersonNameDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PersonNameDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<PersonNameDataSourcesAggregateInput>>;
  NOT?: InputMaybe<PersonNameDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<PersonNameDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PersonNameDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<PersonNameDataSourcesNodeAggregationWhereInput>;
};

export type PersonNameDataSourcesConnection = {
  __typename?: 'PersonNameDataSourcesConnection';
  edges: Array<PersonNameDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PersonNameDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type PersonNameDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<PersonNameDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<PersonNameDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<PersonNameDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type PersonNameDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PersonNameDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PersonNameDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PersonNameDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PersonNameDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PersonNameDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PersonNameDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PersonNameDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PersonNameDataSourcesRelationship = ImportedFrom & {
  __typename?: 'PersonNameDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type PersonNameEdge = {
  __typename?: 'PersonNameEdge';
  cursor: Scalars['String'];
  node: PersonName;
};

export type PersonNameOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more PersonNameSort objects to sort PersonNames by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<PersonNameSort>>;
};

/** Fields to sort PersonNames by. The order in which sorts are applied is not guaranteed when specifying many fields in one PersonNameSort object. */
export type PersonNameSort = {
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type PersonNameWhere = {
  AND?: InputMaybe<Array<PersonNameWhere>>;
  NOT?: InputMaybe<PersonNameWhere>;
  OR?: InputMaybe<Array<PersonNameWhere>>;
  dataSourcesAggregate?: InputMaybe<PersonNameDataSourcesAggregateInput>;
  /** Return PersonNames where all of the related PersonNameDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<PersonNameDataSourcesConnectionWhere>;
  /** Return PersonNames where none of the related PersonNameDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<PersonNameDataSourcesConnectionWhere>;
  /** Return PersonNames where one of the related PersonNameDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<PersonNameDataSourcesConnectionWhere>;
  /** Return PersonNames where some of the related PersonNameDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<PersonNameDataSourcesConnectionWhere>;
  /** Return PersonNames where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return PersonNames where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return PersonNames where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return PersonNames where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type PersonNamesConnection = {
  __typename?: 'PersonNamesConnection';
  edges: Array<PersonNameEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PersonWhere = {
  MoralEntity?: InputMaybe<MoralEntityWhere>;
  PhysicalPerson?: InputMaybe<PhysicalPersonWhere>;
};

export type PhysicalPeopleConnection = {
  __typename?: 'PhysicalPeopleConnection';
  edges: Array<PhysicalPersonEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPerson = {
  __typename?: 'PhysicalPerson';
  annotations: Array<Annotation>;
  annotationsAggregate?: Maybe<PhysicalPersonAnnotationAnnotationsAggregationSelection>;
  annotationsConnection: PhysicalPersonAnnotationsConnection;
  connectedTo: Array<PhysicalPerson>;
  connectedToAggregate?: Maybe<PhysicalPersonPhysicalPersonConnectedToAggregationSelection>;
  connectedToConnection: PhysicalPersonConnectedToConnection;
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<PhysicalPersonDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: PhysicalPersonDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  groups: Array<Group>;
  groupsAggregate?: Maybe<PhysicalPersonGroupGroupsAggregationSelection>;
  groupsConnection: PhysicalPersonGroupsConnection;
  id: Scalars['ID'];
  inquiry: Inquiry;
  name: Scalars['String'];
  otherNames: Array<PersonName>;
  otherNamesAggregate?: Maybe<PhysicalPersonPersonNameOtherNamesAggregationSelection>;
  otherNamesConnection: PhysicalPersonOtherNamesConnection;
  participatesTo: Array<FactoidPersonParticipate>;
  participatesToAggregate?: Maybe<PhysicalPersonFactoidPersonParticipateParticipatesToAggregationSelection>;
  participatesToConnection: PhysicalPersonParticipatesToConnection;
  seeAlso: Array<PhysicalPerson>;
  sources: Array<Source>;
  sourcesAggregate?: Maybe<PhysicalPersonSourceSourcesAggregationSelection>;
  sourcesConnection: PhysicalPersonSourcesConnection;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonAnnotationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonAnnotationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonAnnotationsConnectionSort>>;
  where?: InputMaybe<PhysicalPersonAnnotationsConnectionWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonConnectedToArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<PhysicalPersonOptions>;
  where?: InputMaybe<PhysicalPersonWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonConnectedToAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<PhysicalPersonWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonConnectedToConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonConnectedToConnectionSort>>;
  where?: InputMaybe<PhysicalPersonConnectedToConnectionWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonDataSourcesConnectionSort>>;
  where?: InputMaybe<PhysicalPersonDataSourcesConnectionWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonGroupsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<GroupOptions>;
  where?: InputMaybe<GroupWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonGroupsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<GroupWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonGroupsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonGroupsConnectionSort>>;
  where?: InputMaybe<PhysicalPersonGroupsConnectionWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonOtherNamesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<PersonNameOptions>;
  where?: InputMaybe<PersonNameWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonOtherNamesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<PersonNameWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonOtherNamesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonOtherNamesConnectionSort>>;
  where?: InputMaybe<PhysicalPersonOtherNamesConnectionWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonParticipatesToArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidPersonParticipateOptions>;
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonParticipatesToAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonParticipatesToConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonParticipatesToConnectionSort>>;
  where?: InputMaybe<PhysicalPersonParticipatesToConnectionWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceOptions>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceWhere>;
};


/**
 * PhysicalPerson
 * --------------
 */
export type PhysicalPersonSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PhysicalPersonSourcesConnectionSort>>;
  where?: InputMaybe<PhysicalPersonSourcesConnectionWhere>;
};

export type PhysicalPersonAggregateSelection = {
  __typename?: 'PhysicalPersonAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PhysicalPersonAnnotationAnnotationsAggregationSelection = {
  __typename?: 'PhysicalPersonAnnotationAnnotationsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PhysicalPersonAnnotationAnnotationsNodeAggregateSelection>;
};

export type PhysicalPersonAnnotationAnnotationsNodeAggregateSelection = {
  __typename?: 'PhysicalPersonAnnotationAnnotationsNodeAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type PhysicalPersonAnnotationsAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonAnnotationsAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonAnnotationsAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonAnnotationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PhysicalPersonAnnotationsNodeAggregationWhereInput>;
};

export type PhysicalPersonAnnotationsConnection = {
  __typename?: 'PhysicalPersonAnnotationsConnection';
  edges: Array<PhysicalPersonAnnotationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonAnnotationsConnectionSort = {
  node?: InputMaybe<AnnotationSort>;
};

export type PhysicalPersonAnnotationsConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonAnnotationsConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonAnnotationsConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonAnnotationsConnectionWhere>>;
  node?: InputMaybe<AnnotationWhere>;
};

export type PhysicalPersonAnnotationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonAnnotationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonAnnotationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonAnnotationsNodeAggregationWhereInput>>;
  author_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  author_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  comment_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  createdAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type PhysicalPersonAnnotationsRelationship = {
  __typename?: 'PhysicalPersonAnnotationsRelationship';
  cursor: Scalars['String'];
  node: Annotation;
};

export type PhysicalPersonConnectedToAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonConnectedToAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonConnectedToAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonConnectedToAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PhysicalPersonConnectedToEdgeAggregationWhereInput>;
  node?: InputMaybe<PhysicalPersonConnectedToNodeAggregationWhereInput>;
};

export type PhysicalPersonConnectedToConnection = {
  __typename?: 'PhysicalPersonConnectedToConnection';
  edges: Array<PhysicalPersonConnectedToRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonConnectedToConnectionSort = {
  edge?: InputMaybe<CertaintyAndTypeSort>;
  node?: InputMaybe<PhysicalPersonSort>;
};

export type PhysicalPersonConnectedToConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonConnectedToConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonConnectedToConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonConnectedToConnectionWhere>>;
  edge?: InputMaybe<CertaintyAndTypeWhere>;
  node?: InputMaybe<PhysicalPersonWhere>;
};

export type PhysicalPersonConnectedToEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonConnectedToEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonConnectedToEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonConnectedToEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  type_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  type_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  type_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  type_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PhysicalPersonConnectedToNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonConnectedToNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonConnectedToNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonConnectedToNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PhysicalPersonConnectedToRelationship = CertaintyAndType & {
  __typename?: 'PhysicalPersonConnectedToRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: PhysicalPerson;
  type: Scalars['String'];
};

export type PhysicalPersonDataSourceDataSourcesAggregationSelection = {
  __typename?: 'PhysicalPersonDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PhysicalPersonDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<PhysicalPersonDataSourceDataSourcesNodeAggregateSelection>;
};

export type PhysicalPersonDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'PhysicalPersonDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type PhysicalPersonDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'PhysicalPersonDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PhysicalPersonDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonDataSourcesAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PhysicalPersonDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<PhysicalPersonDataSourcesNodeAggregationWhereInput>;
};

export type PhysicalPersonDataSourcesConnection = {
  __typename?: 'PhysicalPersonDataSourcesConnection';
  edges: Array<PhysicalPersonDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type PhysicalPersonDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type PhysicalPersonDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PhysicalPersonDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PhysicalPersonDataSourcesRelationship = ImportedFrom & {
  __typename?: 'PhysicalPersonDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type PhysicalPersonEdge = {
  __typename?: 'PhysicalPersonEdge';
  cursor: Scalars['String'];
  node: PhysicalPerson;
};

export type PhysicalPersonFactoidPersonParticipateParticipatesToAggregationSelection = {
  __typename?: 'PhysicalPersonFactoidPersonParticipateParticipatesToAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PhysicalPersonFactoidPersonParticipateParticipatesToNodeAggregateSelection>;
};

export type PhysicalPersonFactoidPersonParticipateParticipatesToNodeAggregateSelection = {
  __typename?: 'PhysicalPersonFactoidPersonParticipateParticipatesToNodeAggregateSelection';
  certainty: FloatAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
};

export type PhysicalPersonGroupGroupsAggregationSelection = {
  __typename?: 'PhysicalPersonGroupGroupsAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PhysicalPersonGroupGroupsEdgeAggregateSelection>;
  node?: Maybe<PhysicalPersonGroupGroupsNodeAggregateSelection>;
};

export type PhysicalPersonGroupGroupsEdgeAggregateSelection = {
  __typename?: 'PhysicalPersonGroupGroupsEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type PhysicalPersonGroupGroupsNodeAggregateSelection = {
  __typename?: 'PhysicalPersonGroupGroupsNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PhysicalPersonGroupsAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonGroupsAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonGroupsAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonGroupsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PhysicalPersonGroupsEdgeAggregationWhereInput>;
  node?: InputMaybe<PhysicalPersonGroupsNodeAggregationWhereInput>;
};

export type PhysicalPersonGroupsConnection = {
  __typename?: 'PhysicalPersonGroupsConnection';
  edges: Array<PhysicalPersonGroupsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonGroupsConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<GroupSort>;
};

export type PhysicalPersonGroupsConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonGroupsConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonGroupsConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonGroupsConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<GroupWhere>;
};

export type PhysicalPersonGroupsEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonGroupsEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonGroupsEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonGroupsEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PhysicalPersonGroupsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonGroupsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonGroupsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonGroupsNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PhysicalPersonGroupsRelationship = Certainty & {
  __typename?: 'PhysicalPersonGroupsRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Group;
};

export type PhysicalPersonOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more PhysicalPersonSort objects to sort PhysicalPeople by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<PhysicalPersonSort>>;
};

export type PhysicalPersonOtherNamesAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonOtherNamesAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonOtherNamesAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonOtherNamesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PhysicalPersonOtherNamesEdgeAggregationWhereInput>;
  node?: InputMaybe<PhysicalPersonOtherNamesNodeAggregationWhereInput>;
};

export type PhysicalPersonOtherNamesConnection = {
  __typename?: 'PhysicalPersonOtherNamesConnection';
  edges: Array<PhysicalPersonOtherNamesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonOtherNamesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<PersonNameSort>;
};

export type PhysicalPersonOtherNamesConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonOtherNamesConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonOtherNamesConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonOtherNamesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<PersonNameWhere>;
};

export type PhysicalPersonOtherNamesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonOtherNamesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonOtherNamesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonOtherNamesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PhysicalPersonOtherNamesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonOtherNamesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonOtherNamesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonOtherNamesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PhysicalPersonOtherNamesRelationship = Certainty & {
  __typename?: 'PhysicalPersonOtherNamesRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: PersonName;
};

export type PhysicalPersonParticipatesToAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonParticipatesToAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonParticipatesToAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonParticipatesToAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PhysicalPersonParticipatesToNodeAggregationWhereInput>;
};

export type PhysicalPersonParticipatesToConnection = {
  __typename?: 'PhysicalPersonParticipatesToConnection';
  edges: Array<PhysicalPersonParticipatesToRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonParticipatesToConnectionSort = {
  node?: InputMaybe<FactoidPersonParticipateSort>;
};

export type PhysicalPersonParticipatesToConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonParticipatesToConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonParticipatesToConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonParticipatesToConnectionWhere>>;
  node?: InputMaybe<FactoidPersonParticipateWhere>;
};

export type PhysicalPersonParticipatesToNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonParticipatesToNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonParticipatesToNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonParticipatesToNodeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PhysicalPersonParticipatesToRelationship = {
  __typename?: 'PhysicalPersonParticipatesToRelationship';
  cursor: Scalars['String'];
  node: FactoidPersonParticipate;
};

export type PhysicalPersonPersonNameOtherNamesAggregationSelection = {
  __typename?: 'PhysicalPersonPersonNameOtherNamesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PhysicalPersonPersonNameOtherNamesEdgeAggregateSelection>;
  node?: Maybe<PhysicalPersonPersonNameOtherNamesNodeAggregateSelection>;
};

export type PhysicalPersonPersonNameOtherNamesEdgeAggregateSelection = {
  __typename?: 'PhysicalPersonPersonNameOtherNamesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type PhysicalPersonPersonNameOtherNamesNodeAggregateSelection = {
  __typename?: 'PhysicalPersonPersonNameOtherNamesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PhysicalPersonPhysicalPersonConnectedToAggregationSelection = {
  __typename?: 'PhysicalPersonPhysicalPersonConnectedToAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PhysicalPersonPhysicalPersonConnectedToEdgeAggregateSelection>;
  node?: Maybe<PhysicalPersonPhysicalPersonConnectedToNodeAggregateSelection>;
};

export type PhysicalPersonPhysicalPersonConnectedToEdgeAggregateSelection = {
  __typename?: 'PhysicalPersonPhysicalPersonConnectedToEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
  type: StringAggregateSelectionNonNullable;
};

export type PhysicalPersonPhysicalPersonConnectedToNodeAggregateSelection = {
  __typename?: 'PhysicalPersonPhysicalPersonConnectedToNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

/** Fields to sort PhysicalPeople by. The order in which sorts are applied is not guaranteed when specifying many fields in one PhysicalPersonSort object. */
export type PhysicalPersonSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type PhysicalPersonSourceSourcesAggregationSelection = {
  __typename?: 'PhysicalPersonSourceSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PhysicalPersonSourceSourcesEdgeAggregateSelection>;
  node?: Maybe<PhysicalPersonSourceSourcesNodeAggregateSelection>;
};

export type PhysicalPersonSourceSourcesEdgeAggregateSelection = {
  __typename?: 'PhysicalPersonSourceSourcesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type PhysicalPersonSourceSourcesNodeAggregateSelection = {
  __typename?: 'PhysicalPersonSourceSourcesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  language: StringAggregateSelectionNullable;
  name: StringAggregateSelectionNonNullable;
  reputation: FloatAggregateSelectionNullable;
};

export type PhysicalPersonSourcesAggregateInput = {
  AND?: InputMaybe<Array<PhysicalPersonSourcesAggregateInput>>;
  NOT?: InputMaybe<PhysicalPersonSourcesAggregateInput>;
  OR?: InputMaybe<Array<PhysicalPersonSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PhysicalPersonSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<PhysicalPersonSourcesNodeAggregationWhereInput>;
};

export type PhysicalPersonSourcesConnection = {
  __typename?: 'PhysicalPersonSourcesConnection';
  edges: Array<PhysicalPersonSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PhysicalPersonSourcesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<SourceSort>;
};

export type PhysicalPersonSourcesConnectionWhere = {
  AND?: InputMaybe<Array<PhysicalPersonSourcesConnectionWhere>>;
  NOT?: InputMaybe<PhysicalPersonSourcesConnectionWhere>;
  OR?: InputMaybe<Array<PhysicalPersonSourcesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<SourceWhere>;
};

export type PhysicalPersonSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonSourcesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PhysicalPersonSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PhysicalPersonSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PhysicalPersonSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PhysicalPersonSourcesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  language_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  language_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  language_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  reputation_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  reputation_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LT?: InputMaybe<Scalars['Float']>;
  reputation_MAX_LTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_GTE?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LT?: InputMaybe<Scalars['Float']>;
  reputation_MIN_LTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_GTE?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LT?: InputMaybe<Scalars['Float']>;
  reputation_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PhysicalPersonSourcesRelationship = Certainty & {
  __typename?: 'PhysicalPersonSourcesRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Source;
};

export type PhysicalPersonWhere = {
  AND?: InputMaybe<Array<PhysicalPersonWhere>>;
  NOT?: InputMaybe<PhysicalPersonWhere>;
  OR?: InputMaybe<Array<PhysicalPersonWhere>>;
  annotationsAggregate?: InputMaybe<PhysicalPersonAnnotationsAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonAnnotationsConnections match this filter */
  annotationsConnection_ALL?: InputMaybe<PhysicalPersonAnnotationsConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonAnnotationsConnections match this filter */
  annotationsConnection_NONE?: InputMaybe<PhysicalPersonAnnotationsConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonAnnotationsConnections match this filter */
  annotationsConnection_SINGLE?: InputMaybe<PhysicalPersonAnnotationsConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonAnnotationsConnections match this filter */
  annotationsConnection_SOME?: InputMaybe<PhysicalPersonAnnotationsConnectionWhere>;
  /** Return PhysicalPeople where all of the related Annotations match this filter */
  annotations_ALL?: InputMaybe<AnnotationWhere>;
  /** Return PhysicalPeople where none of the related Annotations match this filter */
  annotations_NONE?: InputMaybe<AnnotationWhere>;
  /** Return PhysicalPeople where one of the related Annotations match this filter */
  annotations_SINGLE?: InputMaybe<AnnotationWhere>;
  /** Return PhysicalPeople where some of the related Annotations match this filter */
  annotations_SOME?: InputMaybe<AnnotationWhere>;
  connectedToAggregate?: InputMaybe<PhysicalPersonConnectedToAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonConnectedToConnections match this filter */
  connectedToConnection_ALL?: InputMaybe<PhysicalPersonConnectedToConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonConnectedToConnections match this filter */
  connectedToConnection_NONE?: InputMaybe<PhysicalPersonConnectedToConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonConnectedToConnections match this filter */
  connectedToConnection_SINGLE?: InputMaybe<PhysicalPersonConnectedToConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonConnectedToConnections match this filter */
  connectedToConnection_SOME?: InputMaybe<PhysicalPersonConnectedToConnectionWhere>;
  /** Return PhysicalPeople where all of the related PhysicalPeople match this filter */
  connectedTo_ALL?: InputMaybe<PhysicalPersonWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPeople match this filter */
  connectedTo_NONE?: InputMaybe<PhysicalPersonWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPeople match this filter */
  connectedTo_SINGLE?: InputMaybe<PhysicalPersonWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPeople match this filter */
  connectedTo_SOME?: InputMaybe<PhysicalPersonWhere>;
  dataSourcesAggregate?: InputMaybe<PhysicalPersonDataSourcesAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<PhysicalPersonDataSourcesConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<PhysicalPersonDataSourcesConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<PhysicalPersonDataSourcesConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<PhysicalPersonDataSourcesConnectionWhere>;
  /** Return PhysicalPeople where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return PhysicalPeople where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return PhysicalPeople where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return PhysicalPeople where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  groupsAggregate?: InputMaybe<PhysicalPersonGroupsAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonGroupsConnections match this filter */
  groupsConnection_ALL?: InputMaybe<PhysicalPersonGroupsConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonGroupsConnections match this filter */
  groupsConnection_NONE?: InputMaybe<PhysicalPersonGroupsConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonGroupsConnections match this filter */
  groupsConnection_SINGLE?: InputMaybe<PhysicalPersonGroupsConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonGroupsConnections match this filter */
  groupsConnection_SOME?: InputMaybe<PhysicalPersonGroupsConnectionWhere>;
  /** Return PhysicalPeople where all of the related Groups match this filter */
  groups_ALL?: InputMaybe<GroupWhere>;
  /** Return PhysicalPeople where none of the related Groups match this filter */
  groups_NONE?: InputMaybe<GroupWhere>;
  /** Return PhysicalPeople where one of the related Groups match this filter */
  groups_SINGLE?: InputMaybe<GroupWhere>;
  /** Return PhysicalPeople where some of the related Groups match this filter */
  groups_SOME?: InputMaybe<GroupWhere>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  otherNamesAggregate?: InputMaybe<PhysicalPersonOtherNamesAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonOtherNamesConnections match this filter */
  otherNamesConnection_ALL?: InputMaybe<PhysicalPersonOtherNamesConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonOtherNamesConnections match this filter */
  otherNamesConnection_NONE?: InputMaybe<PhysicalPersonOtherNamesConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonOtherNamesConnections match this filter */
  otherNamesConnection_SINGLE?: InputMaybe<PhysicalPersonOtherNamesConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonOtherNamesConnections match this filter */
  otherNamesConnection_SOME?: InputMaybe<PhysicalPersonOtherNamesConnectionWhere>;
  /** Return PhysicalPeople where all of the related PersonNames match this filter */
  otherNames_ALL?: InputMaybe<PersonNameWhere>;
  /** Return PhysicalPeople where none of the related PersonNames match this filter */
  otherNames_NONE?: InputMaybe<PersonNameWhere>;
  /** Return PhysicalPeople where one of the related PersonNames match this filter */
  otherNames_SINGLE?: InputMaybe<PersonNameWhere>;
  /** Return PhysicalPeople where some of the related PersonNames match this filter */
  otherNames_SOME?: InputMaybe<PersonNameWhere>;
  participatesToAggregate?: InputMaybe<PhysicalPersonParticipatesToAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonParticipatesToConnections match this filter */
  participatesToConnection_ALL?: InputMaybe<PhysicalPersonParticipatesToConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonParticipatesToConnections match this filter */
  participatesToConnection_NONE?: InputMaybe<PhysicalPersonParticipatesToConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonParticipatesToConnections match this filter */
  participatesToConnection_SINGLE?: InputMaybe<PhysicalPersonParticipatesToConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonParticipatesToConnections match this filter */
  participatesToConnection_SOME?: InputMaybe<PhysicalPersonParticipatesToConnectionWhere>;
  /** Return PhysicalPeople where all of the related FactoidPersonParticipates match this filter */
  participatesTo_ALL?: InputMaybe<FactoidPersonParticipateWhere>;
  /** Return PhysicalPeople where none of the related FactoidPersonParticipates match this filter */
  participatesTo_NONE?: InputMaybe<FactoidPersonParticipateWhere>;
  /** Return PhysicalPeople where one of the related FactoidPersonParticipates match this filter */
  participatesTo_SINGLE?: InputMaybe<FactoidPersonParticipateWhere>;
  /** Return PhysicalPeople where some of the related FactoidPersonParticipates match this filter */
  participatesTo_SOME?: InputMaybe<FactoidPersonParticipateWhere>;
  sourcesAggregate?: InputMaybe<PhysicalPersonSourcesAggregateInput>;
  /** Return PhysicalPeople where all of the related PhysicalPersonSourcesConnections match this filter */
  sourcesConnection_ALL?: InputMaybe<PhysicalPersonSourcesConnectionWhere>;
  /** Return PhysicalPeople where none of the related PhysicalPersonSourcesConnections match this filter */
  sourcesConnection_NONE?: InputMaybe<PhysicalPersonSourcesConnectionWhere>;
  /** Return PhysicalPeople where one of the related PhysicalPersonSourcesConnections match this filter */
  sourcesConnection_SINGLE?: InputMaybe<PhysicalPersonSourcesConnectionWhere>;
  /** Return PhysicalPeople where some of the related PhysicalPersonSourcesConnections match this filter */
  sourcesConnection_SOME?: InputMaybe<PhysicalPersonSourcesConnectionWhere>;
  /** Return PhysicalPeople where all of the related Sources match this filter */
  sources_ALL?: InputMaybe<SourceWhere>;
  /** Return PhysicalPeople where none of the related Sources match this filter */
  sources_NONE?: InputMaybe<SourceWhere>;
  /** Return PhysicalPeople where one of the related Sources match this filter */
  sources_SINGLE?: InputMaybe<SourceWhere>;
  /** Return PhysicalPeople where some of the related Sources match this filter */
  sources_SOME?: InputMaybe<SourceWhere>;
};

/**
 * Place
 * -----
 */
export type Place = {
  __typename?: 'Place';
  alternativeNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  annotations: Array<Annotation>;
  annotationsAggregate?: Maybe<PlaceAnnotationAnnotationsAggregationSelection>;
  annotationsConnection: PlaceAnnotationsConnection;
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<PlaceDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: PlaceDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  factoids: Array<Factoid>;
  factoidsAggregate?: Maybe<PlaceFactoidFactoidsAggregationSelection>;
  factoidsConnection: PlaceFactoidsConnection;
  id: Scalars['ID'];
  inquiry: Inquiry;
  locatedIn: Array<PlaceZoneOverTime>;
  locatedInAggregate?: Maybe<PlacePlaceZoneOverTimeLocatedInAggregationSelection>;
  locatedInConnection: PlaceLocatedInConnection;
  name: Scalars['String'];
};


/**
 * Place
 * -----
 */
export type PlaceAnnotationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceAnnotationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<AnnotationWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceAnnotationsConnectionSort>>;
  where?: InputMaybe<PlaceAnnotationsConnectionWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceDataSourcesConnectionSort>>;
  where?: InputMaybe<PlaceDataSourcesConnectionWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceFactoidsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidOptions>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceFactoidsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceFactoidsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceFactoidsConnectionSort>>;
  where?: InputMaybe<PlaceFactoidsConnectionWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceLocatedInArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<PlaceZoneOverTimeOptions>;
  where?: InputMaybe<PlaceZoneOverTimeWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceLocatedInAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<PlaceZoneOverTimeWhere>;
};


/**
 * Place
 * -----
 */
export type PlaceLocatedInConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceLocatedInConnectionSort>>;
  where?: InputMaybe<PlaceLocatedInConnectionWhere>;
};

export type PlaceAggregateSelection = {
  __typename?: 'PlaceAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PlaceAnnotationAnnotationsAggregationSelection = {
  __typename?: 'PlaceAnnotationAnnotationsAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PlaceAnnotationAnnotationsNodeAggregateSelection>;
};

export type PlaceAnnotationAnnotationsNodeAggregateSelection = {
  __typename?: 'PlaceAnnotationAnnotationsNodeAggregateSelection';
  author: StringAggregateSelectionNonNullable;
  comment: StringAggregateSelectionNullable;
  createdAt: DateTimeAggregateSelectionNonNullable;
  id: IdAggregateSelectionNonNullable;
  updatedAt: DateTimeAggregateSelectionNonNullable;
};

export type PlaceAnnotationsAggregateInput = {
  AND?: InputMaybe<Array<PlaceAnnotationsAggregateInput>>;
  NOT?: InputMaybe<PlaceAnnotationsAggregateInput>;
  OR?: InputMaybe<Array<PlaceAnnotationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PlaceAnnotationsNodeAggregationWhereInput>;
};

export type PlaceAnnotationsConnection = {
  __typename?: 'PlaceAnnotationsConnection';
  edges: Array<PlaceAnnotationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceAnnotationsConnectionSort = {
  node?: InputMaybe<AnnotationSort>;
};

export type PlaceAnnotationsConnectionWhere = {
  AND?: InputMaybe<Array<PlaceAnnotationsConnectionWhere>>;
  NOT?: InputMaybe<PlaceAnnotationsConnectionWhere>;
  OR?: InputMaybe<Array<PlaceAnnotationsConnectionWhere>>;
  node?: InputMaybe<AnnotationWhere>;
};

export type PlaceAnnotationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceAnnotationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceAnnotationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceAnnotationsNodeAggregationWhereInput>>;
  author_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  author_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  author_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  author_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  comment_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  comment_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  comment_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  createdAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  createdAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MAX_LTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_EQUAL?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_GTE?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LT?: InputMaybe<Scalars['DateTime']>;
  updatedAt_MIN_LTE?: InputMaybe<Scalars['DateTime']>;
};

export type PlaceAnnotationsRelationship = {
  __typename?: 'PlaceAnnotationsRelationship';
  cursor: Scalars['String'];
  node: Annotation;
};

export type PlaceDataSourceDataSourcesAggregationSelection = {
  __typename?: 'PlaceDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PlaceDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<PlaceDataSourceDataSourcesNodeAggregateSelection>;
};

export type PlaceDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'PlaceDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type PlaceDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'PlaceDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PlaceDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<PlaceDataSourcesAggregateInput>>;
  NOT?: InputMaybe<PlaceDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<PlaceDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PlaceDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<PlaceDataSourcesNodeAggregationWhereInput>;
};

export type PlaceDataSourcesConnection = {
  __typename?: 'PlaceDataSourcesConnection';
  edges: Array<PlaceDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type PlaceDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<PlaceDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<PlaceDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<PlaceDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type PlaceDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceDataSourcesRelationship = ImportedFrom & {
  __typename?: 'PlaceDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type PlaceEdge = {
  __typename?: 'PlaceEdge';
  cursor: Scalars['String'];
  node: Place;
};

export type PlaceFactoidFactoidsAggregationSelection = {
  __typename?: 'PlaceFactoidFactoidsAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PlaceFactoidFactoidsEdgeAggregateSelection>;
  node?: Maybe<PlaceFactoidFactoidsNodeAggregateSelection>;
};

export type PlaceFactoidFactoidsEdgeAggregateSelection = {
  __typename?: 'PlaceFactoidFactoidsEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type PlaceFactoidFactoidsNodeAggregateSelection = {
  __typename?: 'PlaceFactoidFactoidsNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  duration: DurationAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  originalText: StringAggregateSelectionNullable;
};

export type PlaceFactoidsAggregateInput = {
  AND?: InputMaybe<Array<PlaceFactoidsAggregateInput>>;
  NOT?: InputMaybe<PlaceFactoidsAggregateInput>;
  OR?: InputMaybe<Array<PlaceFactoidsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PlaceFactoidsEdgeAggregationWhereInput>;
  node?: InputMaybe<PlaceFactoidsNodeAggregationWhereInput>;
};

export type PlaceFactoidsConnection = {
  __typename?: 'PlaceFactoidsConnection';
  edges: Array<PlaceFactoidsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceFactoidsConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<FactoidSort>;
};

export type PlaceFactoidsConnectionWhere = {
  AND?: InputMaybe<Array<PlaceFactoidsConnectionWhere>>;
  NOT?: InputMaybe<PlaceFactoidsConnectionWhere>;
  OR?: InputMaybe<Array<PlaceFactoidsConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<FactoidWhere>;
};

export type PlaceFactoidsEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceFactoidsEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceFactoidsEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceFactoidsEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PlaceFactoidsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceFactoidsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceFactoidsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceFactoidsNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  duration_AVERAGE_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GTE?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LTE?: InputMaybe<Scalars['Duration']>;
  originalText_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalText_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceFactoidsRelationship = Certainty & {
  __typename?: 'PlaceFactoidsRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Factoid;
};

export type PlaceLocatedInAggregateInput = {
  AND?: InputMaybe<Array<PlaceLocatedInAggregateInput>>;
  NOT?: InputMaybe<PlaceLocatedInAggregateInput>;
  OR?: InputMaybe<Array<PlaceLocatedInAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PlaceLocatedInNodeAggregationWhereInput>;
};

export type PlaceLocatedInConnection = {
  __typename?: 'PlaceLocatedInConnection';
  edges: Array<PlaceLocatedInRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceLocatedInConnectionSort = {
  node?: InputMaybe<PlaceZoneOverTimeSort>;
};

export type PlaceLocatedInConnectionWhere = {
  AND?: InputMaybe<Array<PlaceLocatedInConnectionWhere>>;
  NOT?: InputMaybe<PlaceLocatedInConnectionWhere>;
  OR?: InputMaybe<Array<PlaceLocatedInConnectionWhere>>;
  node?: InputMaybe<PlaceZoneOverTimeWhere>;
};

export type PlaceLocatedInNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceLocatedInNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceLocatedInNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceLocatedInNodeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type PlaceLocatedInRelationship = {
  __typename?: 'PlaceLocatedInRelationship';
  cursor: Scalars['String'];
  node: PlaceZoneOverTime;
};

export type PlaceOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more PlaceSort objects to sort Places by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<PlaceSort>>;
};

export type PlacePlaceZoneOverTimeLocatedInAggregationSelection = {
  __typename?: 'PlacePlaceZoneOverTimeLocatedInAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PlacePlaceZoneOverTimeLocatedInNodeAggregateSelection>;
};

export type PlacePlaceZoneOverTimeLocatedInNodeAggregateSelection = {
  __typename?: 'PlacePlaceZoneOverTimeLocatedInNodeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

/** Fields to sort Places by. The order in which sorts are applied is not guaranteed when specifying many fields in one PlaceSort object. */
export type PlaceSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type PlaceWhere = {
  AND?: InputMaybe<Array<PlaceWhere>>;
  NOT?: InputMaybe<PlaceWhere>;
  OR?: InputMaybe<Array<PlaceWhere>>;
  alternativeNames?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  alternativeNames_INCLUDES?: InputMaybe<Scalars['String']>;
  annotationsAggregate?: InputMaybe<PlaceAnnotationsAggregateInput>;
  /** Return Places where all of the related PlaceAnnotationsConnections match this filter */
  annotationsConnection_ALL?: InputMaybe<PlaceAnnotationsConnectionWhere>;
  /** Return Places where none of the related PlaceAnnotationsConnections match this filter */
  annotationsConnection_NONE?: InputMaybe<PlaceAnnotationsConnectionWhere>;
  /** Return Places where one of the related PlaceAnnotationsConnections match this filter */
  annotationsConnection_SINGLE?: InputMaybe<PlaceAnnotationsConnectionWhere>;
  /** Return Places where some of the related PlaceAnnotationsConnections match this filter */
  annotationsConnection_SOME?: InputMaybe<PlaceAnnotationsConnectionWhere>;
  /** Return Places where all of the related Annotations match this filter */
  annotations_ALL?: InputMaybe<AnnotationWhere>;
  /** Return Places where none of the related Annotations match this filter */
  annotations_NONE?: InputMaybe<AnnotationWhere>;
  /** Return Places where one of the related Annotations match this filter */
  annotations_SINGLE?: InputMaybe<AnnotationWhere>;
  /** Return Places where some of the related Annotations match this filter */
  annotations_SOME?: InputMaybe<AnnotationWhere>;
  dataSourcesAggregate?: InputMaybe<PlaceDataSourcesAggregateInput>;
  /** Return Places where all of the related PlaceDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<PlaceDataSourcesConnectionWhere>;
  /** Return Places where none of the related PlaceDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<PlaceDataSourcesConnectionWhere>;
  /** Return Places where one of the related PlaceDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<PlaceDataSourcesConnectionWhere>;
  /** Return Places where some of the related PlaceDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<PlaceDataSourcesConnectionWhere>;
  /** Return Places where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Places where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Places where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Places where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  factoidsAggregate?: InputMaybe<PlaceFactoidsAggregateInput>;
  /** Return Places where all of the related PlaceFactoidsConnections match this filter */
  factoidsConnection_ALL?: InputMaybe<PlaceFactoidsConnectionWhere>;
  /** Return Places where none of the related PlaceFactoidsConnections match this filter */
  factoidsConnection_NONE?: InputMaybe<PlaceFactoidsConnectionWhere>;
  /** Return Places where one of the related PlaceFactoidsConnections match this filter */
  factoidsConnection_SINGLE?: InputMaybe<PlaceFactoidsConnectionWhere>;
  /** Return Places where some of the related PlaceFactoidsConnections match this filter */
  factoidsConnection_SOME?: InputMaybe<PlaceFactoidsConnectionWhere>;
  /** Return Places where all of the related Factoids match this filter */
  factoids_ALL?: InputMaybe<FactoidWhere>;
  /** Return Places where none of the related Factoids match this filter */
  factoids_NONE?: InputMaybe<FactoidWhere>;
  /** Return Places where one of the related Factoids match this filter */
  factoids_SINGLE?: InputMaybe<FactoidWhere>;
  /** Return Places where some of the related Factoids match this filter */
  factoids_SOME?: InputMaybe<FactoidWhere>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  locatedInAggregate?: InputMaybe<PlaceLocatedInAggregateInput>;
  /** Return Places where all of the related PlaceLocatedInConnections match this filter */
  locatedInConnection_ALL?: InputMaybe<PlaceLocatedInConnectionWhere>;
  /** Return Places where none of the related PlaceLocatedInConnections match this filter */
  locatedInConnection_NONE?: InputMaybe<PlaceLocatedInConnectionWhere>;
  /** Return Places where one of the related PlaceLocatedInConnections match this filter */
  locatedInConnection_SINGLE?: InputMaybe<PlaceLocatedInConnectionWhere>;
  /** Return Places where some of the related PlaceLocatedInConnections match this filter */
  locatedInConnection_SOME?: InputMaybe<PlaceLocatedInConnectionWhere>;
  /** Return Places where all of the related PlaceZoneOverTimes match this filter */
  locatedIn_ALL?: InputMaybe<PlaceZoneOverTimeWhere>;
  /** Return Places where none of the related PlaceZoneOverTimes match this filter */
  locatedIn_NONE?: InputMaybe<PlaceZoneOverTimeWhere>;
  /** Return Places where one of the related PlaceZoneOverTimes match this filter */
  locatedIn_SINGLE?: InputMaybe<PlaceZoneOverTimeWhere>;
  /** Return Places where some of the related PlaceZoneOverTimes match this filter */
  locatedIn_SOME?: InputMaybe<PlaceZoneOverTimeWhere>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTime = {
  __typename?: 'PlaceZoneOverTime';
  certainty: Scalars['Float'];
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<PlaceZoneOverTimeDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: PlaceZoneOverTimeDataSourcesConnection;
  date: DateValue;
  dateAggregate?: Maybe<PlaceZoneOverTimeDateValueDateAggregationSelection>;
  dateConnection: PlaceZoneOverTimeDateConnection;
  place: Place;
  placeAggregate?: Maybe<PlaceZoneOverTimePlacePlaceAggregationSelection>;
  placeConnection: PlaceZoneOverTimePlaceConnection;
  zone: Zone;
  zoneAggregate?: Maybe<PlaceZoneOverTimeZoneZoneAggregationSelection>;
  zoneConnection: PlaceZoneOverTimeZoneConnection;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesConnectionSort>>;
  where?: InputMaybe<PlaceZoneOverTimeDataSourcesConnectionWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeDateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DateValueOptions>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeDateAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeDateConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceZoneOverTimeDateConnectionSort>>;
  where?: InputMaybe<PlaceZoneOverTimeDateConnectionWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimePlaceArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<PlaceOptions>;
  where?: InputMaybe<PlaceWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimePlaceAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<PlaceWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimePlaceConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceZoneOverTimePlaceConnectionSort>>;
  where?: InputMaybe<PlaceZoneOverTimePlaceConnectionWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeZoneArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ZoneOptions>;
  where?: InputMaybe<ZoneWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeZoneAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ZoneWhere>;
};


/**
 * PlaceZoneOverTime
 * -----------------
 * A place can be part of multiple Zone in time.
 * For example, the Corse in France.
 */
export type PlaceZoneOverTimeZoneConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<PlaceZoneOverTimeZoneConnectionSort>>;
  where?: InputMaybe<PlaceZoneOverTimeZoneConnectionWhere>;
};

export type PlaceZoneOverTimeAggregateSelection = {
  __typename?: 'PlaceZoneOverTimeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
  count: Scalars['Int'];
};

export type PlaceZoneOverTimeDataSourceDataSourcesAggregationSelection = {
  __typename?: 'PlaceZoneOverTimeDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<PlaceZoneOverTimeDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<PlaceZoneOverTimeDataSourceDataSourcesNodeAggregateSelection>;
};

export type PlaceZoneOverTimeDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'PlaceZoneOverTimeDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type PlaceZoneOverTimeDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'PlaceZoneOverTimeDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PlaceZoneOverTimeDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesAggregateInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<PlaceZoneOverTimeDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<PlaceZoneOverTimeDataSourcesNodeAggregationWhereInput>;
};

export type PlaceZoneOverTimeDataSourcesConnection = {
  __typename?: 'PlaceZoneOverTimeDataSourcesConnection';
  edges: Array<PlaceZoneOverTimeDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceZoneOverTimeDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type PlaceZoneOverTimeDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type PlaceZoneOverTimeDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceZoneOverTimeDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceZoneOverTimeDataSourcesRelationship = ImportedFrom & {
  __typename?: 'PlaceZoneOverTimeDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type PlaceZoneOverTimeDateAggregateInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDateAggregateInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDateAggregateInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDateAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PlaceZoneOverTimeDateNodeAggregationWhereInput>;
};

export type PlaceZoneOverTimeDateConnection = {
  __typename?: 'PlaceZoneOverTimeDateConnection';
  edges: Array<PlaceZoneOverTimeDateRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceZoneOverTimeDateConnectionSort = {
  node?: InputMaybe<DateValueSort>;
};

export type PlaceZoneOverTimeDateConnectionWhere = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDateConnectionWhere>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDateConnectionWhere>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDateConnectionWhere>>;
  node?: InputMaybe<DateValueWhere>;
};

export type PlaceZoneOverTimeDateNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeDateNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeDateNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeDateNodeAggregationWhereInput>>;
  endIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  endIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  startIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceZoneOverTimeDateRelationship = {
  __typename?: 'PlaceZoneOverTimeDateRelationship';
  cursor: Scalars['String'];
  node: DateValue;
};

export type PlaceZoneOverTimeDateValueDateAggregationSelection = {
  __typename?: 'PlaceZoneOverTimeDateValueDateAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PlaceZoneOverTimeDateValueDateNodeAggregateSelection>;
};

export type PlaceZoneOverTimeDateValueDateNodeAggregateSelection = {
  __typename?: 'PlaceZoneOverTimeDateValueDateNodeAggregateSelection';
  endIso: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  startIso: StringAggregateSelectionNullable;
};

export type PlaceZoneOverTimeEdge = {
  __typename?: 'PlaceZoneOverTimeEdge';
  cursor: Scalars['String'];
  node: PlaceZoneOverTime;
};

export type PlaceZoneOverTimeOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more PlaceZoneOverTimeSort objects to sort PlaceZoneOverTimes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<PlaceZoneOverTimeSort>>;
};

export type PlaceZoneOverTimePlaceAggregateInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimePlaceAggregateInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimePlaceAggregateInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimePlaceAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PlaceZoneOverTimePlaceNodeAggregationWhereInput>;
};

export type PlaceZoneOverTimePlaceConnection = {
  __typename?: 'PlaceZoneOverTimePlaceConnection';
  edges: Array<PlaceZoneOverTimePlaceRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceZoneOverTimePlaceConnectionSort = {
  node?: InputMaybe<PlaceSort>;
};

export type PlaceZoneOverTimePlaceConnectionWhere = {
  AND?: InputMaybe<Array<PlaceZoneOverTimePlaceConnectionWhere>>;
  NOT?: InputMaybe<PlaceZoneOverTimePlaceConnectionWhere>;
  OR?: InputMaybe<Array<PlaceZoneOverTimePlaceConnectionWhere>>;
  node?: InputMaybe<PlaceWhere>;
};

export type PlaceZoneOverTimePlaceNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimePlaceNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimePlaceNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimePlaceNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceZoneOverTimePlacePlaceAggregationSelection = {
  __typename?: 'PlaceZoneOverTimePlacePlaceAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PlaceZoneOverTimePlacePlaceNodeAggregateSelection>;
};

export type PlaceZoneOverTimePlacePlaceNodeAggregateSelection = {
  __typename?: 'PlaceZoneOverTimePlacePlaceNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type PlaceZoneOverTimePlaceRelationship = {
  __typename?: 'PlaceZoneOverTimePlaceRelationship';
  cursor: Scalars['String'];
  node: Place;
};

/** Fields to sort PlaceZoneOverTimes by. The order in which sorts are applied is not guaranteed when specifying many fields in one PlaceZoneOverTimeSort object. */
export type PlaceZoneOverTimeSort = {
  certainty?: InputMaybe<SortDirection>;
};

export type PlaceZoneOverTimeWhere = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeWhere>>;
  NOT?: InputMaybe<PlaceZoneOverTimeWhere>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeWhere>>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<Scalars['Float']>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
  dataSourcesAggregate?: InputMaybe<PlaceZoneOverTimeDataSourcesAggregateInput>;
  /** Return PlaceZoneOverTimes where all of the related PlaceZoneOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<PlaceZoneOverTimeDataSourcesConnectionWhere>;
  /** Return PlaceZoneOverTimes where none of the related PlaceZoneOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<PlaceZoneOverTimeDataSourcesConnectionWhere>;
  /** Return PlaceZoneOverTimes where one of the related PlaceZoneOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<PlaceZoneOverTimeDataSourcesConnectionWhere>;
  /** Return PlaceZoneOverTimes where some of the related PlaceZoneOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<PlaceZoneOverTimeDataSourcesConnectionWhere>;
  /** Return PlaceZoneOverTimes where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return PlaceZoneOverTimes where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return PlaceZoneOverTimes where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return PlaceZoneOverTimes where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  date?: InputMaybe<DateValueWhere>;
  dateAggregate?: InputMaybe<PlaceZoneOverTimeDateAggregateInput>;
  dateConnection?: InputMaybe<PlaceZoneOverTimeDateConnectionWhere>;
  dateConnection_NOT?: InputMaybe<PlaceZoneOverTimeDateConnectionWhere>;
  date_NOT?: InputMaybe<DateValueWhere>;
  place?: InputMaybe<PlaceWhere>;
  placeAggregate?: InputMaybe<PlaceZoneOverTimePlaceAggregateInput>;
  placeConnection?: InputMaybe<PlaceZoneOverTimePlaceConnectionWhere>;
  placeConnection_NOT?: InputMaybe<PlaceZoneOverTimePlaceConnectionWhere>;
  place_NOT?: InputMaybe<PlaceWhere>;
  zone?: InputMaybe<ZoneWhere>;
  zoneAggregate?: InputMaybe<PlaceZoneOverTimeZoneAggregateInput>;
  zoneConnection?: InputMaybe<PlaceZoneOverTimeZoneConnectionWhere>;
  zoneConnection_NOT?: InputMaybe<PlaceZoneOverTimeZoneConnectionWhere>;
  zone_NOT?: InputMaybe<ZoneWhere>;
};

export type PlaceZoneOverTimeZoneAggregateInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeZoneAggregateInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeZoneAggregateInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeZoneAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<PlaceZoneOverTimeZoneNodeAggregationWhereInput>;
};

export type PlaceZoneOverTimeZoneConnection = {
  __typename?: 'PlaceZoneOverTimeZoneConnection';
  edges: Array<PlaceZoneOverTimeZoneRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlaceZoneOverTimeZoneConnectionSort = {
  node?: InputMaybe<ZoneSort>;
};

export type PlaceZoneOverTimeZoneConnectionWhere = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeZoneConnectionWhere>>;
  NOT?: InputMaybe<PlaceZoneOverTimeZoneConnectionWhere>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeZoneConnectionWhere>>;
  node?: InputMaybe<ZoneWhere>;
};

export type PlaceZoneOverTimeZoneNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<PlaceZoneOverTimeZoneNodeAggregationWhereInput>>;
  NOT?: InputMaybe<PlaceZoneOverTimeZoneNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<PlaceZoneOverTimeZoneNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type PlaceZoneOverTimeZoneRelationship = {
  __typename?: 'PlaceZoneOverTimeZoneRelationship';
  cursor: Scalars['String'];
  node: Zone;
};

export type PlaceZoneOverTimeZoneZoneAggregationSelection = {
  __typename?: 'PlaceZoneOverTimeZoneZoneAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<PlaceZoneOverTimeZoneZoneNodeAggregateSelection>;
};

export type PlaceZoneOverTimeZoneZoneNodeAggregateSelection = {
  __typename?: 'PlaceZoneOverTimeZoneZoneNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
};

export type PlaceZoneOverTimesConnection = {
  __typename?: 'PlaceZoneOverTimesConnection';
  edges: Array<PlaceZoneOverTimeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PlacesConnection = {
  __typename?: 'PlacesConnection';
  edges: Array<PlaceEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  /** DON'T use this method, it's only available internally */
  _getFactoidTypes: Array<FactoidType>;
  /** DON'T use this method, it's only available internally */
  _getFactoids: Array<Factoid>;
  /** DON'T use this method, it's only available internally */
  _getPhysicalPersons: Array<PhysicalPerson>;
  /** DON'T use this method, it's only available internally */
  _getPlaces: Array<Place>;
  /** DON'T use this method, it's only available internally */
  _getRoles: Array<Role>;
  /** DON'T use this method, it's only available internally */
  _getUsers: Array<User>;
  annotations: Array<Annotation>;
  annotationsAggregate: AnnotationAggregateSelection;
  annotationsConnection: AnnotationsConnection;
  dataSources: Array<DataSource>;
  dataSourcesAggregate: DataSourceAggregateSelection;
  dataSourcesConnection: DataSourcesConnection;
  dateValues: Array<DateValue>;
  dateValuesAggregate: DateValueAggregateSelection;
  dateValuesConnection: DateValuesConnection;
  domains: Array<Domain>;
  domainsAggregate: DomainAggregateSelection;
  domainsConnection: DomainsConnection;
  editions: Array<Edition>;
  editionsAggregate: EditionAggregateSelection;
  editionsConnection: EditionsConnection;
  factoidPersonParticipates: Array<FactoidPersonParticipate>;
  factoidPersonParticipatesAggregate: FactoidPersonParticipateAggregateSelection;
  factoidPersonParticipatesConnection: FactoidPersonParticipatesConnection;
  factoidTypes: Array<FactoidType>;
  factoidTypesAggregate: FactoidTypeAggregateSelection;
  factoidTypesConnection: FactoidTypesConnection;
  /** Search a factoid type */
  factoidTypesSearch: SearchFactoidTypesResult;
  factoids: Array<Factoid>;
  factoidsAggregate: FactoidAggregateSelection;
  factoidsConnection: FactoidsConnection;
  /** Search a factoid */
  factoidsSearch: SearchFactoidsResult;
  /** Search for various types at the same time */
  graphSearch: GraphSearchResult;
  groups: Array<Group>;
  groupsAggregate: GroupAggregateSelection;
  groupsConnection: GroupsConnection;
  inquiries: Array<Inquiry>;
  inquiriesAggregate: InquiryAggregateSelection;
  inquiriesConnection: InquiriesConnection;
  /** Inquiry: get all annotations of the inquiry */
  inquiryAnnotations: Array<Annotation>;
  moralEntities: Array<MoralEntity>;
  moralEntitiesAggregate: MoralEntityAggregateSelection;
  moralEntitiesConnection: MoralEntitiesConnection;
  objectTypes: Array<ObjectType>;
  objectTypesAggregate: ObjectTypeAggregateSelection;
  objectTypesConnection: ObjectTypesConnection;
  objects: Array<Object>;
  objectsAggregate: ObjectAggregateSelection;
  objectsConnection: ObjectsConnection;
  personNames: Array<PersonName>;
  personNamesAggregate: PersonNameAggregateSelection;
  personNamesConnection: PersonNamesConnection;
  physicalPeople: Array<PhysicalPerson>;
  physicalPeopleAggregate: PhysicalPersonAggregateSelection;
  physicalPeopleConnection: PhysicalPeopleConnection;
  /** Search a PhysicalPerson */
  physicalPersonsSearch: SearchPhysicalPersonsResult;
  placeZoneOverTimes: Array<PlaceZoneOverTime>;
  placeZoneOverTimesAggregate: PlaceZoneOverTimeAggregateSelection;
  placeZoneOverTimesConnection: PlaceZoneOverTimesConnection;
  places: Array<Place>;
  placesAggregate: PlaceAggregateSelection;
  placesConnection: PlacesConnection;
  /** Search a place */
  placesSearch: SearchPlacesResult;
  ranks: Array<Rank>;
  ranksAggregate: RankAggregateSelection;
  ranksConnection: RanksConnection;
  roles: Array<Role>;
  rolesAggregate: RoleAggregateSelection;
  rolesConnection: RolesConnection;
  /** Search a role */
  rolesSearch: SearchRolesResult;
  sourceTypes: Array<SourceType>;
  sourceTypesAggregate: SourceTypeAggregateSelection;
  sourceTypesConnection: SourceTypesConnection;
  sources: Array<Source>;
  sourcesAggregate: SourceAggregateSelection;
  sourcesConnection: SourcesConnection;
  /** Return the connected user */
  whoami: User;
  zonePartOfOverTimes: Array<ZonePartOfOverTime>;
  zonePartOfOverTimesAggregate: ZonePartOfOverTimeAggregateSelection;
  zonePartOfOverTimesConnection: ZonePartOfOverTimesConnection;
  zones: Array<Zone>;
  zonesAggregate: ZoneAggregateSelection;
  zonesConnection: ZonesConnection;
};


export type QueryAnnotationsArgs = {
  options?: InputMaybe<AnnotationOptions>;
  where?: InputMaybe<AnnotationWhere>;
};


export type QueryAnnotationsAggregateArgs = {
  where?: InputMaybe<AnnotationWhere>;
};


export type QueryAnnotationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<AnnotationSort>>>;
  where?: InputMaybe<AnnotationWhere>;
};


export type QueryDataSourcesArgs = {
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


export type QueryDataSourcesAggregateArgs = {
  where?: InputMaybe<DataSourceWhere>;
};


export type QueryDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<DataSourceSort>>>;
  where?: InputMaybe<DataSourceWhere>;
};


export type QueryDateValuesArgs = {
  options?: InputMaybe<DateValueOptions>;
  where?: InputMaybe<DateValueWhere>;
};


export type QueryDateValuesAggregateArgs = {
  where?: InputMaybe<DateValueWhere>;
};


export type QueryDateValuesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<DateValueSort>>>;
  where?: InputMaybe<DateValueWhere>;
};


export type QueryDomainsArgs = {
  options?: InputMaybe<DomainOptions>;
  where?: InputMaybe<DomainWhere>;
};


export type QueryDomainsAggregateArgs = {
  where?: InputMaybe<DomainWhere>;
};


export type QueryDomainsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<DomainSort>>>;
  where?: InputMaybe<DomainWhere>;
};


export type QueryEditionsArgs = {
  options?: InputMaybe<EditionOptions>;
  where?: InputMaybe<EditionWhere>;
};


export type QueryEditionsAggregateArgs = {
  where?: InputMaybe<EditionWhere>;
};


export type QueryEditionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<EditionSort>>>;
  where?: InputMaybe<EditionWhere>;
};


export type QueryFactoidPersonParticipatesArgs = {
  options?: InputMaybe<FactoidPersonParticipateOptions>;
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


export type QueryFactoidPersonParticipatesAggregateArgs = {
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


export type QueryFactoidPersonParticipatesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<FactoidPersonParticipateSort>>>;
  where?: InputMaybe<FactoidPersonParticipateWhere>;
};


export type QueryFactoidTypesArgs = {
  options?: InputMaybe<FactoidTypeOptions>;
  where?: InputMaybe<FactoidTypeWhere>;
};


export type QueryFactoidTypesAggregateArgs = {
  where?: InputMaybe<FactoidTypeWhere>;
};


export type QueryFactoidTypesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<FactoidTypeSort>>>;
  where?: InputMaybe<FactoidTypeWhere>;
};


export type QueryFactoidTypesSearchArgs = {
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryFactoidsArgs = {
  options?: InputMaybe<FactoidOptions>;
  where?: InputMaybe<FactoidWhere>;
};


export type QueryFactoidsAggregateArgs = {
  where?: InputMaybe<FactoidWhere>;
};


export type QueryFactoidsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<FactoidSort>>>;
  where?: InputMaybe<FactoidWhere>;
};


export type QueryFactoidsSearchArgs = {
  aggregations?: InputMaybe<Array<SearchAggregationInput>>;
  filters?: InputMaybe<Array<SearchFilterInput>>;
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryGraphSearchArgs = {
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGroupsArgs = {
  options?: InputMaybe<GroupOptions>;
  where?: InputMaybe<GroupWhere>;
};


export type QueryGroupsAggregateArgs = {
  where?: InputMaybe<GroupWhere>;
};


export type QueryGroupsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<GroupSort>>>;
  where?: InputMaybe<GroupWhere>;
};


export type QueryInquiriesArgs = {
  options?: InputMaybe<InquiryOptions>;
  where?: InputMaybe<InquiryWhere>;
};


export type QueryInquiriesAggregateArgs = {
  where?: InputMaybe<InquiryWhere>;
};


export type QueryInquiriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<InquirySort>>>;
  where?: InputMaybe<InquiryWhere>;
};


export type QueryInquiryAnnotationsArgs = {
  id: Scalars['ID'];
};


export type QueryMoralEntitiesArgs = {
  options?: InputMaybe<MoralEntityOptions>;
  where?: InputMaybe<MoralEntityWhere>;
};


export type QueryMoralEntitiesAggregateArgs = {
  where?: InputMaybe<MoralEntityWhere>;
};


export type QueryMoralEntitiesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<MoralEntitySort>>>;
  where?: InputMaybe<MoralEntityWhere>;
};


export type QueryObjectTypesArgs = {
  options?: InputMaybe<ObjectTypeOptions>;
  where?: InputMaybe<ObjectTypeWhere>;
};


export type QueryObjectTypesAggregateArgs = {
  where?: InputMaybe<ObjectTypeWhere>;
};


export type QueryObjectTypesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<ObjectTypeSort>>>;
  where?: InputMaybe<ObjectTypeWhere>;
};


export type QueryObjectsArgs = {
  options?: InputMaybe<ObjectOptions>;
  where?: InputMaybe<ObjectWhere>;
};


export type QueryObjectsAggregateArgs = {
  where?: InputMaybe<ObjectWhere>;
};


export type QueryObjectsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<ObjectSort>>>;
  where?: InputMaybe<ObjectWhere>;
};


export type QueryPersonNamesArgs = {
  options?: InputMaybe<PersonNameOptions>;
  where?: InputMaybe<PersonNameWhere>;
};


export type QueryPersonNamesAggregateArgs = {
  where?: InputMaybe<PersonNameWhere>;
};


export type QueryPersonNamesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<PersonNameSort>>>;
  where?: InputMaybe<PersonNameWhere>;
};


export type QueryPhysicalPeopleArgs = {
  options?: InputMaybe<PhysicalPersonOptions>;
  where?: InputMaybe<PhysicalPersonWhere>;
};


export type QueryPhysicalPeopleAggregateArgs = {
  where?: InputMaybe<PhysicalPersonWhere>;
};


export type QueryPhysicalPeopleConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<PhysicalPersonSort>>>;
  where?: InputMaybe<PhysicalPersonWhere>;
};


export type QueryPhysicalPersonsSearchArgs = {
  aggregations?: InputMaybe<Array<SearchAggregationInput>>;
  filters?: InputMaybe<Array<SearchFilterInput>>;
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryPlaceZoneOverTimesArgs = {
  options?: InputMaybe<PlaceZoneOverTimeOptions>;
  where?: InputMaybe<PlaceZoneOverTimeWhere>;
};


export type QueryPlaceZoneOverTimesAggregateArgs = {
  where?: InputMaybe<PlaceZoneOverTimeWhere>;
};


export type QueryPlaceZoneOverTimesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<PlaceZoneOverTimeSort>>>;
  where?: InputMaybe<PlaceZoneOverTimeWhere>;
};


export type QueryPlacesArgs = {
  options?: InputMaybe<PlaceOptions>;
  where?: InputMaybe<PlaceWhere>;
};


export type QueryPlacesAggregateArgs = {
  where?: InputMaybe<PlaceWhere>;
};


export type QueryPlacesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<PlaceSort>>>;
  where?: InputMaybe<PlaceWhere>;
};


export type QueryPlacesSearchArgs = {
  aggregations?: InputMaybe<Array<SearchAggregationInput>>;
  filters?: InputMaybe<Array<SearchFilterInput>>;
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QueryRanksArgs = {
  options?: InputMaybe<RankOptions>;
  where?: InputMaybe<RankWhere>;
};


export type QueryRanksAggregateArgs = {
  where?: InputMaybe<RankWhere>;
};


export type QueryRanksConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<RankSort>>>;
  where?: InputMaybe<RankWhere>;
};


export type QueryRolesArgs = {
  options?: InputMaybe<RoleOptions>;
  where?: InputMaybe<RoleWhere>;
};


export type QueryRolesAggregateArgs = {
  where?: InputMaybe<RoleWhere>;
};


export type QueryRolesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<RoleSort>>>;
  where?: InputMaybe<RoleWhere>;
};


export type QueryRolesSearchArgs = {
  inquiryId: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<SearchSortInput>;
};


export type QuerySourceTypesArgs = {
  options?: InputMaybe<SourceTypeOptions>;
  where?: InputMaybe<SourceTypeWhere>;
};


export type QuerySourceTypesAggregateArgs = {
  where?: InputMaybe<SourceTypeWhere>;
};


export type QuerySourceTypesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<SourceTypeSort>>>;
  where?: InputMaybe<SourceTypeWhere>;
};


export type QuerySourcesArgs = {
  options?: InputMaybe<SourceOptions>;
  where?: InputMaybe<SourceWhere>;
};


export type QuerySourcesAggregateArgs = {
  where?: InputMaybe<SourceWhere>;
};


export type QuerySourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<SourceSort>>>;
  where?: InputMaybe<SourceWhere>;
};


export type QueryZonePartOfOverTimesArgs = {
  options?: InputMaybe<ZonePartOfOverTimeOptions>;
  where?: InputMaybe<ZonePartOfOverTimeWhere>;
};


export type QueryZonePartOfOverTimesAggregateArgs = {
  where?: InputMaybe<ZonePartOfOverTimeWhere>;
};


export type QueryZonePartOfOverTimesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<ZonePartOfOverTimeSort>>>;
  where?: InputMaybe<ZonePartOfOverTimeWhere>;
};


export type QueryZonesArgs = {
  options?: InputMaybe<ZoneOptions>;
  where?: InputMaybe<ZoneWhere>;
};


export type QueryZonesAggregateArgs = {
  where?: InputMaybe<ZoneWhere>;
};


export type QueryZonesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<InputMaybe<ZoneSort>>>;
  where?: InputMaybe<ZoneWhere>;
};

export type QueryOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
};

/**
 * Rank
 * ----
 */
export type Rank = {
  __typename?: 'Rank';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<RankDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: RankDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};


/**
 * Rank
 * ----
 */
export type RankDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Rank
 * ----
 */
export type RankDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Rank
 * ----
 */
export type RankDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<RankDataSourcesConnectionSort>>;
  where?: InputMaybe<RankDataSourcesConnectionWhere>;
};

export type RankAggregateSelection = {
  __typename?: 'RankAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type RankDataSourceDataSourcesAggregationSelection = {
  __typename?: 'RankDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<RankDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<RankDataSourceDataSourcesNodeAggregateSelection>;
};

export type RankDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'RankDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type RankDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'RankDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type RankDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<RankDataSourcesAggregateInput>>;
  NOT?: InputMaybe<RankDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<RankDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<RankDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<RankDataSourcesNodeAggregationWhereInput>;
};

export type RankDataSourcesConnection = {
  __typename?: 'RankDataSourcesConnection';
  edges: Array<RankDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type RankDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type RankDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<RankDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<RankDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<RankDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type RankDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<RankDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<RankDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<RankDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type RankDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<RankDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<RankDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<RankDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type RankDataSourcesRelationship = ImportedFrom & {
  __typename?: 'RankDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type RankEdge = {
  __typename?: 'RankEdge';
  cursor: Scalars['String'];
  node: Rank;
};

export type RankOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more RankSort objects to sort Ranks by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<RankSort>>;
};

/** Fields to sort Ranks by. The order in which sorts are applied is not guaranteed when specifying many fields in one RankSort object. */
export type RankSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type RankWhere = {
  AND?: InputMaybe<Array<RankWhere>>;
  NOT?: InputMaybe<RankWhere>;
  OR?: InputMaybe<Array<RankWhere>>;
  dataSourcesAggregate?: InputMaybe<RankDataSourcesAggregateInput>;
  /** Return Ranks where all of the related RankDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<RankDataSourcesConnectionWhere>;
  /** Return Ranks where none of the related RankDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<RankDataSourcesConnectionWhere>;
  /** Return Ranks where one of the related RankDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<RankDataSourcesConnectionWhere>;
  /** Return Ranks where some of the related RankDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<RankDataSourcesConnectionWhere>;
  /** Return Ranks where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Ranks where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Ranks where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Ranks where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type RanksConnection = {
  __typename?: 'RanksConnection';
  edges: Array<RankEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

/**
 * A report generated by the server
 * --------------------------------
 * This type is used mainly for data import
 */
export type Report = {
  __typename?: 'Report';
  errors: Array<Maybe<Scalars['String']>>;
  success: Scalars['Boolean'];
  warnings: Array<Maybe<Scalars['String']>>;
};

/**
 * Role
 * ----
 */
export type Role = {
  __typename?: 'Role';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<RoleDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: RoleDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
};


/**
 * Role
 * ----
 */
export type RoleDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Role
 * ----
 */
export type RoleDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Role
 * ----
 */
export type RoleDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<RoleDataSourcesConnectionSort>>;
  where?: InputMaybe<RoleDataSourcesConnectionWhere>;
};

export type RoleAggregateSelection = {
  __typename?: 'RoleAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type RoleDataSourceDataSourcesAggregationSelection = {
  __typename?: 'RoleDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<RoleDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<RoleDataSourceDataSourcesNodeAggregateSelection>;
};

export type RoleDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'RoleDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type RoleDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'RoleDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type RoleDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<RoleDataSourcesAggregateInput>>;
  NOT?: InputMaybe<RoleDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<RoleDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<RoleDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<RoleDataSourcesNodeAggregationWhereInput>;
};

export type RoleDataSourcesConnection = {
  __typename?: 'RoleDataSourcesConnection';
  edges: Array<RoleDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type RoleDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type RoleDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<RoleDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<RoleDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<RoleDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type RoleDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<RoleDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<RoleDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<RoleDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type RoleDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<RoleDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<RoleDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<RoleDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type RoleDataSourcesRelationship = ImportedFrom & {
  __typename?: 'RoleDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type RoleEdge = {
  __typename?: 'RoleEdge';
  cursor: Scalars['String'];
  node: Role;
};

export type RoleOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more RoleSort objects to sort Roles by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<RoleSort>>;
};

/** Fields to sort Roles by. The order in which sorts are applied is not guaranteed when specifying many fields in one RoleSort object. */
export type RoleSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type RoleWhere = {
  AND?: InputMaybe<Array<RoleWhere>>;
  NOT?: InputMaybe<RoleWhere>;
  OR?: InputMaybe<Array<RoleWhere>>;
  dataSourcesAggregate?: InputMaybe<RoleDataSourcesAggregateInput>;
  /** Return Roles where all of the related RoleDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<RoleDataSourcesConnectionWhere>;
  /** Return Roles where none of the related RoleDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<RoleDataSourcesConnectionWhere>;
  /** Return Roles where one of the related RoleDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<RoleDataSourcesConnectionWhere>;
  /** Return Roles where some of the related RoleDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<RoleDataSourcesConnectionWhere>;
  /** Return Roles where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Roles where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Roles where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Roles where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
};

export type RolesConnection = {
  __typename?: 'RolesConnection';
  edges: Array<RoleEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SearchAggregation = {
  __typename?: 'SearchAggregation';
  name: Scalars['String'];
  total?: Maybe<Scalars['Int']>;
  values: Array<SearchAggregationItem>;
};

export type SearchAggregationInput = {
  field: Scalars['String'];
  id: Scalars['String'];
  include?: InputMaybe<Scalars['String']>;
  nested?: InputMaybe<Scalars['String']>;
  orderByAlpha?: InputMaybe<Scalars['Boolean']>;
  type: AggregationType;
};

export type SearchAggregationItem = {
  __typename?: 'SearchAggregationItem';
  count: Scalars['Int'];
  key: Scalars['String'];
};

export type SearchFactoidTypesResult = {
  __typename?: 'SearchFactoidTypesResult';
  results: Array<FactoidType>;
  total: Scalars['Int'];
};

export type SearchFactoidsResult = {
  __typename?: 'SearchFactoidsResult';
  aggregations: Array<SearchAggregation>;
  results: Array<Factoid>;
  total: Scalars['Int'];
};

export type SearchFilterInput = {
  field: Scalars['String'];
  type?: InputMaybe<FilterType>;
  values: Array<Scalars['JSON']>;
};

export type SearchPhysicalPersonsResult = {
  __typename?: 'SearchPhysicalPersonsResult';
  aggregations: Array<SearchAggregation>;
  results: Array<PhysicalPerson>;
  total: Scalars['Int'];
};

export type SearchPlacesResult = {
  __typename?: 'SearchPlacesResult';
  aggregations: Array<SearchAggregation>;
  results: Array<Place>;
  total: Scalars['Int'];
};

export type SearchRolesResult = {
  __typename?: 'SearchRolesResult';
  results: Array<Role>;
  total: Scalars['Int'];
};

export type SearchSortInput = {
  field: Scalars['String'];
  order: SortOrder;
};

export enum SortDirection {
  /** Sort by field values in ascending order. */
  Asc = 'ASC',
  /** Sort by field values in descending order. */
  Desc = 'DESC'
}

export enum SortOrder {
  Asc = 'ASC',
  Desc = 'DESC'
}

/**
 * Source
 * ------
 */
export type Source = {
  __typename?: 'Source';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<SourceDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: SourceDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  editions: Array<Edition>;
  editionsAggregate?: Maybe<SourceEditionEditionsAggregationSelection>;
  editionsConnection: SourceEditionsConnection;
  id: Scalars['ID'];
  inquiry: Inquiry;
  issuing: Array<PhysicalPerson>;
  issuingAggregate?: Maybe<SourcePhysicalPersonIssuingAggregationSelection>;
  issuingConnection: SourceIssuingConnection;
  language?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  partOf: Array<SourceType>;
  partOfAggregate?: Maybe<SourceSourceTypePartOfAggregationSelection>;
  partOfConnection: SourcePartOfConnection;
  refering: Array<Factoid>;
  referingAggregate?: Maybe<SourceFactoidReferingAggregationSelection>;
  referingConnection: SourceReferingConnection;
  reputation?: Maybe<Scalars['Float']>;
  types: Array<SourceType>;
  typesAggregate?: Maybe<SourceSourceTypeTypesAggregationSelection>;
  typesConnection: SourceTypesConnection;
};


/**
 * Source
 * ------
 */
export type SourceDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Source
 * ------
 */
export type SourceDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Source
 * ------
 */
export type SourceDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceDataSourcesConnectionSort>>;
  where?: InputMaybe<SourceDataSourcesConnectionWhere>;
};


/**
 * Source
 * ------
 */
export type SourceEditionsArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<EditionOptions>;
  where?: InputMaybe<EditionWhere>;
};


/**
 * Source
 * ------
 */
export type SourceEditionsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<EditionWhere>;
};


/**
 * Source
 * ------
 */
export type SourceEditionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceEditionsConnectionSort>>;
  where?: InputMaybe<SourceEditionsConnectionWhere>;
};


/**
 * Source
 * ------
 */
export type SourceIssuingArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<PhysicalPersonOptions>;
  where?: InputMaybe<PhysicalPersonWhere>;
};


/**
 * Source
 * ------
 */
export type SourceIssuingAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<PhysicalPersonWhere>;
};


/**
 * Source
 * ------
 */
export type SourceIssuingConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceIssuingConnectionSort>>;
  where?: InputMaybe<SourceIssuingConnectionWhere>;
};


/**
 * Source
 * ------
 */
export type SourcePartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceTypeOptions>;
  where?: InputMaybe<SourceTypeWhere>;
};


/**
 * Source
 * ------
 */
export type SourcePartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceTypeWhere>;
};


/**
 * Source
 * ------
 */
export type SourcePartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourcePartOfConnectionSort>>;
  where?: InputMaybe<SourcePartOfConnectionWhere>;
};


/**
 * Source
 * ------
 */
export type SourceReferingArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<FactoidOptions>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * Source
 * ------
 */
export type SourceReferingAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<FactoidWhere>;
};


/**
 * Source
 * ------
 */
export type SourceReferingConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceReferingConnectionSort>>;
  where?: InputMaybe<SourceReferingConnectionWhere>;
};


/**
 * Source
 * ------
 */
export type SourceTypesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceTypeOptions>;
  where?: InputMaybe<SourceTypeWhere>;
};


/**
 * Source
 * ------
 */
export type SourceTypesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceTypeWhere>;
};


/**
 * Source
 * ------
 */
export type SourceTypesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceTypesConnectionSort>>;
  where?: InputMaybe<SourceTypesConnectionWhere>;
};

export type SourceAggregateSelection = {
  __typename?: 'SourceAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  language: StringAggregateSelectionNullable;
  name: StringAggregateSelectionNonNullable;
  reputation: FloatAggregateSelectionNullable;
};

export type SourceDataSourceDataSourcesAggregationSelection = {
  __typename?: 'SourceDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<SourceDataSourceDataSourcesNodeAggregateSelection>;
};

export type SourceDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'SourceDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type SourceDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'SourceDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type SourceDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<SourceDataSourcesAggregateInput>>;
  NOT?: InputMaybe<SourceDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<SourceDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceDataSourcesNodeAggregationWhereInput>;
};

export type SourceDataSourcesConnection = {
  __typename?: 'SourceDataSourcesConnection';
  edges: Array<SourceDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type SourceDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<SourceDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<SourceDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<SourceDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type SourceDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceDataSourcesRelationship = ImportedFrom & {
  __typename?: 'SourceDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type SourceEdge = {
  __typename?: 'SourceEdge';
  cursor: Scalars['String'];
  node: Source;
};

export type SourceEditionEditionsAggregationSelection = {
  __typename?: 'SourceEditionEditionsAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceEditionEditionsEdgeAggregateSelection>;
  node?: Maybe<SourceEditionEditionsNodeAggregateSelection>;
};

export type SourceEditionEditionsEdgeAggregateSelection = {
  __typename?: 'SourceEditionEditionsEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type SourceEditionEditionsNodeAggregateSelection = {
  __typename?: 'SourceEditionEditionsNodeAggregateSelection';
  collection: StringAggregateSelectionNullable;
  editor: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  notes: StringAggregateSelectionNullable;
  pages: StringAggregateSelectionNullable;
  place: StringAggregateSelectionNullable;
  title: StringAggregateSelectionNullable;
};

export type SourceEditionsAggregateInput = {
  AND?: InputMaybe<Array<SourceEditionsAggregateInput>>;
  NOT?: InputMaybe<SourceEditionsAggregateInput>;
  OR?: InputMaybe<Array<SourceEditionsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceEditionsEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceEditionsNodeAggregationWhereInput>;
};

export type SourceEditionsConnection = {
  __typename?: 'SourceEditionsConnection';
  edges: Array<SourceEditionsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceEditionsConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<EditionSort>;
};

export type SourceEditionsConnectionWhere = {
  AND?: InputMaybe<Array<SourceEditionsConnectionWhere>>;
  NOT?: InputMaybe<SourceEditionsConnectionWhere>;
  OR?: InputMaybe<Array<SourceEditionsConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<EditionWhere>;
};

export type SourceEditionsEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceEditionsEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceEditionsEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceEditionsEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type SourceEditionsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceEditionsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceEditionsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceEditionsNodeAggregationWhereInput>>;
  collection_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  collection_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  collection_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  collection_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  collection_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  collection_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  collection_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  collection_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  collection_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  collection_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  collection_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  collection_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  collection_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  collection_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  collection_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  editor_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  editor_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  editor_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  editor_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  editor_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  editor_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  editor_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  editor_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  editor_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  editor_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  editor_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  editor_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  editor_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  editor_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  editor_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  notes_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  notes_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  notes_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  notes_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  notes_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  notes_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  notes_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  notes_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  notes_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  notes_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  notes_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  notes_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  notes_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  notes_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  notes_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  pages_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  pages_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  pages_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  pages_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  pages_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  pages_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  pages_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  pages_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  pages_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  pages_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  pages_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  pages_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  pages_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  pages_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  pages_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  place_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  place_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  place_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  place_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  place_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  place_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  place_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  place_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  place_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  place_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  place_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  place_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  place_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  place_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  place_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  title_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  title_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  title_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  title_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  title_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  title_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  title_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  title_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  title_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  title_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  title_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  title_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  title_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  title_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  title_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceEditionsRelationship = Certainty & {
  __typename?: 'SourceEditionsRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: Edition;
};

export type SourceFactoidReferingAggregationSelection = {
  __typename?: 'SourceFactoidReferingAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceFactoidReferingEdgeAggregateSelection>;
  node?: Maybe<SourceFactoidReferingNodeAggregateSelection>;
};

export type SourceFactoidReferingEdgeAggregateSelection = {
  __typename?: 'SourceFactoidReferingEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNullable;
  page: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type SourceFactoidReferingNodeAggregateSelection = {
  __typename?: 'SourceFactoidReferingNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  duration: DurationAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  originalText: StringAggregateSelectionNullable;
};

export type SourceIssuingAggregateInput = {
  AND?: InputMaybe<Array<SourceIssuingAggregateInput>>;
  NOT?: InputMaybe<SourceIssuingAggregateInput>;
  OR?: InputMaybe<Array<SourceIssuingAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceIssuingEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceIssuingNodeAggregationWhereInput>;
};

export type SourceIssuingConnection = {
  __typename?: 'SourceIssuingConnection';
  edges: Array<SourceIssuingRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceIssuingConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<PhysicalPersonSort>;
};

export type SourceIssuingConnectionWhere = {
  AND?: InputMaybe<Array<SourceIssuingConnectionWhere>>;
  NOT?: InputMaybe<SourceIssuingConnectionWhere>;
  OR?: InputMaybe<Array<SourceIssuingConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<PhysicalPersonWhere>;
};

export type SourceIssuingEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceIssuingEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceIssuingEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceIssuingEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type SourceIssuingNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceIssuingNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceIssuingNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceIssuingNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceIssuingRelationship = Certainty & {
  __typename?: 'SourceIssuingRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: PhysicalPerson;
};

export type SourceOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more SourceSort objects to sort Sources by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<SourceSort>>;
};

export type SourcePartOfAggregateInput = {
  AND?: InputMaybe<Array<SourcePartOfAggregateInput>>;
  NOT?: InputMaybe<SourcePartOfAggregateInput>;
  OR?: InputMaybe<Array<SourcePartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourcePartOfEdgeAggregationWhereInput>;
  node?: InputMaybe<SourcePartOfNodeAggregationWhereInput>;
};

export type SourcePartOfConnection = {
  __typename?: 'SourcePartOfConnection';
  edges: Array<SourcePartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourcePartOfConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<SourceTypeSort>;
};

export type SourcePartOfConnectionWhere = {
  AND?: InputMaybe<Array<SourcePartOfConnectionWhere>>;
  NOT?: InputMaybe<SourcePartOfConnectionWhere>;
  OR?: InputMaybe<Array<SourcePartOfConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<SourceTypeWhere>;
};

export type SourcePartOfEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourcePartOfEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourcePartOfEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourcePartOfEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type SourcePartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourcePartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourcePartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourcePartOfNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourcePartOfRelationship = Certainty & {
  __typename?: 'SourcePartOfRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: SourceType;
};

export type SourcePhysicalPersonIssuingAggregationSelection = {
  __typename?: 'SourcePhysicalPersonIssuingAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourcePhysicalPersonIssuingEdgeAggregateSelection>;
  node?: Maybe<SourcePhysicalPersonIssuingNodeAggregateSelection>;
};

export type SourcePhysicalPersonIssuingEdgeAggregateSelection = {
  __typename?: 'SourcePhysicalPersonIssuingEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type SourcePhysicalPersonIssuingNodeAggregateSelection = {
  __typename?: 'SourcePhysicalPersonIssuingNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type SourceReferingAggregateInput = {
  AND?: InputMaybe<Array<SourceReferingAggregateInput>>;
  NOT?: InputMaybe<SourceReferingAggregateInput>;
  OR?: InputMaybe<Array<SourceReferingAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceReferingEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceReferingNodeAggregationWhereInput>;
};

export type SourceReferingConnection = {
  __typename?: 'SourceReferingConnection';
  edges: Array<SourceReferingRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceReferingConnectionSort = {
  edge?: InputMaybe<FactoidRefersToSourceSort>;
  node?: InputMaybe<FactoidSort>;
};

export type SourceReferingConnectionWhere = {
  AND?: InputMaybe<Array<SourceReferingConnectionWhere>>;
  NOT?: InputMaybe<SourceReferingConnectionWhere>;
  OR?: InputMaybe<Array<SourceReferingConnectionWhere>>;
  edge?: InputMaybe<FactoidRefersToSourceWhere>;
  node?: InputMaybe<FactoidWhere>;
};

export type SourceReferingEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceReferingEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceReferingEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceReferingEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  page_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  page_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  page_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  page_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceReferingNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceReferingNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceReferingNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceReferingNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  duration_AVERAGE_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_GTE?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LT?: InputMaybe<Scalars['Duration']>;
  duration_AVERAGE_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LT?: InputMaybe<Scalars['Duration']>;
  duration_MAX_LTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_EQUAL?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_GTE?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LT?: InputMaybe<Scalars['Duration']>;
  duration_MIN_LTE?: InputMaybe<Scalars['Duration']>;
  originalText_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalText_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalText_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalText_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceReferingRelationship = FactoidRefersToSource & {
  __typename?: 'SourceReferingRelationship';
  certainty?: Maybe<Scalars['Float']>;
  cursor: Scalars['String'];
  node: Factoid;
  page?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

/** Fields to sort Sources by. The order in which sorts are applied is not guaranteed when specifying many fields in one SourceSort object. */
export type SourceSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  language?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  reputation?: InputMaybe<SortDirection>;
};

export type SourceSourceTypePartOfAggregationSelection = {
  __typename?: 'SourceSourceTypePartOfAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceSourceTypePartOfEdgeAggregateSelection>;
  node?: Maybe<SourceSourceTypePartOfNodeAggregateSelection>;
};

export type SourceSourceTypePartOfEdgeAggregateSelection = {
  __typename?: 'SourceSourceTypePartOfEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type SourceSourceTypePartOfNodeAggregateSelection = {
  __typename?: 'SourceSourceTypePartOfNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type SourceSourceTypeTypesAggregationSelection = {
  __typename?: 'SourceSourceTypeTypesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceSourceTypeTypesEdgeAggregateSelection>;
  node?: Maybe<SourceSourceTypeTypesNodeAggregateSelection>;
};

export type SourceSourceTypeTypesEdgeAggregateSelection = {
  __typename?: 'SourceSourceTypeTypesEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type SourceSourceTypeTypesNodeAggregateSelection = {
  __typename?: 'SourceSourceTypeTypesNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

/**
 * SourceType
 * ----------
 */
export type SourceType = {
  __typename?: 'SourceType';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<SourceTypeDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: SourceTypeDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  partOf: Array<SourceType>;
  partOfAggregate?: Maybe<SourceTypeSourceTypePartOfAggregationSelection>;
  partOfConnection: SourceTypePartOfConnection;
};


/**
 * SourceType
 * ----------
 */
export type SourceTypeDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * SourceType
 * ----------
 */
export type SourceTypeDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * SourceType
 * ----------
 */
export type SourceTypeDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceTypeDataSourcesConnectionSort>>;
  where?: InputMaybe<SourceTypeDataSourcesConnectionWhere>;
};


/**
 * SourceType
 * ----------
 */
export type SourceTypePartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<SourceTypeOptions>;
  where?: InputMaybe<SourceTypeWhere>;
};


/**
 * SourceType
 * ----------
 */
export type SourceTypePartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<SourceTypeWhere>;
};


/**
 * SourceType
 * ----------
 */
export type SourceTypePartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<SourceTypePartOfConnectionSort>>;
  where?: InputMaybe<SourceTypePartOfConnectionWhere>;
};

export type SourceTypeAggregateSelection = {
  __typename?: 'SourceTypeAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type SourceTypeDataSourceDataSourcesAggregationSelection = {
  __typename?: 'SourceTypeDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceTypeDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<SourceTypeDataSourceDataSourcesNodeAggregateSelection>;
};

export type SourceTypeDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'SourceTypeDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type SourceTypeDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'SourceTypeDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type SourceTypeDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<SourceTypeDataSourcesAggregateInput>>;
  NOT?: InputMaybe<SourceTypeDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<SourceTypeDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceTypeDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceTypeDataSourcesNodeAggregationWhereInput>;
};

export type SourceTypeDataSourcesConnection = {
  __typename?: 'SourceTypeDataSourcesConnection';
  edges: Array<SourceTypeDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceTypeDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type SourceTypeDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<SourceTypeDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<SourceTypeDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<SourceTypeDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type SourceTypeDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceTypeDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceTypeDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceTypeDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceTypeDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceTypeDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceTypeDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceTypeDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceTypeDataSourcesRelationship = ImportedFrom & {
  __typename?: 'SourceTypeDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type SourceTypeEdge = {
  __typename?: 'SourceTypeEdge';
  cursor: Scalars['String'];
  node: SourceType;
};

export type SourceTypeOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more SourceTypeSort objects to sort SourceTypes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<SourceTypeSort>>;
};

export type SourceTypePartOfAggregateInput = {
  AND?: InputMaybe<Array<SourceTypePartOfAggregateInput>>;
  NOT?: InputMaybe<SourceTypePartOfAggregateInput>;
  OR?: InputMaybe<Array<SourceTypePartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceTypePartOfEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceTypePartOfNodeAggregationWhereInput>;
};

export type SourceTypePartOfConnection = {
  __typename?: 'SourceTypePartOfConnection';
  edges: Array<SourceTypePartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceTypePartOfConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<SourceTypeSort>;
};

export type SourceTypePartOfConnectionWhere = {
  AND?: InputMaybe<Array<SourceTypePartOfConnectionWhere>>;
  NOT?: InputMaybe<SourceTypePartOfConnectionWhere>;
  OR?: InputMaybe<Array<SourceTypePartOfConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<SourceTypeWhere>;
};

export type SourceTypePartOfEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceTypePartOfEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceTypePartOfEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceTypePartOfEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type SourceTypePartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceTypePartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceTypePartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceTypePartOfNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceTypePartOfRelationship = Certainty & {
  __typename?: 'SourceTypePartOfRelationship';
  certainty: Scalars['Float'];
  cursor: Scalars['String'];
  node: SourceType;
};

/** Fields to sort SourceTypes by. The order in which sorts are applied is not guaranteed when specifying many fields in one SourceTypeSort object. */
export type SourceTypeSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type SourceTypeSourceTypePartOfAggregationSelection = {
  __typename?: 'SourceTypeSourceTypePartOfAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<SourceTypeSourceTypePartOfEdgeAggregateSelection>;
  node?: Maybe<SourceTypeSourceTypePartOfNodeAggregateSelection>;
};

export type SourceTypeSourceTypePartOfEdgeAggregateSelection = {
  __typename?: 'SourceTypeSourceTypePartOfEdgeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type SourceTypeSourceTypePartOfNodeAggregateSelection = {
  __typename?: 'SourceTypeSourceTypePartOfNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type SourceTypeWhere = {
  AND?: InputMaybe<Array<SourceTypeWhere>>;
  NOT?: InputMaybe<SourceTypeWhere>;
  OR?: InputMaybe<Array<SourceTypeWhere>>;
  dataSourcesAggregate?: InputMaybe<SourceTypeDataSourcesAggregateInput>;
  /** Return SourceTypes where all of the related SourceTypeDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<SourceTypeDataSourcesConnectionWhere>;
  /** Return SourceTypes where none of the related SourceTypeDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<SourceTypeDataSourcesConnectionWhere>;
  /** Return SourceTypes where one of the related SourceTypeDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<SourceTypeDataSourcesConnectionWhere>;
  /** Return SourceTypes where some of the related SourceTypeDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<SourceTypeDataSourcesConnectionWhere>;
  /** Return SourceTypes where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return SourceTypes where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return SourceTypes where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return SourceTypes where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  partOfAggregate?: InputMaybe<SourceTypePartOfAggregateInput>;
  /** Return SourceTypes where all of the related SourceTypePartOfConnections match this filter */
  partOfConnection_ALL?: InputMaybe<SourceTypePartOfConnectionWhere>;
  /** Return SourceTypes where none of the related SourceTypePartOfConnections match this filter */
  partOfConnection_NONE?: InputMaybe<SourceTypePartOfConnectionWhere>;
  /** Return SourceTypes where one of the related SourceTypePartOfConnections match this filter */
  partOfConnection_SINGLE?: InputMaybe<SourceTypePartOfConnectionWhere>;
  /** Return SourceTypes where some of the related SourceTypePartOfConnections match this filter */
  partOfConnection_SOME?: InputMaybe<SourceTypePartOfConnectionWhere>;
  /** Return SourceTypes where all of the related SourceTypes match this filter */
  partOf_ALL?: InputMaybe<SourceTypeWhere>;
  /** Return SourceTypes where none of the related SourceTypes match this filter */
  partOf_NONE?: InputMaybe<SourceTypeWhere>;
  /** Return SourceTypes where one of the related SourceTypes match this filter */
  partOf_SINGLE?: InputMaybe<SourceTypeWhere>;
  /** Return SourceTypes where some of the related SourceTypes match this filter */
  partOf_SOME?: InputMaybe<SourceTypeWhere>;
};

export type SourceTypesAggregateInput = {
  AND?: InputMaybe<Array<SourceTypesAggregateInput>>;
  NOT?: InputMaybe<SourceTypesAggregateInput>;
  OR?: InputMaybe<Array<SourceTypesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<SourceTypesEdgeAggregationWhereInput>;
  node?: InputMaybe<SourceTypesNodeAggregationWhereInput>;
};

export type SourceTypesConnection = {
  __typename?: 'SourceTypesConnection';
  edges: Array<SourceTypeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type SourceTypesConnectionSort = {
  edge?: InputMaybe<CertaintySort>;
  node?: InputMaybe<SourceTypeSort>;
};

export type SourceTypesConnectionWhere = {
  AND?: InputMaybe<Array<SourceTypesConnectionWhere>>;
  NOT?: InputMaybe<SourceTypesConnectionWhere>;
  OR?: InputMaybe<Array<SourceTypesConnectionWhere>>;
  edge?: InputMaybe<CertaintyWhere>;
  node?: InputMaybe<SourceTypeWhere>;
};

export type SourceTypesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceTypesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceTypesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceTypesEdgeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

export type SourceTypesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SourceTypesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SourceTypesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SourceTypesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type SourceWhere = {
  AND?: InputMaybe<Array<SourceWhere>>;
  NOT?: InputMaybe<SourceWhere>;
  OR?: InputMaybe<Array<SourceWhere>>;
  dataSourcesAggregate?: InputMaybe<SourceDataSourcesAggregateInput>;
  /** Return Sources where all of the related SourceDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<SourceDataSourcesConnectionWhere>;
  /** Return Sources where none of the related SourceDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<SourceDataSourcesConnectionWhere>;
  /** Return Sources where one of the related SourceDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<SourceDataSourcesConnectionWhere>;
  /** Return Sources where some of the related SourceDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<SourceDataSourcesConnectionWhere>;
  /** Return Sources where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Sources where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Sources where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Sources where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  editionsAggregate?: InputMaybe<SourceEditionsAggregateInput>;
  /** Return Sources where all of the related SourceEditionsConnections match this filter */
  editionsConnection_ALL?: InputMaybe<SourceEditionsConnectionWhere>;
  /** Return Sources where none of the related SourceEditionsConnections match this filter */
  editionsConnection_NONE?: InputMaybe<SourceEditionsConnectionWhere>;
  /** Return Sources where one of the related SourceEditionsConnections match this filter */
  editionsConnection_SINGLE?: InputMaybe<SourceEditionsConnectionWhere>;
  /** Return Sources where some of the related SourceEditionsConnections match this filter */
  editionsConnection_SOME?: InputMaybe<SourceEditionsConnectionWhere>;
  /** Return Sources where all of the related Editions match this filter */
  editions_ALL?: InputMaybe<EditionWhere>;
  /** Return Sources where none of the related Editions match this filter */
  editions_NONE?: InputMaybe<EditionWhere>;
  /** Return Sources where one of the related Editions match this filter */
  editions_SINGLE?: InputMaybe<EditionWhere>;
  /** Return Sources where some of the related Editions match this filter */
  editions_SOME?: InputMaybe<EditionWhere>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  issuingAggregate?: InputMaybe<SourceIssuingAggregateInput>;
  /** Return Sources where all of the related SourceIssuingConnections match this filter */
  issuingConnection_ALL?: InputMaybe<SourceIssuingConnectionWhere>;
  /** Return Sources where none of the related SourceIssuingConnections match this filter */
  issuingConnection_NONE?: InputMaybe<SourceIssuingConnectionWhere>;
  /** Return Sources where one of the related SourceIssuingConnections match this filter */
  issuingConnection_SINGLE?: InputMaybe<SourceIssuingConnectionWhere>;
  /** Return Sources where some of the related SourceIssuingConnections match this filter */
  issuingConnection_SOME?: InputMaybe<SourceIssuingConnectionWhere>;
  /** Return Sources where all of the related PhysicalPeople match this filter */
  issuing_ALL?: InputMaybe<PhysicalPersonWhere>;
  /** Return Sources where none of the related PhysicalPeople match this filter */
  issuing_NONE?: InputMaybe<PhysicalPersonWhere>;
  /** Return Sources where one of the related PhysicalPeople match this filter */
  issuing_SINGLE?: InputMaybe<PhysicalPersonWhere>;
  /** Return Sources where some of the related PhysicalPeople match this filter */
  issuing_SOME?: InputMaybe<PhysicalPersonWhere>;
  language?: InputMaybe<Scalars['String']>;
  language_CONTAINS?: InputMaybe<Scalars['String']>;
  language_ENDS_WITH?: InputMaybe<Scalars['String']>;
  language_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  language_STARTS_WITH?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  name_CONTAINS?: InputMaybe<Scalars['String']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']>;
  name_IN?: InputMaybe<Array<Scalars['String']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']>;
  partOfAggregate?: InputMaybe<SourcePartOfAggregateInput>;
  /** Return Sources where all of the related SourcePartOfConnections match this filter */
  partOfConnection_ALL?: InputMaybe<SourcePartOfConnectionWhere>;
  /** Return Sources where none of the related SourcePartOfConnections match this filter */
  partOfConnection_NONE?: InputMaybe<SourcePartOfConnectionWhere>;
  /** Return Sources where one of the related SourcePartOfConnections match this filter */
  partOfConnection_SINGLE?: InputMaybe<SourcePartOfConnectionWhere>;
  /** Return Sources where some of the related SourcePartOfConnections match this filter */
  partOfConnection_SOME?: InputMaybe<SourcePartOfConnectionWhere>;
  /** Return Sources where all of the related SourceTypes match this filter */
  partOf_ALL?: InputMaybe<SourceTypeWhere>;
  /** Return Sources where none of the related SourceTypes match this filter */
  partOf_NONE?: InputMaybe<SourceTypeWhere>;
  /** Return Sources where one of the related SourceTypes match this filter */
  partOf_SINGLE?: InputMaybe<SourceTypeWhere>;
  /** Return Sources where some of the related SourceTypes match this filter */
  partOf_SOME?: InputMaybe<SourceTypeWhere>;
  referingAggregate?: InputMaybe<SourceReferingAggregateInput>;
  /** Return Sources where all of the related SourceReferingConnections match this filter */
  referingConnection_ALL?: InputMaybe<SourceReferingConnectionWhere>;
  /** Return Sources where none of the related SourceReferingConnections match this filter */
  referingConnection_NONE?: InputMaybe<SourceReferingConnectionWhere>;
  /** Return Sources where one of the related SourceReferingConnections match this filter */
  referingConnection_SINGLE?: InputMaybe<SourceReferingConnectionWhere>;
  /** Return Sources where some of the related SourceReferingConnections match this filter */
  referingConnection_SOME?: InputMaybe<SourceReferingConnectionWhere>;
  /** Return Sources where all of the related Factoids match this filter */
  refering_ALL?: InputMaybe<FactoidWhere>;
  /** Return Sources where none of the related Factoids match this filter */
  refering_NONE?: InputMaybe<FactoidWhere>;
  /** Return Sources where one of the related Factoids match this filter */
  refering_SINGLE?: InputMaybe<FactoidWhere>;
  /** Return Sources where some of the related Factoids match this filter */
  refering_SOME?: InputMaybe<FactoidWhere>;
  reputation?: InputMaybe<Scalars['Float']>;
  reputation_GT?: InputMaybe<Scalars['Float']>;
  reputation_GTE?: InputMaybe<Scalars['Float']>;
  reputation_IN?: InputMaybe<Array<InputMaybe<Scalars['Float']>>>;
  reputation_LT?: InputMaybe<Scalars['Float']>;
  reputation_LTE?: InputMaybe<Scalars['Float']>;
  typesAggregate?: InputMaybe<SourceTypesAggregateInput>;
  /** Return Sources where all of the related SourceTypesConnections match this filter */
  typesConnection_ALL?: InputMaybe<SourceTypesConnectionWhere>;
  /** Return Sources where none of the related SourceTypesConnections match this filter */
  typesConnection_NONE?: InputMaybe<SourceTypesConnectionWhere>;
  /** Return Sources where one of the related SourceTypesConnections match this filter */
  typesConnection_SINGLE?: InputMaybe<SourceTypesConnectionWhere>;
  /** Return Sources where some of the related SourceTypesConnections match this filter */
  typesConnection_SOME?: InputMaybe<SourceTypesConnectionWhere>;
  /** Return Sources where all of the related SourceTypes match this filter */
  types_ALL?: InputMaybe<SourceTypeWhere>;
  /** Return Sources where none of the related SourceTypes match this filter */
  types_NONE?: InputMaybe<SourceTypeWhere>;
  /** Return Sources where one of the related SourceTypes match this filter */
  types_SINGLE?: InputMaybe<SourceTypeWhere>;
  /** Return Sources where some of the related SourceTypes match this filter */
  types_SOME?: InputMaybe<SourceTypeWhere>;
};

export type SourcesConnection = {
  __typename?: 'SourcesConnection';
  edges: Array<SourceEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type StringAggregateSelectionNonNullable = {
  __typename?: 'StringAggregateSelectionNonNullable';
  longest: Scalars['String'];
  shortest: Scalars['String'];
};

export type StringAggregateSelectionNullable = {
  __typename?: 'StringAggregateSelectionNullable';
  longest?: Maybe<Scalars['String']>;
  shortest?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
  inquiries: Array<Inquiry>;
  inquiriesAggregate?: Maybe<UserInquiryInquiriesAggregationSelection>;
  inquiriesConnection: UserInquiriesConnection;
  isAdmin: Scalars['Boolean'];
  username: Scalars['String'];
};


export type UserInquiriesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<InquiryOptions>;
  where?: InputMaybe<InquiryWhere>;
};


export type UserInquiriesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<InquiryWhere>;
};


export type UserInquiriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<UserInquiriesConnectionSort>>;
  where?: InputMaybe<UserInquiriesConnectionWhere>;
};

export type UserInquiriesConnection = {
  __typename?: 'UserInquiriesConnection';
  edges: Array<UserInquiriesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type UserInquiriesConnectionSort = {
  node?: InputMaybe<InquirySort>;
};

export type UserInquiriesConnectionWhere = {
  AND?: InputMaybe<Array<UserInquiriesConnectionWhere>>;
  NOT?: InputMaybe<UserInquiriesConnectionWhere>;
  OR?: InputMaybe<Array<UserInquiriesConnectionWhere>>;
  node?: InputMaybe<InquiryWhere>;
};

export type UserInquiriesRelationship = {
  __typename?: 'UserInquiriesRelationship';
  cursor: Scalars['String'];
  node: Inquiry;
};

export type UserInquiryInquiriesAggregationSelection = {
  __typename?: 'UserInquiryInquiriesAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<UserInquiryInquiriesNodeAggregateSelection>;
};

export type UserInquiryInquiriesNodeAggregateSelection = {
  __typename?: 'UserInquiryInquiriesNodeAggregateSelection';
  createdAt: DateTimeAggregateSelectionNonNullable;
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type Zone = {
  __typename?: 'Zone';
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<ZoneDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: ZoneDataSourcesConnection;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  partOf: Array<ZonePartOfOverTime>;
  partOfAggregate?: Maybe<ZoneZonePartOfOverTimePartOfAggregationSelection>;
  partOfConnection: ZonePartOfConnection;
};


/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type ZoneDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type ZoneDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type ZoneDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ZoneDataSourcesConnectionSort>>;
  where?: InputMaybe<ZoneDataSourcesConnectionWhere>;
};


/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type ZonePartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ZonePartOfOverTimeOptions>;
  where?: InputMaybe<ZonePartOfOverTimeWhere>;
};


/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type ZonePartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ZonePartOfOverTimeWhere>;
};


/**
 * Zone
 * -----
 * A geographic zone , like 'France', 'Europe', ...
 */
export type ZonePartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ZonePartOfConnectionSort>>;
  where?: InputMaybe<ZonePartOfConnectionWhere>;
};

export type ZoneAggregateSelection = {
  __typename?: 'ZoneAggregateSelection';
  count: Scalars['Int'];
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
};

export type ZoneDataSourceDataSourcesAggregationSelection = {
  __typename?: 'ZoneDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<ZoneDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<ZoneDataSourceDataSourcesNodeAggregateSelection>;
};

export type ZoneDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'ZoneDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type ZoneDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'ZoneDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ZoneDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<ZoneDataSourcesAggregateInput>>;
  NOT?: InputMaybe<ZoneDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<ZoneDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<ZoneDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<ZoneDataSourcesNodeAggregationWhereInput>;
};

export type ZoneDataSourcesConnection = {
  __typename?: 'ZoneDataSourcesConnection';
  edges: Array<ZoneDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZoneDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type ZoneDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<ZoneDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<ZoneDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<ZoneDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type ZoneDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZoneDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<ZoneDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZoneDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZoneDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZoneDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ZoneDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZoneDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZoneDataSourcesRelationship = ImportedFrom & {
  __typename?: 'ZoneDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type ZoneEdge = {
  __typename?: 'ZoneEdge';
  cursor: Scalars['String'];
  node: Zone;
};

export type ZoneOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more ZoneSort objects to sort Zones by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ZoneSort>>;
};

export type ZonePartOfAggregateInput = {
  AND?: InputMaybe<Array<ZonePartOfAggregateInput>>;
  NOT?: InputMaybe<ZonePartOfAggregateInput>;
  OR?: InputMaybe<Array<ZonePartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<ZonePartOfNodeAggregationWhereInput>;
};

export type ZonePartOfConnection = {
  __typename?: 'ZonePartOfConnection';
  edges: Array<ZonePartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZonePartOfConnectionSort = {
  node?: InputMaybe<ZonePartOfOverTimeSort>;
};

export type ZonePartOfConnectionWhere = {
  AND?: InputMaybe<Array<ZonePartOfConnectionWhere>>;
  NOT?: InputMaybe<ZonePartOfConnectionWhere>;
  OR?: InputMaybe<Array<ZonePartOfConnectionWhere>>;
  node?: InputMaybe<ZonePartOfOverTimeWhere>;
};

export type ZonePartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZonePartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ZonePartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZonePartOfNodeAggregationWhereInput>>;
  certainty_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_GTE?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LT?: InputMaybe<Scalars['Float']>;
  certainty_AVERAGE_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LT?: InputMaybe<Scalars['Float']>;
  certainty_MAX_LTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_GTE?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LT?: InputMaybe<Scalars['Float']>;
  certainty_MIN_LTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_EQUAL?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_GTE?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LT?: InputMaybe<Scalars['Float']>;
  certainty_SUM_LTE?: InputMaybe<Scalars['Float']>;
};

/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTime = {
  __typename?: 'ZonePartOfOverTime';
  certainty: Scalars['Float'];
  dataSources: Array<DataSource>;
  dataSourcesAggregate?: Maybe<ZonePartOfOverTimeDataSourceDataSourcesAggregationSelection>;
  dataSourcesConnection: ZonePartOfOverTimeDataSourcesConnection;
  date: DateValue;
  dateAggregate?: Maybe<ZonePartOfOverTimeDateValueDateAggregationSelection>;
  dateConnection: ZonePartOfOverTimeDateConnection;
  partOf: Zone;
  partOfAggregate?: Maybe<ZonePartOfOverTimeZonePartOfAggregationSelection>;
  partOfConnection: ZonePartOfOverTimePartOfConnection;
  zone: Zone;
  zoneAggregate?: Maybe<ZonePartOfOverTimeZoneZoneAggregationSelection>;
  zoneConnection: ZonePartOfOverTimeZoneConnection;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeDataSourcesArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DataSourceOptions>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeDataSourcesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DataSourceWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeDataSourcesConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesConnectionSort>>;
  where?: InputMaybe<ZonePartOfOverTimeDataSourcesConnectionWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeDateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<DateValueOptions>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeDateAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<DateValueWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeDateConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ZonePartOfOverTimeDateConnectionSort>>;
  where?: InputMaybe<ZonePartOfOverTimeDateConnectionWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimePartOfArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ZoneOptions>;
  where?: InputMaybe<ZoneWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimePartOfAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ZoneWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimePartOfConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ZonePartOfOverTimePartOfConnectionSort>>;
  where?: InputMaybe<ZonePartOfOverTimePartOfConnectionWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeZoneArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  options?: InputMaybe<ZoneOptions>;
  where?: InputMaybe<ZoneWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeZoneAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']>;
  where?: InputMaybe<ZoneWhere>;
};


/**
 * ZonePartOfOverTime
 * -----------------
 * A zone can be part of multiple Zone in time.
 * For example, GB was part of Europe till the brexit.
 */
export type ZonePartOfOverTimeZoneConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  directed?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  sort?: InputMaybe<Array<ZonePartOfOverTimeZoneConnectionSort>>;
  where?: InputMaybe<ZonePartOfOverTimeZoneConnectionWhere>;
};

export type ZonePartOfOverTimeAggregateSelection = {
  __typename?: 'ZonePartOfOverTimeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
  count: Scalars['Int'];
};

export type ZonePartOfOverTimeDataSourceDataSourcesAggregationSelection = {
  __typename?: 'ZonePartOfOverTimeDataSourceDataSourcesAggregationSelection';
  count: Scalars['Int'];
  edge?: Maybe<ZonePartOfOverTimeDataSourceDataSourcesEdgeAggregateSelection>;
  node?: Maybe<ZonePartOfOverTimeDataSourceDataSourcesNodeAggregateSelection>;
};

export type ZonePartOfOverTimeDataSourceDataSourcesEdgeAggregateSelection = {
  __typename?: 'ZonePartOfOverTimeDataSourceDataSourcesEdgeAggregateSelection';
  originalId: StringAggregateSelectionNullable;
  permalink: StringAggregateSelectionNullable;
};

export type ZonePartOfOverTimeDataSourceDataSourcesNodeAggregateSelection = {
  __typename?: 'ZonePartOfOverTimeDataSourceDataSourcesNodeAggregateSelection';
  id: IdAggregateSelectionNonNullable;
  name: StringAggregateSelectionNonNullable;
};

export type ZonePartOfOverTimeDataSourcesAggregateInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesAggregateInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDataSourcesAggregateInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  edge?: InputMaybe<ZonePartOfOverTimeDataSourcesEdgeAggregationWhereInput>;
  node?: InputMaybe<ZonePartOfOverTimeDataSourcesNodeAggregationWhereInput>;
};

export type ZonePartOfOverTimeDataSourcesConnection = {
  __typename?: 'ZonePartOfOverTimeDataSourcesConnection';
  edges: Array<ZonePartOfOverTimeDataSourcesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZonePartOfOverTimeDataSourcesConnectionSort = {
  edge?: InputMaybe<ImportedFromSort>;
  node?: InputMaybe<DataSourceSort>;
};

export type ZonePartOfOverTimeDataSourcesConnectionWhere = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesConnectionWhere>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDataSourcesConnectionWhere>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesConnectionWhere>>;
  edge?: InputMaybe<ImportedFromWhere>;
  node?: InputMaybe<DataSourceWhere>;
};

export type ZonePartOfOverTimeDataSourcesEdgeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesEdgeAggregationWhereInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDataSourcesEdgeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesEdgeAggregationWhereInput>>;
  originalId_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  originalId_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  originalId_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  originalId_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  permalink_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  permalink_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  permalink_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZonePartOfOverTimeDataSourcesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDataSourcesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDataSourcesNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZonePartOfOverTimeDataSourcesRelationship = ImportedFrom & {
  __typename?: 'ZonePartOfOverTimeDataSourcesRelationship';
  cursor: Scalars['String'];
  node: DataSource;
  originalId?: Maybe<Scalars['String']>;
  permalink?: Maybe<Scalars['String']>;
};

export type ZonePartOfOverTimeDateAggregateInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDateAggregateInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDateAggregateInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDateAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<ZonePartOfOverTimeDateNodeAggregationWhereInput>;
};

export type ZonePartOfOverTimeDateConnection = {
  __typename?: 'ZonePartOfOverTimeDateConnection';
  edges: Array<ZonePartOfOverTimeDateRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZonePartOfOverTimeDateConnectionSort = {
  node?: InputMaybe<DateValueSort>;
};

export type ZonePartOfOverTimeDateConnectionWhere = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDateConnectionWhere>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDateConnectionWhere>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDateConnectionWhere>>;
  node?: InputMaybe<DateValueWhere>;
};

export type ZonePartOfOverTimeDateNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeDateNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeDateNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeDateNodeAggregationWhereInput>>;
  endIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  endIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  endIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  endIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  startIso_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  startIso_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  startIso_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZonePartOfOverTimeDateRelationship = {
  __typename?: 'ZonePartOfOverTimeDateRelationship';
  cursor: Scalars['String'];
  node: DateValue;
};

export type ZonePartOfOverTimeDateValueDateAggregationSelection = {
  __typename?: 'ZonePartOfOverTimeDateValueDateAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<ZonePartOfOverTimeDateValueDateNodeAggregateSelection>;
};

export type ZonePartOfOverTimeDateValueDateNodeAggregateSelection = {
  __typename?: 'ZonePartOfOverTimeDateValueDateNodeAggregateSelection';
  endIso: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
  startIso: StringAggregateSelectionNullable;
};

export type ZonePartOfOverTimeEdge = {
  __typename?: 'ZonePartOfOverTimeEdge';
  cursor: Scalars['String'];
  node: ZonePartOfOverTime;
};

export type ZonePartOfOverTimeOptions = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  /** Specify one or more ZonePartOfOverTimeSort objects to sort ZonePartOfOverTimes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ZonePartOfOverTimeSort>>;
};

export type ZonePartOfOverTimePartOfAggregateInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimePartOfAggregateInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimePartOfAggregateInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimePartOfAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<ZonePartOfOverTimePartOfNodeAggregationWhereInput>;
};

export type ZonePartOfOverTimePartOfConnection = {
  __typename?: 'ZonePartOfOverTimePartOfConnection';
  edges: Array<ZonePartOfOverTimePartOfRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZonePartOfOverTimePartOfConnectionSort = {
  node?: InputMaybe<ZoneSort>;
};

export type ZonePartOfOverTimePartOfConnectionWhere = {
  AND?: InputMaybe<Array<ZonePartOfOverTimePartOfConnectionWhere>>;
  NOT?: InputMaybe<ZonePartOfOverTimePartOfConnectionWhere>;
  OR?: InputMaybe<Array<ZonePartOfOverTimePartOfConnectionWhere>>;
  node?: InputMaybe<ZoneWhere>;
};

export type ZonePartOfOverTimePartOfNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimePartOfNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimePartOfNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimePartOfNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZonePartOfOverTimePartOfRelationship = {
  __typename?: 'ZonePartOfOverTimePartOfRelationship';
  cursor: Scalars['String'];
  node: Zone;
};

/** Fields to sort ZonePartOfOverTimes by. The order in which sorts are applied is not guaranteed when specifying many fields in one ZonePartOfOverTimeSort object. */
export type ZonePartOfOverTimeSort = {
  certainty?: InputMaybe<SortDirection>;
};

export type ZonePartOfOverTimeWhere = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeWhere>>;
  NOT?: InputMaybe<ZonePartOfOverTimeWhere>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeWhere>>;
  certainty?: InputMaybe<Scalars['Float']>;
  certainty_GT?: InputMaybe<Scalars['Float']>;
  certainty_GTE?: InputMaybe<Scalars['Float']>;
  certainty_IN?: InputMaybe<Array<Scalars['Float']>>;
  certainty_LT?: InputMaybe<Scalars['Float']>;
  certainty_LTE?: InputMaybe<Scalars['Float']>;
  dataSourcesAggregate?: InputMaybe<ZonePartOfOverTimeDataSourcesAggregateInput>;
  /** Return ZonePartOfOverTimes where all of the related ZonePartOfOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<ZonePartOfOverTimeDataSourcesConnectionWhere>;
  /** Return ZonePartOfOverTimes where none of the related ZonePartOfOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<ZonePartOfOverTimeDataSourcesConnectionWhere>;
  /** Return ZonePartOfOverTimes where one of the related ZonePartOfOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<ZonePartOfOverTimeDataSourcesConnectionWhere>;
  /** Return ZonePartOfOverTimes where some of the related ZonePartOfOverTimeDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<ZonePartOfOverTimeDataSourcesConnectionWhere>;
  /** Return ZonePartOfOverTimes where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return ZonePartOfOverTimes where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return ZonePartOfOverTimes where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return ZonePartOfOverTimes where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  date?: InputMaybe<DateValueWhere>;
  dateAggregate?: InputMaybe<ZonePartOfOverTimeDateAggregateInput>;
  dateConnection?: InputMaybe<ZonePartOfOverTimeDateConnectionWhere>;
  dateConnection_NOT?: InputMaybe<ZonePartOfOverTimeDateConnectionWhere>;
  date_NOT?: InputMaybe<DateValueWhere>;
  partOf?: InputMaybe<ZoneWhere>;
  partOfAggregate?: InputMaybe<ZonePartOfOverTimePartOfAggregateInput>;
  partOfConnection?: InputMaybe<ZonePartOfOverTimePartOfConnectionWhere>;
  partOfConnection_NOT?: InputMaybe<ZonePartOfOverTimePartOfConnectionWhere>;
  partOf_NOT?: InputMaybe<ZoneWhere>;
  zone?: InputMaybe<ZoneWhere>;
  zoneAggregate?: InputMaybe<ZonePartOfOverTimeZoneAggregateInput>;
  zoneConnection?: InputMaybe<ZonePartOfOverTimeZoneConnectionWhere>;
  zoneConnection_NOT?: InputMaybe<ZonePartOfOverTimeZoneConnectionWhere>;
  zone_NOT?: InputMaybe<ZoneWhere>;
};

export type ZonePartOfOverTimeZoneAggregateInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeZoneAggregateInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeZoneAggregateInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeZoneAggregateInput>>;
  count?: InputMaybe<Scalars['Int']>;
  count_GT?: InputMaybe<Scalars['Int']>;
  count_GTE?: InputMaybe<Scalars['Int']>;
  count_LT?: InputMaybe<Scalars['Int']>;
  count_LTE?: InputMaybe<Scalars['Int']>;
  node?: InputMaybe<ZonePartOfOverTimeZoneNodeAggregationWhereInput>;
};

export type ZonePartOfOverTimeZoneConnection = {
  __typename?: 'ZonePartOfOverTimeZoneConnection';
  edges: Array<ZonePartOfOverTimeZoneRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZonePartOfOverTimeZoneConnectionSort = {
  node?: InputMaybe<ZoneSort>;
};

export type ZonePartOfOverTimeZoneConnectionWhere = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeZoneConnectionWhere>>;
  NOT?: InputMaybe<ZonePartOfOverTimeZoneConnectionWhere>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeZoneConnectionWhere>>;
  node?: InputMaybe<ZoneWhere>;
};

export type ZonePartOfOverTimeZoneNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ZonePartOfOverTimeZoneNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ZonePartOfOverTimeZoneNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ZonePartOfOverTimeZoneNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']>;
};

export type ZonePartOfOverTimeZonePartOfAggregationSelection = {
  __typename?: 'ZonePartOfOverTimeZonePartOfAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<ZonePartOfOverTimeZonePartOfNodeAggregateSelection>;
};

export type ZonePartOfOverTimeZonePartOfNodeAggregateSelection = {
  __typename?: 'ZonePartOfOverTimeZonePartOfNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
};

export type ZonePartOfOverTimeZoneRelationship = {
  __typename?: 'ZonePartOfOverTimeZoneRelationship';
  cursor: Scalars['String'];
  node: Zone;
};

export type ZonePartOfOverTimeZoneZoneAggregationSelection = {
  __typename?: 'ZonePartOfOverTimeZoneZoneAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<ZonePartOfOverTimeZoneZoneNodeAggregateSelection>;
};

export type ZonePartOfOverTimeZoneZoneNodeAggregateSelection = {
  __typename?: 'ZonePartOfOverTimeZoneZoneNodeAggregateSelection';
  description: StringAggregateSelectionNullable;
  id: IdAggregateSelectionNonNullable;
};

export type ZonePartOfOverTimesConnection = {
  __typename?: 'ZonePartOfOverTimesConnection';
  edges: Array<ZonePartOfOverTimeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ZonePartOfRelationship = {
  __typename?: 'ZonePartOfRelationship';
  cursor: Scalars['String'];
  node: ZonePartOfOverTime;
};

/** Fields to sort Zones by. The order in which sorts are applied is not guaranteed when specifying many fields in one ZoneSort object. */
export type ZoneSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
};

export type ZoneWhere = {
  AND?: InputMaybe<Array<ZoneWhere>>;
  NOT?: InputMaybe<ZoneWhere>;
  OR?: InputMaybe<Array<ZoneWhere>>;
  dataSourcesAggregate?: InputMaybe<ZoneDataSourcesAggregateInput>;
  /** Return Zones where all of the related ZoneDataSourcesConnections match this filter */
  dataSourcesConnection_ALL?: InputMaybe<ZoneDataSourcesConnectionWhere>;
  /** Return Zones where none of the related ZoneDataSourcesConnections match this filter */
  dataSourcesConnection_NONE?: InputMaybe<ZoneDataSourcesConnectionWhere>;
  /** Return Zones where one of the related ZoneDataSourcesConnections match this filter */
  dataSourcesConnection_SINGLE?: InputMaybe<ZoneDataSourcesConnectionWhere>;
  /** Return Zones where some of the related ZoneDataSourcesConnections match this filter */
  dataSourcesConnection_SOME?: InputMaybe<ZoneDataSourcesConnectionWhere>;
  /** Return Zones where all of the related DataSources match this filter */
  dataSources_ALL?: InputMaybe<DataSourceWhere>;
  /** Return Zones where none of the related DataSources match this filter */
  dataSources_NONE?: InputMaybe<DataSourceWhere>;
  /** Return Zones where one of the related DataSources match this filter */
  dataSources_SINGLE?: InputMaybe<DataSourceWhere>;
  /** Return Zones where some of the related DataSources match this filter */
  dataSources_SOME?: InputMaybe<DataSourceWhere>;
  description?: InputMaybe<Scalars['String']>;
  description_CONTAINS?: InputMaybe<Scalars['String']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']>;
  id_IN?: InputMaybe<Array<Scalars['ID']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']>;
  partOfAggregate?: InputMaybe<ZonePartOfAggregateInput>;
  /** Return Zones where all of the related ZonePartOfConnections match this filter */
  partOfConnection_ALL?: InputMaybe<ZonePartOfConnectionWhere>;
  /** Return Zones where none of the related ZonePartOfConnections match this filter */
  partOfConnection_NONE?: InputMaybe<ZonePartOfConnectionWhere>;
  /** Return Zones where one of the related ZonePartOfConnections match this filter */
  partOfConnection_SINGLE?: InputMaybe<ZonePartOfConnectionWhere>;
  /** Return Zones where some of the related ZonePartOfConnections match this filter */
  partOfConnection_SOME?: InputMaybe<ZonePartOfConnectionWhere>;
  /** Return Zones where all of the related ZonePartOfOverTimes match this filter */
  partOf_ALL?: InputMaybe<ZonePartOfOverTimeWhere>;
  /** Return Zones where none of the related ZonePartOfOverTimes match this filter */
  partOf_NONE?: InputMaybe<ZonePartOfOverTimeWhere>;
  /** Return Zones where one of the related ZonePartOfOverTimes match this filter */
  partOf_SINGLE?: InputMaybe<ZonePartOfOverTimeWhere>;
  /** Return Zones where some of the related ZonePartOfOverTimes match this filter */
  partOf_SOME?: InputMaybe<ZonePartOfOverTimeWhere>;
};

export type ZoneZonePartOfOverTimePartOfAggregationSelection = {
  __typename?: 'ZoneZonePartOfOverTimePartOfAggregationSelection';
  count: Scalars['Int'];
  node?: Maybe<ZoneZonePartOfOverTimePartOfNodeAggregateSelection>;
};

export type ZoneZonePartOfOverTimePartOfNodeAggregateSelection = {
  __typename?: 'ZoneZonePartOfOverTimePartOfNodeAggregateSelection';
  certainty: FloatAggregateSelectionNonNullable;
};

export type ZonesConnection = {
  __typename?: 'ZonesConnection';
  edges: Array<ZoneEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type AnnotationFullFragment = { __typename?: 'Annotation', id: string, author: string, comment?: string | null, createdAt: any, patch: Array<any> };

export type DataSourceFragment = { __typename?: 'DataSource', id: string, name: string };

type ImportedFrom_DateValueDataSourcesRelationship_Fragment = { __typename?: 'DateValueDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_DomainDataSourcesRelationship_Fragment = { __typename?: 'DomainDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_EditionDataSourcesRelationship_Fragment = { __typename?: 'EditionDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_FactoidDataSourcesRelationship_Fragment = { __typename?: 'FactoidDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_FactoidPersonParticipateDataSourcesRelationship_Fragment = { __typename?: 'FactoidPersonParticipateDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_FactoidTypeDataSourcesRelationship_Fragment = { __typename?: 'FactoidTypeDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_GroupDataSourcesRelationship_Fragment = { __typename?: 'GroupDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_MoralEntityDataSourcesRelationship_Fragment = { __typename?: 'MoralEntityDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_ObjectDataSourcesRelationship_Fragment = { __typename?: 'ObjectDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_ObjectTypeDataSourcesRelationship_Fragment = { __typename?: 'ObjectTypeDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_PersonNameDataSourcesRelationship_Fragment = { __typename?: 'PersonNameDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_PhysicalPersonDataSourcesRelationship_Fragment = { __typename?: 'PhysicalPersonDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_PlaceDataSourcesRelationship_Fragment = { __typename?: 'PlaceDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_PlaceZoneOverTimeDataSourcesRelationship_Fragment = { __typename?: 'PlaceZoneOverTimeDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_RankDataSourcesRelationship_Fragment = { __typename?: 'RankDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_RoleDataSourcesRelationship_Fragment = { __typename?: 'RoleDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_SourceDataSourcesRelationship_Fragment = { __typename?: 'SourceDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_SourceTypeDataSourcesRelationship_Fragment = { __typename?: 'SourceTypeDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_ZoneDataSourcesRelationship_Fragment = { __typename?: 'ZoneDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

type ImportedFrom_ZonePartOfOverTimeDataSourcesRelationship_Fragment = { __typename?: 'ZonePartOfOverTimeDataSourcesRelationship', originalId?: string | null, permalink?: string | null };

export type ImportedFromFragment = ImportedFrom_DateValueDataSourcesRelationship_Fragment | ImportedFrom_DomainDataSourcesRelationship_Fragment | ImportedFrom_EditionDataSourcesRelationship_Fragment | ImportedFrom_FactoidDataSourcesRelationship_Fragment | ImportedFrom_FactoidPersonParticipateDataSourcesRelationship_Fragment | ImportedFrom_FactoidTypeDataSourcesRelationship_Fragment | ImportedFrom_GroupDataSourcesRelationship_Fragment | ImportedFrom_MoralEntityDataSourcesRelationship_Fragment | ImportedFrom_ObjectDataSourcesRelationship_Fragment | ImportedFrom_ObjectTypeDataSourcesRelationship_Fragment | ImportedFrom_PersonNameDataSourcesRelationship_Fragment | ImportedFrom_PhysicalPersonDataSourcesRelationship_Fragment | ImportedFrom_PlaceDataSourcesRelationship_Fragment | ImportedFrom_PlaceZoneOverTimeDataSourcesRelationship_Fragment | ImportedFrom_RankDataSourcesRelationship_Fragment | ImportedFrom_RoleDataSourcesRelationship_Fragment | ImportedFrom_SourceDataSourcesRelationship_Fragment | ImportedFrom_SourceTypeDataSourcesRelationship_Fragment | ImportedFrom_ZoneDataSourcesRelationship_Fragment | ImportedFrom_ZonePartOfOverTimeDataSourcesRelationship_Fragment;

export type DateInlineFragment = { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null };

export type FactoidTypeInlineFragment = { __typename?: 'FactoidType', id: string, name: string, description?: string | null };

export type FactoidInlineFragment = { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type FactoidFragment = { __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

export type FactoidFullFragment = { __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'FactoidDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'FactoidDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, alternativeNames?: Array<string | null> | null, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, locatedIn: Array<{ __typename?: 'PlaceZoneOverTime', certainty: number, date: { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }, zone: { __typename?: 'Zone', id: string, description?: string | null } }>, factoidsConnection: { __typename?: 'PlaceFactoidsConnection', totalCount: number }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'FactoidSourcesRelationship', certainty?: number | null, page?: string | null, permalink?: string | null, node: { __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }> }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, annotations: Array<{ __typename?: 'Annotation', id: string, author: string, comment?: string | null, createdAt: any, patch: Array<any> }> };

export type FactoidFullGraphFragment = { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> };

export type FactoidGraphFragment = { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

type GraphSearchHit_Factoid_Fragment = { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

type GraphSearchHit_MoralEntity_Fragment = { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

type GraphSearchHit_PhysicalPerson_Fragment = { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

type GraphSearchHit_Place_Fragment = { __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type GraphSearchHitFragment = GraphSearchHit_Factoid_Fragment | GraphSearchHit_MoralEntity_Fragment | GraphSearchHit_PhysicalPerson_Fragment | GraphSearchHit_Place_Fragment;

export type GroupInlineFragment = { __typename?: 'Group', id: string, name: string, description?: string | null };

export type InquiryFragment = { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> };

export type ObjectInlineFragment = { __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> };

export type PhysicalPersonInlineFragment = { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type PhysicalPersonFragment = { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> };

export type PhysicalPersonFullFragment = { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'PhysicalPersonDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'PhysicalPersonDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }>, annotations: Array<{ __typename?: 'Annotation', id: string, author: string, comment?: string | null, createdAt: any, patch: Array<any> }> };

export type PhysicalPersonFullGraphFragment = { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> };

export type PhysicalPersonGraphFragment = { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> };

export type MoralEntityInlineFragment = { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type MoralEntityFragment = { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type MoralEntityFullFragment = { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type MoralEntityFullGraphFragment = { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type MoralEntityGraphFragment = { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type PlaceInlineFragment = { __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type PlaceFragment = { __typename?: 'Place', id: string, name: string, alternativeNames?: Array<string | null> | null, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, locatedIn: Array<{ __typename?: 'PlaceZoneOverTime', certainty: number, date: { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }, zone: { __typename?: 'Zone', id: string, description?: string | null } }>, factoidsConnection: { __typename?: 'PlaceFactoidsConnection', totalCount: number }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

export type PlaceFullFragment = { __typename?: 'Place', id: string, name: string, alternativeNames?: Array<string | null> | null, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'PlaceDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'PlaceDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, locatedIn: Array<{ __typename?: 'PlaceZoneOverTime', certainty: number, date: { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }, zone: { __typename?: 'Zone', id: string, description?: string | null } }>, factoidsConnection: { __typename?: 'PlaceFactoidsConnection', totalCount: number }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> };

export type PlaceFullGraphFragment = { __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> };

export type PlaceGraphFragment = { __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

export type SearchAggregationFragment = { __typename?: 'SearchAggregation', name: string, total?: number | null, values: Array<{ __typename?: 'SearchAggregationItem', key: string, count: number }> };

export type SourceInlineFragment = { __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } };

export type SourceFragment = { __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

export type SourceFullFragment = { __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'SourceDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'SourceDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> };

export type UserFragment = { __typename?: 'User', id: string, username: string, isAdmin: boolean, inquiries: Array<{ __typename?: 'Inquiry', id: string, name: string }> };

export type InquiryAnnotationCreateMutationVariables = Exact<{
  id: Scalars['ID'];
  author: Scalars['String'];
  comment: Scalars['String'];
  patch: Array<PatchInput> | PatchInput;
}>;


export type InquiryAnnotationCreateMutation = { __typename?: 'Mutation', result?: boolean | null };

export type FactoidByIdQueryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type FactoidByIdQueryQuery = { __typename?: 'Query', factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'FactoidDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'FactoidDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, alternativeNames?: Array<string | null> | null, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, locatedIn: Array<{ __typename?: 'PlaceZoneOverTime', certainty: number, date: { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }, zone: { __typename?: 'Zone', id: string, description?: string | null } }>, factoidsConnection: { __typename?: 'PlaceFactoidsConnection', totalCount: number }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'FactoidSourcesRelationship', certainty?: number | null, page?: string | null, permalink?: string | null, node: { __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }> }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, annotations: Array<{ __typename?: 'Annotation', id: string, author: string, comment?: string | null, createdAt: any, patch: Array<any> }> }> };

export type GraphFactoidByIdQueryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GraphFactoidByIdQueryQuery = { __typename?: 'Query', factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> }> };

export type FactoidsSearchQueryQueryVariables = Exact<{
  inquiryId: Scalars['ID'];
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
  aggregations?: InputMaybe<Array<SearchAggregationInput> | SearchAggregationInput>;
  filters?: InputMaybe<Array<SearchFilterInput> | SearchFilterInput>;
  sort?: InputMaybe<SearchSortInput>;
}>;


export type FactoidsSearchQueryQuery = { __typename?: 'Query', search: { __typename?: 'SearchFactoidsResult', total: number, results: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, aggregations: Array<{ __typename?: 'SearchAggregation', name: string, total?: number | null, values: Array<{ __typename?: 'SearchAggregationItem', key: string, count: number }> }> } };

export type FactoidTypesQueryVariables = Exact<{
  search: Scalars['String'];
  inquiryId: Scalars['ID'];
}>;


export type FactoidTypesQuery = { __typename?: 'Query', factoidTypesSearch: { __typename?: 'SearchFactoidTypesResult', results: Array<{ __typename?: 'FactoidType', name: string, id: string, description?: string | null }> } };

export type GraphSearchQueryQueryVariables = Exact<{
  inquiryId: Scalars['ID'];
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
}>;


export type GraphSearchQueryQuery = { __typename?: 'Query', search: { __typename?: 'GraphSearchResult', total: number, results: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } };

export type InquiriesQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
}>;


export type InquiriesQuery = { __typename?: 'Query', inquiries: Array<{ __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }> };

export type InquiryGetByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type InquiryGetByIdQuery = { __typename?: 'Query', inquiries: Array<{ __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }> };

export type InquiryCreateMutationVariables = Exact<{
  name: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
}>;


export type InquiryCreateMutation = { __typename?: 'Mutation', inquiry?: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } | null };

export type InquiryUpdateMutationVariables = Exact<{
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
}>;


export type InquiryUpdateMutation = { __typename?: 'Mutation', inquiry?: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } | null };

export type InquiryDeleteMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type InquiryDeleteMutation = { __typename?: 'Mutation', inquiryDelete?: boolean | null };

export type PhysicalPersonByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PhysicalPersonByIdQuery = { __typename?: 'Query', physicalPeople: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'PhysicalPersonDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'PhysicalPersonDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }>, annotations: Array<{ __typename?: 'Annotation', id: string, author: string, comment?: string | null, createdAt: any, patch: Array<any> }> }> };

export type GraphPhysicalPersonByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GraphPhysicalPersonByIdQuery = { __typename?: 'Query', physicalPeople: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> }>, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> } }> }> };

export type PhysicalPersonsSearchQueryQueryVariables = Exact<{
  inquiryId: Scalars['ID'];
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
  aggregations?: InputMaybe<Array<SearchAggregationInput> | SearchAggregationInput>;
  filters?: InputMaybe<Array<SearchFilterInput> | SearchFilterInput>;
  sort?: InputMaybe<SearchSortInput>;
}>;


export type PhysicalPersonsSearchQueryQuery = { __typename?: 'Query', search: { __typename?: 'SearchPhysicalPersonsResult', total: number, results: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }>, aggregations: Array<{ __typename?: 'SearchAggregation', name: string, total?: number | null, values: Array<{ __typename?: 'SearchAggregationItem', key: string, count: number }> }> } };

export type MoralEntityByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MoralEntityByIdQuery = { __typename?: 'Query', moralEntities: Array<{ __typename?: 'MoralEntity', id: string, name: string, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

export type GraphMoralEntityByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GraphMoralEntityByIdQuery = { __typename?: 'Query', moralEntities: Array<{ __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> };

export type PlaceByIdQueryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PlaceByIdQueryQuery = { __typename?: 'Query', places: Array<{ __typename?: 'Place', id: string, name: string, alternativeNames?: Array<string | null> | null, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'PlaceDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'PlaceDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, locatedIn: Array<{ __typename?: 'PlaceZoneOverTime', certainty: number, date: { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }, zone: { __typename?: 'Zone', id: string, description?: string | null } }>, factoidsConnection: { __typename?: 'PlaceFactoidsConnection', totalCount: number }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> }> };

export type GraphPlaceByIdQueryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GraphPlaceByIdQueryQuery = { __typename?: 'Query', places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, persons: Array<{ __typename?: 'FactoidPersonParticipate', person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } }>, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> }> };

export type PlacesSearchQueryQueryVariables = Exact<{
  inquiryId: Scalars['ID'];
  search: Scalars['String'];
  skip?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
  aggregations?: InputMaybe<Array<SearchAggregationInput> | SearchAggregationInput>;
  filters?: InputMaybe<Array<SearchFilterInput> | SearchFilterInput>;
  sort?: InputMaybe<SearchSortInput>;
}>;


export type PlacesSearchQueryQuery = { __typename?: 'Query', search: { __typename?: 'SearchPlacesResult', total: number, results: Array<{ __typename?: 'Place', id: string, name: string, alternativeNames?: Array<string | null> | null, description?: string | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, locatedIn: Array<{ __typename?: 'PlaceZoneOverTime', certainty: number, date: { __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }, zone: { __typename?: 'Zone', id: string, description?: string | null } }>, factoidsConnection: { __typename?: 'PlaceFactoidsConnection', totalCount: number }, factoids: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }>, aggregations: Array<{ __typename?: 'SearchAggregation', name: string, total?: number | null, values: Array<{ __typename?: 'SearchAggregationItem', key: string, count: number }> }> } };

export type RolesQueryVariables = Exact<{
  search: Scalars['String'];
  inquiryId: Scalars['ID'];
}>;


export type RolesQuery = { __typename?: 'Query', rolesSearch: { __typename?: 'SearchRolesResult', results: Array<{ __typename?: 'Role', name: string, id: string, description?: string | null }> } };

export type SourceByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type SourceByIdQuery = { __typename?: 'Query', sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, dataSourcesConnection: { __typename?: 'SourceDataSourcesConnection', totalCount: number, edges: Array<{ __typename?: 'SourceDataSourcesRelationship', originalId?: string | null, permalink?: string | null, node: { __typename?: 'DataSource', id: string, name: string } }> }, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, issuingConnection: { __typename?: 'SourceIssuingConnection', totalCount: number }, issuing: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, otherNames: Array<{ __typename?: 'PersonName', id: string, name: string }>, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, connectedToConnection: { __typename?: 'PhysicalPersonConnectedToConnection', totalCount: number }, connectedTo: Array<{ __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, participatesToConnection: { __typename?: 'PhysicalPersonParticipatesToConnection', totalCount: number }, participatesTo: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, factoid: { __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, sourcesConnection: { __typename?: 'PhysicalPersonSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, groupsConnection: { __typename?: 'PhysicalPersonGroupsConnection', totalCount: number }, groups: Array<{ __typename?: 'Group', id: string, name: string, description?: string | null }> }>, referingConnection: { __typename?: 'SourceReferingConnection', totalCount: number }, refering: Array<{ __typename?: 'Factoid', id: string, description?: string | null, duration?: any | null, originalText?: string | null, certainty: number, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> }, datesConnection: { __typename?: 'FactoidDatesConnection', totalCount: number }, dates: Array<{ __typename?: 'DateValue', id: string, startIso?: string | null, endIso?: string | null, start?: any | null, end?: any | null }>, impactsConnection: { __typename?: 'FactoidImpactsConnection', totalCount: number }, impacts: Array<{ __typename?: 'Object', id: string, name: string, description?: string | null, types: Array<{ __typename?: 'ObjectType', id: string, name: string }> }>, personsConnection: { __typename?: 'FactoidPersonsConnection', totalCount: number }, persons: Array<{ __typename?: 'FactoidPersonParticipate', id: string, certainty?: number | null, person: { __typename?: 'MoralEntity', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } } | { __typename?: 'PhysicalPerson', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }, roles: Array<{ __typename?: 'Role', id: string, name: string, description?: string | null }>, ranks: Array<{ __typename?: 'Rank', id: string, name: string, description?: string | null }> }>, placesConnection: { __typename?: 'FactoidPlacesConnection', totalCount: number }, places: Array<{ __typename?: 'Place', id: string, name: string, description?: string | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, typesConnection: { __typename?: 'FactoidTypesConnection', totalCount: number }, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, sourcesConnection: { __typename?: 'FactoidSourcesConnection', totalCount: number }, sources: Array<{ __typename?: 'Source', id: string, name: string, description?: string | null, language?: string | null, reputation?: number | null, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }>, linkedToConnection: { __typename?: 'FactoidLinkedToConnection', totalCount: number }, linkedTo: Array<{ __typename?: 'Factoid', id: string, description?: string | null, certainty: number, types: Array<{ __typename?: 'FactoidType', id: string, name: string, description?: string | null }>, inquiry: { __typename?: 'Inquiry', id: string, name: string, description?: string | null, createdAt: any, dataSources: Array<{ __typename?: 'DataSource', id: string, name: string }> } }> }> }> };

export type WhoAmIQueryVariables = Exact<{ [key: string]: never; }>;


export type WhoAmIQuery = { __typename?: 'Query', user: { __typename?: 'User', id: string, username: string, isAdmin: boolean, inquiries: Array<{ __typename?: 'Inquiry', id: string, name: string }> } };

export type LoginMutationVariables = Exact<{
  username: Scalars['ID'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', jwt?: string | null };

export type UserChangePasswordMutationVariables = Exact<{
  username?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
}>;


export type UserChangePasswordMutation = { __typename?: 'Mutation', success?: boolean | null };

export const DataSourceFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DataSource"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DataSource"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode<DataSourceFragment, unknown>;
export const ImportedFromFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ImportedFrom"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ImportedFrom"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"originalId"}},{"kind":"Field","name":{"kind":"Name","value":"permalink"}}]}}]} as unknown as DocumentNode<ImportedFromFragment, unknown>;
export const InquiryFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"Inquiry"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Inquiry"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}}]}}]} as unknown as DocumentNode<InquiryFragment, unknown>;
export const DateInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DateInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DateValue"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"startIso"}},{"kind":"Field","name":{"kind":"Name","value":"endIso"}},{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]} as unknown as DocumentNode<DateInlineFragment, unknown>;
export const ObjectInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ObjectInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Object"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"types"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<ObjectInlineFragment, unknown>;
export const PhysicalPersonInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PhysicalPersonInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<PhysicalPersonInlineFragment, unknown>;
export const FactoidTypeInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"FactoidTypeInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"FactoidType"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]} as unknown as DocumentNode<FactoidTypeInlineFragment, unknown>;
export const FactoidInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"FactoidInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Factoid"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"types"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidTypeInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<FactoidInlineFragment, unknown>;
export const SourceInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SourceInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Source"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"reputation"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<SourceInlineFragment, unknown>;
export const GroupInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GroupInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Group"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]} as unknown as DocumentNode<GroupInlineFragment, unknown>;
export const PhysicalPersonFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PhysicalPerson"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"otherNames"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedToConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participatesToConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participatesTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"factoid"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"roles"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ranks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"sourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"sources"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SourceInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"groupsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"groups"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GroupInline"}}]}}]}}]} as unknown as DocumentNode<PhysicalPersonFragment, unknown>;
export const MoralEntityFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MoralEntity"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<MoralEntityFragment, unknown>;
export const PlaceFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"Place"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Place"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeNames"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"locatedIn"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"date"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DateInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"zone"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"factoidsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"factoids"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}}]}}]} as unknown as DocumentNode<PlaceFragment, unknown>;
export const SourceFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"Source"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Source"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"reputation"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"issuingConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"issuing"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"referingConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"refering"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}}]}}]} as unknown as DocumentNode<SourceFragment, unknown>;
export const MoralEntityInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MoralEntityInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<MoralEntityInlineFragment, unknown>;
export const PlaceInlineFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PlaceInline"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Place"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<PlaceInlineFragment, unknown>;
export const FactoidFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"Factoid"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Factoid"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"duration"}},{"kind":"Field","name":{"kind":"Name","value":"originalText"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"datesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dates"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DateInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"impactsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"impacts"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ObjectInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"personsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"persons"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"person"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonInline"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntityInline"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"roles"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ranks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"placesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"places"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PlaceInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"typesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"types"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidTypeInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"sourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"sources"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SourceInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"linkedToConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"linkedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}}]}}]} as unknown as DocumentNode<FactoidFragment, unknown>;
export const AnnotationFullFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AnnotationFull"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Annotation"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"author"}},{"kind":"Field","name":{"kind":"Name","value":"comment"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"patch"}}]}}]} as unknown as DocumentNode<AnnotationFullFragment, unknown>;
export const FactoidFullFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"FactoidFull"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Factoid"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"duration"}},{"kind":"Field","name":{"kind":"Name","value":"originalText"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dataSourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}},{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ImportedFrom"}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"datesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dates"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DateInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"impactsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"impacts"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ObjectInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"personsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"persons"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"person"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPerson"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntity"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"roles"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ranks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"placesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"places"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Place"}}]}},{"kind":"Field","name":{"kind":"Name","value":"typesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"types"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidTypeInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"sourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}},{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"page"}},{"kind":"Field","name":{"kind":"Name","value":"permalink"}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SourceInline"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"sources"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Source"}}]}},{"kind":"Field","name":{"kind":"Name","value":"linkedToConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"linkedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Factoid"}}]}},{"kind":"Field","name":{"kind":"Name","value":"annotations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AnnotationFull"}}]}}]}}]} as unknown as DocumentNode<FactoidFullFragment, unknown>;
export const FactoidGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"FactoidGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Factoid"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"types"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidTypeInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DateInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"persons"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"person"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonInline"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntityInline"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"places"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PlaceInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"linkedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}}]}}]} as unknown as DocumentNode<FactoidGraphFragment, unknown>;
export const PhysicalPersonGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PhysicalPersonGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participatesTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"100"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"factoid"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidGraph"}}]}}]}}]}}]} as unknown as DocumentNode<PhysicalPersonGraphFragment, unknown>;
export const MoralEntityGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MoralEntityGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<MoralEntityGraphFragment, unknown>;
export const PlaceGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PlaceGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Place"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"factoids"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}}]}}]} as unknown as DocumentNode<PlaceGraphFragment, unknown>;
export const FactoidFullGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"FactoidFullGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Factoid"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"types"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidTypeInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DateInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"persons"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"person"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonGraph"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntityGraph"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"places"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PlaceGraph"}}]}},{"kind":"Field","name":{"kind":"Name","value":"linkedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidGraph"}}]}}]}}]} as unknown as DocumentNode<FactoidFullGraphFragment, unknown>;
export const GraphSearchHitFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GraphSearchHit"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"GraphSearchHit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonInline"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntityInline"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Place"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PlaceInline"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Factoid"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidInline"}}]}}]}}]} as unknown as DocumentNode<GraphSearchHitFragment, unknown>;
export const PhysicalPersonFullFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PhysicalPersonFull"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"otherNames"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dataSourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}},{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ImportedFrom"}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedToConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"3"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPerson"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participatesToConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participatesTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"100"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"factoid"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Factoid"}}]}},{"kind":"Field","name":{"kind":"Name","value":"roles"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ranks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"sourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"sources"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"3"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Source"}}]}},{"kind":"Field","name":{"kind":"Name","value":"groupsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"groups"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"3"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GroupInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"annotations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AnnotationFull"}}]}}]}}]} as unknown as DocumentNode<PhysicalPersonFullFragment, unknown>;
export const PhysicalPersonFullGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PhysicalPersonFullGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"PhysicalPerson"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonGraph"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participatesTo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"100"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"factoid"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidGraph"}}]}}]}}]}}]} as unknown as DocumentNode<PhysicalPersonFullGraphFragment, unknown>;
export const MoralEntityFullFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MoralEntityFull"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<MoralEntityFullFragment, unknown>;
export const MoralEntityFullGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MoralEntityFullGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"MoralEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}}]} as unknown as DocumentNode<MoralEntityFullGraphFragment, unknown>;
export const PlaceFullFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PlaceFull"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Place"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeNames"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dataSourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}},{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ImportedFrom"}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"locatedIn"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"certainty"}},{"kind":"Field","name":{"kind":"Name","value":"date"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DateInline"}}]}},{"kind":"Field","name":{"kind":"Name","value":"zone"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"factoidsConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"factoids"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Factoid"}}]}}]}}]} as unknown as DocumentNode<PlaceFullFragment, unknown>;
export const PlaceFullGraphFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PlaceFullGraph"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Place"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"factoids"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"50"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidGraph"}}]}}]}}]} as unknown as DocumentNode<PlaceFullGraphFragment, unknown>;
export const SearchAggregationFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SearchAggregation"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"SearchAggregation"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"values"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<SearchAggregationFragment, unknown>;
export const SourceFullFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SourceFull"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Source"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"reputation"}},{"kind":"Field","name":{"kind":"Name","value":"dataSources"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dataSourcesConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}},{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ImportedFrom"}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DataSource"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"inquiry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}},{"kind":"Field","name":{"kind":"Name","value":"issuingConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"issuing"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPerson"}}]}},{"kind":"Field","name":{"kind":"Name","value":"referingConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}},{"kind":"Field","name":{"kind":"Name","value":"refering"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"5"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Factoid"}}]}}]}}]} as unknown as DocumentNode<SourceFullFragment, unknown>;
export const UserFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"User"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"isAdmin"}},{"kind":"Field","name":{"kind":"Name","value":"inquiries"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<UserFragment, unknown>;
export const InquiryAnnotationCreateDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"inquiryAnnotationCreate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"author"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"comment"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"patch"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"PatchInput"}}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"result"},"name":{"kind":"Name","value":"inquiryAnnotationCreate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"author"},"value":{"kind":"Variable","name":{"kind":"Name","value":"author"}}},{"kind":"Argument","name":{"kind":"Name","value":"comment"},"value":{"kind":"Variable","name":{"kind":"Name","value":"comment"}}},{"kind":"Argument","name":{"kind":"Name","value":"patch"},"value":{"kind":"Variable","name":{"kind":"Name","value":"patch"}}}]}]}}]} as unknown as DocumentNode<InquiryAnnotationCreateMutation, InquiryAnnotationCreateMutationVariables>;
export const FactoidByIdQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"factoidByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"factoids"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidFull"}}]}}]}},...FactoidFullFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...ImportedFromFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...ObjectInlineFragmentDoc.definitions,...PhysicalPersonFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SourceInlineFragmentDoc.definitions,...GroupInlineFragmentDoc.definitions,...MoralEntityFragmentDoc.definitions,...PlaceFragmentDoc.definitions,...SourceFragmentDoc.definitions,...FactoidFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...AnnotationFullFragmentDoc.definitions]} as unknown as DocumentNode<FactoidByIdQueryQuery, FactoidByIdQueryQueryVariables>;
export const GraphFactoidByIdQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"graphFactoidByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"factoids"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"FactoidFullGraph"}}]}}]}},...FactoidFullGraphFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...PhysicalPersonGraphFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...FactoidGraphFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...MoralEntityGraphFragmentDoc.definitions,...PlaceGraphFragmentDoc.definitions]} as unknown as DocumentNode<GraphFactoidByIdQueryQuery, GraphFactoidByIdQueryQueryVariables>;
export const FactoidsSearchQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"factoidsSearchQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"skip"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"limit"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"10"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"aggregations"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchAggregationInput"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filters"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchFilterInput"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sort"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchSortInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"search"},"name":{"kind":"Name","value":"factoidsSearch"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"inquiryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"skip"},"value":{"kind":"Variable","name":{"kind":"Name","value":"skip"}}},{"kind":"Argument","name":{"kind":"Name","value":"limit"},"value":{"kind":"Variable","name":{"kind":"Name","value":"limit"}}},{"kind":"Argument","name":{"kind":"Name","value":"aggregations"},"value":{"kind":"Variable","name":{"kind":"Name","value":"aggregations"}}},{"kind":"Argument","name":{"kind":"Name","value":"sort"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sort"}}},{"kind":"Argument","name":{"kind":"Name","value":"filters"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filters"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"results"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Factoid"}}]}},{"kind":"Field","name":{"kind":"Name","value":"aggregations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SearchAggregation"}}]}}]}}]}},...FactoidFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...ObjectInlineFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SourceInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...SearchAggregationFragmentDoc.definitions]} as unknown as DocumentNode<FactoidsSearchQueryQuery, FactoidsSearchQueryQueryVariables>;
export const FactoidTypesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"factoidTypes"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"factoidTypesSearch"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"inquiryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"20"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"results"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}}]}}]} as unknown as DocumentNode<FactoidTypesQuery, FactoidTypesQueryVariables>;
export const GraphSearchQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"graphSearchQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"skip"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"limit"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"10"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"search"},"name":{"kind":"Name","value":"graphSearch"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"inquiryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"skip"},"value":{"kind":"Variable","name":{"kind":"Name","value":"skip"}}},{"kind":"Argument","name":{"kind":"Name","value":"limit"},"value":{"kind":"Variable","name":{"kind":"Name","value":"limit"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"results"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GraphSearchHit"}}]}}]}}]}},...GraphSearchHitFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions]} as unknown as DocumentNode<GraphSearchQueryQuery, GraphSearchQueryQueryVariables>;
export const InquiriesDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"inquiries"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"skip"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"limit"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"10"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"inquiries"},"name":{"kind":"Name","value":"inquiries"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"offset"},"value":{"kind":"Variable","name":{"kind":"Name","value":"skip"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"Variable","name":{"kind":"Name","value":"limit"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"createdAt"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}},...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions]} as unknown as DocumentNode<InquiriesQuery, InquiriesQueryVariables>;
export const InquiryGetByIdDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"inquiryGetById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"inquiries"},"name":{"kind":"Name","value":"inquiries"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}},...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions]} as unknown as DocumentNode<InquiryGetByIdQuery, InquiryGetByIdQueryVariables>;
export const InquiryCreateDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"inquiryCreate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"description"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"inquiry"},"name":{"kind":"Name","value":"inquiryCreate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"Argument","name":{"kind":"Name","value":"description"},"value":{"kind":"Variable","name":{"kind":"Name","value":"description"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}},...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions]} as unknown as DocumentNode<InquiryCreateMutation, InquiryCreateMutationVariables>;
export const InquiryUpdateDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"inquiryUpdate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"description"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"inquiry"},"name":{"kind":"Name","value":"inquiryUpdate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"Argument","name":{"kind":"Name","value":"description"},"value":{"kind":"Variable","name":{"kind":"Name","value":"description"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Inquiry"}}]}}]}},...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions]} as unknown as DocumentNode<InquiryUpdateMutation, InquiryUpdateMutationVariables>;
export const InquiryDeleteDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"inquiryDelete"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"inquiryDelete"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<InquiryDeleteMutation, InquiryDeleteMutationVariables>;
export const PhysicalPersonByIdDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"physicalPersonById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"physicalPeople"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonFull"}}]}}]}},...PhysicalPersonFullFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...ImportedFromFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...PhysicalPersonFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SourceInlineFragmentDoc.definitions,...GroupInlineFragmentDoc.definitions,...FactoidFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...ObjectInlineFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...SourceFragmentDoc.definitions,...AnnotationFullFragmentDoc.definitions]} as unknown as DocumentNode<PhysicalPersonByIdQuery, PhysicalPersonByIdQueryVariables>;
export const GraphPhysicalPersonByIdDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"graphPhysicalPersonById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"physicalPeople"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPersonFullGraph"}}]}}]}},...PhysicalPersonFullGraphFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...PhysicalPersonGraphFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...FactoidGraphFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions]} as unknown as DocumentNode<GraphPhysicalPersonByIdQuery, GraphPhysicalPersonByIdQueryVariables>;
export const PhysicalPersonsSearchQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"physicalPersonsSearchQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"skip"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"limit"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"10"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"aggregations"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchAggregationInput"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filters"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchFilterInput"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sort"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchSortInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"search"},"name":{"kind":"Name","value":"physicalPersonsSearch"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"inquiryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"skip"},"value":{"kind":"Variable","name":{"kind":"Name","value":"skip"}}},{"kind":"Argument","name":{"kind":"Name","value":"limit"},"value":{"kind":"Variable","name":{"kind":"Name","value":"limit"}}},{"kind":"Argument","name":{"kind":"Name","value":"aggregations"},"value":{"kind":"Variable","name":{"kind":"Name","value":"aggregations"}}},{"kind":"Argument","name":{"kind":"Name","value":"sort"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sort"}}},{"kind":"Argument","name":{"kind":"Name","value":"filters"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filters"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"results"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PhysicalPerson"}}]}},{"kind":"Field","name":{"kind":"Name","value":"aggregations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SearchAggregation"}}]}}]}}]}},...PhysicalPersonFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SourceInlineFragmentDoc.definitions,...GroupInlineFragmentDoc.definitions,...SearchAggregationFragmentDoc.definitions]} as unknown as DocumentNode<PhysicalPersonsSearchQueryQuery, PhysicalPersonsSearchQueryQueryVariables>;
export const MoralEntityByIdDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"moralEntityById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"moralEntities"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntityFull"}}]}}]}},...MoralEntityFullFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...InquiryFragmentDoc.definitions]} as unknown as DocumentNode<MoralEntityByIdQuery, MoralEntityByIdQueryVariables>;
export const GraphMoralEntityByIdDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"graphMoralEntityById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"moralEntities"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MoralEntityFullGraph"}}]}}]}},...MoralEntityFullGraphFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions]} as unknown as DocumentNode<GraphMoralEntityByIdQuery, GraphMoralEntityByIdQueryVariables>;
export const PlaceByIdQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"placeByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"places"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PlaceFull"}}]}}]}},...PlaceFullFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...ImportedFromFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...FactoidFragmentDoc.definitions,...ObjectInlineFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SourceInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions]} as unknown as DocumentNode<PlaceByIdQueryQuery, PlaceByIdQueryQueryVariables>;
export const GraphPlaceByIdQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"graphPlaceByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"places"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PlaceFullGraph"}}]}}]}},...PlaceFullGraphFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...FactoidGraphFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions]} as unknown as DocumentNode<GraphPlaceByIdQueryQuery, GraphPlaceByIdQueryQueryVariables>;
export const PlacesSearchQueryDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"placesSearchQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"skip"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"limit"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"10"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"aggregations"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchAggregationInput"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filters"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchFilterInput"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sort"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"SearchSortInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"search"},"name":{"kind":"Name","value":"placesSearch"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"inquiryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"skip"},"value":{"kind":"Variable","name":{"kind":"Name","value":"skip"}}},{"kind":"Argument","name":{"kind":"Name","value":"limit"},"value":{"kind":"Variable","name":{"kind":"Name","value":"limit"}}},{"kind":"Argument","name":{"kind":"Name","value":"aggregations"},"value":{"kind":"Variable","name":{"kind":"Name","value":"aggregations"}}},{"kind":"Argument","name":{"kind":"Name","value":"sort"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sort"}}},{"kind":"Argument","name":{"kind":"Name","value":"filters"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filters"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"results"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"Place"}}]}},{"kind":"Field","name":{"kind":"Name","value":"aggregations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SearchAggregation"}}]}}]}}]}},...PlaceFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SearchAggregationFragmentDoc.definitions]} as unknown as DocumentNode<PlacesSearchQueryQuery, PlacesSearchQueryQueryVariables>;
export const RolesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"roles"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"rolesSearch"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"inquiryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"inquiryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"20"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"results"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"description"}}]}}]}}]}}]} as unknown as DocumentNode<RolesQuery, RolesQueryVariables>;
export const SourceByIdDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"sourceById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"sources"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SourceFull"}}]}}]}},...SourceFullFragmentDoc.definitions,...DataSourceFragmentDoc.definitions,...ImportedFromFragmentDoc.definitions,...InquiryFragmentDoc.definitions,...PhysicalPersonFragmentDoc.definitions,...PhysicalPersonInlineFragmentDoc.definitions,...FactoidInlineFragmentDoc.definitions,...FactoidTypeInlineFragmentDoc.definitions,...SourceInlineFragmentDoc.definitions,...GroupInlineFragmentDoc.definitions,...FactoidFragmentDoc.definitions,...DateInlineFragmentDoc.definitions,...ObjectInlineFragmentDoc.definitions,...MoralEntityInlineFragmentDoc.definitions,...PlaceInlineFragmentDoc.definitions]} as unknown as DocumentNode<SourceByIdQuery, SourceByIdQueryVariables>;
export const WhoAmIDocument = {"kind":"Document", "definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"whoAmI"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"user"},"name":{"kind":"Name","value":"whoami"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"User"}}]}}]}},...UserFragmentDoc.definitions]} as unknown as DocumentNode<WhoAmIQuery, WhoAmIQueryVariables>;
export const LoginDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"login"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"username"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"jwt"},"name":{"kind":"Name","value":"userLogin"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"username"},"value":{"kind":"Variable","name":{"kind":"Name","value":"username"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}]}]}}]} as unknown as DocumentNode<LoginMutation, LoginMutationVariables>;
export const UserChangePasswordDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"userChangePassword"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"username"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"success"},"name":{"kind":"Name","value":"userChangePassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"username"},"value":{"kind":"Variable","name":{"kind":"Name","value":"username"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}]}]}}]} as unknown as DocumentNode<UserChangePasswordMutation, UserChangePasswordMutationVariables>;