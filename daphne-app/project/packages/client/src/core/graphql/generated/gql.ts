/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel-plugin for production.
 */
const documents = {
    "\n  fragment AnnotationFull on Annotation {\n    id\n    author\n    comment\n    createdAt\n    patch\n  }\n": types.AnnotationFullFragmentDoc,
    "\n  fragment DataSource on DataSource {\n    id\n    name\n  }\n": types.DataSourceFragmentDoc,
    "\n  fragment ImportedFrom on ImportedFrom {\n    originalId\n    permalink\n  }\n": types.ImportedFromFragmentDoc,
    "\n  fragment DateInline on DateValue {\n    id\n    startIso\n    endIso\n    start\n    end\n  }\n": types.DateInlineFragmentDoc,
    "\n  fragment FactoidTypeInline on FactoidType {\n    id\n    name\n    description\n  }\n": types.FactoidTypeInlineFragmentDoc,
    "\n  fragment FactoidInline on Factoid {\n    id\n    description\n    certainty\n    types {\n      ...FactoidTypeInline\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.FactoidInlineFragmentDoc,
    "\n  fragment Factoid on Factoid {\n    id\n    description\n    duration\n    originalText\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    datesConnection {\n      totalCount\n    }\n    dates(options: { limit: 1 }) {\n      ...DateInline\n    }\n    impactsConnection {\n      totalCount\n    }\n    impacts(options: { limit: 1 }) {\n      ...ObjectInline\n    }\n    personsConnection {\n      totalCount\n    }\n    persons(options: { limit: 1 }) {\n      id\n      certainty\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonInline\n        }\n        ... on MoralEntity {\n          ...MoralEntityInline\n        }\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    placesConnection {\n      totalCount\n    }\n    places(options: { limit: 1 }) {\n      ...PlaceInline\n    }\n    typesConnection {\n      totalCount\n    }\n    types(options: { limit: 1 }) {\n      ...FactoidTypeInline\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 1 }) {\n      ...SourceInline\n    }\n    linkedToConnection {\n      totalCount\n    }\n    linkedTo(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n": types.FactoidFragmentDoc,
    "\n  fragment FactoidFull on Factoid {\n    id\n    description\n    duration\n    originalText\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    datesConnection {\n      totalCount\n    }\n    dates(options: { limit: 5 }) {\n      ...DateInline\n    }\n    impactsConnection {\n      totalCount\n    }\n    impacts(options: { limit: 5 }) {\n      ...ObjectInline\n    }\n    personsConnection {\n      totalCount\n    }\n    persons(options: { limit: 5 }) {\n      id\n      certainty\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPerson\n        }\n        ... on MoralEntity {\n          ...MoralEntity\n        }\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    placesConnection {\n      totalCount\n    }\n    places(options: { limit: 5 }) {\n      ...Place\n    }\n    typesConnection {\n      totalCount\n    }\n    types(options: { limit: 5 }) {\n      ...FactoidTypeInline\n    }\n    sourcesConnection {\n      totalCount\n      edges {\n        certainty\n        page\n        permalink\n        node {\n          ...SourceInline\n        }\n      }\n    }\n    sources(options: { limit: 5 }) {\n      ...Source\n    }\n    linkedToConnection {\n      totalCount\n    }\n    linkedTo(options: { limit: 5 }) {\n      ...Factoid\n    }\n    annotations {\n      ...AnnotationFull\n    }\n  }\n": types.FactoidFullFragmentDoc,
    "\n  fragment FactoidFullGraph on Factoid {\n    id\n    description\n    inquiry {\n      ...Inquiry\n    }\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    types {\n      ...FactoidTypeInline\n    }\n    dates {\n      ...DateInline\n    }\n\n    persons(options: { limit: 50 }) {\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonGraph\n        }\n        ... on MoralEntity {\n          ...MoralEntityGraph\n        }\n      }\n    }\n    places(options: { limit: 50 }) {\n      ...PlaceGraph\n    }\n    linkedTo(options: { limit: 50 }) {\n      ...FactoidGraph\n    }\n  }\n": types.FactoidFullGraphFragmentDoc,
    "\n  fragment FactoidGraph on Factoid {\n    id\n    description\n    inquiry {\n      ...Inquiry\n    }\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    types(options: { limit: 1 }) {\n      ...FactoidTypeInline\n    }\n    dates {\n      ...DateInline\n    }\n\n    persons(options: { limit: 50 }) {\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonInline\n        }\n        ... on MoralEntity {\n          ...MoralEntityInline\n        }\n      }\n    }\n    places(options: { limit: 50 }) {\n      ...PlaceInline\n    }\n    linkedTo(options: { limit: 50 }) {\n      ...FactoidInline\n    }\n  }\n": types.FactoidGraphFragmentDoc,
    "\n  fragment GraphSearchHit on GraphSearchHit {\n    ... on PhysicalPerson {\n      ...PhysicalPersonInline\n    }\n    ... on MoralEntity {\n      ...MoralEntityInline\n    }\n    ... on Place {\n      ...PlaceInline\n    }\n    ... on Factoid {\n      ...FactoidInline\n    }\n  }\n": types.GraphSearchHitFragmentDoc,
    "\n  fragment GroupInline on Group {\n    id\n    name\n    description\n  }\n": types.GroupInlineFragmentDoc,
    "\n  fragment Inquiry on Inquiry {\n    id\n    name\n    description\n    createdAt\n    dataSources {\n      ...DataSource\n    }\n  }\n": types.InquiryFragmentDoc,
    "\n  fragment ObjectInline on Object {\n    id\n    name\n    description\n    types {\n      id\n      name\n    }\n  }\n": types.ObjectInlineFragmentDoc,
    "\n  fragment PhysicalPersonInline on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.PhysicalPersonInlineFragmentDoc,
    "\n  fragment PhysicalPerson on PhysicalPerson {\n    id\n    name\n    description\n    otherNames {\n      id\n      name\n    }\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    connectedToConnection {\n      totalCount\n    }\n    connectedTo(options: { limit: 1 }) {\n      ...PhysicalPersonInline\n    }\n    participatesToConnection {\n      totalCount\n    }\n    participatesTo(options: { limit: 1 }) {\n      id\n      certainty\n      factoid {\n        ...FactoidInline\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 1 }) {\n      ...SourceInline\n    }\n    groupsConnection {\n      totalCount\n    }\n    groups(options: { limit: 1 }) {\n      ...GroupInline\n    }\n  }\n": types.PhysicalPersonFragmentDoc,
    "\n  fragment PhysicalPersonFull on PhysicalPerson {\n    id\n    name\n    description\n    otherNames {\n      id\n      name\n    }\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    connectedToConnection {\n      totalCount\n    }\n    connectedTo(options: { limit: 3 }) {\n      ...PhysicalPerson\n    }\n    participatesToConnection {\n      totalCount\n    }\n    participatesTo(options: { limit: 100 }) {\n      id\n      certainty\n      factoid {\n        ...Factoid\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 3 }) {\n      ...Source\n    }\n    groupsConnection {\n      totalCount\n    }\n    groups(options: { limit: 3 }) {\n      ...GroupInline\n    }\n    annotations {\n      ...AnnotationFull\n    }\n  }\n": types.PhysicalPersonFullFragmentDoc,
    "\n  fragment PhysicalPersonFullGraph on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    connectedTo(options: { limit: 50 }) {\n      ...PhysicalPersonGraph\n    }\n    participatesTo(options: { limit: 100 }) {\n      factoid {\n        ...FactoidGraph\n      }\n    }\n  }\n": types.PhysicalPersonFullGraphFragmentDoc,
    "\n  fragment PhysicalPersonGraph on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    connectedTo(options: { limit: 50 }) {\n      ...PhysicalPersonInline\n    }\n    participatesTo(options: { limit: 100 }) {\n      factoid {\n        ...FactoidGraph\n      }\n    }\n  }\n": types.PhysicalPersonGraphFragmentDoc,
    "\n  fragment MoralEntityInline on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.MoralEntityInlineFragmentDoc,
    "\n  fragment MoralEntity on MoralEntity {\n    id\n    name\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.MoralEntityFragmentDoc,
    "\n  fragment MoralEntityFull on MoralEntity {\n    id\n    name\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.MoralEntityFullFragmentDoc,
    "\n  fragment MoralEntityFullGraph on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.MoralEntityFullGraphFragmentDoc,
    "\n  fragment MoralEntityGraph on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.MoralEntityGraphFragmentDoc,
    "\n  fragment PlaceInline on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.PlaceInlineFragmentDoc,
    "\n  fragment Place on Place {\n    id\n    name\n    alternativeNames\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n\n    locatedIn {\n      certainty\n      date {\n        ...DateInline\n      }\n      zone {\n        id\n        description\n      }\n    }\n    factoidsConnection {\n      totalCount\n    }\n    factoids(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n": types.PlaceFragmentDoc,
    "\n  fragment PlaceFull on Place {\n    id\n    name\n    alternativeNames\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n\n    locatedIn {\n      certainty\n      date {\n        ...DateInline\n      }\n      zone {\n        id\n        description\n      }\n    }\n    factoidsConnection {\n      totalCount\n    }\n    factoids(options: { limit: 5 }) {\n      ...Factoid\n    }\n  }\n": types.PlaceFullFragmentDoc,
    "\n  fragment PlaceFullGraph on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    factoids(options: { limit: 50 }) {\n      ...FactoidGraph\n    }\n  }\n": types.PlaceFullGraphFragmentDoc,
    "\n  fragment PlaceGraph on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    factoids(options: { limit: 50 }) {\n      ...FactoidInline\n    }\n  }\n": types.PlaceGraphFragmentDoc,
    "\n  fragment SearchAggregation on SearchAggregation {\n    name\n    total\n    values {\n      key\n      count\n    }\n  }\n": types.SearchAggregationFragmentDoc,
    "\n  fragment SourceInline on Source {\n    id\n    name\n    description\n    language\n    reputation\n    inquiry {\n      ...Inquiry\n    }\n  }\n": types.SourceInlineFragmentDoc,
    "\n  fragment Source on Source {\n    id\n    name\n    description\n    language\n    reputation\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    issuingConnection {\n      totalCount\n    }\n    issuing(options: { limit: 1 }) {\n      ...PhysicalPersonInline\n    }\n    referingConnection {\n      totalCount\n    }\n    refering(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n": types.SourceFragmentDoc,
    "\n  fragment SourceFull on Source {\n    id\n    name\n    description\n    language\n    reputation\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    issuingConnection {\n      totalCount\n    }\n    issuing(options: { limit: 5 }) {\n      ...PhysicalPerson\n    }\n    referingConnection {\n      totalCount\n    }\n    refering(options: { limit: 5 }) {\n      ...Factoid\n    }\n  }\n": types.SourceFullFragmentDoc,
    "\n  fragment User on User {\n    id\n    username\n    isAdmin\n    inquiries {\n      id\n      name\n    }\n  }\n": types.UserFragmentDoc,
    "\n  mutation inquiryAnnotationCreate($id: ID!, $author: String!, $comment: String!, $patch: [PatchInput!]!) {\n    result: inquiryAnnotationCreate(id: $id, author: $author, comment: $comment, patch: $patch)\n  }\n": types.InquiryAnnotationCreateDocument,
    "\n  query factoidByIdQuery($id: ID!) {\n    factoids(where: { id: $id }) {\n      ...FactoidFull\n    }\n  }\n": types.FactoidByIdQueryDocument,
    "\n  query graphFactoidByIdQuery($id: ID!) {\n    factoids(where: { id: $id }) {\n      ...FactoidFullGraph\n    }\n  }\n": types.GraphFactoidByIdQueryDocument,
    "\n  query factoidsSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: factoidsSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...Factoid\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n": types.FactoidsSearchQueryDocument,
    "\n  query factoidTypes($search: String!, $inquiryId: ID!) {\n    factoidTypesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {\n      results {\n        name\n        id\n        description\n      }\n    }\n  }\n": types.FactoidTypesDocument,
    "\n  query graphSearchQuery($inquiryId: ID!, $search: String!, $skip: Int = 0, $limit: Int = 10) {\n    search: graphSearch(inquiryId: $inquiryId, search: $search, skip: $skip, limit: $limit) {\n      total\n      results {\n        ...GraphSearchHit\n      }\n    }\n  }\n": types.GraphSearchQueryDocument,
    "\n  query inquiries($skip: Int = 0, $limit: Int = 10) {\n    inquiries: inquiries(options: { offset: $skip, limit: $limit, sort: [{ createdAt: ASC }] }) {\n      ...Inquiry\n    }\n  }\n": types.InquiriesDocument,
    "\n  query inquiryGetById($id: ID!) {\n    inquiries: inquiries(where: { id: $id }) {\n      ...Inquiry\n    }\n  }\n": types.InquiryGetByIdDocument,
    "\n  mutation inquiryCreate($name: String!, $description: String, $password: String!) {\n    inquiry: inquiryCreate(name: $name, description: $description, password: $password) {\n      ...Inquiry\n    }\n  }\n": types.InquiryCreateDocument,
    "\n  mutation inquiryUpdate($id: ID!, $name: String, $description: String, $password: String) {\n    inquiry: inquiryUpdate(id: $id, name: $name, description: $description, password: $password) {\n      ...Inquiry\n    }\n  }\n": types.InquiryUpdateDocument,
    "\n  mutation inquiryDelete($id: ID!) {\n    inquiryDelete(id: $id)\n  }\n": types.InquiryDeleteDocument,
    "\n  query physicalPersonById($id: ID!) {\n    physicalPeople(where: { id: $id }) {\n      ...PhysicalPersonFull\n    }\n  }\n": types.PhysicalPersonByIdDocument,
    "\n  query graphPhysicalPersonById($id: ID!) {\n    physicalPeople(where: { id: $id }) {\n      ...PhysicalPersonFullGraph\n    }\n  }\n": types.GraphPhysicalPersonByIdDocument,
    "\n  query physicalPersonsSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: physicalPersonsSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...PhysicalPerson\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n": types.PhysicalPersonsSearchQueryDocument,
    "\n  query moralEntityById($id: ID!) {\n    moralEntities(where: { id: $id }) {\n      ...MoralEntityFull\n    }\n  }\n": types.MoralEntityByIdDocument,
    "\n  query graphMoralEntityById($id: ID!) {\n    moralEntities(where: { id: $id }) {\n      ...MoralEntityFullGraph\n    }\n  }\n": types.GraphMoralEntityByIdDocument,
    "\n  query placeByIdQuery($id: ID!) {\n    places(where: { id: $id }) {\n      ...PlaceFull\n    }\n  }\n": types.PlaceByIdQueryDocument,
    "\n  query graphPlaceByIdQuery($id: ID!) {\n    places(where: { id: $id }) {\n      ...PlaceFullGraph\n    }\n  }\n": types.GraphPlaceByIdQueryDocument,
    "\n  query placesSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: placesSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...Place\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n": types.PlacesSearchQueryDocument,
    "\n  query roles($search: String!, $inquiryId: ID!) {\n    rolesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {\n      results {\n        name\n        id\n        description\n      }\n    }\n  }\n": types.RolesDocument,
    "\n  query sourceById($id: ID!) {\n    sources(where: { id: $id }) {\n      ...SourceFull\n    }\n  }\n": types.SourceByIdDocument,
    "\n  query whoAmI {\n    user: whoami {\n      ...User\n    }\n  }\n": types.WhoAmIDocument,
    "\n  mutation login($username: ID!, $password: String!) {\n    jwt: userLogin(username: $username, password: $password)\n  }\n": types.LoginDocument,
    "\n  mutation userChangePassword($username: String, $password: String!) {\n    success: userChangePassword(username: $username, password: $password)\n  }\n": types.UserChangePasswordDocument,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function graphql(source: string): unknown;

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment AnnotationFull on Annotation {\n    id\n    author\n    comment\n    createdAt\n    patch\n  }\n"): (typeof documents)["\n  fragment AnnotationFull on Annotation {\n    id\n    author\n    comment\n    createdAt\n    patch\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment DataSource on DataSource {\n    id\n    name\n  }\n"): (typeof documents)["\n  fragment DataSource on DataSource {\n    id\n    name\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment ImportedFrom on ImportedFrom {\n    originalId\n    permalink\n  }\n"): (typeof documents)["\n  fragment ImportedFrom on ImportedFrom {\n    originalId\n    permalink\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment DateInline on DateValue {\n    id\n    startIso\n    endIso\n    start\n    end\n  }\n"): (typeof documents)["\n  fragment DateInline on DateValue {\n    id\n    startIso\n    endIso\n    start\n    end\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment FactoidTypeInline on FactoidType {\n    id\n    name\n    description\n  }\n"): (typeof documents)["\n  fragment FactoidTypeInline on FactoidType {\n    id\n    name\n    description\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment FactoidInline on Factoid {\n    id\n    description\n    certainty\n    types {\n      ...FactoidTypeInline\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment FactoidInline on Factoid {\n    id\n    description\n    certainty\n    types {\n      ...FactoidTypeInline\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment Factoid on Factoid {\n    id\n    description\n    duration\n    originalText\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    datesConnection {\n      totalCount\n    }\n    dates(options: { limit: 1 }) {\n      ...DateInline\n    }\n    impactsConnection {\n      totalCount\n    }\n    impacts(options: { limit: 1 }) {\n      ...ObjectInline\n    }\n    personsConnection {\n      totalCount\n    }\n    persons(options: { limit: 1 }) {\n      id\n      certainty\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonInline\n        }\n        ... on MoralEntity {\n          ...MoralEntityInline\n        }\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    placesConnection {\n      totalCount\n    }\n    places(options: { limit: 1 }) {\n      ...PlaceInline\n    }\n    typesConnection {\n      totalCount\n    }\n    types(options: { limit: 1 }) {\n      ...FactoidTypeInline\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 1 }) {\n      ...SourceInline\n    }\n    linkedToConnection {\n      totalCount\n    }\n    linkedTo(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n"): (typeof documents)["\n  fragment Factoid on Factoid {\n    id\n    description\n    duration\n    originalText\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    datesConnection {\n      totalCount\n    }\n    dates(options: { limit: 1 }) {\n      ...DateInline\n    }\n    impactsConnection {\n      totalCount\n    }\n    impacts(options: { limit: 1 }) {\n      ...ObjectInline\n    }\n    personsConnection {\n      totalCount\n    }\n    persons(options: { limit: 1 }) {\n      id\n      certainty\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonInline\n        }\n        ... on MoralEntity {\n          ...MoralEntityInline\n        }\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    placesConnection {\n      totalCount\n    }\n    places(options: { limit: 1 }) {\n      ...PlaceInline\n    }\n    typesConnection {\n      totalCount\n    }\n    types(options: { limit: 1 }) {\n      ...FactoidTypeInline\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 1 }) {\n      ...SourceInline\n    }\n    linkedToConnection {\n      totalCount\n    }\n    linkedTo(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment FactoidFull on Factoid {\n    id\n    description\n    duration\n    originalText\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    datesConnection {\n      totalCount\n    }\n    dates(options: { limit: 5 }) {\n      ...DateInline\n    }\n    impactsConnection {\n      totalCount\n    }\n    impacts(options: { limit: 5 }) {\n      ...ObjectInline\n    }\n    personsConnection {\n      totalCount\n    }\n    persons(options: { limit: 5 }) {\n      id\n      certainty\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPerson\n        }\n        ... on MoralEntity {\n          ...MoralEntity\n        }\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    placesConnection {\n      totalCount\n    }\n    places(options: { limit: 5 }) {\n      ...Place\n    }\n    typesConnection {\n      totalCount\n    }\n    types(options: { limit: 5 }) {\n      ...FactoidTypeInline\n    }\n    sourcesConnection {\n      totalCount\n      edges {\n        certainty\n        page\n        permalink\n        node {\n          ...SourceInline\n        }\n      }\n    }\n    sources(options: { limit: 5 }) {\n      ...Source\n    }\n    linkedToConnection {\n      totalCount\n    }\n    linkedTo(options: { limit: 5 }) {\n      ...Factoid\n    }\n    annotations {\n      ...AnnotationFull\n    }\n  }\n"): (typeof documents)["\n  fragment FactoidFull on Factoid {\n    id\n    description\n    duration\n    originalText\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    datesConnection {\n      totalCount\n    }\n    dates(options: { limit: 5 }) {\n      ...DateInline\n    }\n    impactsConnection {\n      totalCount\n    }\n    impacts(options: { limit: 5 }) {\n      ...ObjectInline\n    }\n    personsConnection {\n      totalCount\n    }\n    persons(options: { limit: 5 }) {\n      id\n      certainty\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPerson\n        }\n        ... on MoralEntity {\n          ...MoralEntity\n        }\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    placesConnection {\n      totalCount\n    }\n    places(options: { limit: 5 }) {\n      ...Place\n    }\n    typesConnection {\n      totalCount\n    }\n    types(options: { limit: 5 }) {\n      ...FactoidTypeInline\n    }\n    sourcesConnection {\n      totalCount\n      edges {\n        certainty\n        page\n        permalink\n        node {\n          ...SourceInline\n        }\n      }\n    }\n    sources(options: { limit: 5 }) {\n      ...Source\n    }\n    linkedToConnection {\n      totalCount\n    }\n    linkedTo(options: { limit: 5 }) {\n      ...Factoid\n    }\n    annotations {\n      ...AnnotationFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment FactoidFullGraph on Factoid {\n    id\n    description\n    inquiry {\n      ...Inquiry\n    }\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    types {\n      ...FactoidTypeInline\n    }\n    dates {\n      ...DateInline\n    }\n\n    persons(options: { limit: 50 }) {\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonGraph\n        }\n        ... on MoralEntity {\n          ...MoralEntityGraph\n        }\n      }\n    }\n    places(options: { limit: 50 }) {\n      ...PlaceGraph\n    }\n    linkedTo(options: { limit: 50 }) {\n      ...FactoidGraph\n    }\n  }\n"): (typeof documents)["\n  fragment FactoidFullGraph on Factoid {\n    id\n    description\n    inquiry {\n      ...Inquiry\n    }\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    types {\n      ...FactoidTypeInline\n    }\n    dates {\n      ...DateInline\n    }\n\n    persons(options: { limit: 50 }) {\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonGraph\n        }\n        ... on MoralEntity {\n          ...MoralEntityGraph\n        }\n      }\n    }\n    places(options: { limit: 50 }) {\n      ...PlaceGraph\n    }\n    linkedTo(options: { limit: 50 }) {\n      ...FactoidGraph\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment FactoidGraph on Factoid {\n    id\n    description\n    inquiry {\n      ...Inquiry\n    }\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    types(options: { limit: 1 }) {\n      ...FactoidTypeInline\n    }\n    dates {\n      ...DateInline\n    }\n\n    persons(options: { limit: 50 }) {\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonInline\n        }\n        ... on MoralEntity {\n          ...MoralEntityInline\n        }\n      }\n    }\n    places(options: { limit: 50 }) {\n      ...PlaceInline\n    }\n    linkedTo(options: { limit: 50 }) {\n      ...FactoidInline\n    }\n  }\n"): (typeof documents)["\n  fragment FactoidGraph on Factoid {\n    id\n    description\n    inquiry {\n      ...Inquiry\n    }\n    certainty\n    dataSources {\n      ...DataSource\n    }\n    types(options: { limit: 1 }) {\n      ...FactoidTypeInline\n    }\n    dates {\n      ...DateInline\n    }\n\n    persons(options: { limit: 50 }) {\n      person {\n        ... on PhysicalPerson {\n          ...PhysicalPersonInline\n        }\n        ... on MoralEntity {\n          ...MoralEntityInline\n        }\n      }\n    }\n    places(options: { limit: 50 }) {\n      ...PlaceInline\n    }\n    linkedTo(options: { limit: 50 }) {\n      ...FactoidInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment GraphSearchHit on GraphSearchHit {\n    ... on PhysicalPerson {\n      ...PhysicalPersonInline\n    }\n    ... on MoralEntity {\n      ...MoralEntityInline\n    }\n    ... on Place {\n      ...PlaceInline\n    }\n    ... on Factoid {\n      ...FactoidInline\n    }\n  }\n"): (typeof documents)["\n  fragment GraphSearchHit on GraphSearchHit {\n    ... on PhysicalPerson {\n      ...PhysicalPersonInline\n    }\n    ... on MoralEntity {\n      ...MoralEntityInline\n    }\n    ... on Place {\n      ...PlaceInline\n    }\n    ... on Factoid {\n      ...FactoidInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment GroupInline on Group {\n    id\n    name\n    description\n  }\n"): (typeof documents)["\n  fragment GroupInline on Group {\n    id\n    name\n    description\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment Inquiry on Inquiry {\n    id\n    name\n    description\n    createdAt\n    dataSources {\n      ...DataSource\n    }\n  }\n"): (typeof documents)["\n  fragment Inquiry on Inquiry {\n    id\n    name\n    description\n    createdAt\n    dataSources {\n      ...DataSource\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment ObjectInline on Object {\n    id\n    name\n    description\n    types {\n      id\n      name\n    }\n  }\n"): (typeof documents)["\n  fragment ObjectInline on Object {\n    id\n    name\n    description\n    types {\n      id\n      name\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PhysicalPersonInline on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment PhysicalPersonInline on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PhysicalPerson on PhysicalPerson {\n    id\n    name\n    description\n    otherNames {\n      id\n      name\n    }\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    connectedToConnection {\n      totalCount\n    }\n    connectedTo(options: { limit: 1 }) {\n      ...PhysicalPersonInline\n    }\n    participatesToConnection {\n      totalCount\n    }\n    participatesTo(options: { limit: 1 }) {\n      id\n      certainty\n      factoid {\n        ...FactoidInline\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 1 }) {\n      ...SourceInline\n    }\n    groupsConnection {\n      totalCount\n    }\n    groups(options: { limit: 1 }) {\n      ...GroupInline\n    }\n  }\n"): (typeof documents)["\n  fragment PhysicalPerson on PhysicalPerson {\n    id\n    name\n    description\n    otherNames {\n      id\n      name\n    }\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    connectedToConnection {\n      totalCount\n    }\n    connectedTo(options: { limit: 1 }) {\n      ...PhysicalPersonInline\n    }\n    participatesToConnection {\n      totalCount\n    }\n    participatesTo(options: { limit: 1 }) {\n      id\n      certainty\n      factoid {\n        ...FactoidInline\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 1 }) {\n      ...SourceInline\n    }\n    groupsConnection {\n      totalCount\n    }\n    groups(options: { limit: 1 }) {\n      ...GroupInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PhysicalPersonFull on PhysicalPerson {\n    id\n    name\n    description\n    otherNames {\n      id\n      name\n    }\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    connectedToConnection {\n      totalCount\n    }\n    connectedTo(options: { limit: 3 }) {\n      ...PhysicalPerson\n    }\n    participatesToConnection {\n      totalCount\n    }\n    participatesTo(options: { limit: 100 }) {\n      id\n      certainty\n      factoid {\n        ...Factoid\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 3 }) {\n      ...Source\n    }\n    groupsConnection {\n      totalCount\n    }\n    groups(options: { limit: 3 }) {\n      ...GroupInline\n    }\n    annotations {\n      ...AnnotationFull\n    }\n  }\n"): (typeof documents)["\n  fragment PhysicalPersonFull on PhysicalPerson {\n    id\n    name\n    description\n    otherNames {\n      id\n      name\n    }\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    connectedToConnection {\n      totalCount\n    }\n    connectedTo(options: { limit: 3 }) {\n      ...PhysicalPerson\n    }\n    participatesToConnection {\n      totalCount\n    }\n    participatesTo(options: { limit: 100 }) {\n      id\n      certainty\n      factoid {\n        ...Factoid\n      }\n      roles {\n        id\n        name\n        description\n      }\n      ranks {\n        id\n        name\n        description\n      }\n    }\n    sourcesConnection {\n      totalCount\n    }\n    sources(options: { limit: 3 }) {\n      ...Source\n    }\n    groupsConnection {\n      totalCount\n    }\n    groups(options: { limit: 3 }) {\n      ...GroupInline\n    }\n    annotations {\n      ...AnnotationFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PhysicalPersonFullGraph on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    connectedTo(options: { limit: 50 }) {\n      ...PhysicalPersonGraph\n    }\n    participatesTo(options: { limit: 100 }) {\n      factoid {\n        ...FactoidGraph\n      }\n    }\n  }\n"): (typeof documents)["\n  fragment PhysicalPersonFullGraph on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    connectedTo(options: { limit: 50 }) {\n      ...PhysicalPersonGraph\n    }\n    participatesTo(options: { limit: 100 }) {\n      factoid {\n        ...FactoidGraph\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PhysicalPersonGraph on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    connectedTo(options: { limit: 50 }) {\n      ...PhysicalPersonInline\n    }\n    participatesTo(options: { limit: 100 }) {\n      factoid {\n        ...FactoidGraph\n      }\n    }\n  }\n"): (typeof documents)["\n  fragment PhysicalPersonGraph on PhysicalPerson {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    connectedTo(options: { limit: 50 }) {\n      ...PhysicalPersonInline\n    }\n    participatesTo(options: { limit: 100 }) {\n      factoid {\n        ...FactoidGraph\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment MoralEntityInline on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment MoralEntityInline on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment MoralEntity on MoralEntity {\n    id\n    name\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment MoralEntity on MoralEntity {\n    id\n    name\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment MoralEntityFull on MoralEntity {\n    id\n    name\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment MoralEntityFull on MoralEntity {\n    id\n    name\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment MoralEntityFullGraph on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment MoralEntityFullGraph on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment MoralEntityGraph on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment MoralEntityGraph on MoralEntity {\n    id\n    name\n    description\n\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PlaceInline on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment PlaceInline on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment Place on Place {\n    id\n    name\n    alternativeNames\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n\n    locatedIn {\n      certainty\n      date {\n        ...DateInline\n      }\n      zone {\n        id\n        description\n      }\n    }\n    factoidsConnection {\n      totalCount\n    }\n    factoids(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n"): (typeof documents)["\n  fragment Place on Place {\n    id\n    name\n    alternativeNames\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n\n    locatedIn {\n      certainty\n      date {\n        ...DateInline\n      }\n      zone {\n        id\n        description\n      }\n    }\n    factoidsConnection {\n      totalCount\n    }\n    factoids(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PlaceFull on Place {\n    id\n    name\n    alternativeNames\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n\n    locatedIn {\n      certainty\n      date {\n        ...DateInline\n      }\n      zone {\n        id\n        description\n      }\n    }\n    factoidsConnection {\n      totalCount\n    }\n    factoids(options: { limit: 5 }) {\n      ...Factoid\n    }\n  }\n"): (typeof documents)["\n  fragment PlaceFull on Place {\n    id\n    name\n    alternativeNames\n    description\n\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n\n    locatedIn {\n      certainty\n      date {\n        ...DateInline\n      }\n      zone {\n        id\n        description\n      }\n    }\n    factoidsConnection {\n      totalCount\n    }\n    factoids(options: { limit: 5 }) {\n      ...Factoid\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PlaceFullGraph on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    factoids(options: { limit: 50 }) {\n      ...FactoidGraph\n    }\n  }\n"): (typeof documents)["\n  fragment PlaceFullGraph on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    factoids(options: { limit: 50 }) {\n      ...FactoidGraph\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment PlaceGraph on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    factoids(options: { limit: 50 }) {\n      ...FactoidInline\n    }\n  }\n"): (typeof documents)["\n  fragment PlaceGraph on Place {\n    id\n    name\n    description\n    inquiry {\n      ...Inquiry\n    }\n\n    factoids(options: { limit: 50 }) {\n      ...FactoidInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment SearchAggregation on SearchAggregation {\n    name\n    total\n    values {\n      key\n      count\n    }\n  }\n"): (typeof documents)["\n  fragment SearchAggregation on SearchAggregation {\n    name\n    total\n    values {\n      key\n      count\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment SourceInline on Source {\n    id\n    name\n    description\n    language\n    reputation\n    inquiry {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  fragment SourceInline on Source {\n    id\n    name\n    description\n    language\n    reputation\n    inquiry {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment Source on Source {\n    id\n    name\n    description\n    language\n    reputation\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    issuingConnection {\n      totalCount\n    }\n    issuing(options: { limit: 1 }) {\n      ...PhysicalPersonInline\n    }\n    referingConnection {\n      totalCount\n    }\n    refering(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n"): (typeof documents)["\n  fragment Source on Source {\n    id\n    name\n    description\n    language\n    reputation\n    dataSources {\n      ...DataSource\n    }\n    inquiry {\n      ...Inquiry\n    }\n    issuingConnection {\n      totalCount\n    }\n    issuing(options: { limit: 1 }) {\n      ...PhysicalPersonInline\n    }\n    referingConnection {\n      totalCount\n    }\n    refering(options: { limit: 1 }) {\n      ...FactoidInline\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment SourceFull on Source {\n    id\n    name\n    description\n    language\n    reputation\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    issuingConnection {\n      totalCount\n    }\n    issuing(options: { limit: 5 }) {\n      ...PhysicalPerson\n    }\n    referingConnection {\n      totalCount\n    }\n    refering(options: { limit: 5 }) {\n      ...Factoid\n    }\n  }\n"): (typeof documents)["\n  fragment SourceFull on Source {\n    id\n    name\n    description\n    language\n    reputation\n    dataSources {\n      ...DataSource\n    }\n    dataSourcesConnection {\n      totalCount\n      edges {\n        ...ImportedFrom\n        node {\n          ...DataSource\n        }\n      }\n    }\n    inquiry {\n      ...Inquiry\n    }\n    issuingConnection {\n      totalCount\n    }\n    issuing(options: { limit: 5 }) {\n      ...PhysicalPerson\n    }\n    referingConnection {\n      totalCount\n    }\n    refering(options: { limit: 5 }) {\n      ...Factoid\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  fragment User on User {\n    id\n    username\n    isAdmin\n    inquiries {\n      id\n      name\n    }\n  }\n"): (typeof documents)["\n  fragment User on User {\n    id\n    username\n    isAdmin\n    inquiries {\n      id\n      name\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  mutation inquiryAnnotationCreate($id: ID!, $author: String!, $comment: String!, $patch: [PatchInput!]!) {\n    result: inquiryAnnotationCreate(id: $id, author: $author, comment: $comment, patch: $patch)\n  }\n"): (typeof documents)["\n  mutation inquiryAnnotationCreate($id: ID!, $author: String!, $comment: String!, $patch: [PatchInput!]!) {\n    result: inquiryAnnotationCreate(id: $id, author: $author, comment: $comment, patch: $patch)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query factoidByIdQuery($id: ID!) {\n    factoids(where: { id: $id }) {\n      ...FactoidFull\n    }\n  }\n"): (typeof documents)["\n  query factoidByIdQuery($id: ID!) {\n    factoids(where: { id: $id }) {\n      ...FactoidFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query graphFactoidByIdQuery($id: ID!) {\n    factoids(where: { id: $id }) {\n      ...FactoidFullGraph\n    }\n  }\n"): (typeof documents)["\n  query graphFactoidByIdQuery($id: ID!) {\n    factoids(where: { id: $id }) {\n      ...FactoidFullGraph\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query factoidsSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: factoidsSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...Factoid\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n"): (typeof documents)["\n  query factoidsSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: factoidsSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...Factoid\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query factoidTypes($search: String!, $inquiryId: ID!) {\n    factoidTypesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {\n      results {\n        name\n        id\n        description\n      }\n    }\n  }\n"): (typeof documents)["\n  query factoidTypes($search: String!, $inquiryId: ID!) {\n    factoidTypesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {\n      results {\n        name\n        id\n        description\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query graphSearchQuery($inquiryId: ID!, $search: String!, $skip: Int = 0, $limit: Int = 10) {\n    search: graphSearch(inquiryId: $inquiryId, search: $search, skip: $skip, limit: $limit) {\n      total\n      results {\n        ...GraphSearchHit\n      }\n    }\n  }\n"): (typeof documents)["\n  query graphSearchQuery($inquiryId: ID!, $search: String!, $skip: Int = 0, $limit: Int = 10) {\n    search: graphSearch(inquiryId: $inquiryId, search: $search, skip: $skip, limit: $limit) {\n      total\n      results {\n        ...GraphSearchHit\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query inquiries($skip: Int = 0, $limit: Int = 10) {\n    inquiries: inquiries(options: { offset: $skip, limit: $limit, sort: [{ createdAt: ASC }] }) {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  query inquiries($skip: Int = 0, $limit: Int = 10) {\n    inquiries: inquiries(options: { offset: $skip, limit: $limit, sort: [{ createdAt: ASC }] }) {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query inquiryGetById($id: ID!) {\n    inquiries: inquiries(where: { id: $id }) {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  query inquiryGetById($id: ID!) {\n    inquiries: inquiries(where: { id: $id }) {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  mutation inquiryCreate($name: String!, $description: String, $password: String!) {\n    inquiry: inquiryCreate(name: $name, description: $description, password: $password) {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  mutation inquiryCreate($name: String!, $description: String, $password: String!) {\n    inquiry: inquiryCreate(name: $name, description: $description, password: $password) {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  mutation inquiryUpdate($id: ID!, $name: String, $description: String, $password: String) {\n    inquiry: inquiryUpdate(id: $id, name: $name, description: $description, password: $password) {\n      ...Inquiry\n    }\n  }\n"): (typeof documents)["\n  mutation inquiryUpdate($id: ID!, $name: String, $description: String, $password: String) {\n    inquiry: inquiryUpdate(id: $id, name: $name, description: $description, password: $password) {\n      ...Inquiry\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  mutation inquiryDelete($id: ID!) {\n    inquiryDelete(id: $id)\n  }\n"): (typeof documents)["\n  mutation inquiryDelete($id: ID!) {\n    inquiryDelete(id: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query physicalPersonById($id: ID!) {\n    physicalPeople(where: { id: $id }) {\n      ...PhysicalPersonFull\n    }\n  }\n"): (typeof documents)["\n  query physicalPersonById($id: ID!) {\n    physicalPeople(where: { id: $id }) {\n      ...PhysicalPersonFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query graphPhysicalPersonById($id: ID!) {\n    physicalPeople(where: { id: $id }) {\n      ...PhysicalPersonFullGraph\n    }\n  }\n"): (typeof documents)["\n  query graphPhysicalPersonById($id: ID!) {\n    physicalPeople(where: { id: $id }) {\n      ...PhysicalPersonFullGraph\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query physicalPersonsSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: physicalPersonsSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...PhysicalPerson\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n"): (typeof documents)["\n  query physicalPersonsSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: physicalPersonsSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...PhysicalPerson\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query moralEntityById($id: ID!) {\n    moralEntities(where: { id: $id }) {\n      ...MoralEntityFull\n    }\n  }\n"): (typeof documents)["\n  query moralEntityById($id: ID!) {\n    moralEntities(where: { id: $id }) {\n      ...MoralEntityFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query graphMoralEntityById($id: ID!) {\n    moralEntities(where: { id: $id }) {\n      ...MoralEntityFullGraph\n    }\n  }\n"): (typeof documents)["\n  query graphMoralEntityById($id: ID!) {\n    moralEntities(where: { id: $id }) {\n      ...MoralEntityFullGraph\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query placeByIdQuery($id: ID!) {\n    places(where: { id: $id }) {\n      ...PlaceFull\n    }\n  }\n"): (typeof documents)["\n  query placeByIdQuery($id: ID!) {\n    places(where: { id: $id }) {\n      ...PlaceFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query graphPlaceByIdQuery($id: ID!) {\n    places(where: { id: $id }) {\n      ...PlaceFullGraph\n    }\n  }\n"): (typeof documents)["\n  query graphPlaceByIdQuery($id: ID!) {\n    places(where: { id: $id }) {\n      ...PlaceFullGraph\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query placesSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: placesSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...Place\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n"): (typeof documents)["\n  query placesSearchQuery(\n    $inquiryId: ID!\n    $search: String!\n    $skip: Int = 0\n    $limit: Int = 10\n    $aggregations: [SearchAggregationInput!] = []\n    $filters: [SearchFilterInput!] = []\n    $sort: SearchSortInput\n  ) {\n    search: placesSearch(\n      inquiryId: $inquiryId\n      search: $search\n      skip: $skip\n      limit: $limit\n      aggregations: $aggregations\n      sort: $sort\n      filters: $filters\n    ) {\n      total\n      results {\n        ...Place\n      }\n      aggregations {\n        ...SearchAggregation\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query roles($search: String!, $inquiryId: ID!) {\n    rolesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {\n      results {\n        name\n        id\n        description\n      }\n    }\n  }\n"): (typeof documents)["\n  query roles($search: String!, $inquiryId: ID!) {\n    rolesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {\n      results {\n        name\n        id\n        description\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query sourceById($id: ID!) {\n    sources(where: { id: $id }) {\n      ...SourceFull\n    }\n  }\n"): (typeof documents)["\n  query sourceById($id: ID!) {\n    sources(where: { id: $id }) {\n      ...SourceFull\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query whoAmI {\n    user: whoami {\n      ...User\n    }\n  }\n"): (typeof documents)["\n  query whoAmI {\n    user: whoami {\n      ...User\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  mutation login($username: ID!, $password: String!) {\n    jwt: userLogin(username: $username, password: $password)\n  }\n"): (typeof documents)["\n  mutation login($username: ID!, $password: String!) {\n    jwt: userLogin(username: $username, password: $password)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  mutation userChangePassword($username: String, $password: String!) {\n    success: userChangePassword(username: $username, password: $password)\n  }\n"): (typeof documents)["\n  mutation userChangePassword($username: String, $password: String!) {\n    success: userChangePassword(username: $username, password: $password)\n  }\n"];

export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;