import { graphql } from "../generated/gql";

export const DateInlineFragment = graphql(`
  fragment DateInline on DateValue {
    id
    startIso
    endIso
    start
    end
  }
`);
