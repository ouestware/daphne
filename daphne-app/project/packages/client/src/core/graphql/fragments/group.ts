import { graphql } from "../generated/gql";

export const GroupInlineFragment = graphql(`
  fragment GroupInline on Group {
    id
    name
    description
  }
`);
