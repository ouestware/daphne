import { graphql } from "../generated/gql";

export const PhysicalPersonInlineFragment = graphql(`
  fragment PhysicalPersonInline on PhysicalPerson {
    id
    name
    description
    inquiry {
      ...Inquiry
    }
  }
`);

export const PhysicalPersonFragment = graphql(`
  fragment PhysicalPerson on PhysicalPerson {
    id
    name
    description
    otherNames {
      id
      name
    }
    dataSources {
      ...DataSource
    }
    inquiry {
      ...Inquiry
    }
    connectedToConnection {
      totalCount
    }
    connectedTo(options: { limit: 1 }) {
      ...PhysicalPersonInline
    }
    participatesToConnection {
      totalCount
    }
    participatesTo(options: { limit: 1 }) {
      id
      certainty
      factoid {
        ...FactoidInline
      }
      roles {
        id
        name
        description
      }
      ranks {
        id
        name
        description
      }
    }
    sourcesConnection {
      totalCount
    }
    sources(options: { limit: 1 }) {
      ...SourceInline
    }
    groupsConnection {
      totalCount
    }
    groups(options: { limit: 1 }) {
      ...GroupInline
    }
  }
`);

export const PhysicalPersonFullFragment = graphql(`
  fragment PhysicalPersonFull on PhysicalPerson {
    id
    name
    description
    otherNames {
      id
      name
    }
    dataSources {
      ...DataSource
    }
    dataSourcesConnection {
      totalCount
      edges {
        ...ImportedFrom
        node {
          ...DataSource
        }
      }
    }
    inquiry {
      ...Inquiry
    }
    connectedToConnection {
      totalCount
    }
    connectedTo(options: { limit: 3 }) {
      ...PhysicalPerson
    }
    participatesToConnection {
      totalCount
    }
    participatesTo(options: { limit: 100 }) {
      id
      certainty
      factoid {
        ...Factoid
      }
      roles {
        id
        name
        description
      }
      ranks {
        id
        name
        description
      }
    }
    sourcesConnection {
      totalCount
    }
    sources(options: { limit: 3 }) {
      ...Source
    }
    groupsConnection {
      totalCount
    }
    groups(options: { limit: 3 }) {
      ...GroupInline
    }
    annotations {
      ...AnnotationFull
    }
  }
`);

export const PhysicalPersonFullGraphFragment = graphql(`
  fragment PhysicalPersonFullGraph on PhysicalPerson {
    id
    name
    description
    inquiry {
      ...Inquiry
    }

    connectedTo(options: { limit: 50 }) {
      ...PhysicalPersonGraph
    }
    participatesTo(options: { limit: 100 }) {
      factoid {
        ...FactoidGraph
      }
    }
  }
`);

export const PhysicalPersonGraphFragment = graphql(`
  fragment PhysicalPersonGraph on PhysicalPerson {
    id
    name
    description
    inquiry {
      ...Inquiry
    }

    connectedTo(options: { limit: 50 }) {
      ...PhysicalPersonInline
    }
    participatesTo(options: { limit: 100 }) {
      factoid {
        ...FactoidGraph
      }
    }
  }
`);

export const MoralEntityInlineFragment = graphql(`
  fragment MoralEntityInline on MoralEntity {
    id
    name
    description

    inquiry {
      ...Inquiry
    }
  }
`);

export const MoralEntityFragment = graphql(`
  fragment MoralEntity on MoralEntity {
    id
    name
    description

    dataSources {
      ...DataSource
    }
    inquiry {
      ...Inquiry
    }
  }
`);

export const MoralEntityFullFragment = graphql(`
  fragment MoralEntityFull on MoralEntity {
    id
    name
    description

    dataSources {
      ...DataSource
    }
    inquiry {
      ...Inquiry
    }
  }
`);

export const MoralEntityFullGraphFragment = graphql(`
  fragment MoralEntityFullGraph on MoralEntity {
    id
    name
    description

    inquiry {
      ...Inquiry
    }
  }
`);

export const MoralEntityGraphFragment = graphql(`
  fragment MoralEntityGraph on MoralEntity {
    id
    name
    description

    inquiry {
      ...Inquiry
    }
  }
`);
