import { graphql } from "../generated/gql";

export const FactoidTypeInlineFragment = graphql(`
  fragment FactoidTypeInline on FactoidType {
    id
    name
    description
  }
`);

export const FactoidInlineFragment = graphql(`
  fragment FactoidInline on Factoid {
    id
    description
    certainty
    types {
      ...FactoidTypeInline
    }
    inquiry {
      ...Inquiry
    }
  }
`);

export const FactoidFragment = graphql(`
  fragment Factoid on Factoid {
    id
    description
    duration
    originalText
    certainty
    dataSources {
      ...DataSource
    }
    inquiry {
      ...Inquiry
    }
    datesConnection {
      totalCount
    }
    dates(options: { limit: 1 }) {
      ...DateInline
    }
    impactsConnection {
      totalCount
    }
    impacts(options: { limit: 1 }) {
      ...ObjectInline
    }
    personsConnection {
      totalCount
    }
    persons(options: { limit: 1 }) {
      id
      certainty
      person {
        ... on PhysicalPerson {
          ...PhysicalPersonInline
        }
        ... on MoralEntity {
          ...MoralEntityInline
        }
      }
      roles {
        id
        name
        description
      }
      ranks {
        id
        name
        description
      }
    }
    placesConnection {
      totalCount
    }
    places(options: { limit: 1 }) {
      ...PlaceInline
    }
    typesConnection {
      totalCount
    }
    types(options: { limit: 1 }) {
      ...FactoidTypeInline
    }
    sourcesConnection {
      totalCount
    }
    sources(options: { limit: 1 }) {
      ...SourceInline
    }
    linkedToConnection {
      totalCount
    }
    linkedTo(options: { limit: 1 }) {
      ...FactoidInline
    }
  }
`);

export const FactoidFullFragment = graphql(`
  fragment FactoidFull on Factoid {
    id
    description
    duration
    originalText
    certainty
    dataSources {
      ...DataSource
    }
    dataSourcesConnection {
      totalCount
      edges {
        ...ImportedFrom
        node {
          ...DataSource
        }
      }
    }
    inquiry {
      ...Inquiry
    }
    datesConnection {
      totalCount
    }
    dates(options: { limit: 5 }) {
      ...DateInline
    }
    impactsConnection {
      totalCount
    }
    impacts(options: { limit: 5 }) {
      ...ObjectInline
    }
    personsConnection {
      totalCount
    }
    persons(options: { limit: 5 }) {
      id
      certainty
      person {
        ... on PhysicalPerson {
          ...PhysicalPerson
        }
        ... on MoralEntity {
          ...MoralEntity
        }
      }
      roles {
        id
        name
        description
      }
      ranks {
        id
        name
        description
      }
    }
    placesConnection {
      totalCount
    }
    places(options: { limit: 5 }) {
      ...Place
    }
    typesConnection {
      totalCount
    }
    types(options: { limit: 5 }) {
      ...FactoidTypeInline
    }
    sourcesConnection {
      totalCount
      edges {
        certainty
        page
        permalink
        node {
          ...SourceInline
        }
      }
    }
    sources(options: { limit: 5 }) {
      ...Source
    }
    linkedToConnection {
      totalCount
    }
    linkedTo(options: { limit: 5 }) {
      ...Factoid
    }
    annotations {
      ...AnnotationFull
    }
  }
`);

export const FactoidFullGraphFragment = graphql(`
  fragment FactoidFullGraph on Factoid {
    id
    description
    inquiry {
      ...Inquiry
    }
    certainty
    dataSources {
      ...DataSource
    }
    types {
      ...FactoidTypeInline
    }
    dates {
      ...DateInline
    }

    persons(options: { limit: 50 }) {
      person {
        ... on PhysicalPerson {
          ...PhysicalPersonGraph
        }
        ... on MoralEntity {
          ...MoralEntityGraph
        }
      }
    }
    places(options: { limit: 50 }) {
      ...PlaceGraph
    }
    linkedTo(options: { limit: 50 }) {
      ...FactoidGraph
    }
  }
`);

export const FactoidGraphFragment = graphql(`
  fragment FactoidGraph on Factoid {
    id
    description
    inquiry {
      ...Inquiry
    }
    certainty
    dataSources {
      ...DataSource
    }
    types(options: { limit: 1 }) {
      ...FactoidTypeInline
    }
    dates {
      ...DateInline
    }

    persons(options: { limit: 50 }) {
      person {
        ... on PhysicalPerson {
          ...PhysicalPersonInline
        }
        ... on MoralEntity {
          ...MoralEntityInline
        }
      }
    }
    places(options: { limit: 50 }) {
      ...PlaceInline
    }
    linkedTo(options: { limit: 50 }) {
      ...FactoidInline
    }
  }
`);
