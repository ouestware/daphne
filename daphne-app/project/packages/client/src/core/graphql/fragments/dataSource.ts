import { graphql } from "../generated/gql";
import { DataSource, ImportedFrom } from "../schema";

export const DataSourceFragment = graphql(`
  fragment DataSource on DataSource {
    id
    name
  }
`);

export const ImportedFromFragment = graphql(`
  fragment ImportedFrom on ImportedFrom {
    originalId
    permalink
  }
`);

/**
 * mergeDataSourceSonnections: transform gql relationships data into one array of dataSource embeding relationship properties
 */
export const mergeDataSourcesConnections = (
  edges: (ImportedFrom & { node: DataSource })[],
): (DataSource & ImportedFrom)[] => {
  return edges.map((e) => ({
    ...e.node,
    originalId: e.originalId,
    permalink: e.permalink,
  }));
};
