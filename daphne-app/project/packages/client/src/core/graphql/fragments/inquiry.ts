import { graphql } from "../generated/gql";

export const InquiryFragment = graphql(`
  fragment Inquiry on Inquiry {
    id
    name
    description
    createdAt
    dataSources {
      ...DataSource
    }
  }
`);
