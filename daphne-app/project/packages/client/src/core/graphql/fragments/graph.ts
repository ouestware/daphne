import { graphql } from "../generated";

export const GraphSearchHitFragment = graphql(`
  fragment GraphSearchHit on GraphSearchHit {
    ... on PhysicalPerson {
      ...PhysicalPersonInline
    }
    ... on MoralEntity {
      ...MoralEntityInline
    }
    ... on Place {
      ...PlaceInline
    }
    ... on Factoid {
      ...FactoidInline
    }
  }
`);
