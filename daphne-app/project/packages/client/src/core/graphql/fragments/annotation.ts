import { graphql } from "../generated/gql";

export const AnnotationFullFragment = graphql(`
  fragment AnnotationFull on Annotation {
    id
    author
    comment
    createdAt
    patch
  }
`);
