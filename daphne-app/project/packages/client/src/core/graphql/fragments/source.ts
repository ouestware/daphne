import { graphql } from "../generated/gql";

export const SourceInlineFragment = graphql(`
  fragment SourceInline on Source {
    id
    name
    description
    language
    reputation
    inquiry {
      ...Inquiry
    }
  }
`);

export const SourceFragment = graphql(`
  fragment Source on Source {
    id
    name
    description
    language
    reputation
    dataSources {
      ...DataSource
    }
    inquiry {
      ...Inquiry
    }
    issuingConnection {
      totalCount
    }
    issuing(options: { limit: 1 }) {
      ...PhysicalPersonInline
    }
    referingConnection {
      totalCount
    }
    refering(options: { limit: 1 }) {
      ...FactoidInline
    }
  }
`);

export const SourceFullFragment = graphql(`
  fragment SourceFull on Source {
    id
    name
    description
    language
    reputation
    dataSources {
      ...DataSource
    }
    dataSourcesConnection {
      totalCount
      edges {
        ...ImportedFrom
        node {
          ...DataSource
        }
      }
    }
    inquiry {
      ...Inquiry
    }
    issuingConnection {
      totalCount
    }
    issuing(options: { limit: 5 }) {
      ...PhysicalPerson
    }
    referingConnection {
      totalCount
    }
    refering(options: { limit: 5 }) {
      ...Factoid
    }
  }
`);
