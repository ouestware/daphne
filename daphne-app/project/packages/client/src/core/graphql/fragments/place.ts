import { graphql } from "../generated/gql";

export const PlaceInlineFragment = graphql(`
  fragment PlaceInline on Place {
    id
    name
    description
    inquiry {
      ...Inquiry
    }
  }
`);

export const PlaceFragment = graphql(`
  fragment Place on Place {
    id
    name
    alternativeNames
    description

    dataSources {
      ...DataSource
    }
    inquiry {
      ...Inquiry
    }

    locatedIn {
      certainty
      date {
        ...DateInline
      }
      zone {
        id
        description
      }
    }
    factoidsConnection {
      totalCount
    }
    factoids(options: { limit: 1 }) {
      ...FactoidInline
    }
  }
`);

export const PlaceFullFragment = graphql(`
  fragment PlaceFull on Place {
    id
    name
    alternativeNames
    description

    dataSources {
      ...DataSource
    }
    dataSourcesConnection {
      totalCount
      edges {
        ...ImportedFrom
        node {
          ...DataSource
        }
      }
    }
    inquiry {
      ...Inquiry
    }

    locatedIn {
      certainty
      date {
        ...DateInline
      }
      zone {
        id
        description
      }
    }
    factoidsConnection {
      totalCount
    }
    factoids(options: { limit: 5 }) {
      ...Factoid
    }
  }
`);

export const PlaceFullGraphFragment = graphql(`
  fragment PlaceFullGraph on Place {
    id
    name
    description
    inquiry {
      ...Inquiry
    }

    factoids(options: { limit: 50 }) {
      ...FactoidGraph
    }
  }
`);

export const PlaceGraphFragment = graphql(`
  fragment PlaceGraph on Place {
    id
    name
    description
    inquiry {
      ...Inquiry
    }

    factoids(options: { limit: 50 }) {
      ...FactoidInline
    }
  }
`);
