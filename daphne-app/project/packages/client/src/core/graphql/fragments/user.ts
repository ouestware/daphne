import { graphql } from "../generated/gql";

export const UserFragment = graphql(`
  fragment User on User {
    id
    username
    isAdmin
    inquiries {
      id
      name
    }
  }
`);
