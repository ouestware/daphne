import { graphql } from "../generated/gql";

export const ObjectInlineFragment = graphql(`
  fragment ObjectInline on Object {
    id
    name
    description
    types {
      id
      name
    }
  }
`);
