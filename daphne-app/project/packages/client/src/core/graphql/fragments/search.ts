import { graphql } from "../generated/gql";

export const SearchAggregationFragment = graphql(`
  fragment SearchAggregation on SearchAggregation {
    name
    total
    values {
      key
      count
    }
  }
`);
