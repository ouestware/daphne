import { graphql } from "../generated/gql";

export const placeByIdQuery = graphql(`
  query placeByIdQuery($id: ID!) {
    places(where: { id: $id }) {
      ...PlaceFull
    }
  }
`);

export const graphPlaceByIdQuery = graphql(`
  query graphPlaceByIdQuery($id: ID!) {
    places(where: { id: $id }) {
      ...PlaceFullGraph
    }
  }
`);

export const placesSearchQuery = graphql(`
  query placesSearchQuery(
    $inquiryId: ID!
    $search: String!
    $skip: Int = 0
    $limit: Int = 10
    $aggregations: [SearchAggregationInput!] = []
    $filters: [SearchFilterInput!] = []
    $sort: SearchSortInput
  ) {
    search: placesSearch(
      inquiryId: $inquiryId
      search: $search
      skip: $skip
      limit: $limit
      aggregations: $aggregations
      sort: $sort
      filters: $filters
    ) {
      total
      results {
        ...Place
      }
      aggregations {
        ...SearchAggregation
      }
    }
  }
`);
