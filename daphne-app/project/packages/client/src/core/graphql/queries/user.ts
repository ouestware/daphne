import { graphql } from "../generated/gql";

export const whoAmIQuery = graphql(`
  query whoAmI {
    user: whoami {
      ...User
    }
  }
`);

export const loginQuery = graphql(`
  mutation login($username: ID!, $password: String!) {
    jwt: userLogin(username: $username, password: $password)
  }
`);

export const changePasswordQuery = graphql(`
  mutation userChangePassword($username: String, $password: String!) {
    success: userChangePassword(username: $username, password: $password)
  }
`);
