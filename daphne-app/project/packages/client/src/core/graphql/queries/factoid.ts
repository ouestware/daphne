import { graphql } from "../generated/gql";

export const factoidByIdQuery = graphql(`
  query factoidByIdQuery($id: ID!) {
    factoids(where: { id: $id }) {
      ...FactoidFull
    }
  }
`);

export const graphFactoidByIdQuery = graphql(`
  query graphFactoidByIdQuery($id: ID!) {
    factoids(where: { id: $id }) {
      ...FactoidFullGraph
    }
  }
`);

export const factoidsSearchQuery = graphql(`
  query factoidsSearchQuery(
    $inquiryId: ID!
    $search: String!
    $skip: Int = 0
    $limit: Int = 10
    $aggregations: [SearchAggregationInput!] = []
    $filters: [SearchFilterInput!] = []
    $sort: SearchSortInput
  ) {
    search: factoidsSearch(
      inquiryId: $inquiryId
      search: $search
      skip: $skip
      limit: $limit
      aggregations: $aggregations
      sort: $sort
      filters: $filters
    ) {
      total
      results {
        ...Factoid
      }
      aggregations {
        ...SearchAggregation
      }
    }
  }
`);

export const factoidTypesSearchESQuery = graphql(`
  query factoidTypes($search: String!, $inquiryId: ID!) {
    factoidTypesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {
      results {
        name
        id
        description
      }
    }
  }
`);
