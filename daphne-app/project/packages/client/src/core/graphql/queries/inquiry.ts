import { graphql } from "../generated/gql";

export const inquiriesList = graphql(`
  query inquiries($skip: Int = 0, $limit: Int = 10) {
    inquiries: inquiries(options: { offset: $skip, limit: $limit, sort: [{ createdAt: ASC }] }) {
      ...Inquiry
    }
  }
`);

export const inquiryById = graphql(`
  query inquiryGetById($id: ID!) {
    inquiries: inquiries(where: { id: $id }) {
      ...Inquiry
    }
  }
`);

export const inquiryCreateQuery = graphql(`
  mutation inquiryCreate($name: String!, $description: String, $password: String!) {
    inquiry: inquiryCreate(name: $name, description: $description, password: $password) {
      ...Inquiry
    }
  }
`);

export const inquiryUpdateQuery = graphql(`
  mutation inquiryUpdate($id: ID!, $name: String, $description: String, $password: String) {
    inquiry: inquiryUpdate(id: $id, name: $name, description: $description, password: $password) {
      ...Inquiry
    }
  }
`);

export const inquiryDeleteQuery = graphql(`
  mutation inquiryDelete($id: ID!) {
    inquiryDelete(id: $id)
  }
`);
