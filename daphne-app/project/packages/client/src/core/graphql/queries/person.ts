import { graphql } from "../generated/gql";

export const physicalPersonByIdQuery = graphql(`
  query physicalPersonById($id: ID!) {
    physicalPeople(where: { id: $id }) {
      ...PhysicalPersonFull
    }
  }
`);

export const graphPhysicalPersonByIdQuery = graphql(`
  query graphPhysicalPersonById($id: ID!) {
    physicalPeople(where: { id: $id }) {
      ...PhysicalPersonFullGraph
    }
  }
`);

export const physicalPersonSearchQuery = graphql(`
  query physicalPersonsSearchQuery(
    $inquiryId: ID!
    $search: String!
    $skip: Int = 0
    $limit: Int = 10
    $aggregations: [SearchAggregationInput!] = []
    $filters: [SearchFilterInput!] = []
    $sort: SearchSortInput
  ) {
    search: physicalPersonsSearch(
      inquiryId: $inquiryId
      search: $search
      skip: $skip
      limit: $limit
      aggregations: $aggregations
      sort: $sort
      filters: $filters
    ) {
      total
      results {
        ...PhysicalPerson
      }
      aggregations {
        ...SearchAggregation
      }
    }
  }
`);

export const moralEntityByIdQuery = graphql(`
  query moralEntityById($id: ID!) {
    moralEntities(where: { id: $id }) {
      ...MoralEntityFull
    }
  }
`);

export const graphMoralEntityByIdQuery = graphql(`
  query graphMoralEntityById($id: ID!) {
    moralEntities(where: { id: $id }) {
      ...MoralEntityFullGraph
    }
  }
`);
