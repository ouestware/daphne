import { graphql } from "../generated/gql";

export const createAnnotation = graphql(`
  mutation inquiryAnnotationCreate($id: ID!, $author: String!, $comment: String!, $patch: [PatchInput!]!) {
    result: inquiryAnnotationCreate(id: $id, author: $author, comment: $comment, patch: $patch)
  }
`);
