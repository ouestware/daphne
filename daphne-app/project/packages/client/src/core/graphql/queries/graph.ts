import { graphql } from "../generated/gql";

export const graphSearchQuery = graphql(`
  query graphSearchQuery($inquiryId: ID!, $search: String!, $skip: Int = 0, $limit: Int = 10) {
    search: graphSearch(inquiryId: $inquiryId, search: $search, skip: $skip, limit: $limit) {
      total
      results {
        ...GraphSearchHit
      }
    }
  }
`);
