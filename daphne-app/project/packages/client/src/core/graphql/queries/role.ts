import { graphql } from "../generated/gql";

export const rolesSearchESQuery = graphql(`
  query roles($search: String!, $inquiryId: ID!) {
    rolesSearch(inquiryId: $inquiryId, search: $search, limit: 20) {
      results {
        name
        id
        description
      }
    }
  }
`);
