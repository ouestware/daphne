import { graphql } from "../generated/gql";

export const sourceByIdQuery = graphql(`
  query sourceById($id: ID!) {
    sources(where: { id: $id }) {
      ...SourceFull
    }
  }
`);
