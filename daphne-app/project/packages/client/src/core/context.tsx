import { pick } from "lodash";
import { FC, PropsWithChildren, createContext, useState, useContext, useEffect } from "react";

import { UserFragment } from "./graphql";
import { NotificationState } from "./notifications";
import { ModalRequest } from "./modals";

/**
 * Type definition of the context
 */
export interface AppContextType {
  auth: { user?: UserFragment | null; jwt?: string };
  notifications: Array<NotificationState>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  modal?: ModalRequest<any, any>;
}

const initialContext: AppContextType = {
  auth: {},
  notifications: [],
  modal: undefined,
};

function serializeContext(context: AppContextType): string {
  return JSON.stringify(pick(context, ["auth"]));
}

export type AppContextSetter = (value: AppContextType | ((prev: AppContextType) => AppContextType)) => void;

/**
 * Application context.
 */
export const AppContext = createContext<{
  context: AppContextType;
  setContext: AppContextSetter;
} | null>(null);

/**
 * Application context provider.
 */
export const AppContextProvider: FC<PropsWithChildren> = ({ children }) => {
  const [context, setContext] = useState<AppContextType>(initialContext);
  const [mounted, setMounted] = useState<boolean>(false);

  /**
   * When component mount
   * => load the context from the session
   */
  useEffect(() => {
    const data = sessionStorage.getItem("context");
    if (data) {
      setContext((prev) => ({ ...prev, ...JSON.parse(data) }));
    }
    setMounted(true);
  }, []);

  /**
   * When context change
   * => save it in the session storage
   */
  useEffect(() => {
    if (mounted) {
      sessionStorage.setItem("context", serializeContext(context));
    }
  }, [context, mounted]);

  return (
    <AppContext.Provider value={{ context, setContext }}>
      <>{children}</>
    </AppContext.Provider>
  );
};

export function useAppContext(): [AppContextType, AppContextSetter] {
  const appContext = useContext(AppContext);
  if (!appContext) {
    throw new Error("There is no context");
  }
  return [appContext.context, appContext.setContext];
}
