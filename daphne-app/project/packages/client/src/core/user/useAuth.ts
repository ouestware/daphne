import { useCallback, useState } from "react";
import { useMutation, useLazyQuery } from "@apollo/client";

import { useAppContext } from "../context";
import { loginQuery, whoAmIQuery, changePasswordQuery } from "../graphql/";

export function useAuth() {
  const [{ auth }, setContext] = useAppContext();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);
  const [userLogin] = useMutation(loginQuery);
  const [userChangePassword] = useMutation(changePasswordQuery);
  const [whoAmI] = useLazyQuery(whoAmIQuery);

  const login = useCallback(
    async (username: string, password: string) => {
      setLoading(true);
      setError(null);
      try {
        // User login and get JWT
        const loginResp = await userLogin({ variables: { username, password } });
        if (!loginResp.data || !loginResp.data.jwt) throw new Error("Login query failed, response is missing");
        const jwt = loginResp.data.jwt;

        // Get user info
        const whomaiResp = await whoAmI({
          context: {
            headers: {
              Authorization: `Bearer ${jwt}`,
            },
          },
        });

        if (!whomaiResp.data || !whomaiResp.data.user) throw new Error("Failed to retrieve user");
        const user = whomaiResp.data.user;
        setContext((prev) => ({ ...prev, auth: { user, jwt } }));
      } catch (e) {
        setError(e as Error);
        throw e;
      } finally {
        setLoading(false);
      }
    },
    [setContext, userLogin, whoAmI],
  );

  const logout = useCallback(() => {
    setContext((prev) => ({ ...prev, auth: {} }));
  }, [setContext]);

  const changePassword = useCallback(
    async (password: string, confirmation: string, username?: string) => {
      setLoading(true);
      setError(null);
      try {
        if (!auth.user) throw new Error("Vous devez être connecté pour changer de mot-de-passe");
        if (username && !auth.user.isAdmin)
          throw new Error("Vous devez être admin pour changer le password d'un utilisateur");
        if (password !== confirmation) throw new Error("Le mot-de-passe et sa confirmation ne correspondent pas");
        await userChangePassword({ variables: { password, username: username || auth.user.username } });
      } catch (e) {
        setError(e as Error);
        throw e;
      } finally {
        setLoading(false);
      }
    },
    [auth.user, userChangePassword],
  );

  return { user: auth.user, login, logout, changePassword, loading, error };
}
