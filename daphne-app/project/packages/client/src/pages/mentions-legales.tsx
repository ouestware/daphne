import { NextPage } from "next";
import MentionsLegalesContent from "../assets/markdowns/MentionsLegales.md";

import { Markdown } from "../components/Markdown";
import { Layout } from "../layout";

export const MentionsLegalesPages: NextPage = () => {
  return (
    <Layout>
      <Markdown content={MentionsLegalesContent} />
    </Layout>
  );
};

export default MentionsLegalesPages;
