import { NextPage } from "next";
import AProposContent from "../assets/markdowns/APropos.md";

import { Markdown } from "../components/Markdown";
import { Layout } from "../layout";

export const AProposPage: NextPage = () => {
  return (
    <Layout>
      <Markdown content={AProposContent} />
    </Layout>
  );
};

export default AProposPage;
