import { NextPage, GetServerSideProps } from "next";
import { flatten, isArray, isEmpty, max, min, sortBy } from "lodash";
import { useMemo } from "react";

import { initializeApollo, addApolloState } from "../../../core/graphql/client";
import { physicalPersonByIdQuery, PhysicalPersonFullFragment } from "../../../core/graphql";
import { Layout } from "../../../layout/inquiry";
import { FactoidLite } from "../../../components/data/Factoid";
import { getPersonFactoidsSearchLink, PersonLite } from "../../../components/data/Person";
import { SourceLite } from "../../../components/data/Source";
import FactoidsTimeline from "../../../components/viz/FactoidsTimeline";
import { DataSourceBadge } from "packages/client/src/components/data/DataSource";
import { mergeDataSourcesConnections } from "../../../core/graphql/fragments/dataSource";
import { Annotations } from "packages/client/src/components/annotation/Annotations";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: physicalPersonByIdQuery,
    variables: {
      id: `${context.query.inquiry}-${isArray(context.query.id) ? context.query.id.join("/") : context.query.id}`,
    },
  });
  if (!result || (result.data?.physicalPeople || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      person: result.data.physicalPeople[0],
    },
  });
};

export const PersonPage: NextPage<{ person: PhysicalPersonFullFragment }> = ({ person }) => {
  const sortedFactoids = useMemo(() => {
    return sortBy(
      person.participatesTo.filter((p) => p.factoid.dates && p.factoid.dates.length > 0),
      (p) => [
        p.factoid.dates[0]?.startIso,
        // something wrong here will be replaced by ES at some point
        -1 *
          Math.abs(
            (p.factoid.dates[0]?.end ? new Date(p.factoid.dates[0].end).getTime() : Infinity) -
              (p.factoid.dates[0]?.start ? new Date(p.factoid.dates[0].start).getTime() : -Infinity),
          ),
      ],
    );
  }, [person.participatesTo]);
  const noDateFactoids = useMemo(() => {
    return person.participatesTo.filter((p) => !p.factoid.dates || p.factoid.dates.length === 0);
  }, [person.participatesTo]);
  const timeScope = useMemo(() => {
    const minDate = new Date(min(sortedFactoids[0].factoid.dates.map((d) => d.startIso || d.endIso)) || "");
    const maxDate = new Date(
      max(flatten(sortedFactoids.map((f) => f.factoid.dates.map((d) => d.endIso || d.startIso)))) || "",
    );
    const years = maxDate.getFullYear() - minDate.getFullYear() + 1;
    const months = years > 1 ? 0 : maxDate.getMonth() - minDate.getMonth();
    return {
      startTime: minDate,
      years,
      months,
    };
  }, [sortedFactoids]);

  return (
    <Layout inquiry={person.inquiry} item={person}>
      <div className="row">
        {!isEmpty(person.otherNames) && (
          <>
            <div className="col-3">Autre(s) nom(s)</div>
            <div className="col-9">{person.otherNames.map((n) => n.name).join(" |")}</div>
          </>
        )}

        {!isEmpty(person.description) && (
          <>
            <div className="col-3">Description</div>
            <div className="col-9">{person.description}</div>
          </>
        )}

        {!isEmpty(person.connectedTo) && (
          <>
            <div className="col-3">Connecté à</div>
            <div className="col-9">
              <ul className="list-unstyled">
                {sortBy(person.connectedTo, (p) => p.name).map((p) => (
                  <li key={p.id} className="mb-3">
                    <PersonLite person={p} />
                  </li>
                ))}
                {person.connectedToConnection.totalCount > person.connectedTo.length && (
                  <li>+ {person.connectedToConnection.totalCount - person.connectedTo.length} autres personnes</li>
                )}
              </ul>
            </div>
          </>
        )}

        {!isEmpty(person.participatesTo) && (
          <>
            <div className="col-12 mt-2">
              <h2 className="text-factoid">
                A participé à{" "}
                <a className="text-factoid" href={getPersonFactoidsSearchLink(person)}>
                  {person.participatesToConnection.totalCount} Factoïdes
                </a>
              </h2>
            </div>
            {/* TODO use ROLES */}
            {person.participatesToConnection.totalCount === person.participatesTo.length && (
              <>
                <div className="col-3">{sortedFactoids.length} Factoïds datés</div>
                <div className="col-9">
                  <FactoidsTimeline
                    startYear={timeScope.startTime.getFullYear()}
                    timeScope={timeScope}
                    total={person.participatesToConnection.totalCount}
                    factoids={sortedFactoids.map((p) => p.factoid)}
                  />
                </div>
                <div className="col-3">{noDateFactoids.length} Factoids non datés</div>
                <div className="col-9">
                  <ul className="list-unstyled">
                    {noDateFactoids.map((p) => (
                      <li key={p.id} className="mb-3">
                        <FactoidLite factoid={p.factoid} />
                      </li>
                    ))}
                    {person.participatesToConnection.totalCount > person.participatesTo.length && (
                      <li>
                        + {person.participatesToConnection.totalCount - person.participatesTo.length} autres faits
                      </li>
                    )}
                  </ul>
                </div>
              </>
            )}
          </>
        )}

        {!isEmpty(person.groups) && (
          <>
            <div className="col-3">Groupes</div>
            <div className="col-9">{person.groups.map((n) => n.name).join(" | ")}</div>
          </>
        )}

        {!isEmpty(person.sources) && (
          <>
            <div className="col-3">Sources</div>
            <div className="col-9">
              <ul className="list-unstyled">
                {person.sources.map((source) => (
                  <li key={source.id} className="mb-3">
                    <SourceLite source={source} />
                  </li>
                ))}
                {person.sourcesConnection.totalCount > person.sources.length && (
                  <li>+ {person.sourcesConnection.totalCount - person.sources.length} autres sources</li>
                )}
              </ul>
            </div>
          </>
        )}

        {!isEmpty(person.dataSourcesConnection) && (
          <>
            <div className="col-3">Source{person.dataSourcesConnection.totalCount > 1 ? "s" : ""} des données</div>
            <div className="col-9">
              <ul className="list-unstyled">
                <DataSourceBadge sources={mergeDataSourcesConnections(person.dataSourcesConnection.edges)} />
              </ul>
            </div>
          </>
        )}
        {!isEmpty(person.annotations) && <Annotations annotations={person.annotations} />}
      </div>
    </Layout>
  );
};

export default PersonPage;
