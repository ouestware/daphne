import { NextPage, GetServerSideProps } from "next";
import { isArray, isEmpty, sortBy, pick } from "lodash";

import { initializeApollo, addApolloState } from "../../../core/graphql/client";
import { factoidByIdQuery, FactoidFullFragment } from "../../../core/graphql";
import { Layout } from "../../../layout/inquiry";
import { Dates } from "../../../components/data/Date";
import { FactoidLite } from "../../../components/data/Factoid";
import { PersonLite } from "../../../components/data/Person";
import { PlaceLite } from "../../../components/data/Place";
import { DataSourceBadge } from "../../../components/data/DataSource";
import { mergeDataSourcesConnections } from "../../../core/graphql/fragments/dataSource";
import { SourcesBadge } from "../../../components/data/Source";
import { Annotations } from "packages/client/src/components/annotation/Annotations";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: factoidByIdQuery,
    variables: {
      id: `${context.query.inquiry}-${isArray(context.query.id) ? context.query.id.join("/") : context.query.id}`,
    },
  });
  if (!result || (result.data?.factoids || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      factoid: result.data.factoids[0],
    },
  });
};

export const FactoidPage: NextPage<{ factoid: FactoidFullFragment }> = ({ factoid }) => {
  return (
    <Layout item={factoid} inquiry={factoid.inquiry}>
      {!isEmpty(factoid.description) && (
        <div className="row">
          <div className="col-3">Description</div>
          <div className="col-9">{factoid.description}</div>
        </div>
      )}

      <div className="row">
        <div className="col-3">Incertitude</div>
        <div className="col-9">{factoid.certainty}</div>
      </div>

      {!isEmpty(factoid.originalText) && (
        <div className="row">
          <div className="col-3">Texte</div>
          <div className="col-9">{factoid.originalText}</div>
        </div>
      )}

      {!isEmpty(factoid.duration) && (
        <div className="row">
          <div className="col-3">Durée</div>
          <div className="col-9">{factoid.duration}</div>
        </div>
      )}

      {!isEmpty(factoid.dates) && (
        <div className="row">
          <div className="col-3">Date(s)</div>
          <div className="col-9">
            <Dates dates={factoid.dates} />
          </div>
        </div>
      )}
      {!isEmpty(factoid.impacts) && (
        <div className="row">
          <div className="col-3">Impact(s)</div>
          <div className="col-9">
            {factoid.impacts
              .map((n) => n.name)
              .sort()
              .join(" | ")}
          </div>
        </div>
      )}
      {!isEmpty(factoid.places) && (
        <div className="row">
          <div className="col-3">Lieu(x)</div>
          <div className="col-9">
            <ul className="list-unstyled">
              {sortBy(factoid.places, (p) => p.name).map((place) => (
                <li key={place.id} className="mb-3">
                  <PlaceLite place={place} />
                </li>
              ))}
              {factoid.placesConnection.totalCount > factoid.places.length && (
                <li>+ {factoid.placesConnection.totalCount - factoid.places.length} autres lieux</li>
              )}
            </ul>
          </div>
        </div>
      )}
      {!isEmpty(factoid.types) && (
        <div className="row">
          <div className="col-3">Type(s)</div>
          <div className="col-9">
            {factoid.types
              .map((n) => n.name)
              .sort()
              .join(" | ")}
          </div>
        </div>
      )}
      {!isEmpty(factoid.sources) && (
        <div className="row">
          <div className="col-3">Source(s)</div>
          <div className="col-9">
            {" "}
            {factoid.sourcesConnection.edges.map((s) => (
              <SourcesBadge
                key={s.node.id}
                sources={[{ ...s.node, ...pick(s, ["certainty", "page", "permalink"]) }]}
                sourcesCount={1}
              />
            ))}
          </div>
        </div>
      )}
      {!isEmpty(factoid.linkedTo) && (
        <div className="row">
          <div className="col-3">Lié à</div>
          <div className="col-9">
            <ul className="list-unstyled">
              {factoid.linkedTo.map((f) => (
                <li key={f.id} className="mb-3">
                  <FactoidLite factoid={f} />
                </li>
              ))}
              {factoid.linkedToConnection.totalCount > factoid.linkedTo.length && (
                <li>+ {factoid.linkedToConnection.totalCount - factoid.linkedTo.length} autres faits</li>
              )}
            </ul>
          </div>
        </div>
      )}
      {!isEmpty(factoid.persons) && (
        <div className="row">
          <div className="col-3">Personne(s)</div>
          <div className="col-9">
            <ul className="list-unstyled">
              {sortBy(factoid.persons, (p) => p.person.name).map((p) => (
                <li key={p.id} className="mb-3">
                  <PersonLite person={p.person} />
                </li>
              ))}
              {factoid.personsConnection.totalCount > factoid.persons.length && (
                <li>+ {factoid.personsConnection.totalCount - factoid.persons.length} autres personnes</li>
              )}
            </ul>
          </div>
        </div>
      )}
      {!isEmpty(factoid.dataSourcesConnection) && (
        <div className="row">
          <div className="col-3">Source{factoid.dataSourcesConnection.totalCount > 1 ? "s" : ""} des données</div>
          <div className="col-9">
            <DataSourceBadge sources={mergeDataSourcesConnections(factoid.dataSourcesConnection.edges)} />
          </div>
        </div>
      )}
      {!isEmpty(factoid.annotations) && <Annotations annotations={factoid.annotations} />}
    </Layout>
  );
};

export default FactoidPage;
