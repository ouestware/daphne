import { NextPage, GetServerSideProps } from "next";
import { isArray, isEmpty, sortBy } from "lodash";

import { initializeApollo, addApolloState } from "../../../core/graphql/client";
import { placeByIdQuery, PlaceFullFragment } from "../../../core/graphql";
import { Layout } from "../../../layout/inquiry";
import { FactoidLite } from "../../../components/data/Factoid";
import { DateDisplay } from "../../../components/data/Date";

import { DataSourceBadge } from "../../../components/data/DataSource";
import { mergeDataSourcesConnections } from "../../../core/graphql/fragments/dataSource";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: placeByIdQuery,
    variables: {
      id: `${context.query.inquiry}-${isArray(context.query.id) ? context.query.id.join("/") : context.query.id}`,
    },
  });
  if (!result || (result.data?.places || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      place: result.data.places[0],
    },
  });
};

export const PlacePage: NextPage<{ place: PlaceFullFragment }> = ({ place }) => {
  return (
    <Layout inquiry={place.inquiry} item={place}>
      <div className="row">
        {!isEmpty(place.alternativeNames) && (
          <>
            <div className="col-3">Autres noms</div>
            <dl className="col-9">
              {place.alternativeNames?.map((name, i) => (
                <dd key={i}>{name}</dd>
              ))}
            </dl>
          </>
        )}
        {!isEmpty(place.description) && (
          <>
            <div className="col-3">Description</div>
            <div className="col-9">{place.description}</div>
          </>
        )}

        {!isEmpty(place.locatedIn) && (
          <>
            <div className="col-3">Situation géopgraphique</div>
            <div className="col-9">
              <ul>
                {sortBy(place.locatedIn, (l) => l.date.startIso).map((l, i) => (
                  <li key={i}>
                    <DateDisplay date={l.date} />
                    {l.zone.description}
                    {l.certainty}
                  </li>
                ))}
              </ul>
            </div>
          </>
        )}

        {!isEmpty(place.factoids) && (
          <>
            <div className="col-3">Cité dans </div>
            <div className="col-9">
              <ul className="list-unstyled">
                {sortBy(place.factoids, (p) => p.dates[0]?.startIso).map((p) => (
                  <li key={p.id} className="mb-3">
                    <FactoidLite factoid={p} />
                  </li>
                ))}
                {place.factoidsConnection.totalCount > place.factoids.length && (
                  <li>+ {place.factoidsConnection.totalCount - place.factoids.length} autres faits</li>
                )}
              </ul>
            </div>
          </>
        )}

        {!isEmpty(place.dataSourcesConnection) && (
          <div className="row">
            <div className="col-3">Source{place.dataSourcesConnection.totalCount > 1 ? "s" : ""} des données</div>
            <div className="col-9">
              <DataSourceBadge sources={mergeDataSourcesConnections(place.dataSourcesConnection.edges)} />
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default PlacePage;
