import { debounce, head, omit, pick } from "lodash";
import { NextPage, GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { useState, useCallback, useEffect, useMemo } from "react";
import { FaSearch, FaBars } from "react-icons/fa";
import InfiniteScroll from "react-infinite-scroll-component";

import config from "../../config";
import { initializeApollo, addApolloState } from "../../core/graphql/client";
import { SearchAggregationFragment, inquiryById, InquiryFragment, AggregationType } from "../../core/graphql";
import { DropDownMenu } from "../../components/DropDownMenu";
import { Layout } from "../../layout/inquiry";
import { Loader } from "../../components/Loader";
import { ScrollToTop } from "../../components/ScrollToTop";
import { getFiltersForQuery, getFiltersFromQuery } from "../../components/search/utils";
import { SEARCH_CONFIG } from "../../components/search/definitions";
import { SearchType, SearchConfig } from "../../components/search/types";
import { useAppSearch } from "../../components/search/useAppSearch";
import { SearchFacet } from "../../components/search/SearchFacet";
import { SearchSort } from "../../components/search/SearchSort";
import { SearchResultItem } from "../../components/search/SearchResultItem";
import { SearchHistogram } from "../../components/search/SearchHistogram";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: inquiryById,
    variables: {
      id: `${context.query.inquiry}`,
    },
  });
  if (!result || (result.data?.inquiries || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      inquiry: result.data.inquiries[0],
    },
  });
};

export const SearchPage: NextPage<{ inquiry: InquiryFragment }> = ({ inquiry }) => {
  const router = useRouter();
  const { loading, search } = useAppSearch();
  const [sort, setSort] = useState<string>(`${router.query.sort}` || "score");
  const [inquiryId] = useState<string>(`${router.query.inquiry || "*"}`);
  const [type, setType] = useState<SearchType>(
    Object.keys(SEARCH_CONFIG).includes(`${router.query.type}`) ? (`${router.query.type}` as SearchType) : "factoids",
  );
  const [page, setPage] = useState<number>(0);
  const [total, setTotal] = useState<number>(0);
  const [hasMore, setHasMore] = useState<boolean>(true);
  const [query, setQuery] = useState<string>(`${router.query.query || ""}`);
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const [searchResults, setSearchResults] = useState<Array<any>>([]);
  const [facetResults, setFacetResults] = useState<{
    [key: string]: SearchAggregationFragment;
  }>({});
  const [facetFilters, setFacetFilters] = useState<{
    [key: string]: Array<string>;
  }>(getFiltersFromQuery(router.query, "f."));

  const searchDef: SearchConfig = SEARCH_CONFIG[type];

  const doSearch = useCallback(
    async (currentPage = 0, text?: string) => {
      const currentSearch = text || query;
      if (text) setQuery(query);
      setPage(currentPage);

      const { data } = await search(type, {
        inquiryId,
        search: currentSearch,
        skip: currentPage * config.paginationSize,
        limit: config.paginationSize,
        aggregations: searchDef.facets
          .filter((f) => f.aggType)
          .map((f) => ({ ...omit(f, ["label", "filterType", "aggType"]), type: f.aggType as AggregationType })),
        filters: Object.keys(facetFilters)
          .filter(
            (facetId) =>
              searchDef.facets.find((e) => e.id === facetId) &&
              facetFilters[facetId] &&
              facetFilters[facetId].length > 0,
          )
          .map((facetId) => {
            const values = facetFilters[facetId];
            const facetDef = searchDef.facets.find((e) => e.id === facetId);
            if (!facetDef) throw new Error("Bad filter input");
            return {
              field: `${facetDef.nested ? facetDef.nested + "." : ""}${facetDef.field}`,
              type: facetDef.filterType,
              values,
            };
          }),
        sort: head(
          searchDef.sorts
            .filter((s) => s.id === sort)
            .map((s) => ({
              field: s.field,
              order: s.order,
            })),
        ),
      });
      setFacetResults(
        data
          ? data.search.aggregations.reduce(
              (acc, curr) => ({
                ...acc,
                [curr.name]: { ...curr, selected: [] },
              }),
              {},
            )
          : {},
      );
      setSearchResults((prev) => [...(currentPage > 0 ? prev : []), ...(data ? data.search.results : [])]);
      setHasMore(data ? data.search.results.length === config.paginationSize : false);
      setTotal(data ? data.search.total : 0);
    },
    [type, search, facetFilters, searchDef, sort, query, inquiryId],
  );

  /**
   * When type changed
   * => reset filters and sort
   */
  useEffect(() => {
    setSort((prev) => {
      if (searchDef.sorts.find((e) => e.id === prev)) return prev;
      return "score";
    });
    setFacetFilters((prev) => {
      const keepKeys = Object.keys(prev).filter((key) => searchDef.facets.find((f) => f.id === key));
      return pick(prev, keepKeys);
    });
  }, [type]);

  /**
   * When search type, sort, filters or query change
   * => redo a query from scratch and change url
   */
  useEffect(() => {
    doSearch(0);
    router.push(
      `/${router.query.inquiry}/search?${new URLSearchParams({
        type,
        query,
        sort,
        ...getFiltersForQuery(facetFilters, "f."),
      }).toString()}`,
      undefined,
      { shallow: true },
    );
  }, [type, search, facetFilters, searchDef, sort, query, doSearch, router.query.inquiry]);

  const histoFacet = useMemo(() => {
    const def = searchDef.facets.find((e) => e.aggType === "date_histogram_per_year");
    return def ? facetResults[def.id] : null;
  }, [facetResults, searchDef]);

  return (
    <>
      {searchDef && (
        <Layout title={`Recherche de ${searchDef.label}`} inquiry={inquiry}>
          {/* Search input */}
          <div className="row">
            <form
              onSubmit={(e) => {
                e.preventDefault();
                doSearch(0);
              }}
            >
              <div className="input-group mb-3">
                <DropDownMenu
                  title={searchDef.label}
                  options={Object.keys(SEARCH_CONFIG).map((st) => ({
                    onClick: () => setType(st as SearchType),
                    label: SEARCH_CONFIG[st as SearchType].label,
                  }))}
                />

                <input
                  type="string"
                  name="search"
                  className="form-control"
                  placeholder="recherche..."
                  defaultValue={query}
                  onChange={debounce((e) => {
                    setQuery(e.target.value);
                  }, config.debounceTimeMs)}
                />

                <button type="submit" className="btn btn-outline-secondary">
                  <FaSearch className="me-1 " />
                </button>
              </div>
            </form>
          </div>

          <div className="row">
            {/* Search facets */}
            <div className="col-4">
              <SearchSort sorts={searchDef.sorts} selected={sort} onChange={setSort} />
              {searchDef.facets.map((facet) => (
                <SearchFacet
                  id={facet.id}
                  key={facet.id}
                  className="mt-4"
                  facet={facet}
                  data={facetResults[facet.id]}
                  selected={facetFilters[facet.id]}
                  max={total}
                  doSearch={async (text) => {
                    const { data } = await search(
                      type,
                      {
                        inquiryId,
                        search: query,
                        skip: 0,
                        limit: 0,
                        filters: [],
                        sort: undefined,
                        aggregations: [
                          {
                            ...omit(facet, ["label", "aggType", "filterType"]),
                            type: facet.aggType || AggregationType.Terms,
                            include: text,
                            orderByAlpha: true,
                          },
                        ],
                      },
                      false,
                    );
                    return data?.search?.aggregations[0].values.map((e: any) => e.key) || [];
                  }}
                  onChange={(e) => {
                    setFacetFilters((prev) => ({
                      ...prev,
                      [facet.id]: e,
                    }));
                  }}
                />
              ))}
            </div>

            {/* Search results */}
            <div className="col-8">
              {searchResults && (
                <>
                  <h2>
                    <FaBars className="me-1" />
                    {total} {searchDef.label}
                  </h2>

                  {histoFacet && histoFacet.values.length > 0 && (
                    <SearchHistogram
                      className="my-3"
                      facet={histoFacet}
                      //  before allowing selection we need to fix issue with absolute dates retrieval
                      // onSelectionEnd={(dates) => {
                      //   setFacetFilters((prev) => ({ ...prev, dates: dates === null ? [] : [dates.from, dates.to] }));
                      // }}
                    />
                  )}

                  <InfiniteScroll
                    dataLength={searchResults.length}
                    hasMore={hasMore}
                    next={() => {
                      doSearch(page + 1);
                    }}
                    loader={<></>}
                  >
                    {searchResults.map((item) => (
                      <div className="col-12 mb-4" key={item.id}>
                        <SearchResultItem item={item} />
                      </div>
                    ))}
                  </InfiniteScroll>
                </>
              )}
            </div>
          </div>
          {loading && <Loader />}
          <ScrollToTop />
        </Layout>
      )}
    </>
  );
};

export default SearchPage;
