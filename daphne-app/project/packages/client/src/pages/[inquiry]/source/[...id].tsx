import { NextPage, GetServerSideProps } from "next";
import { isArray, isEmpty, sortBy } from "lodash";

import { initializeApollo, addApolloState } from "../../../core/graphql/client";
import { sourceByIdQuery, SourceFullFragment } from "../../../core/graphql";
import { Layout } from "../../../layout/inquiry";
import { FactoidLite } from "../../../components/data/Factoid";
import { PersonLite } from "../../../components/data/Person";

import { DataSourceBadge } from "../../../components/data/DataSource";
import { mergeDataSourcesConnections } from "../../../core/graphql/fragments/dataSource";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: sourceByIdQuery,
    variables: {
      id: `${context.query.inquiry}-${isArray(context.query.id) ? context.query.id.join("/") : context.query.id}`,
    },
  });
  if (!result || (result.data?.sources || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      source: result.data.sources[0],
    },
  });
};

export const SourcePage: NextPage<{ source: SourceFullFragment }> = ({ source }) => {
  return (
    <Layout inquiry={source.inquiry} item={source}>
      <div className="row">
        {!isEmpty(source.description) && (
          <>
            <div className="col-3">Description</div>
            <div className="col-9">{source.description}</div>
          </>
        )}

        {!isEmpty(source.language) && (
          <>
            <div className="col-3">Language</div>
            <div className="col-9">{source.language}</div>
          </>
        )}

        {!isEmpty(source.reputation) && (
          <>
            <div className="col-3">Réputation</div>
            <div className="col-9">{source.reputation}</div>
          </>
        )}

        {!isEmpty(source.issuing) && (
          <>
            <div className="col-3">Recense</div>
            <div className="col-9">
              <ul className="list-unstyled">
                {sortBy(source.issuing, (p) => p.name).map((p) => (
                  <li key={p.id} className="mb-3">
                    <PersonLite person={p} />
                  </li>
                ))}
                {source.issuingConnection.totalCount > source.issuing.length && (
                  <li>+ {source.issuingConnection.totalCount - source.issuing.length} autres personnes</li>
                )}
              </ul>
            </div>
          </>
        )}

        {!isEmpty(source.refering) && (
          <>
            <div className="col-3">Se réfère à</div>
            <div className="col-9">
              <ul className="list-unstyled">
                {source.refering.map((p) => (
                  <li key={p.id} className="mb-3">
                    <FactoidLite factoid={p} />
                  </li>
                ))}
                {source.referingConnection.totalCount > source.refering.length && (
                  <li>+ {source.referingConnection.totalCount - source.refering.length} autres faits</li>
                )}
              </ul>
            </div>
          </>
        )}

        {!isEmpty(source.dataSourcesConnection) && (
          <div className="row">
            <div className="col-3">Source{source.dataSourcesConnection.totalCount > 1 ? "s" : ""} des données</div>
            <div className="col-9">
              <DataSourceBadge sources={mergeDataSourcesConnections(source.dataSourcesConnection.edges)} />
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default SourcePage;
