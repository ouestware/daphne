import { useCallback, useMemo } from "react";
import { RGENode } from "react-graph-expand";
import { GetServerSideProps, NextPage } from "next";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

import { Layout } from "../../layout/inquiry";
import { FullNodeType } from "../../utils/graph";
import { useGetGraph } from "../../components/graph/useGetGraph";
import { Loader } from "../../components/Loader";
import { useGraphSearch } from "../../components/graph/useGraphSearch";
import { addApolloState, initializeApollo, inquiryById, InquiryFragment } from "../../core/graphql";

import "react-graph-expand/style.css";
import { useFactoidsAggregations } from "../../components/graph/useFactoisAggregation";

const GraphExplorer = dynamic(() => import("../../components/graph/GraphExplorer").then((mod) => mod.GraphExplorer), {
  ssr: false,
  loading: () => <Loader message="Chargement du composant..." />,
});

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: inquiryById,
    variables: {
      id: `${context.query.inquiry}`,
    },
  });
  if (!result || (result.data?.inquiries || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      inquiry: result.data.inquiries[0],
    },
  });
};

const GraphPage: NextPage<{ inquiry: InquiryFragment }> = ({ inquiry }) => {
  const { search } = useGraphSearch();
  const { getGraph } = useGetGraph();

  const router = useRouter();
  const initialQuery: Pick<RGENode, "id" | "type">[] | undefined = useMemo(() => {
    const query = router.query.nodes;
    const nodes = typeof query === "string" ? [query] : Array.isArray(query) ? query : [];
    if (!nodes.length) return undefined;
    return nodes
      .map((node) => {
        const [type, id] = node.split(/\/(.*)/);
        return { id, type };
      })
      .filter(({ id, type }) => id && type);
  }, [router.query]);

  const graphSearch = useCallback(
    async (query?: string) => {
      const { data } = await search({
        inquiryId: inquiry.id,
        search: query || "",
        skip: 0,
        limit: 10,
      });

      return {
        candidates: data?.results || [],
        total: data?.total,
      };
    },
    [search, inquiry.id],
  );

  const getNode = useCallback(
    async (node: Pick<RGENode, "id" | "type">) => {
      const { data } = await getGraph(node.id, node.type as FullNodeType);
      return data || {};
    },
    [getGraph],
  );

  const { factoidsAggregations } = useFactoidsAggregations(inquiry.id);

  return (
    <Layout title="Exploration" inquiry={inquiry} customLayout>
      <GraphExplorer
        ready
        search={graphSearch}
        getNode={getNode}
        initialQuery={initialQuery}
        factoidsAggregations={factoidsAggregations}
      />
    </Layout>
  );
};

export default GraphPage;
