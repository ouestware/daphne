import { AppProps } from "next/app";
import Router from "next/router";
import { useEffect, useState } from "react";

import "../styles/index.scss";
import { AppContextProvider } from "../core/context";
import { ApolloProvider } from "../core/graphql/client";
import { Loader } from "../components/Loader";

function MyApp({ Component, pageProps }: AppProps) {
  const [routerLoading, setRouterLoading] = useState<boolean>(false);

  useEffect(() => {
    Router.events.on("routeChangeStart", () => {
      setRouterLoading(true);
    });
    Router.events.on("routeChangeComplete", () => setRouterLoading(false));
    Router.events.on("routeChangeError", () => setRouterLoading(false));
  }, []);

  return (
    <AppContextProvider>
      <ApolloProvider pageProps={pageProps}>
        <Component {...pageProps} />
        {routerLoading && <Loader />}
      </ApolloProvider>
    </AppContextProvider>
  );
}

export default MyApp;
