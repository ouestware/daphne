import type { NextPage } from "next";

import { Layout } from "../layout";
import { Forbidden } from "../components/Forbidden";

const ForbiddenPage: NextPage = () => {
  return (
    <Layout title={"Forbidden"}>
      <Forbidden />
    </Layout>
  );
};

export default ForbiddenPage;
