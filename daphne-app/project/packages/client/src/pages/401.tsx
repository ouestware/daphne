import type { NextPage } from "next";

import { Layout } from "../layout";
import { Unauthorized } from "../components/Unauthorized";

const UnauthorizedPage: NextPage<{ username?: string }> = ({ username }) => {
  return (
    <Layout title={"Unauthorized"}>
      <Unauthorized username={username} />
    </Layout>
  );
};

export default UnauthorizedPage;
