import type { NextPage } from "next";

import { Layout } from "../layout";
import { Error } from "../components/Error";

const ErrorPage: NextPage = () => {
  return (
    <Layout title={"Error"}>
      <Error />
    </Layout>
  );
};

export default ErrorPage;
