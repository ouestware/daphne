import { NextPage } from "next";

import { Layout } from "../../layout/admin";
import { Markdown } from "../../components/Markdown";
import AdminWelcomeContent from "../../assets/markdowns/AdminWelcome.md";

export const AdminInquiriesPage: NextPage = () => {
  return (
    <Layout title="Administration">
      <Markdown content={AdminWelcomeContent} />
    </Layout>
  );
};

export default AdminInquiriesPage;
