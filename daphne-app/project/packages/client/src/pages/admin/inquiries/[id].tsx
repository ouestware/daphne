import { NextPage, GetServerSideProps } from "next";

import { initializeApollo, addApolloState, inquiryById, InquiryFragment } from "../../../core/graphql";
import { useNotifications } from "../../../core/notifications";
import { Layout } from "../../../layout/admin";
import { InquiryForm } from "../../../components/inquiry/InquiryForm";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: inquiryById,
    variables: { id: `${context.query.id}` },
  });
  if (!result || (result.data?.inquiries || []).length === 0) {
    return {
      notFound: true,
    };
  }
  return addApolloState(apolloClient, {
    props: {
      inquiry: result.data.inquiries[0],
    },
  });
};

export const AdminInquiryUpdatePage: NextPage<{ inquiry: InquiryFragment }> = ({ inquiry }) => {
  const { notify } = useNotifications();

  return (
    <Layout title={`Edition de ${inquiry.name}`}>
      <InquiryForm
        inquiry={inquiry}
        onSuccess={(e) => {
          notify({ type: "success", message: `L'enquête ${e.name} a été créée avec succès` });
        }}
      />
    </Layout>
  );
};

export default AdminInquiryUpdatePage;
