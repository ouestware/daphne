import { NextPage } from "next";
import Link from "next/link";
import { isNil } from "lodash";
import { useCallback, useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useLazyQuery } from "@apollo/client";
import { FaPlus, FaRegFolderOpen, FaRegEdit, FaTrashAlt } from "react-icons/fa";

import config from "../../../config";
import { InquiryFragment, inquiriesList } from "../../../core/graphql";
import { useModal } from "../../../core/modals";
import { Layout } from "../../../layout/admin";
import { InquiryCard } from "../../../components/inquiry/InquiryCard";
import { InquiryDeleteModal } from "../../../components/inquiry/InquiryDeleteModal";
import { Loader } from "../../../components/Loader";

export const AdminInquiriesPage: NextPage = () => {
  const { openModal } = useModal();
  const [fetch, { loading }] = useLazyQuery(inquiriesList);
  const [result, setResult] = useState<Array<InquiryFragment>>([]);
  const [skip, setSkip] = useState<number>(0);
  const [hasMore, setHasMore] = useState<boolean>(true);

  const load = useCallback(
    async (limit = config.paginationSize) => {
      try {
        const resp = await fetch({ variables: { skip: skip, limit } });
        if (!isNil(resp.data) && !isNil(resp.data.inquiries)) {
          setHasMore(resp.data.inquiries.length === config.paginationSize);
          setResult((prev) => {
            const rs = [...(skip > 0 ? prev : []), ...(resp.data?.inquiries || [])] as Array<InquiryFragment>;
            setSkip(rs.length);
            return rs;
          });
        }
      } catch (e) {
        console.error(e);
      }
    },
    [fetch, skip],
  );

  useEffect(() => {
    load();
  }, [load]);

  return (
    <Layout title="Liste des Enquêtes">
      <InfiniteScroll
        className="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 g-4 pb-4"
        dataLength={result.length}
        hasMore={hasMore}
        next={() => load()}
        loader={<></>}
      >
        {result.map((e) => (
          <div className="col" key={e.id}>
            <InquiryCard inquiry={e}>
              <ul>
                <li>
                  <Link href={`/${e.id}/search`} title={`Consulter l'enquête ${e.name}`}>
                    <FaRegFolderOpen className="me-1" /> Consulter
                  </Link>
                </li>
                <li>
                  <Link href={`/admin/inquiries/${e.id}`} title={`Modifier l'enquête ${e.name}`}>
                    <FaRegEdit className="me-2" />
                    Modifier
                  </Link>
                </li>
                <li>
                  <button
                    className="btn btn-a"
                    onClick={() => {
                      openModal({
                        component: InquiryDeleteModal,
                        arguments: {
                          inquiry: e,
                        },
                        afterSubmit: () => {
                          setSkip(0);
                          load(result.length);
                        },
                      });
                    }}
                    title={`Supprimer l'enquête ${e.name}`}
                  >
                    <FaTrashAlt className="me-1" /> Supprimer
                  </button>
                </li>
              </ul>
            </InquiryCard>
          </div>
        ))}
        <div className="col">
          <div className="card inquiry justify-content-between">
            <div className="card-image p-5">
              <Link className="stretched-link" href="/admin/inquiries/create" title="Créer une nouvelle enquête">
                <FaPlus size="4em" />
              </Link>
            </div>
            <div className="card-footer text-center">Créer une enquête</div>
          </div>
        </div>
      </InfiniteScroll>
      {loading && <Loader />}
    </Layout>
  );
};

export default AdminInquiriesPage;
