import { NextPage } from "next";
import { useRouter } from "next/router";

import { useNotifications } from "../../../core/notifications";
import { Layout } from "../../../layout/admin";
import { InquiryForm } from "../../../components/inquiry/InquiryForm";

export const AdminInquiryCreatePage: NextPage = () => {
  const router = useRouter();
  const { notify } = useNotifications();
  return (
    <Layout title="Nouvelle enquête">
      <InquiryForm
        onSuccess={(e) => {
          notify({ type: "success", message: `L'enquête ${e.name} a été créée avec succès` });
          router.push({ pathname: `/admin/inquiries/${e.id}` });
        }}
      />
    </Layout>
  );
};

export default AdminInquiryCreatePage;
