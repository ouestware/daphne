import { NextPage } from "next";

import { useNotifications } from "../../core/notifications";
import { Layout } from "../../layout/admin";
import { ChangePasswordForm } from "../../components/user/ChangePasswordForm";

const AdminPasswordPage: NextPage = () => {
  const { notify } = useNotifications();

  return (
    <Layout title="Modification du mot-de-passe">
      <ChangePasswordForm
        onSuccess={() =>
          notify({
            type: "success",
            title: "Changement de mot-de-passe",
            message: "Votre mot-de-passe a été mis à jour avec succès",
          })
        }
      />
    </Layout>
  );
};

export default AdminPasswordPage;
