import { NextPage, GetServerSideProps } from "next";
import { useState, useCallback } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useLazyQuery } from "@apollo/client";
import { isNil } from "lodash";

import config from "../config";
import { initializeApollo, addApolloState } from "../core/graphql/client";
import { inquiriesList, InquiryFragment } from "../core/graphql";
import { Layout } from "../layout";
import { Loader } from "../components/Loader";
import { InquiryCard, ProsoVizCard } from "../components/inquiry/InquiryCard";

export const getServerSideProps: GetServerSideProps = async () => {
  const apolloClient = initializeApollo();
  const result = await apolloClient.query({
    query: inquiriesList,
    variables: { limit: config.paginationSize },
  });
  return addApolloState(apolloClient, {
    props: {
      inquiries: result.data?.inquiries || [],
    },
  });
};

const HomePage: NextPage<{ inquiries: Array<InquiryFragment> }> = ({ inquiries }) => {
  const [fetch, { loading }] = useLazyQuery(inquiriesList);
  const [result, setResult] = useState<Array<InquiryFragment>>(inquiries);
  const [skip, setSkip] = useState<number>(inquiries.length);
  const [hasMore, setHasMore] = useState<boolean>(true);

  const load = useCallback(
    async (limit = config.paginationSize) => {
      try {
        const resp = await fetch({ variables: { skip: skip, limit } });
        if (!isNil(resp.data) && !isNil(resp.data.inquiries)) {
          setHasMore(resp.data.inquiries.length === config.paginationSize);
          setResult((prev) => {
            const rs = [...(skip > 0 ? prev : []), ...(resp.data?.inquiries || [])] as Array<InquiryFragment>;
            setSkip(rs.length);
            return rs;
          });
        }
      } catch (e) {
        console.error(e);
      }
    },
    [fetch, skip],
  );

  return (
    <Layout>
      <InfiniteScroll
        className="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 g-4 pb-4"
        dataLength={result.length}
        hasMore={hasMore}
        next={() => load()}
        loader={<></>}
      >
        {inquiries.map((inquiry) => (
          <div className="col" key={inquiry.id}>
            <InquiryCard inquiry={inquiry} />
          </div>
        ))}
        <div className="col">
          <ProsoVizCard />
        </div>
      </InfiniteScroll>
      {loading && <Loader />}
    </Layout>
  );
};

export default HomePage;
