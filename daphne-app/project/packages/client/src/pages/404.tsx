import type { NextPage } from "next";

import { Layout } from "../layout";
import { NotFound } from "../components/NotFound";

const NotFoundPage: NextPage = () => {
  return (
    <Layout title={"Not found"}>
      <NotFound />
    </Layout>
  );
};

export default NotFoundPage;
