/**
 * To know why and how to use it, check :
 * - https://stackoverflow.com/questions/63126355/loading-react-hooks-using-dynamic-imports/63438354#63438354
 * - https://codesandbox.io/s/musing-dhawan-noxj2?file=/src/SuspenseWay/index.ts:0-875
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const cache: Record<string, any> = {};
const errorsCache: Record<string, Error | undefined> = {};

// <Suspense> catches the thrown promise
// and rerenders children when promise resolves
export const useSuspense = <T,>(importPromiseFunc: Promise<T> | (() => Promise<T>), cacheKey: string): T => {
  const cachedModule = cache[cacheKey];
  // already loaded previously
  if (cachedModule) return cachedModule;

  //prevents import() loop on failed imports
  if (errorsCache[cacheKey]) throw errorsCache[cacheKey];

  const importPromise = typeof importPromiseFunc === "function" ? importPromiseFunc() : importPromiseFunc;

  // gets caught by Suspense
  throw importPromise
    .then((mod) => (cache[cacheKey] = mod))
    .catch((err) => {
      errorsCache[cacheKey] = err;
    });
};
