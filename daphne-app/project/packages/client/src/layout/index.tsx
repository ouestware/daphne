import { FC, PropsWithChildren, ComponentType } from "react";
import cx from "classnames";
import Head from "next/head";
import { isNil } from "lodash";

import config from "../config";
import { Modals } from "../core/modals";
import { Notifications } from "../core/notifications";
import { Loader } from "../components/Loader";
import { Header } from "./Header";
import { Footer } from "./Footer";
import { Heading } from "./Heading";

export interface LayoutProps {
  /**
   * Class that will be added to the root element
   */
  className?: string;
  /**
   * If <code>true</code> will display a loader on the page.
   * Useful with next router when a page is loading
   */
  loading?: boolean;
  /**
   * Title that will be used in the HTML head.
   */
  title?: string | null;
  /**
   * Description that will be used in the HTML head.
   */
  description?: string | null;
  /**
   * To override the header component (ie. the top bar)
   */
  header?: ComponentType<any>;
  /**
   * To override the heading component (ie. the title of the page).
   * Default heading just display the title in a H1 tag
   */
  heading?: ComponentType<{ title?: string | null }>;
  /**
   * Allows having children that do not go inside a classic ".row" container
   */
  customLayout?: boolean;
}
export const Layout: FC<PropsWithChildren<LayoutProps>> = ({
  className,
  loading,
  title,
  description,
  children,
  header,
  heading,
  customLayout,
}) => {
  const HeaderComponent = header || Header;
  const HeadingComponent = heading || Heading;

  return (
    <div id="root" className={cx(className)}>
      <Head>
        <title>{isNil(title) ? config.site.title : title}</title>
        <meta name="description" content={isNil(description) ? config.site.description : description} />
        <link rel="icon" href={config.site.favicon} />
      </Head>

      <HeaderComponent />

      <main>
        <div className="container py-3 d-flex flex-column">
          <HeadingComponent title={title} />
          {!customLayout && children}
        </div>

        {customLayout && children}
      </main>

      <Modals />
      <Notifications />

      <Footer />
      {loading && <Loader />}
    </div>
  );
};
