import { FC } from "react";
import Link from "next/link";

export const Footer: FC = () => {
  return (
    <footer className="container d-flex flex-wrap justify-content-center align-items-center py-1 my-1 border-top">
      <ul className="nav col-md-4">
        <li className="nav-item">
          <Link href="/a-propos" className="nav-link px-2 text-muted" title="À propos">
            À propos
          </Link>
        </li>
        <li className="nav-item">
          <Link href="/mentions-legales" className="nav-link px-2 text-muted" title="Mentions légales">
            Mentions légales
          </Link>
        </li>
        <li className="nav-item">
          <Link href="https://gitlab.com/ouestware/daphne/" className="nav-link px-2 text-muted" title="Code Source">
            Code Source
          </Link>
        </li>
        <li className="nav-item">
          <Link href="/admin" className="nav-link px-2 text-muted" title="Admin">
            Admin
          </Link>
        </li>
      </ul>
    </footer>
  );
};
