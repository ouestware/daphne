import { FC } from "react";
import Link from "next/link";
import { FaUserSecret } from "react-icons/fa";

import config from "../../config";
import { InquiryFragment } from "../../core/graphql";
import { useModal } from "../../core/modals";
import { useAuth } from "../../core/user/useAuth";
import { InquiryLoginModal } from "../../components/inquiry/InquiryLoginModal";
import { Toggle } from "../../components/Toggle";
import { InquiryManagementModal } from "../../components/inquiry/InquiryManagementModal";
import { FactoidAnnotationModal } from "../../components/annotation/FactoidAnnotationModal";

import { icons } from "../../components/data/data-styles";

export const Header: FC<{ inquiry: InquiryFragment }> = ({ inquiry }) => {
  const { user } = useAuth();
  const { openModal } = useModal();

  const inquiryModeEnabled = user && user.username === inquiry.id ? true : false;

  return (
    <header className="sticky-top border-bottom">
      <div className="py-1 px-3 d-flex flex-column flex-md-row align-items-center">
        <Link href="/" className="navbar-brand" title="Accueil">
          {config.site.title}
        </Link>
        <span className="mx-2">&gt;</span>
        <Link href={`/${inquiry.id}/search`} className="navbar-brand" title={`Enquête ${inquiry.name}`}>
          {inquiry.name}
        </Link>
        <nav className="d-inline-flex mt-2 mt-md-0 ms-md-auto">
          {inquiryModeEnabled && (
            <button
              className="btn btn-primary me-2"
              type="button"
              onClick={() => {
                openModal({
                  component: FactoidAnnotationModal,
                  arguments: { inquiryId: inquiry.id },
                  afterSubmit: ({ trimedFactoidId }) => {
                    // redirect to new factoid
                    window.location.assign(`/${inquiry.id}/factoid/${trimedFactoidId}`);
                  },
                });
              }}
            >
              <icons.FactoidFill size="1.1rem" /> créer un factoïde
            </button>
          )}
          <Toggle
            id="mode"
            value={inquiryModeEnabled}
            onChange={() => {
              if (!inquiryModeEnabled) {
                openModal({
                  component: InquiryLoginModal,
                  arguments: { inquiry },
                });
              } else {
                openModal({
                  component: InquiryManagementModal,
                  arguments: { inquiry },
                });
              }
            }}
            label={
              <>
                <FaUserSecret className="me-1" />
                {inquiryModeEnabled ? "Mode enquêteur" : "Activer mode enquêteur"}
              </>
            }
            title={inquiryModeEnabled ? "Cliquer pour désactiver le mode enquêteur" : "Activer le mode enquêteur"}
          />
        </nav>
      </div>
    </header>
  );
};
