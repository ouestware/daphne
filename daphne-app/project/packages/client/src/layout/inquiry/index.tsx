import cx from "classnames";
import { FC, PropsWithChildren } from "react";
import { isNil, omit } from "lodash";
import Link from "next/link";

import {
  InquiryFragment,
  FactoidFragment,
  PlaceFragment,
  SourceFragment,
  PhysicalPersonFragment,
} from "../../core/graphql";
import { Layout as LayoutDefault, LayoutProps } from "../index";
import { icons } from "../../components/data/data-styles";
import { getFactoidTitle } from "../../components/data/Factoid";
import { Header } from "./Header";
import { Heading } from "../Heading";
import { getGraphItemLink, Neo4JInlineNode } from "../../utils/graph";
import {
  FactoidAnnotationModal,
  FactoidAnnotationModalProps,
} from "../../components/annotation/FactoidAnnotationModal";
import { useModal } from "../../core/modals";
import { useAuth } from "../../core/user/useAuth";
import { BsFillPencilFill } from "react-icons/bs";

type LayoutInquiryProps = LayoutProps & {
  inquiry: InquiryFragment;
  item?: FactoidFragment | PlaceFragment | SourceFragment | PhysicalPersonFragment;
};

function getPageTitle(props: LayoutInquiryProps): string | undefined {
  const titlePrefix = `Enquête ${props.inquiry.name}`;

  if (isNil(props.item)) return `${titlePrefix} - ${props.title}`;
  switch (props.item.__typename) {
    case "Factoid":
      return `${titlePrefix} - ${getFactoidTitle(props.item)}`;
    case "Place":
      return `${titlePrefix} - ${props.item.name}`;
    case "PhysicalPerson":
      return `${titlePrefix} - ${props.item.name}`;
    case "Source":
      return `${titlePrefix} - ${props.item.name}`;
    default:
      return "";
  }
}

const GraphButton: FC<{ item: Neo4JInlineNode; btnClassName?: string }> = ({ item, btnClassName = "btn-primary" }) => {
  return (
    <Link href={getGraphItemLink(item)} className={cx("btn", btnClassName)}>
      <icons.Graph className="me-2" /> Explorer le graphe
    </Link>
  );
};

const AnnotationButton: FC<{
  item: FactoidFragment | PlaceFragment | SourceFragment | PhysicalPersonFragment;
  inquiryId: string;
}> = ({ item, inquiryId }) => {
  const { openModal } = useModal();

  const { user } = useAuth();

  const inquiryModeEnabled = user && user.username === inquiryId ? true : false;

  let component: FC<FactoidAnnotationModalProps> | null = null;
  switch (item.__typename) {
    case "Factoid":
      component = FactoidAnnotationModal;
      break;
  }

  return inquiryModeEnabled && component !== null ? (
    <button
      className="btn btn-factoid text-white"
      onClick={() => {
        if (component !== null && item.__typename === "Factoid")
          openModal({
            component,
            arguments: { item, inquiryId: item.inquiry.id },
            afterSubmit: () => {
              window.location.reload();
            },
          });
      }}
    >
      <BsFillPencilFill className="me-2" /> Annoter
    </button>
  ) : null;
};

function getPageHeading(props: LayoutInquiryProps): string | null | JSX.Element {
  if (isNil(props.item)) return props.title || null;

  switch (props.item.__typename) {
    case "Factoid":
      return (
        <span className="text-factoid">
          <div>
            <icons.Factoid className="me-2" /> {getFactoidTitle(props.item)}
          </div>
          <GraphButton item={props.item as Neo4JInlineNode} btnClassName="btn-factoid text-white me-3" />
          <AnnotationButton item={props.item} inquiryId={props.inquiry.id} />
        </span>
      );
    case "PhysicalPerson":
      return (
        <span className="text-person">
          <div>
            <icons.Person className="me-2" /> {props.item.name}
          </div>
          <GraphButton item={props.item as Neo4JInlineNode} btnClassName="btn-person text-white" />
        </span>
      );
    case "Place":
      return (
        <span className="text-place">
          <div>
            <icons.Place className="me-2" /> {props.item.name}
          </div>
          <GraphButton item={props.item as Neo4JInlineNode} btnClassName="btn-place text-white" />
        </span>
      );
    case "Source":
      return (
        <span className="text-source">
          <div>
            <icons.Source className="me-2" /> {props.item.name}
          </div>
        </span>
      );
    default:
      return null;
  }
}

export const Layout: FC<PropsWithChildren<LayoutInquiryProps>> = (props) => {
  const layoutProps = {
    ...omit(props, ["children", "inquiry", "item"]),
    title: getPageTitle(props),
    header: () => <Header inquiry={props.inquiry} />,
    heading: () => <Heading title={getPageHeading(props)} />,
  };

  return <LayoutDefault {...layoutProps}>{props.children}</LayoutDefault>;
};
