import { FC } from "react";
import cx from "classnames";

export const Heading: FC<{
  title?: string | JSX.Element | null;
}> = ({ title }) => {
  if (!title) return null;
  return (
    <div
      className={cx(
        "heading d-flex flex-wrap align-items-center justify-content-between flex-shrink-0 flex-grow-0",
      )}
    >
      <h1>{title}</h1>
    </div>
  );
};
