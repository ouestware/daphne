import { FC } from "react";
import Link from "next/link";

import config from "../config";

export const Header: FC = () => {
  return (
    <header className="sticky-top border-bottom">
      <div className="py-1 px-3 d-flex flex-column flex-md-row align-items-center">
        <Link href="/" className="navbar-brand" title="Accueil">
          {config.site.title}
        </Link>
        <nav className="d-inline-flex mt-2 mt-md-0 ms-md-auto"></nav>
      </div>
    </header>
  );
};
