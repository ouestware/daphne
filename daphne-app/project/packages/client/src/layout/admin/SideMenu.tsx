import { FC } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import cx from "classnames";
import { FaSignOutAlt, FaKey, FaUserSecret } from "react-icons/fa";

import { useAuth } from "../../core/user/useAuth";

export const SideMenu: FC<{ className?: string }> = ({ className }) => {
  const { logout } = useAuth();
  const router = useRouter();

  return (
    <div className={cx(className)}>
      <div className="row">
        <ul className="nav flex-column nav-pills">
          <li className="nav-item">
            <Link className="nav-link" href="/admin/inquiries">
              <FaUserSecret className="me-1" />
              Enquête
            </Link>
          </li>
          <li className="nav-item">
            <hr />
          </li>
          <li className="nav-item">
            <Link className="nav-link" href="/admin/password">
              <FaKey className="me-1" />
              Mot-de-passe
            </Link>
          </li>
          <li className="nav-item">
            <button
              className="nav-link btn btn-a"
              onClick={() => {
                logout();
                router.push({ pathname: "/" });
              }}
            >
              <FaSignOutAlt className="me-1" />
              Se déconnecter
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
};
