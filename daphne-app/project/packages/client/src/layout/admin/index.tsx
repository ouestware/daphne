import { FC, PropsWithChildren } from "react";
import { omit } from "lodash";

import { useAuth } from "../../core/user/useAuth";
import Unauthrorized from "../../pages/401";
import Forbidden from "../../pages/403";
import { Layout as LayoutDefault, LayoutProps } from "../index";
import { SideMenu } from "./SideMenu";

export const Layout: FC<PropsWithChildren<LayoutProps>> = (props) => {
  const { user } = useAuth();
  const layoutProps = {
    ...omit(props, ["children"]),
    title: `Administration - ${props.title}`,
    className: "admin",
  };

  return (
    <>
      {!user && <Unauthrorized username="admin" />}
      {user && !user.isAdmin && <Forbidden />}
      {user && user.isAdmin && (
        <LayoutDefault {...layoutProps}>
          <div className="row">
            <div className="col-3">
              <SideMenu />
            </div>
            <div className="col-9">{props.children}</div>
          </div>
        </LayoutDefault>
      )}
    </>
  );
};
