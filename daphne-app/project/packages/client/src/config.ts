const config = {
  notificationTimeoutMs: 3000,
  debounceTimeMs: 500,
  paginationSize: 20,
  site: {
    favicon: "/favicon.ico",
    title: "Daphné",
    description: "",
  },
  graphql: {
    uri: "/graphql",
    server_url: process.env.GRAPHQL_SERVER_URL || "http://localhost/graphql",
  },
};

export default config;
