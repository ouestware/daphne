export function displayStringDateTime(value: string | number, options?: { time: boolean }): string | undefined {
  try {
    const date = new Date(`${value}`);
    if (date)
      return new Intl.DateTimeFormat("en-GB", {
        dateStyle: "short",
        timeStyle: options?.time === false ? undefined : "short",
      }).format(date);
  } catch (e) {
    console.error(e);
  }
  return undefined;
}

export function displayDateYear(value: string | number): number | undefined {
  try {
    const date = new Date(parseInt(`${value}`));
    if (date) return date.getFullYear();
  } catch (e) {
    console.error(e);
  }
  return undefined;
}

export function dateToStringForInput(value: string | number | Date): string | undefined {
  try {
    const date = value instanceof Date ? value : new Date(parseInt(`${value}`));
    if (date)
      return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, "0")}-${(date.getDay() + 1)
        .toString()
        .padStart(2, "0")}`;
  } catch (e) {
    console.error(e);
  }
  return undefined;
}

/**
 * Display the date to the "from ago" format.
 */
export function dateToFromAgo(date: Date): string {
  const seconds = Math.round((Date.now() - date.getTime()) / 1000);
  const prefix = seconds < 0 ? "in " : "";
  const suffix = seconds < 0 ? "" : " ago";
  const absSecond = Math.abs(seconds);

  const times = [
    absSecond / 60 / 60 / 24 / 365, // years
    absSecond / 60 / 60 / 24 / 30, // months
    absSecond / 60 / 60 / 24 / 7, // weeks
    absSecond / 60 / 60 / 24, // days
    absSecond / 60 / 60, // hours
    absSecond / 60, // minutes
    absSecond, // seconds
  ];

  return (
    ["year", "month", "week", "day", "hour", "minute", "second"]
      .map((name, index) => {
        const time = Math.floor(times[index]);
        if (time > 0) return `${prefix}${time} ${name}${time > 1 ? "s" : ""}${suffix}`;
        return null;
      })
      .reduce((acc, curr) => (acc === null && curr !== null ? curr : null), null) || "now"
  );
}
