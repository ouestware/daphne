import { DataBatch, RGEEdge, RGENode } from "react-graph-expand";
import { Coordinates } from "sigma/types";

import { getFactoidLink, getFactoidTitle } from "../components/data/Factoid";
import {
  FactoidFullGraphFragment,
  FactoidGraphFragment,
  FactoidInlineFragment,
  MoralEntityFullGraphFragment,
  MoralEntityGraphFragment,
  MoralEntityInlineFragment,
  PhysicalPersonFullGraphFragment,
  PhysicalPersonGraphFragment,
  PhysicalPersonInlineFragment,
  PlaceFullGraphFragment,
  PlaceGraphFragment,
  PlaceInlineFragment,
} from "../core/graphql";
import { keyBy } from "lodash";
import { getPersonLink } from "../components/data/Person";
import { getPlaceLink } from "../components/data/Place";

export type Neo4JInlineNode =
  | FactoidInlineFragment
  | PhysicalPersonInlineFragment
  | MoralEntityInlineFragment
  | PlaceInlineFragment;
export type Neo4JNode = FactoidGraphFragment | PhysicalPersonGraphFragment | MoralEntityGraphFragment | PlaceGraphFragment;
export type Neo4JFullNode =
  | FactoidFullGraphFragment
  | PhysicalPersonFullGraphFragment
  | MoralEntityFullGraphFragment
  | PlaceFullGraphFragment;

export type FullNodeType = "person" | "moralEntity" | "place" | "factoid";

export function getItemLink(data: Neo4JInlineNode): string {
  switch (data.__typename) {
    case "MoralEntity":
    case "PhysicalPerson":
      return getPersonLink(data);
    case "Place":
      return getPlaceLink(data);
    case "Factoid":
      return getFactoidLink(data);
    default:
      throw new Error(`Unrecognized __typename ${data.__typename}.`);
  }
}

const TYPENAME_TO_TYPE_DICT: Record<string, string> = {
  MoralEntity: "person",
  PhysicalPerson: "person",
  Place: "place",
  Factoid: "factoid",
};
export function getGraphItemLink(data: Neo4JInlineNode): string {
  return `/${data.inquiry.id}/graph?nodes=${TYPENAME_TO_TYPE_DICT[data.__typename as string]}/${data.id}`;
}

/**
 * Helper to get a RGE node from Neo4J data:
 */
export function neo4jDataToGraphNode(data: Neo4JInlineNode): RGENode {
  switch (data.__typename) {
    case "MoralEntity":
      return {
        type: "moralEntity",
        id: data.id,
        label: data.name,
        attributes: data,
        state: {},
      };
    case "PhysicalPerson":
      return {
        type: "person",
        id: data.id,
        label: data.name,
        attributes: data,
        state: {},
      };
    case "Place":
      return {
        type: "place",
        id: data.id,
        label: data.name,
        attributes: data,
        state: {},
      };
    case "Factoid":
      return {
        type: "factoid",
        id: data.id,
        label: getFactoidTitle(data),
        attributes: data,
        state: {},
      };
    default:
      throw new Error(`Unrecognized __typename ${data.__typename}.`);
  }
}

/**
 * Helpers to get a RGE data batch from Neo4J data:
 */
function getNodeNeighbors(data: Neo4JNode): RGENode[] {
  switch (data.__typename) {
    case "MoralEntity":
      return [];
    case "PhysicalPerson":
      return [
        ...data.participatesTo.map(({ factoid }) => neo4jDataToGraphNode(factoid)),
        ...data.connectedTo.map(neo4jDataToGraphNode),
      ];
    case "Place":
      return data.factoids.map(neo4jDataToGraphNode);
    case "Factoid": {
      return [
        ...data.persons.map(({ person }) => neo4jDataToGraphNode(person)),
        ...data.places.map(neo4jDataToGraphNode),
      ];
    }
    default:
      throw new Error(`Unrecognized __typename ${data.__typename}.`);
  }
}

function getEdge(node1: RGENode, node2: RGENode): RGEEdge {
  if (node1.type > node2.type) return getEdge(node2, node1);

  return {
    id: [node1.id, node2.id].join("---"),
    type: [node1.type, node2.type].join("---"),
    source: node1.id,
    target: node2.id,
    attributes: {},
    state: {},
  };
}

export function getGraphBatch(data: Neo4JFullNode): DataBatch {
  let nodes: (RGENode & { coordinates?: Coordinates })[] = [];
  let edges: RGEEdge[] = [];

  // Add graph center:
  const centerNode = neo4jDataToGraphNode(data);
  nodes.push(centerNode);

  // Crawl:
  const neighbors = getNodeNeighbors(data);
  nodes = nodes.concat(neighbors);
  edges = edges.concat(neighbors.map((neighbor) => getEdge(centerNode, neighbor)));

  neighbors.forEach((neighbor) => {
    const neighborNeighbors = getNodeNeighbors(neighbor.attributes as Neo4JNode);
    nodes = nodes.concat(neighborNeighbors);
    edges = edges.concat(neighborNeighbors.map((neighborNeighbor) => getEdge(neighbor, neighborNeighbor)));
  });

  let i = 0;
  nodes.forEach((node) => {
    if (node.id === centerNode.id) {
      node.coordinates = {
        x: 0,
        y: 0,
      };
      node.state = { expanded: true };
    } else {
      const angle = (i++ / (nodes.length - 1)) * 2 * Math.PI;
      node.coordinates = {
        x: 50 * Math.cos(angle),
        y: 50 * Math.sin(angle),
      };
    }
  });

  return {
    nodes: keyBy(nodes, "id"),
    edges: keyBy(edges, "id"),
  };
}
