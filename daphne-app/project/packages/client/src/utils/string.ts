/**
 * Takes a string and returns a desaccentuated and lower-cased new string.
 */
export function normalize(string: string): string {
  return string
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase();
}
