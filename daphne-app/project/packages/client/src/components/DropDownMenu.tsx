import { FC, useState, useRef } from "react";
import useOnClickOutside from "use-onclickoutside";

export const DropDownMenu: FC<{
  title: string;
  options: Array<{ onClick: () => void; label: string; element?: JSX.Element }>;
}> = ({ title, options }) => {
  const ref = useRef<HTMLUListElement>(null);
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  useOnClickOutside(ref, () => setIsOpen(false));

  return (
    <>
      <button type="button" title={title} className="btn btn-secondary dropdown-toggle" onClick={toggle}>
        {title}
      </button>
      {isOpen && (
        <ul ref={ref} className="dropdown-menu show">
          {options.map((opt, i) => (
            <li key={i}>
              <button
                className="dropdown-item"
                title={opt.label}
                onClick={() => {
                  opt.onClick();
                  setIsOpen(false);
                }}
              >
                {opt.element ? opt.element : opt.label}
              </button>
            </li>
          ))}
        </ul>
      )}
    </>
  );
};
