import { max, min, range } from "lodash";
import { FC } from "react";
import cx from "classnames";

import { FactoidFragment, FactoidFullFragment, FactoidPersonParticipate } from "../../core/graphql";

import { getFactoidTitle } from "../data/Factoid";
import withSize, { SizeState } from "../../layout/WithSize";
import { FactoidLite } from "../data/Factoid";
import { Tooltip } from "../Tooltip";

export interface FactoidsTimelineProps {
  startYear: number;
  timeScope: { years: number };
  factoids: FactoidFragment[] | FactoidFullFragment[] | FactoidPersonParticipate[];
  total: number;
}

const FactoidsTimeline: FC<FactoidsTimelineProps & SizeState> = ({ width, startYear, timeScope, factoids }) => {
  const dateToPixels = (date: Date | undefined | null, startEnd: "start" | "end"): number => {
    if (date) {
      return (
        min([
          (Math.abs(new Date(date).getTime() - new Date(startYear, 0, 1).getTime()) * width) /
            Math.abs(new Date(startYear + timeScope.years - 1, 11, 31).getTime() - new Date(startYear, 0, 1).getTime()),
          width,
        ]) || 0
      );
    } else return startEnd === "start" ? 0 : width;
  };

  // Compute x step value
  const xCaptionStep =
    [1, 2, 5, 10, 20].find((n) => {
      return dateToPixels(new Date(startYear + n, 0, 1), "start") >= 40;
    }) || 50;

  return (
    <>
      {/* YEARS CAPTION */}
      <ul className="timeline-years-caption">
        {range(0, timeScope.years, xCaptionStep).map((yearToAdd) => (
          <li
            className={cx(xCaptionStep === 1 && "single-year")}
            key={yearToAdd}
            style={{
              width: (xCaptionStep * width) / (timeScope.years || 1),
            }}
          >
            <span>{`${startYear + yearToAdd}`}</span>
          </li>
        ))}
      </ul>
      {/* TIMELINE */}
      <div className="factoids-timeline">
        {factoids.map((f) => {
          const factoid = "factoid" in f ? f.factoid : f;
          if (!factoid.dates || factoid.dates.length === 0) return null;
          const start = dateToPixels(factoid.dates[0].start, "start");

          const anchorCenter = width < 320;
          const anchorRight = width > 320 && width - start <= 200;

          return (
            <div key={factoid.id} className={cx("factoid-row")}>
              {factoid.dates.map((date) => {
                const start = dateToPixels(date.start, "start");
                const end = dateToPixels(date.end, "end");

                return (
                  <div
                    key={date.id}
                    style={{
                      marginLeft: `${start}px`,
                    }}
                  >
                    <Tooltip className="bg-light" style={{ height: "fit-content" }}>
                      <>
                        <div
                          className="factoid-date"
                          style={{
                            justifyContent: anchorCenter ? "center" : anchorRight ? "flex-end" : "flex-start",
                            width: `${max([end - start, 30])}px`,
                            opacity: factoid.certainty,
                          }}
                        />
                        <div className="factoid-title">{getFactoidTitle(factoid)}</div>
                      </>
                      <FactoidLite factoid={factoid} />
                    </Tooltip>
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default withSize<FactoidsTimelineProps & SizeState>(FactoidsTimeline);
