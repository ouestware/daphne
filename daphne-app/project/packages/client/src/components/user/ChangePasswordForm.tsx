import { useRouter } from "next/router";
import { FC, useEffect, useState } from "react";

import { useAuth } from "../../core/user/useAuth";
import { Loader } from "../../components/Loader";
import { parseError } from "../../utils/error";

export const ChangePasswordForm: FC<{ username?: string; onSuccess: () => void }> = ({ username, onSuccess }) => {
  const router = useRouter();
  const { user, loading, error, changePassword } = useAuth();
  const [password, setPassword] = useState<string>("");
  const [confirmation, setConfirmation] = useState<string>("");

  useEffect(() => {
    if (!user || (!username && !user.isAdmin)) {
      router.replace({ pathname: "/403" });
    }
  }, [user, router, username]);

  return (
    <form
      onSubmit={async (e) => {
        e.preventDefault();
        try {
          await changePassword(password, confirmation);
          onSuccess();
        } catch (e) {
          console.error(e);
        }
      }}
    >
      {error && (
        <div className="alert alert-danger d-flex text-center align-items-center" role="alert">
          {parseError(error)}
        </div>
      )}

      <div className="mb-3 row">
        <label htmlFor="login" className="col-sm-2 col-form-label">
          Password
        </label>
        <div className="col-sm-10">
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
      </div>

      <div className="mb-3 row">
        <label htmlFor="confirmation" className="col-sm-2 col-form-label">
          Confirmation
        </label>
        <div className="col-sm-10">
          <input
            type="password"
            className="form-control"
            id="confirmation"
            value={confirmation}
            onChange={(e) => setConfirmation(e.target.value)}
            required
          />
        </div>
      </div>

      <div className="mb-3 row">
        <button className="btn btn-primary" type="submit">
          Enregistrer
        </button>
      </div>
      {loading && <Loader />}
    </form>
  );
};
