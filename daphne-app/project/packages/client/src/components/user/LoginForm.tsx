import { FC, useState, useEffect } from "react";
import { isNil } from "lodash";
import { FaSignInAlt } from "react-icons/fa";

import { useAuth } from "../../core/user/useAuth";
import { Loader } from "../../components/Loader";
import { parseError } from "../../utils/error";

export const LoginForm: FC<{ username?: string; onSuccess?: () => void }> = ({ username, onSuccess }) => {
  const { login, loading, error } = useAuth();
  const [user, setUser] = useState<string>(username || "");
  const [password, setPassword] = useState<string>("");

  useEffect(() => {
    setUser(isNil(username) ? "" : username);
  }, [username]);

  return (
    <form
      onSubmit={async (e) => {
        e.preventDefault();
        try {
          await login(user, password);
          if (onSuccess) onSuccess();
        } catch (e) {
          console.error(e);
        }
      }}
    >
      {error && (
        <div className="alert alert-danger d-flex text-center align-items-center" role="alert">
          {parseError(error)}
        </div>
      )}

      {isNil(username) && (
        <div className="mb-3 row">
          <label htmlFor="login" className="col-sm-2 col-form-label">
            Login
          </label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control"
              id="login"
              value={user}
              onChange={(e) => setUser(e.target.value)}
              required
            />
          </div>
        </div>
      )}

      <div className="mb-3 row">
        <label htmlFor="password" className="col-sm-2 col-form-label">
          Password
        </label>
        <div className="col-sm-10">
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
      </div>

      <button className="btn btn-primary" type="submit" disabled={loading}>
        <FaSignInAlt className="me-1" />
        Se connecter
      </button>

      {loading === true && <Loader />}
    </form>
  );
};
