import { FC, useCallback, useState, useEffect } from "react";
import { MdOutlineVerticalAlignTop } from "react-icons/md";

export const ScrollToTop: FC = () => {
  const [showBtn, setShowBtn] = useState<boolean>(false);

  const goToTop = useCallback(() => {
    if (window) {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  }, []);

  useEffect(() => {
    if (window) {
      const fn = () => {
        setShowBtn(window.scrollY > window.innerHeight ? true : false);
      };
      window.document.addEventListener("scroll", fn, true);
      return window.document.removeEventListener("scroll", fn);
    }
  }, [showBtn]);

  return (
    <>
      {showBtn && (
        <div style={{ position: "fixed", right: "1em", bottom: "1em" }}>
          <button className="btn btn-secondary" title="Scroll to top" onClick={goToTop}>
            <MdOutlineVerticalAlignTop />
          </button>
        </div>
      )}
    </>
  );
};
