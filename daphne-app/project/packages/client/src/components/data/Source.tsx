import { FC } from "react";
import cx from "classnames";
import Link from "next/link";

import { FactoidsBadge } from "./Factoid";
import { PersonsBadge } from "./Person";
import { getBadgeClassNames, icons } from "./data-styles";
import { FactoidRefersToSource, SourceFragment, SourceFullFragment, SourceInlineFragment } from "../../core/graphql";
import { truncate } from "lodash";

export function getSourceLink(source: Pick<SourceFragment, "id" | "inquiry">): string {
  return `/${source.inquiry.id}/source/${source.id.replace(new RegExp(`^${source.inquiry.id}-`), "")}`;
}
export const SourcesBadge: FC<{
  sources: (SourceInlineFragment & Partial<FactoidRefersToSource>)[];
  sourcesCount: number;
  className?: string;
}> = ({ sources, sourcesCount, className }) => {
  if (!sourcesCount) return null;

  const content = (
    <div className={cx(getBadgeClassNames("source"), className)}>
      {sourcesCount > 1 && (
        <>
          {sourcesCount} sources
          <icons.Source className="ms-2" />
        </>
      )}
      {sourcesCount === 1 && (
        <>
          <icons.Source className="me-2" /> {truncate(sources[0].name, { length: 50 })}{" "}
          {sources[0].permalink && <icons.Permalink />}
        </>
      )}
    </div>
  );
  return sourcesCount === 1 && sources[0].permalink ? <a href={sources[0].permalink}>{content}</a> : content;
};

export const SourceLite: FC<{ source: SourceFragment | SourceFullFragment }> = ({ source }) => {
  return (
    <div>
      <div className="fs-4">
        <Link className="link-source" href={getSourceLink(source)} title={`Source: ${source.name}`}>
          <icons.Source /> {source.name}
        </Link>
      </div>
      {source.description && <div className="text-muted">{source.description}</div>}
      <div className="badges d-flex align-items-center flex-wrap">
        <FactoidsBadge
          factoids={source.refering}
          factoidsCount={source.referingConnection.totalCount}
          className="me-2"
        />
        <PersonsBadge persons={source.issuing} personsCount={source.issuingConnection.totalCount} className="me-2" />
      </div>
    </div>
  );
};
