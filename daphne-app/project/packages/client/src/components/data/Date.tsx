import { FC, Fragment } from "react";
import cx from "classnames";

import { DateInlineFragment } from "../../core/graphql";
import { getBadgeClassNames, icons } from "./data-styles";

export const DateDisplay: FC<{ date: DateInlineFragment }> = ({ date }) => {
  if (!date.startIso && !date.endIso) return <i>date indéfinie</i>;
  if (!date.startIso) return <>jusqu&apos;en {date.endIso}</>;
  if (!date.endIso) return <>à partir de {date.startIso}</>;

  if (date.startIso === date.endIso) return <>en {date.startIso}</>;

  return (
    <>
      de {date.startIso} à {date.endIso}
    </>
  );
};

export const Dates: FC<{ dates: DateInlineFragment[] }> = ({ dates }) => {
  return (
    <>
      {dates.map((date, i, a) => (
        <Fragment key={i}>
          <DateDisplay date={date} />
          {i < a.length - 1 ? " - " : null}
        </Fragment>
      ))}
    </>
  );
};

export const DatesBadge: FC<{ dates: DateInlineFragment[]; className?: string }> = ({ dates, className }) => {
  if (!dates.length) return null;

  return (
    <div className={cx(getBadgeClassNames("date"), className)}>
      {dates.length > 1 && (
        <>
          {dates.length} dates
          <icons.Date className="ms-2" />
        </>
      )}
      {dates.length === 1 && (
        <>
          <icons.Date className="me-2" /> <DateDisplay date={dates[0]} />
        </>
      )}
    </div>
  );
};
