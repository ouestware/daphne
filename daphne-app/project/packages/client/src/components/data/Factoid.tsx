import { FC } from "react";
import cx from "classnames";
import Link from "next/link";

import { PersonsBadge } from "./Person";
import { PlacesBadge } from "./Place";
import { DatesBadge } from "./Date";
import { SourcesBadge } from "./Source";
import { CertaintyBadge } from "./Certainty";
import { getBadgeClassNames, icons } from "./data-styles";
import { FactoidFragment, FactoidFullFragment, FactoidInlineFragment } from "../../core/graphql";

export function getFactoidLink(factoid: Pick<FactoidFragment, "id" | "inquiry">): string {
  return `/${factoid.inquiry.id}/factoid/${factoid.id.replace(new RegExp(`^${factoid.inquiry.id}-`), "")}`;
}

export function getFactoidTitle(factoid: Pick<FactoidFragment, "id" | "types">): string {
  return factoid.types?.map((t) => t.name).join(", ") || "";
}

export const FactoidsBadge: FC<{
  factoids: FactoidInlineFragment[];
  factoidsCount: number;
  className?: string;
}> = ({ factoids, factoidsCount, className }) => {
  if (!factoidsCount) return null;

  return (
    <div className={cx(getBadgeClassNames("factoid"), className)}>
      {factoidsCount > 1 && (
        <>
          {factoidsCount} faits
          <icons.Factoid className="ms-2" />
        </>
      )}
      {factoidsCount === 1 && (
        <>
          <icons.Factoid className="me-2" />
          {getFactoidTitle(factoids[0])}
        </>
      )}
    </div>
  );
};

export const FactoidLite: FC<{ factoid: FactoidFragment | FactoidFullFragment }> = ({ factoid }) => {
  return (
    <div>
      <div className="fs-4">
        <Link className="link-factoid" href={getFactoidLink(factoid)} title={`Factoid: ${getFactoidTitle(factoid)}`}>
          <icons.Factoid /> {getFactoidTitle(factoid)}
        </Link>
      </div>
      {factoid.description && <div className="text-muted">{factoid.description}</div>}
      <div className="badges d-flex align-items-center flex-wrap">
        <CertaintyBadge certainty={factoid.certainty} className="me-2" />
        <PersonsBadge
          persons={factoid.persons.map((n) => n.person)}
          personsCount={factoid.personsConnection.totalCount}
          className="me-2"
        />
        <FactoidsBadge
          factoids={factoid.linkedTo}
          factoidsCount={factoid.linkedToConnection.totalCount}
          className="me-2"
        />
        <DatesBadge dates={factoid.dates} className="me-2" />
        <PlacesBadge places={factoid.places} placesCount={factoid.placesConnection.totalCount} className="me-2" />
        <SourcesBadge sources={factoid.sources} sourcesCount={factoid.sourcesConnection.totalCount} />
      </div>
    </div>
  );
};
