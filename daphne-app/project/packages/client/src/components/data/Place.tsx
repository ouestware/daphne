import { FC } from "react";
import cx from "classnames";
import Link from "next/link";

import { FactoidsBadge } from "./Factoid";
import { getBadgeClassNames, icons } from "./data-styles";
import { PlaceFragment, PlaceFullFragment, PlaceInlineFragment } from "../../core/graphql";

export function getPlaceLink(place: Pick<PlaceFragment, "id" | "inquiry">): string {
  return `/${place.inquiry.id}/place/${place.id.replace(new RegExp(`^${place.inquiry.id}-`), "")}`;
}

export const PlacesBadge: FC<{
  places: PlaceInlineFragment[];
  placesCount: number;
  className?: string;
}> = ({ places, placesCount, className }) => {
  if (!placesCount) return null;

  return (
    <div className={cx(getBadgeClassNames("place"), className)}>
      {placesCount > 1 && (
        <>
          {placesCount} lieux
          <icons.Place className="ms-2" />
        </>
      )}
      {placesCount === 1 && (
        <>
          <icons.Place className="me-2" /> {places[0].name}
        </>
      )}
    </div>
  );
};

export const PlaceLite: FC<{ place: PlaceFragment | PlaceFullFragment }> = ({ place }) => {
  return (
    <div>
      <div className="fs-4">
        <Link className="link-place" href={getPlaceLink(place)} title={`Lieu: ${place.name}`}>
          <icons.Place /> {place.name}
        </Link>
      </div>
      {place.description && <div className="text-muted">{place.description}</div>}
      <div className="badges d-flex align-items-center flex-wrap">
        <FactoidsBadge factoids={place.factoids} factoidsCount={place.factoidsConnection.totalCount} className="me-2" />
      </div>
    </div>
  );
};
