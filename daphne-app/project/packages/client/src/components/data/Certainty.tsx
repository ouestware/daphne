import { FC } from "react";
import cx from "classnames";
import { isNil } from "lodash";

import { getBadgeClassNames, icons } from "./data-styles";

export const CertaintyBadge: FC<{ certainty?: number; className?: string }> = ({ certainty, className }) => {
  if (isNil(certainty)) return null;

  return (
    <div className={cx(getBadgeClassNames("certainty"), className)}>
      <icons.Certainty className="me-2" /> {certainty.toFixed(1)}
    </div>
  );
};
