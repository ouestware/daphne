import { FC, Fragment } from "react";
import cx from "classnames";

import { getBadgeClassNames, icons } from "./data-styles";
import { DataSource, ImportedFrom } from "../../core/graphql";

export const DataSourceBadge: FC<{ sources?: Array<DataSource & ImportedFrom>; className?: string }> = ({
  sources,
  className,
}) => {
  if (!sources) return null;

  return (
    <>
      {sources
        .filter((n) => n.name !== "unknown")
        .map((n) => {
          const badgeContent = (
            <div key={n.id} className={cx(getBadgeClassNames("dataSource"), className)}>
              <icons.DataSource className="me-2" /> {n.name} {n.permalink && <icons.Permalink className="ms-2" />}
            </div>
          );
          return (
            <Fragment key={n.id}>{n.permalink ? <a href={n.permalink}>{badgeContent}</a> : badgeContent}</Fragment>
          );
        })}
    </>
  );
};
