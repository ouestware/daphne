import { MdPlace } from "react-icons/md";
import { FaDatabase } from "react-icons/fa";
import { TbBuildingBank } from "react-icons/tb";
import { AiOutlineCalendar } from "react-icons/ai";
import { BsFillFilePersonFill, BsCheck } from "react-icons/bs";
import { BiBookAlt, BiNetworkChart } from "react-icons/bi";
import { RiExternalLinkFill } from "react-icons/ri";
import { HiOutlineDocumentMagnifyingGlass, HiDocumentMagnifyingGlass } from "react-icons/hi2";

// [jacomyal] I don't know yet how to easily tell Next.js to load this sass file
// from here...
// import sassColors from "../../styles/variables.scss";
// export const colors = sassColors as Record<string, string>;

export const icons = {
  DataSource: FaDatabase,
  Person: BsFillFilePersonFill,
  PhysicalPerson: BsFillFilePersonFill,
  MoralEntity: TbBuildingBank,
  Factoid: HiOutlineDocumentMagnifyingGlass,
  FactoidFill: HiDocumentMagnifyingGlass,
  Place: MdPlace,
  Date: AiOutlineCalendar,
  Source: BiBookAlt,
  Certainty: BsCheck,
  Graph: BiNetworkChart,
  Permalink: RiExternalLinkFill,
} as const;

export function getBadgeClassNames(themeColor: string): string {
  return `rounded-pill d-inline-flex align-items-center text-nowrap small border mb-1 px-2 py-0 border-${themeColor} text-${themeColor}`;
}
