import { FC } from "react";
import cx from "classnames";
import Link from "next/link";

import { getBadgeClassNames, icons } from "./data-styles";

import { FactoidsBadge } from "./Factoid";
import {
  MoralEntityFragment,
  MoralEntityFullFragment,
  MoralEntityInlineFragment,
  PhysicalPerson,
  PhysicalPersonFragment,
  PhysicalPersonFullFragment,
  PhysicalPersonInlineFragment,
} from "../../core/graphql";

export function getPersonLink(person: Pick<PhysicalPersonFragment | MoralEntityFragment, "id" | "inquiry">): string {
  return `/${person.inquiry.id}/person/${person.id.replace(new RegExp(`^${person.inquiry.id}-`), "")}`;
}

export function getPersonFactoidsSearchLink(
  person: Pick<PhysicalPersonFragment | MoralEntityFragment, "name" | "inquiry">,
): string {
  return `/${person.inquiry.id}/search?type=factoids&query=&sort=score&f.people=${encodeURIComponent(person.name)}`;
}

export const PersonsBadge: FC<{
  persons: (PhysicalPersonInlineFragment | MoralEntityInlineFragment)[];
  personsCount: number;
  className?: string;
}> = ({ persons, personsCount, className }) => {
  if (!personsCount) return null;

  return (
    <div className={cx(getBadgeClassNames("person"), className)}>
      {personsCount > 1 && (
        <>
          {personsCount} personnes
          <icons.Person className="ms-2" />
        </>
      )}
      {personsCount === 1 && (
        <>
          {persons[0].__typename === "MoralEntity" ? (
            <icons.MoralEntity className="me-2" />
          ) : (
            <icons.PhysicalPerson className="me-2" />
          )}
          {persons[0].name}
        </>
      )}
    </div>
  );
};

export const PersonLite: FC<{
  person: PhysicalPersonFragment | MoralEntityFragment | PhysicalPersonFullFragment | MoralEntityFullFragment;
}> = ({ person }) => {
  if (person.__typename === "MoralEntity") {
    return (
      <div className="person-lite moral-entity-lite">
        <div className="fs-4">
          <Link className="link-person" href={getPersonLink(person)} title={`Personne: ${person.name}`}>
            <icons.MoralEntity /> {person.name}
          </Link>
        </div>
        {person.description && <div className="text-muted">{person.description}</div>}
      </div>
    );
  }

  person = person as PhysicalPerson;

  return (
    <div className="person-lite physical-person-lite">
      <div className="fs-4">
        <Link className="link-person" href={getPersonLink(person)} title={`Personne: ${person.name}`}>
          <icons.PhysicalPerson /> {person.name}
        </Link>
      </div>
      {person.description && <div className="text-muted">{person.description}</div>}
      <div className="badges d-flex align-items-center flex-wrap">
        <PersonsBadge
          persons={person.connectedTo}
          personsCount={person.connectedToConnection.totalCount}
          className="me-2"
        />
        <FactoidsBadge
          factoids={person.participatesTo.map((n) => n.factoid)}
          factoidsCount={person.participatesToConnection.totalCount}
        />
      </div>
    </div>
  );
};
