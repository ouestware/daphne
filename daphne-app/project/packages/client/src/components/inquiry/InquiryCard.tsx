import cx from "classnames";
import Link from "next/link";
import { FaChartPie, FaUserSecret } from "react-icons/fa";
import { BsThreeDotsVertical } from "react-icons/bs";
import { FC, CSSProperties, ReactElement } from "react";

import { InquiryFragment } from "../../core/graphql";
import { displayStringDateTime } from "../../utils/date";
import { Tooltip } from "../../components/Tooltip";

/**
 * Property definition of the component
 */
export interface InquiryCardProps {
  /**
   * HTML id
   */
  id?: string;
  /**
   * HTML class
   */
  className?: string;
  /**
   * HTML CSS style
   */
  style?: CSSProperties;
  /**
   * Inquiry data
   */
  inquiry: InquiryFragment;

  children?: ReactElement;
}

export const InquiryCard: FC<InquiryCardProps> = ({ id, className, style, inquiry, children }) => {
  const props = { id, className: cx("card rounded inquiry", className), style };

  return (
    <div {...props}>
      <div className="card-image">
        <FaUserSecret style={{ width: "50%", height: "50%" }} />
      </div>
      <div className="card-body">
        <Link className="stretched-link" href={`/${inquiry.id}/search`} title={inquiry.name}>
          <h5 className="card-title">{inquiry.name}</h5>
        </Link>
        {inquiry.description && <p className="card-text">{inquiry.description}</p>}
      </div>
      <div className="card-footer d-flex justify-content-between align-items-center">
        <ul className="list-unstyled list-inline mb-0">
          <li className="list-inline-item-separator">{displayStringDateTime(inquiry.createdAt, { time: false })}</li>
        </ul>
        {children && (
          <Tooltip tag="span" className="flex-shrink-0">
            <button className="btn btn-ico text-muted">
              <BsThreeDotsVertical />
            </button>
            {children}
          </Tooltip>
        )}
      </div>
    </div>
  );
};

export const ProsoVizCard: FC = () => {
  return (
    <div className="card rounded inquiry">
      <div className="card-image">
        <FaChartPie style={{ width: "50%", height: "50%" }} />
      </div>
      <div className="card-body">
        <Link className="stretched-link" href="https://stardisblue.github.io/prosovis/" title="Prosovis">
          <h5 className="card-title">Prosovis siprojuris</h5>
        </Link>
        <p className="card-text">Prototype de visualisation des données du corpus Siprojuris</p>
      </div>
      <div className="card-footer d-flex justify-content-between align-items-center">
        <ul className="list-unstyled list-inline mb-0">
          <li className="list-inline-item-separator">{displayStringDateTime("2022-10-06", { time: false })}</li>
        </ul>
      </div>
    </div>
  );
};
