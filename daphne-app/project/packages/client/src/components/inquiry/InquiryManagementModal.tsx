import { FC, useState } from "react";

import { InquiryFragment } from "../../core/graphql";
import { useNotifications } from "../../core/notifications";
import { Modal, ModalProps } from "../../core/modals";
import { useAuth } from "../../core/user/useAuth";

export const InquiryManagementModal: FC<ModalProps<{ inquiry: InquiryFragment; onSuccess?: () => void }>> = ({
  arguments: { inquiry },

  cancel,
}) => {
  const { notify } = useNotifications();
  const { logout } = useAuth();
  const [investigatorName, setInvestigatorName] = useState<string | undefined>(
    localStorage.getItem("investigatorName") || undefined,
  );

  return (
    <Modal title={`Gérer l'enquête ${inquiry.name}`} showHeader onClose={() => cancel()}>
      <>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            if (investigatorName) localStorage.setItem("investigatorName", investigatorName);
            else localStorage.removeItem("investigatorName");
            notify({ type: "success", message: "Nom d'enquêteur enregistré" });
          }}
        >
          <div className="mb-3">
            <label className="form-label" htmlFor="investigatorName">
              Votre nom d&apos;enquêteur
            </label>
            <input
              className="form-control"
              type="test"
              id="investigatorName"
              value={investigatorName}
              onChange={(e) => {
                setInvestigatorName(e.target.value || undefined);
              }}
            />
          </div>
          <div className="d-flex justify-content-end">
            <button type="submit" className="btn btn-primary">
              Valider
            </button>
          </div>
        </form>
        <hr />
        <form
          onSubmit={(e) => {
            e.preventDefault();
            logout();
            cancel();
          }}
        >
          Vous pouvez quitter le mode enquêteur en vous déconnectant pour retourner en mode exploration.
          <div className="d-flex justify-content-end">
            <button className="btn btn-warning" type="submit">
              Se déconnecter
            </button>
          </div>
        </form>
      </>
    </Modal>
  );
};
