import { FC, CSSProperties, useState, useEffect } from "react";
import { useMutation } from "@apollo/client";
import { isNil } from "lodash";

import { parseError } from "../../utils/error";
import { InquiryFragment, inquiryCreateQuery, inquiryUpdateQuery } from "../../core/graphql";
import { Loader } from "../Loader";

export interface InquiryFormProps {
  /**
   * HTML id
   */
  id?: string;
  /**
   * HTML class
   */
  className?: string;
  /**
   * HTML CSS style
   */
  style?: CSSProperties;
  /**
   * The corpus to edit
   */
  inquiry?: InquiryFragment;
  /**
   * What we do when form submit is successfull ?
   */
  onSuccess?: (e: InquiryFragment) => void;
}

export const InquiryForm: FC<InquiryFormProps> = ({ id, className, style, inquiry, onSuccess }) => {
  const htmlProps = { id, className, style };
  const [data, setData] = useState<Partial<InquiryFragment>>(inquiry || {});
  const [password, setPassword] = useState<string>("");
  const [formError, setFormError] = useState<string | null>(null);
  const [hasBeenSubmited, setHasBeenSubmited] = useState<boolean>(false);
  const [confirmation, setConfirmation] = useState<string>("");
  const [create, { loading: createLd, error: createError }] = useMutation(inquiryCreateQuery);
  const [update, { loading: updateLd, error: updateError }] = useMutation(inquiryUpdateQuery);

  useEffect(() => {
    let stringError: string | null | undefined = null;
    if ((password.trim().length || confirmation.trim().length) && password !== confirmation) {
      stringError = "Le mot-de-passe et sa confirmation ne correspondent pas";
    }
    if (!data.name || data.name.trim().length === 0) {
      stringError = "Le nom est requis";
    }
    if (!inquiry && password.trim().length === 0) {
      stringError = "Le mot-de-passe est requis";
    }
    setFormError(stringError);
  }, [data, password, confirmation, inquiry]);

  return (
    <div {...htmlProps}>
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          setHasBeenSubmited(true);
          if (isNil(formError)) {
            try {
              let newInquiry: InquiryFragment | null | undefined = null;
              if (inquiry) {
                const resp = await update({
                  variables: {
                    id: inquiry.id,
                    name: data.name || inquiry.name,
                    description: data.description,
                    password: password.trim().length > 0 ? password : undefined,
                  },
                });
                newInquiry = resp.data?.inquiry;
              } else {
                const resp = await create({
                  variables: {
                    name: data.name || "",
                    description: data.description,
                    password,
                  },
                });
                newInquiry = resp.data?.inquiry;
              }
              if (onSuccess && newInquiry) onSuccess(newInquiry);
            } catch (e) {
              console.error(e);
            }
          }
        }}
      >
        {hasBeenSubmited && formError && (
          <div className="alert alert-danger d-flex text-center align-items-center" role="alert">
            {formError}
          </div>
        )}
        {createError && (
          <div className="alert alert-danger d-flex text-center align-items-center" role="alert">
            {parseError(createError)}
          </div>
        )}
        {updateError && (
          <div className="alert alert-danger d-flex text-center align-items-center" role="alert">
            {parseError(updateError)}
          </div>
        )}

        <div className="mb-3 row">
          <label htmlFor="name" className="col-sm-2 col-form-label">
            Nom
          </label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control"
              id="name"
              value={data.name || ""}
              onChange={(e) => setData((prev) => ({ ...prev, name: e.target.value }))}
              required={true}
            />
          </div>
        </div>

        <div className="mb-3 row">
          <label htmlFor="description" className="col-sm-2 col-form-label">
            Description
          </label>
          <div className="col-sm-10">
            <textarea
              className="form-control"
              id="description"
              value={data.description || ""}
              onChange={(e) => setData((prev) => ({ ...prev, description: e.target.value }))}
            />
          </div>
        </div>

        <div className="mb-3 row">
          <label htmlFor="password" className="col-sm-2 col-form-label">
            Password
          </label>
          <div className="col-sm-10">
            <input
              type="password"
              className="form-control"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>

        <div className="mb-3 row">
          <label htmlFor="Confirmation" className="col-sm-2 col-form-label">
            Confirmation
          </label>
          <div className="col-sm-10">
            <input
              type="password"
              className="form-control"
              id="confirmation"
              value={confirmation}
              onChange={(e) => setConfirmation(e.target.value)}
            />
          </div>
        </div>

        <div className="mb-3 row">
          <button className="btn btn-primary" type="submit" disabled={!isNil(formError)}>
            Enregistrer
          </button>
        </div>
        {(createLd || updateLd) && <Loader />}
      </form>
    </div>
  );
};
