import { FC } from "react";

import { InquiryFragment } from "../../core/graphql";
import { useNotifications } from "../../core/notifications";
import { Modal, ModalProps } from "../../core/modals";
import { LoginForm } from "../user/LoginForm";

export const InquiryLoginModal: FC<ModalProps<{ inquiry: InquiryFragment; onSuccess?: () => void }>> = ({
  arguments: { inquiry },
  submit,
  cancel,
}) => {
  const { notify } = useNotifications();

  return (
    <Modal title={`S'identifier pour l'enquête ${inquiry.name}`} showHeader onClose={() => cancel()}>
      <LoginForm
        username={inquiry.id}
        onSuccess={() => {
          notify({
            type: "success",
            message: `Vous êtes à présent connecté en tant qu'enquêteur pour ${inquiry.name}`,
          });
          submit({});
        }}
      />
    </Modal>
  );
};
