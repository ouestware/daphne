import { FC } from "react";
import { useMutation } from "@apollo/client";

import { parseError } from "../../utils/error";
import { InquiryFragment, inquiryDeleteQuery } from "../../core/graphql";
import { ModalProps, Modal } from "../../core/modals";
import { useNotifications } from "../../core/notifications";
import { Spinner } from "../../components/Loader";

export const InquiryDeleteModal: FC<ModalProps<{ inquiry: InquiryFragment }>> = ({
  arguments: { inquiry },
  submit,
  cancel,
}) => {
  const { notify } = useNotifications();
  const [inquiryDelete, { loading, error, client }] = useMutation(inquiryDeleteQuery);

  return (
    <Modal title={`Suppression de ${inquiry.name}`} showHeader onClose={() => cancel()}>
      <>
        {error && (
          <div className="alert alert-danger d-flex text-center align-items-center" role="alert">
            {parseError(error)}
          </div>
        )}
        <p>
          Vous êtes sur le point de supprimer définitivement l&apos;enquête{" "}
          <span className="fw-bold">{inquiry.name}</span>. Cette opération est irréversible.
        </p>
        <p>Souhaitez-vous continuer ?</p>
      </>
      <>
        <button
          type="button"
          className="btn btn-outline-secondary me-2"
          onClick={() => {
            if (!loading) cancel();
          }}
          disabled={loading === true}
        >
          Annuler
        </button>
        <button
          type="submit"
          className="btn btn-primary"
          disabled={loading === true}
          onClick={async () => {
            try {
              await inquiryDelete({ variables: { id: inquiry.id } });
              await client.cache.reset();
              notify({ type: "success", message: `L'enquête ${inquiry.name} a été supprimée avec succès` });
              submit({});
            } catch (e) {
              console.error(e);
            }
          }}
        >
          Confirmer
          {loading && <Spinner className="ms-2 spinner-border-sm" />}
        </button>
      </>
    </Modal>
  );
};
