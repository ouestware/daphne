import { useCallback } from "react";

import { AggregationType, SearchAggregation } from "../../core/graphql";

import { useAppSearch } from "../search/useAppSearch";

export function useFactoidsAggregations(inquiryId: string) {
  const { error, loading, search } = useAppSearch();

  const factoidsAggregations = useCallback(async (): Promise<SearchAggregation[]> => {
    try {
      const result = await search("factoids", {
        inquiryId,
        search: "",
        skip: 0,
        limit: 0,
        filters: [],
        aggregations: [
          {
            id: "dates",
            type: AggregationType.DateHistogramPerYear,
            field: "dates",
          },
          {
            id: "types",
            type: AggregationType.Terms,
            field: "types",
          },
        ],
      });
      if (result.data) return result.data.search.aggregations;
      else throw new Error("");
    } catch {
      return [];
    }
  }, [search, inquiryId]);

  return {
    loading,
    error,
    factoidsAggregations,
  };
}
