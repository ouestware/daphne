import { FC, useMemo } from "react";
import { ReactGraphExpand, NodeTypeConfig } from "react-graph-expand";

import { GraphExplorerProps } from "./types";
import { GraphControls } from "./GraphControls";
import { GraphFilters } from "./GraphFilters";

export const GraphExplorer: FC<GraphExplorerProps> = (props) => {
  const nodeTypes: Record<string, NodeTypeConfig & { label: string }> = useMemo(
    () => ({
      person: {
        label: "Personnes",
        color: "#c845e6",
        size: 15,
      },
      moralEntity: {
        label: "Entités morales",
        color: "#c845e6",
        size: 10,
      },
      place: {
        label: "Lieux",
        color: "#7663ff",
        size: 10,
      },
      factoid: {
        label: "Factoïdes",
        color: "#793ee6",
        size: 7,
      },
    }),
    [],
  );

  return (
    <div className="App position-relative flex-grow-1 w-100" id="graph-root">
      <ReactGraphExpand style={{ background: "white" }} initialState={{ nodeTypes }}>
        <GraphControls {...props} />
        <GraphFilters nodeTypes={nodeTypes} factoidsAggregations={props.factoidsAggregations} />
      </ReactGraphExpand>
    </div>
  );
};
