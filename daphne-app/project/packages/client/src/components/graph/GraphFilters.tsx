import { map, uniq, without } from "lodash";
import { FC, useEffect, useState } from "react";
import { NodeTypeConfig, useRGEActions } from "react-graph-expand";
import { FactoidFullGraphFragment, SearchAggregation } from "../../core/graphql";
import { SearchHistogram } from "../search/SearchHistogram";
import Select from "react-select";

export const GraphFilters: FC<{
  nodeTypes: Record<string, NodeTypeConfig & { label: string }>;
  factoidsAggregations: () => Promise<SearchAggregation[]>;
}> = ({ nodeTypes, factoidsAggregations }) => {
  const { setNodeFilter } = useRGEActions();

  const [selectedTypes, setSelectedTypes] = useState(Object.keys(nodeTypes));
  const [datesHistogram, setDateHistograms] = useState<SearchAggregation | undefined>(undefined);
  const [factoidsTypes, setFactoidsTypes] = useState<SearchAggregation | undefined>(undefined);
  const [selectedDates, setSelectedDates] = useState<[string, string] | null>(null);
  const [selectedFactoidTypes, setSelectedFactoidTypes] = useState<string[]>([]);

  useEffect(() => {
    factoidsAggregations()
      .then((aggs) => {
        if (aggs.length > 1) {
          const datesAggs = aggs.find((a) => a.name === "dates");
          setDateHistograms(datesAggs);
          const typesAggs = aggs.find((a) => a.name === "types");
          setFactoidsTypes(typesAggs);
        }
      })
      .catch((e) => {
        //todo notifications
        console.log(e);
      });
  }, [factoidsAggregations]);

  useEffect(() => {
    const set = new Set(selectedTypes);

    // Here we regenerate the filtering functions, based on the various inputs
    // available (right now, only node types):
    setNodeFilter({
      nodeFilter: ({ type, attributes }) => {
        return (
          set.has(type) &&
          (type !== "factoid" ||
            selectedFactoidTypes.length === 0 ||
            (attributes as FactoidFullGraphFragment).types.find((t) => selectedFactoidTypes.includes(t.name)) !==
              undefined) &&
          (type !== "factoid" ||
            selectedDates === null ||
            !("dates" in attributes) ||
            (attributes as FactoidFullGraphFragment).dates.find(
              (d) => (!d.startIso || selectedDates[1] >= d.startIso) && (!d.endIso || selectedDates[0] <= d.endIso),
            ) !== undefined)
        );
      },
    });
  }, [selectedTypes, selectedFactoidTypes, setNodeFilter, selectedDates]);

  return (
    <div className="graph-filters container mt-4">
      <div className="row">
        <div className="col-4">
          <h3>Types de noeuds</h3>
          <ul className="list-unstyled">
            {map(nodeTypes, ({ color, label }, key) => (
              <li key={key} className="form-check">
                <input
                  id={`node-type-${key}`}
                  className="form-check-input"
                  type="checkbox"
                  checked={selectedTypes.includes(key)}
                  onChange={(e) =>
                    setSelectedTypes(e.target.checked ? uniq(selectedTypes.concat(key)) : without(selectedTypes, key))
                  }
                />
                <label className="form-check-label" htmlFor={`node-type-${key}`}>
                  <span className="circle me-1" style={{ background: color }} />
                  {label}
                </label>
              </li>
            ))}
          </ul>
        </div>
        <div className="col-4">
          {datesHistogram && (
            <>
              {" "}
              <h3>Dates des factoïdes</h3>
              <SearchHistogram
                facet={datesHistogram}
                onSelectionEnd={(dates) => {
                  setSelectedDates(dates !== null ? [dates.from, dates.to] : null);
                }}
              />
            </>
          )}
        </div>
        <div className="col-4">
          {factoidsTypes && (
            <>
              {" "}
              <h3>Types de factoïdes</h3>
              <Select
                key={JSON.stringify(factoidsTypes)}
                isMulti
                options={factoidsTypes.values.map((f) => ({ label: f.key, value: f.key }))}
                onChange={(values) => {
                  setSelectedFactoidTypes(values.map((v) => v.label));
                }}
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
};
