import { useCallback, useState } from "react";
import { useApolloClient } from "@apollo/client";
import { DataBatch } from "react-graph-expand";

import {
  graphFactoidByIdQuery,
  graphMoralEntityByIdQuery,
  graphPhysicalPersonByIdQuery,
  graphPlaceByIdQuery,
} from "../../core/graphql";
import { FullNodeType, getGraphBatch, Neo4JFullNode } from "../../utils/graph";

export function useGetGraph() {
  const client = useApolloClient();

  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const getGraph = useCallback(
    async (id: string, type: FullNodeType): Promise<{ data: DataBatch | null }> => {
      setLoading(true);
      setError(null);
      try {
        let node: Neo4JFullNode | undefined;

        switch (type) {
          case "person":
            node = (
              await client.query({
                query: graphPhysicalPersonByIdQuery,
                variables: { id },
              })
            ).data.physicalPeople[0];
            break;
          case "moralEntity":
            node = (
              await client.query({
                query: graphMoralEntityByIdQuery,
                variables: { id },
              })
            ).data.moralEntities[0];
            break;
          case "place":
            node = (
              await client.query({
                query: graphPlaceByIdQuery,
                variables: { id },
              })
            ).data.places[0];
            break;
          case "factoid":
          default:
            node = (
              await client.query({
                query: graphFactoidByIdQuery,
                variables: { id },
              })
            ).data.factoids[0];
            break;
        }

        return { data: node ? getGraphBatch(node) : {} };
      } catch (e) {
        setError(e as Error);
        return { data: null };
      } finally {
        setLoading(false);
      }
    },
    [client],
  );

  return {
    loading,
    error,
    getGraph,
  };
}
