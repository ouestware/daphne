import { DataBatch, RGENode } from "react-graph-expand";
import { SearchAggregation } from "../../core/graphql";

export interface GraphExplorerProps {
  ready?: boolean;
  getNode: (node: Pick<RGENode, "id" | "type">) => Promise<DataBatch>;
  search: (query: string) => Promise<{ candidates: RGENode[]; total?: number }>;
  factoidsAggregations: () => Promise<SearchAggregation[]>;
  initialQuery?: Pick<RGENode, "id" | "type">[];
}
