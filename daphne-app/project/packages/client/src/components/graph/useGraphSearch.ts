import { useState, useCallback } from "react";
import { useApolloClient } from "@apollo/client";

import { graphSearchQuery } from "../../core/graphql";
import { neo4jDataToGraphNode } from "../../utils/graph";

export function useGraphSearch() {
  const client = useApolloClient();

  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const search = useCallback(
    async (params: { inquiryId: string; search: string; skip: number; limit: number }) => {
      setLoading(true);
      setError(null);
      try {
        const response = await client.query({ query: graphSearchQuery, variables: params });

        return {
          data: {
            total: response.data.search.total,
            results: response.data.search.results.map((res) => neo4jDataToGraphNode(res)),
          },
        };
      } catch (e) {
        setError(e as Error);
        return { data: null };
      } finally {
        setLoading(false);
      }
    },
    [client],
  );

  return {
    loading,
    error,
    search,
  };
}
