import { FC, useEffect, useMemo, useState } from "react";
import { useSigma } from "@react-sigma/core";
import {
  CursorSelectionController,
  DoubleClickController,
  DraggingController,
  GraphPlaceholder,
  HoverController,
  MarqueeController,
  RGENode,
  SearchInput,
  useExpandNode,
  useFA2,
  useLayouts,
  useRGEActions,
  useRGEState,
  useSerialize,
} from "react-graph-expand";
import Link from "next/link";
import cx from "classnames";

import { ImTree } from "react-icons/im";
import { GiArrowCursor } from "react-icons/gi";
import { MdRestorePage } from "react-icons/md";
import { TbCircles, TbMarquee2 } from "react-icons/tb";
import { FaRegDotCircle, FaTrash } from "react-icons/fa";
import { IoMdRemoveCircleOutline } from "react-icons/io";
import { BiCollapseAlt, BiExpandAlt } from "react-icons/bi";
import { BsPause, BsPlay, BsZoomIn, BsZoomOut } from "react-icons/bs";
import { AiFillSave, AiOutlineFullscreen, AiOutlineFullscreenExit, AiOutlineLink } from "react-icons/ai";

import { getItemLink, Neo4JInlineNode } from "../../utils/graph";
import { Spinner } from "../Loader";
import { GraphExplorerProps } from "./types";
import { useFullScreen } from "./useFullScreen";

export const GraphControls: FC<GraphExplorerProps> = ({ ready, search, getNode, initialQuery }) => {
  const [selectionType, setSelectionType] = useState<"mouse" | "marquee">("mouse");

  const sigma = useSigma();
  const { setSelection, removeData, updateStates, clear } = useRGEActions();
  const { selection, isLoading, data } = useRGEState();
  const expandNodes = useExpandNode(getNode);
  const { circlePack, hierarchical } = useLayouts();
  const { importBatch, exportBatch } = useSerialize();
  const { isFullScreen, toggle } = useFullScreen(document.getElementById("graph-root") as HTMLDivElement);
  const { isRunning, start, stop } = useFA2(3000);

  const onlySelectedItem = useMemo(
    () => (selection.size === 1 ? (data.nodes[Array.from(selection)[0]]?.attributes as Neo4JInlineNode) : null),
    [selection],
  );
  const zoomOptions = { duration: 200, factor: 1.5 };

  const hasSelection = !!selection.size;
  const isGraphEmpty = !Object.keys(data.nodes).length;

  useEffect(() => {
    if (ready && initialQuery?.length) {
      expandNodes(initialQuery as RGENode[]).then(() => {
        setSelection({ nodes: initialQuery.map(({ id }) => id) });
      });
    }
  }, [ready]);

  if (!ready)
    return (
      <GraphPlaceholder>
        <span className="text-muted">Loading...</span>
      </GraphPlaceholder>
    );

  return (
    <>
      <HoverController disabled={selectionType === "marquee"} />
      <DoubleClickController fetch={getNode} />
      <DraggingController />
      <MarqueeController disabled={selectionType !== "marquee"} />
      <CursorSelectionController disabled={selectionType !== "mouse"} />
      <GraphPlaceholder className="text-muted">
        <p>Le graphe est vide.</p>
        <p>Vous pouvez chercher des noeuds à l&apos;aide du champs de recherche.</p>
      </GraphPlaceholder>

      <div className="buttons d-flex flex-row" style={{ position: "absolute", top: 10, left: 10 }}>
        <SearchInput
          search={search}
          fetch={getNode}
          queries={[]}
          placeholder="Cherchez des personnes, places ou factoïds"
          queryPlaceholder="...ou utilisez une requête pré-enregistrée :"
          queryDescriptionPlaceholder="Commencez à taper"
          selectedNodesMessage={(count) => `${count} éléments sélectionnés`}
        />
        {onlySelectedItem && (
          <Link href={getItemLink(onlySelectedItem)} className="btn ms-2 btn-primary">
            <AiOutlineLink />
          </Link>
        )}
        {isLoading && (
          <span className="ms-2">
            <Spinner />
          </span>
        )}
      </div>

      <div className="buttons d-flex flex-column" style={{ position: "absolute", bottom: 10, left: 10 }}>
        {/* Selection modes */}
        <div className="mt-4">
          <button
            title="Outil de sélection manuel"
            className={cx("btn me-2", selectionType === "mouse" ? "btn-primary" : "btn-outline-primary")}
            onClick={() => setSelectionType("mouse")}
            disabled={isLoading}
          >
            <GiArrowCursor />
          </button>
          <button
            title="Outil de sélection rectangle"
            className={cx("btn me-2", selectionType === "marquee" ? "btn-primary" : "btn-outline-primary")}
            onClick={() => setSelectionType("marquee")}
            disabled={isLoading}
          >
            <TbMarquee2 />
          </button>
        </div>

        {/* Selection actions */}
        <div className="mt-4">
          <button
            title="Charger les voisinages des noeuds sélectionnés"
            className="btn btn-primary me-2"
            onClick={() => expandNodes(Array.from(selection).map((id) => ({ ...data.nodes[id], id })))}
            disabled={isLoading || !hasSelection}
          >
            <BiExpandAlt />
          </button>
          <button
            title="Cacher toutes les feuilles attachées aux noeuds sélectionnés"
            className="btn btn-primary me-2"
            onClick={() => {
              updateStates({
                nodes: Array.from(selection).reduce((iter, node) => ({ ...iter, [node]: { collapsed: true } }), {}),
              });
            }}
            disabled={isLoading || !hasSelection}
          >
            <BiCollapseAlt />
          </button>
          <button
            title="Retirer la sélection du graphe"
            className="btn btn-primary me-2"
            onClick={() => removeData({ nodes: Array.from(selection) })}
            disabled={isLoading || !hasSelection}
          >
            <IoMdRemoveCircleOutline />
          </button>
        </div>

        {/* Other actions */}
        <div className="mt-4">
          <button
            title={
              isRunning ? "Démarrer l'algorithme de placement physique" : "Stopper l'algorithme de placement physique"
            }
            className="btn btn-primary me-2"
            onClick={() => (isRunning ? stop() : start())}
            disabled={isGraphEmpty || isLoading}
          >
            {isRunning ? <BsPause /> : <BsPlay />}
          </button>
          <button
            title="Appliquer l'algorithme de placement hiérarchique"
            className="btn me-2 btn-primary"
            disabled={isGraphEmpty || isLoading}
            onClick={() => hierarchical(selection.size === 1 ? Array.from(selection)[0] : undefined)}
          >
            <ImTree />
          </button>
          <button
            title="Appliquer l'algorithme de placement par groupes"
            className="btn me-2 btn-primary"
            onClick={() => circlePack()}
            disabled={isGraphEmpty || isLoading}
          >
            <TbCircles />
          </button>
        </div>
      </div>

      <div
        className="buttons d-flex flex-column align-items-end"
        style={{ position: "absolute", bottom: 10, right: 10 }}
      >
        <div className="mt-2">
          <button
            title="Zoomer"
            className="btn btn-primary"
            onClick={() => {
              sigma.getCamera().animatedZoom(zoomOptions);
            }}
            disabled={isLoading}
          >
            <BsZoomIn />
          </button>
        </div>
        <div className="mt-2">
          <button
            title="Dézoomer"
            className="btn btn-primary"
            onClick={() => {
              sigma.getCamera().animatedUnzoom(zoomOptions);
            }}
            disabled={isLoading}
          >
            <BsZoomOut />
          </button>
        </div>
        <div className="mt-2">
          <button
            title="Réinitialiser le zoom"
            className="btn btn-primary"
            onClick={() => {
              sigma.getCamera().animatedReset(zoomOptions);
            }}
            disabled={isLoading}
          >
            <FaRegDotCircle />
          </button>
        </div>

        <div className="mt-4">
          <button
            title={isFullScreen ? "Réduire le plein écran" : "Passer en plein écran"}
            className="btn btn-primary"
            onClick={() => toggle()}
            disabled={isLoading}
          >
            {isFullScreen ? <AiOutlineFullscreenExit /> : <AiOutlineFullscreen />}
          </button>
        </div>

        {/* Save / load data */}
        <div className="mt-4">
          <button
            title="Sauvegarder le graphe localement"
            className="btn me-2 btn-primary"
            onClick={() => {
              localStorage.setItem("react-graph-expand", exportBatch());
            }}
            disabled={isLoading}
          >
            <AiFillSave />
          </button>
          <button
            title="Charger le dernier graphe sauvegardé localement"
            className="btn me-2 btn-primary"
            onClick={() => {
              const data = localStorage.getItem("react-graph-expand");
              if (data) importBatch(data);
            }}
            disabled={isLoading || !localStorage.getItem("react-graph-expand")}
          >
            <MdRestorePage />
          </button>
          <button
            title="Réinitialiser le graphe"
            className="btn btn-primary"
            onClick={() => clear({})}
            disabled={isLoading}
          >
            <FaTrash />
          </button>
        </div>
      </div>
    </>
  );
};
