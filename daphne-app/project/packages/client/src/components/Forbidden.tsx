import { FC } from "react";
import cx from "classnames";

export const Forbidden: FC<{ className?: string }> = ({ className }) => {
  return (
    <div className={cx("container text-center col-6", className)}>
      <div className="row">
        <h1>403</h1>
        <h2>Accès non authorizé</h2>
        <p>Vous n&apos;avez pas accès à cette page.</p>
      </div>
    </div>
  );
};
