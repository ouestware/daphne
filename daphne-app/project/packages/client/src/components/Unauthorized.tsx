import { FC } from "react";
import cx from "classnames";
import { LoginForm } from "./user/LoginForm";

export const Unauthorized: FC<{ className?: string; username?: string }> = ({ className, username }) => {
  return (
    <div className={cx("container text-center col-6", className)}>
      <div className="row">
        <h1>401</h1>
        <h2>Accès non authorizé</h2>
        <p>Vous devez être connecté pour avoir accès à cette page.</p>
        <LoginForm username={username} />
      </div>
    </div>
  );
};
