import { FC } from "react";
import cx from "classnames";

export const NotFound: FC<{ className?: string }> = ({ className }) => {
  return (
    <div className={cx("container text-center col-6", className)}>
      <div className="row">
        <h1>404</h1>
        <h2>Page non trouvée</h2>
        <p>La page que vous cherchez n&apos;existe pas ou a été déplacée.</p>
        <p>Veuillez vérifier l&apos;url pour voir si vous n&apos;avez pas fait une erreur</p>
      </div>
    </div>
  );
};
