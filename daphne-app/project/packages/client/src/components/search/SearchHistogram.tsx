import cx from "classnames";
import { maxBy, head, last } from "lodash";
import { FC, CSSProperties, useMemo, useRef, useState, useEffect } from "react";

import { SearchAggregationFragment } from "../../core/graphql";
import { displayDateYear } from "../../utils/date";
import { getElementPosition, isInSelection } from "./utils";

export interface SearchHistogramProps {
  className?: string;
  style?: CSSProperties;
  facet: SearchAggregationFragment;
  height?: string;
  onSelectionEnd?: (dates: { from: string; to: string } | null) => void;
}
export const SearchHistogram: FC<SearchHistogramProps> = ({
  className,
  style,
  facet,
  height = "100px",
  onSelectionEnd,
}) => {
  const barchartRef = useRef<HTMLDivElement>(null);

  const max = useMemo(() => {
    const maxItem = maxBy(facet.values, (v) => v.count);
    return maxItem ? maxItem.count : 0;
  }, [facet.values]);

  const firstValue = useMemo(() => head(facet.values), [facet.values]);
  const lastValue = useMemo(() => last(facet.values), [facet.values]);

  // State for the period selection (brush) defined by  data's index (not per year)
  const [selection, setSelection] = useState<[number, number] | null>(null);
  const [selecting, setSelecting] = useState<boolean>(false);
  // selection Drag
  // When selection is in progress
  // => we register an on move on the document to track the mouse
  // => we register an mouseup to finalize the selection
  useEffect(() => {
    if (onSelectionEnd && barchartRef.current && selecting) {
      // function that handle the mouse move and track the closest item for the selection
      const mouseMoveHandler = (e: MouseEvent) => {
        const barChartWidth = (barchartRef.current as HTMLElement).clientWidth;
        const barChartStartX = getElementPosition(barchartRef.current as HTMLElement)[0];
        const barChartEndX = barChartStartX + barChartWidth;
        const mouseX = e.clientX;

        // Mouse if before the element => select 0
        if (mouseX <= barChartStartX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], 0];
            }
            return null;
          });
        }

        // Mouse if after the element => take last item
        if (mouseX >= barChartEndX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], facet.values.length - 1];
            }
            return null;
          });
        }

        // Mouse if in the element range => search the closest item
        if (barChartStartX < mouseX && mouseX < barChartEndX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], Math.round((mouseX - barChartStartX) / (barChartWidth / facet.values.length))];
            }
            return null;
          });
        }
      };

      // function to finalized the selection
      const mouseUpHandler = () => {
        // if selection monde is on
        if (onSelectionEnd && selection) {
          // Searching the data for the inf/sup border
          // Note : a selection can start by selecting the up border, so we need to reorder
          const from = selection[0] < selection[1] ? selection[0] : selection[1];
          const to = selection[0] < selection[1] ? selection[1] : selection[0];
          // Calling the function with the data that match  the borders
          onSelectionEnd({
            from: `${displayDateYear(Number(facet.values[from].key))}-01-01`,
            to: `${displayDateYear(Number(facet.values[to].key))}-12-31`,
          });
        }
        // reset the selection
        setSelecting(false);
      };

      document.addEventListener("mousemove", mouseMoveHandler as EventListenerOrEventListenerObject);
      document.addEventListener("mouseup", mouseUpHandler as EventListenerOrEventListenerObject);
      // cleanup
      return () => {
        document.removeEventListener("mousemove", mouseMoveHandler as EventListenerOrEventListenerObject);
        document.removeEventListener("mouseup", mouseUpHandler as EventListenerOrEventListenerObject);
      };
    }
    return undefined;
  }, [barchartRef, facet.values, selection, onSelectionEnd, selecting, setSelecting]);

  return (
    <div className={cx(className, "histogram")} style={{ ...style, height }}>
      <div className="histogram-data" ref={barchartRef}>
        {facet.values.map((v, index) => {
          const selected = isInSelection(index, selection);

          return (
            <div
              key={index}
              className="histogram-column"
              title={`${displayDateYear(Number(v.key))} : ${v.count}`}
              onMouseDown={() => {
                // if selection mode is on
                if (onSelectionEnd) {
                  if (selected) {
                    //|| (d.value === 0 && !highlightedPeriod)) {
                    // reset selection
                    setSelection(null);
                    onSelectionEnd(null);
                    setSelecting(false);
                  } else {
                    // apply
                    setSelecting(true);
                    setSelection([index, index]);
                  }
                }
              }}
            >
              <div
                className={cx(
                  "histogram-column-data",
                  onSelectionEnd && selected && "selected",
                  // drag cursor when selecting by dragging is possible and not already selected
                  onSelectionEnd && v.count !== 0 && !selected && "selectable",
                  // pointer cursor (reset) when selecting by dragging is possible and already selected
                  onSelectionEnd && selected && "clickable",
                )}
                style={{ height: `${(v.count / max) * 100}%`, backgroundColor: "var(--bs-primary)" }}
              />
            </div>
          );
        })}
        <div className="histogram-scale-y">
          <span>0</span>
          <span>{max}</span>
        </div>
      </div>
      <div className="histogram-scale-x">
        <span>{firstValue && <>{displayDateYear(Number(firstValue.key))}</>}</span>
        <span>{lastValue && <>{displayDateYear(Number(lastValue.key))}</>}</span>
      </div>
    </div>
  );
};
