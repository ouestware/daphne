import { isNil, fromPairs, toPairs, isArray, every } from "lodash";

export function getFiltersForQuery(obj: { [key: string]: unknown }, prefix: string): { [key: string]: unknown } {
  return fromPairs(
    toPairs(obj)
      .filter(([_, value]) => !(!value || (isArray(value) && every(value, (v) => !v))))
      .map(([key, value]) => {
        return [`${prefix}${key}`, isArray(value) ? value.join("|") : value];
      }),
  );
}

export function getFiltersFromQuery(
  obj: { [key: string]: string | Array<string> | undefined },
  prefix: string,
): { [key: string]: Array<string> } {
  return fromPairs(
    toPairs(obj)
      .filter(([key, value]) => key.startsWith(prefix) && !isNil(value) && value !== "")
      .map(([key, value]) => [key.replace(prefix, ""), isArray(value) ? value : (value || "").split("|")]),
  );
}

/**
 * Return the position on the page of an html element
 */
export function getElementPosition(element: HTMLElement): [number, number] {
  let x = 0;
  let y = 0;
  let ele = element;
  let loop = true;
  while (loop) {
    x += ele.offsetLeft;
    y += ele.offsetTop;
    if (ele.offsetParent === null) {
      loop = false;
      break;
    }
    ele = ele.offsetParent as HTMLElement;
  }
  return [x, y];
}
/**
 * Check if the index is in the defined range.
 */
export function isInSelection(index: number, range: [number, number] | null): boolean {
  let result = false;
  if (range) {
    const from: number = range[0];
    const to: number = range[1];
    if (from <= to && from <= index && index <= to) result = true;
    if (from > to && to <= index && index <= from) result = true;
  }
  return result;
}
