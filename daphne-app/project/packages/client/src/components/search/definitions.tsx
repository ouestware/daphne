import { AggregationType, SortOrder, FilterType } from "../../core/graphql";
import { SearchType, SearchConfig } from "./types";

export const SEARCH_CONFIG: { [key in SearchType]: SearchConfig } = {
  factoids: {
    id: "factoids",
    label: "Factoids",
    facets: [
      {
        id: "dates",
        label: "Dates",
        aggType: AggregationType.DateHistogramPerYear,
        filterType: FilterType.Range,
        field: "dates",
      },
      {
        id: "certainty",
        label: "Confiance",
        filterType: FilterType.Range,
        field: "certainty",
      },
      {
        id: "types",
        label: "Types",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        field: "types",
      },
      {
        id: "people",
        label: "Personnes",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        nested: "people",
        field: "name.raw",
      },
      {
        id: "places",
        label: "Lieux",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        nested: "places",
        field: "name.raw",
      },
      {
        id: "institutions",
        label: "Institutions",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        nested: "institutions",
        field: "name.raw",
      },
      {
        id: "sources",
        label: "Data Sources",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        nested: "dataSources",
        field: "name.raw",
      },
    ],
    sorts: [
      {
        id: "score",
        label: "Pertinence",
        field: "_score",
        order: SortOrder.Desc,
      },
      {
        id: "certainty",
        label: "Confiance",
        field: "certainty",
        order: SortOrder.Desc,
      },
      {
        id: "date-asc",
        label: "Date croissante",
        field: "dates",
        order: SortOrder.Asc,
      },
      {
        id: "date-desc",
        label: "Date décroissante",
        field: "dates",
        order: SortOrder.Desc,
      },
      {
        id: "type-asc",
        label: "Type croissant",
        field: "types",
        order: SortOrder.Asc,
      },
      {
        id: "type-desc",
        label: "Type décroissant",
        field: "types",
        order: SortOrder.Desc,
      },
    ],
  },
  places: {
    id: "places",
    label: "Lieux",
    facets: [
      {
        id: "sources",
        label: "Data Sources",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        nested: "dataSources",
        field: "name.raw",
      },
    ],
    sorts: [
      {
        id: "score",
        label: "Pertinence",
        field: "_score",
        order: SortOrder.Desc,
      },
      {
        id: "nbFactoids-desc",
        label: "Nombre de Factoids décroissant",
        field: "nbFactoids",
        order: SortOrder.Desc,
      },
      {
        id: "nbFactoids-asc",
        label: "Nombre de Factoids croissant",
        field: "nbFactoids",
        order: SortOrder.Asc,
      },
    ],
  },
  people: {
    id: "people",
    label: "Personnes",
    facets: [
      {
        id: "sources",
        label: "Data Sources",
        aggType: AggregationType.Terms,
        filterType: FilterType.Terms,
        nested: "dataSources",
        field: "name.raw",
      },
    ],
    sorts: [
      {
        id: "score",
        label: "Pertinence",
        field: "_score",
        order: SortOrder.Desc,
      },
      {
        id: "nbFactoids-desc",
        label: "Nombre de Factoids décroissant",
        field: "nbFactoids",
        order: SortOrder.Desc,
      },
      {
        id: "nbFactoids-asc",
        label: "Nombre de Factoids croissant",
        field: "nbFactoids",
        order: SortOrder.Asc,
      },
    ],
  },
};
