import cx from "classnames";
import { isNil } from "lodash";
import { FC, CSSProperties, useCallback, useState, useEffect } from "react";
import AsyncSelect from "react-select/async";
import { FaFilter } from "react-icons/fa";
import Slider from "rc-slider";

import { SearchAggregationFragment, FilterType } from "../../core/graphql";
import { dateToStringForInput } from "../../utils/date";
import { SearchConfig } from "./types";
import { TiDeleteOutline } from "react-icons/ti";

interface FacetSearchSelectProperties {
  id: string;
  /**
   * Do a earch on the facet
   */
  doSearch: (query: string) => Promise<Array<string>>;
  /**
   * What to do when you click on an item
   */
  onChange: (values: Array<string>) => Promise<void> | void;
  /**
   * Selected values
   */
  selected: Array<string>;
}

const FacetSearchSelect: FC<FacetSearchSelectProperties> = ({ id, doSearch, onChange, selected = [] }) => {
  const loadOptions = useCallback(
    async (txt: string) => {
      const result = await doSearch(txt || "");
      return result.map((e) => ({ value: e, label: e }));
    },
    [doSearch],
  );

  return (
    <AsyncSelect
      instanceId={id}
      isMulti
      loadOptions={loadOptions}
      defaultOptions={true}
      value={selected.filter((e) => !isNil(e)).map((e) => ({ value: e, label: e }))}
      onChange={(values) => onChange(values.map((v) => v.value))}
    />
  );
};

/**
 * Facet for date range
 */
const FacetDateSelection: FC<FacetSearchSelectProperties & { data?: SearchAggregationFragment }> = ({
  id,
  selected,
  onChange,
  data,
}) => {
  const [start, setStart] = useState<string>("");
  const [end, setEnd] = useState<string>("");
  const [min, setMin] = useState<string | undefined>(undefined);
  const [max, setMax] = useState<string | undefined>(undefined);
  const [error, setError] = useState<string | undefined>(undefined);
  const [disabled, setDisabled] = useState<boolean>(false);

  useEffect(() => {
    const nMin = data && data.values.length > 0 ? dateToStringForInput(data.values[0].key) : undefined;
    const nMax =
      data && data.values.length > 0
        ? dateToStringForInput(data.values[data.values.length - 1].key) || undefined
        : undefined;

    let nStart = "";
    let nEnd = "";
    if (selected && selected.length === 2) {
      nStart = selected[0];
      nEnd = selected[1];
    }
    setDisabled(data?.total === 0);
    setStart(nStart);
    setEnd(nEnd);
    setMin(nStart && (!nMin || nStart < nMin) ? nStart : nMin);
    setMax(nEnd && (!nMax || nEnd > nMax) ? nEnd : nMax);
  }, [data, selected]);

  useEffect(() => {
    // it works because date format is yyyy-mm-dd :)
    if (start && end && start > end) {
      setError("La date de début doit être inférieur à la date de fin");
    } else setError(undefined);
  }, [start, end]);

  return (
    <form
      id={id}
      onSubmit={(e) => {
        e.preventDefault();
        if (!error) {
          onChange(start != "" || end != "" ? [start, end] : []);
        }
      }}
    >
      {error && <p className="text-danger text-center">{error}</p>}
      <div className="row mb-1">
        <label className="col-sm-2 col-form-label" htmlFor="start">
          De
        </label>
        <div className="col-sm-10 d-flex align-items-baseline">
          <input
            id="start"
            className="form-control validation"
            type="date"
            min={min}
            max={max}
            disabled={disabled}
            value={start}
            onChange={(e) => {
              setStart(e.target.value);
            }}
          />
          <button
            className="btn btn-link"
            type="button"
            disabled={disabled || start === ""}
            onClick={() => {
              onChange(end ? ["", end] : []);
            }}
          >
            <TiDeleteOutline size="1.5rem" />
          </button>
        </div>
      </div>
      <div className="row mb-2">
        <label className="col-sm-2 col-form-label" htmlFor="end">
          A
        </label>
        <div className="col-sm-10 d-flex align-items-baseline">
          <input
            id="end"
            className="form-control validation"
            type="date"
            min={min}
            max={max}
            disabled={disabled}
            value={end}
            onChange={(e) => setEnd(e.target.value)}
          />
          <button
            className="btn btn-link"
            type="button"
            disabled={disabled || end === ""}
            onClick={() => {
              onChange(start ? [start, ""] : []);
            }}
          >
            <TiDeleteOutline size="1.5rem" />
          </button>
        </div>
      </div>
      <button className="btn btn-primary" type="submit" disabled={disabled}>
        Valider
      </button>
    </form>
  );
};

/**
 * Facet for number range
 */
const FacetRange: FC<FacetSearchSelectProperties> = ({ selected, onChange }) => {
  const [start, setStart] = useState<number>(0);
  const [end, setEnd] = useState<number>(1);

  useEffect(() => {
    if (selected && selected.length === 2) {
      setStart(parseFloat(selected[0]));
      setEnd(parseFloat(selected[1]));
    } else {
      setStart(0);
      setEnd(1);
    }
  }, [selected]);

  return (
    <div style={{ padding: "0 6px 15px 6px" }}>
      <Slider
        step={0.1}
        range
        allowCross={false}
        min={0}
        max={1}
        value={[start, end]}
        onChange={(e) => {
          const data = e as [number, number];
          setStart(data[0]);
          setEnd(data[1]);
        }}
        onAfterChange={(e) => {
          const data = e as [number, number];
          setStart(data[0]);
          setEnd(data[1]);
          onChange(data.map((i) => `${i}`));
        }}
        trackStyle={[{ backgroundColor: "var(--bs-primary)" }]}
        handleStyle={{ borderColor: "var(--bs-primary)" }}
      />
    </div>
  );
};

interface SearchFacetProperties {
  className?: string;
  style?: CSSProperties;
  /**
   * Facet def
   */
  facet: SearchConfig["facets"][0];
  /**
   * facet data
   */
  data?: SearchAggregationFragment;
  /**
   * Maximum number. It is used as a base ref for the progress bar.
   */
  max?: number;
  /**
   * List of selected elements
   */
  selected: Array<string>;
}
export const SearchFacet: FC<SearchFacetProperties & FacetSearchSelectProperties> = ({
  className,
  style,
  facet,
  data,
  max,
  doSearch,
  onChange,
  selected,
}) => {
  return (
    <div className={cx(className, "search-facet")} style={style}>
      <h5>
        <FaFilter className="me-1" />
        {facet.label}
      </h5>

      {facet.filterType === FilterType.Range ? (
        <>
          {facet.aggType && facet.aggType.startsWith("date_") ? (
            <FacetDateSelection data={data} id={facet.id} selected={selected} doSearch={doSearch} onChange={onChange} />
          ) : (
            <FacetRange id={facet.id} selected={selected} doSearch={doSearch} onChange={onChange} />
          )}
        </>
      ) : (
        <>
          <FacetSearchSelect id={facet.id} selected={selected} doSearch={doSearch} onChange={onChange} />
          <div className="list-group my-0">
            {data &&
              data.values &&
              data.values.map((value) => (
                <button
                  key={value.key}
                  className="list-group-item list-group-item-action mx-0 px-0 border border-0"
                  title={
                    max
                      ? `Filtrer - ${Math.round((value.count / max) * 100) || "< 1"}% des résultats de la recherche`
                      : `Filtrer`
                  }
                  onClick={() => {
                    if (onChange) {
                      if (selected && selected.includes(value.key)) onChange(selected.filter((e) => e !== value.key));
                      else onChange([...(selected || []), value.key]);
                    }
                  }}
                >
                  <div className="d-flex">
                    <span>{value.key}</span>
                    <span className="text-muted fst-italic fs-6 ms-2">({value.count})</span>
                  </div>
                  {max && (
                    <div className="progress" style={{ height: "5px" }}>
                      <div className="progress-bar" style={{ width: `${(value.count / max) * 100}%` }} />
                    </div>
                  )}
                </button>
              ))}
            {!isNil(data) && !isNil(data.total) && data.total - data.values.length > 0 && (
              <span className="text-muted fst-italic fs-6">
                ... et {data.total - data.values.length} autres valeurs
              </span>
            )}
          </div>
        </>
      )}
    </div>
  );
};
