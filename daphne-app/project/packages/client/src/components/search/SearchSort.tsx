import { FC, CSSProperties } from "react";
import cx from "classnames";
import { FaSort } from "react-icons/fa";
import Select from "react-select";

import { SearchConfig } from "./types";

interface SearchSortProperties {
  className?: string;
  style?: CSSProperties;
  /**
   * Sorts def
   */
  sorts: SearchConfig["sorts"];
  selected: string;
  onChange: (sortId: string) => void;
}
export const SearchSort: FC<SearchSortProperties> = ({ className, style, sorts, selected, onChange }) => {
  const options = sorts.map((s) => ({ value: s.id, label: s.label }));
  return (
    <div className={cx(className, "search-sort")} style={style}>
      <h5>
        <FaSort className="me-1" />
        Trier par
      </h5>

      <Select
        instanceId="sort"
        value={options.find((e) => e.value === selected)}
        options={options}
        onChange={(e) => {
          if (e) onChange(e.value);
        }}
      />
    </div>
  );
};
