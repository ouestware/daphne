import { AggregationType, SortOrder, FilterType } from "../../core/graphql";

export interface SearchConfig {
  /**
   * Id of the search engine
   */
  id: SearchType;
  /**
   * Name of the search engine that will be displayed in UI.
   */
  label: string;
  /**
   * List of facet/filters that should be executed
   */
  facets: Array<{
    id: string;
    /**
     *Name of the facet that will be displayed in UI.
     */
    label: string;
    /**
     * Type of the facet (pretty much the ES type) if needed.
     */
    aggType?: AggregationType;
    /**
     * Filter type (can be range or terms) if needed
     */
    filterType: FilterType;
    /**
     * If this fact should be done on a nested object,
     * you have to provide its name here.
     */
    nested?: string;
    /**
     * Name of the field on which we do the aggs
     */
    field: string;
  }>;
  sorts: Array<{
    id: string;
    label: string;
    field: string;
    order: SortOrder;
  }>;
}
export type SearchType = "factoids" | "places" | "people";
