import { FC } from "react";

import { FactoidFragment, PlaceFragment, PhysicalPersonFragment } from "../../core/graphql";
import { FactoidLite } from "../data/Factoid";
import { PlaceLite } from "../data/Place";
import { PersonLite } from "../data/Person";

export const SearchResultItem: FC<{ item: FactoidFragment | PlaceFragment | PhysicalPersonFragment }> = ({ item }) => {
  switch (item.__typename) {
    case "PhysicalPerson":
      return <PersonLite person={item} />;
    case "Place":
      return <PlaceLite place={item} />;
    case "Factoid":
      return <FactoidLite factoid={item} />;
    default:
      return null;
  }
};
