import { useState, useCallback } from "react";
import { useApolloClient } from "@apollo/client";

import { factoidsSearchQuery } from "../../core/graphql/queries/factoid";
import { placesSearchQuery } from "../../core/graphql/queries/place";
import { SearchFilterInput, SearchSortInput, SearchAggregationInput } from "../../core/graphql";
import { physicalPersonSearchQuery } from "../../core/graphql/queries/person";
import { SearchType } from "./types";

export function useAppSearch() {
  const client = useApolloClient();

  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const search = useCallback(
    async (
      type: SearchType,
      params: {
        inquiryId: string;
        search: string;
        skip: number;
        limit: number;
        sort?: SearchSortInput;
        filters: Array<SearchFilterInput>;
        aggregations: Array<SearchAggregationInput>;
      },
      withLoader = true,
    ) => {
      if (withLoader) setLoading(true);
      setError(null);
      try {
        switch (type) {
          case "people": {
            const data = await client.query({ query: physicalPersonSearchQuery, variables: params });
            return data;
          }
          case "places": {
            const data = await client.query({ query: placesSearchQuery, variables: params });
            return data;
          }
          default: {
            const data = await client.query({ query: factoidsSearchQuery, variables: params });
            return data;
          }
        }
      } catch (e) {
        setError(e as Error);
        return { data: null };
      } finally {
        if (withLoader) setLoading(false);
      }
    },
    [client],
  );

  return {
    loading,
    error,
    search,
  };
}
