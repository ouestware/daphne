import React, { ComponentType, FC, HTMLAttributes, isValidElement, LegacyRef, useEffect, useState } from "react";
import TetherComponent from "react-tether";
import cx from "classnames";

export type VAttach = "top" | "middle" | "bottom";
export type HAttach = "left" | "center" | "right";

function reverseAttachment(value: [VAttach, HAttach]): [VAttach, HAttach] {
  let v: VAttach = "middle";
  if (value[0] !== "middle") {
    v = value[0] === "top" ? "bottom" : "top";
  }
  let h: HAttach = "center";
  if (value[1] !== "center") {
    h = value[1] === "left" ? "right" : "left";
  }
  return [v, h];
}

type TooltipContentComponent = ComponentType<{ closeTooltip: () => void }>;

interface TippedProps extends Omit<HTMLAttributes<HTMLElement>, "children"> {
  children: [JSX.Element, JSX.Element | TooltipContentComponent];
  tag?: keyof JSX.IntrinsicElements;
  disabled?: boolean;
  attachment?: [VAttach, HAttach];
  targetAttachment?: [VAttach, HAttach];
  offset?: string;
  autoShowInHover?: boolean;
  className?: string;
  rootClassName?: string;
}

export const Tooltip: FC<TippedProps> = (props) => {
  const {
    children,
    tag,
    disabled,
    style,
    attachment,
    targetAttachment,
    offset,
    autoShowInHover = true,
    className,
    rootClassName,
    ...attributes
  } = props;
  const [target, tooltipContent] = children;
  const [showTip, setShowTip] = useState<boolean>(false);
  const [closeTimeout, setCloseTimeout] = useState<number | null>(null);

  const attach: [VAttach, HAttach] = attachment ? attachment : ["bottom", "center"];
  const targetAttach = targetAttachment || reverseAttachment(attach);

  // when opening tooltip add a handler on body click to close it
  useEffect(() => {
    // only on tooltip appearance
    if (!showTip) return;
    const handleClickBody = () => {
      setShowTip(false);
    };
    setTimeout(() => document.body.addEventListener("click", handleClickBody), 0);
    return () => document.body.removeEventListener("click", handleClickBody);
  }, [showTip]);

  const onMouseEnter = () => {
    !disabled && autoShowInHover && setShowTip(true);
    if (closeTimeout) {
      clearTimeout(closeTimeout);
      setCloseTimeout(null);
    }
  };

  const onMouseLeave = () => {
    if (autoShowInHover) {
      setCloseTimeout((prev) => {
        if (prev) {
          clearTimeout(prev);
        }
        const id = setTimeout(() => {
          setShowTip(false);
        }, 200);
        return id as unknown as number;
      });
    }
  };

  return React.createElement(
    tag || "div",
    {
      ...attributes,
      onMouseEnter,
      onMouseLeave,
      onClick: (e: MouseEvent) => {
        // do not propagate to the body click handler
        e.stopPropagation();
        setShowTip(!showTip);
      },
      className: rootClassName,
      style: {
        ...(style || {}),
        position: "relative",
      },
    },

    target,
    showTip && (
      <TetherComponent
        className={`over-modal ${targetAttach.join(" ")}`}
        attachment={targetAttach.join(" ")}
        targetAttachment={attach.join(" ")}
        offset={offset || "0 -15px"}
        constraints={[{ to: "window", attachment: "together", pin: true }]}
        renderTarget={(ref) => <div ref={ref as LegacyRef<HTMLDivElement>} className="tooltip-anchor" />}
        renderElement={(ref) =>
          showTip && (
            <div ref={ref as LegacyRef<HTMLDivElement>} className={cx("tooltip", "show")}>
              <div className={cx("tooltip-inner", className)}>
                {isValidElement(tooltipContent)
                  ? tooltipContent
                  : React.createElement(tooltipContent as TooltipContentComponent, {
                      closeTooltip: () => setShowTip(false),
                    })}
              </div>
            </div>
          )
        }
      />
    ),
  );
};
