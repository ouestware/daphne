import { FC, CSSProperties } from "react";
import cx from "classnames";

export const Spinner: FC<{ className?: string; style?: CSSProperties }> = ({ className, style }) => (
  <div className={cx("spinner-border", className)} style={style} role="status">
    <span className="visually-hidden">Loading...</span>
  </div>
);

export const Loader: FC<{ message?: string }> = ({ message }) => (
  <div className="loader-fill d-flex justify-content-center align-items-center flex-column">
    <Spinner style={{ width: "3rem", height: " 3rem" }} />
    {message && <span>{message}</span>}
  </div>
);
