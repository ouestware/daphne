import { FC, useState, useEffect } from "react";
import cx from "classnames";

import { parseError } from "../utils/error";
import { NotFound } from "./NotFound";

export const Error: FC<{ className?: string; error?: Error }> = ({ className, error }) => {
  const [isNotFound, setNotFound] = useState<boolean>(false);
  const [message, setMessage] = useState<string | null>(null);

  useEffect(() => {
    if (error) setNotFound(parseError(error).endsWith("404"));
    setMessage(error ? parseError(error) : null);
  }, [error]);

  return (
    <>
      {isNotFound ? (
        <NotFound />
      ) : (
        <div className={cx("container text-center col-4", className)}>
          <div className="row">
            <h1>500</h1>
            <h2>Une erreur est survenue</h2>
            {message && <p>{message}</p>}
          </div>
        </div>
      )}
    </>
  );
};
