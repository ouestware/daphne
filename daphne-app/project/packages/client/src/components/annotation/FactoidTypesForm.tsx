import { FC } from "react";
import { FactoidFragment, factoidTypesSearchESQuery } from "../../core/graphql";
import { useApolloClient } from "@apollo/client";
import AsyncSelect from "react-select/async";

import { omit } from "lodash";

export interface FactoidTypeAnnotationInput {
  id: string;
  name: string;
}

export const factoidTypesFromFactoid = (factoid?: FactoidFragment): FactoidTypeAnnotationInput[] => {
  return factoid ? factoid.types.map((t) => omit(t, "__typename")) : [];
};

export const FactoidTypesForm: FC<{
  inquiryId: string;
  factoidTypes: FactoidTypeAnnotationInput[];
  onChange: (factoidTypes: FactoidTypeAnnotationInput[]) => void;
}> = ({ factoidTypes, onChange, inquiryId }) => {
  const client = useApolloClient();

  return (
    <div>
      <label className="form-label" htmlFor="factoidTypes">
        Types de factoïde
      </label>
      <AsyncSelect
        id="factoidTypes"
        isMulti
        loadOptions={async (query: string) => {
          // const result = await client.query({ query: rolesSearchGQLQuery, variables: { name: query } });
          // return result.data.roles.map((r) => ({ value: r.id, label: r.name }));
          const result = await client.query({
            query: factoidTypesSearchESQuery,
            variables: { search: query, inquiryId },
          });
          return result.data.factoidTypesSearch.results.map((r) => ({ value: r.id, label: r.name }));
        }}
        defaultOptions={true}
        value={factoidTypes.map((ft) => ({ value: ft.id, label: ft.name }))}
        onChange={(values) => onChange(values.map((v) => ({ id: v.value, name: v.label })))}
      />
    </div>
  );
};
