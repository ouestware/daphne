import { useCallback, FC, useState } from "react";
import { SortOrder, PhysicalPerson, Role, FactoidFragment } from "../../core/graphql";
import { useApolloClient } from "@apollo/client";
import AsyncSelect from "react-select/async";
import { TiDelete, TiUserAdd } from "react-icons/ti";

import { useAppSearch } from "../search/useAppSearch";
import { SearchType } from "../search/types";
import { rolesSearchESQuery } from "../../core/graphql/queries/role";

export interface FactoidParticipationAnnotationInput {
  id: string | null;
  person: { id: string; name: string };
  roles: { id: string; name: string }[];
}

export const participationsFromFactoid = (factoid?: FactoidFragment): FactoidParticipationAnnotationInput[] => {
  return factoid
    ? factoid.persons.map((p) => ({
        id: p.id,
        person: {
          id: p.person.id,
          name: p.person.name,
        },
        roles: p.roles.map((r) => ({ id: r.id, name: r.name })),
      }))
    : [];
};

export const FactoidParticipationsForm: FC<{
  inquiryId: string;
  participations: FactoidParticipationAnnotationInput[];
  onChange: (participations: FactoidParticipationAnnotationInput[]) => void;
}> = ({ participations, onChange, inquiryId }) => {
  const { search } = useAppSearch();
  const client = useApolloClient();

  const [newPerson, setNewPerson] = useState<{ value: string; label: string } | null>(null);

  const loadOptions = useCallback(
    (type: SearchType) => async (query: string) => {
      const { data } = await search(type, {
        inquiryId,
        search: query,
        skip: 0,
        filters: [],
        aggregations: [],
        limit: 20,
        sort: { field: "_score", order: SortOrder.Desc },
      });
      if (data)
        return data.search.results.map((r: unknown) => ({
          value: (r as PhysicalPerson | Role).id,
          label: (r as PhysicalPerson | Role).name,
        }));
      else return [];
    },
    [search, inquiryId],
  );

  return (
    <form onSubmit={(e) => e.preventDefault()}>
      <details open>
        <summary className="fs-4">{participations.length} Personnes</summary>
        {participations.map((p) => (
          <div className="d-flex mb-3 flex-column border p-2" key={p.id}>
            <div className="position-relative mb-2">
              <div className="fs-5">{p.person.name}</div>
              <button
                className="btn btn-danger position-absolute top-0 end-0 p-0"
                onClick={(e) => {
                  e.stopPropagation();
                  onChange([...participations.filter((pp) => pp.id !== p.id)]);
                }}
                title="supprimer"
              >
                <TiDelete size="1.5rem" />
              </button>
            </div>
            <div className="d-flex align-items-baseline">
              <label className="form-label me-2" htmlFor={`roles-${p.id}`}>
                Roles
              </label>
              <AsyncSelect
                id={`roles-${p.id}`}
                isMulti
                loadOptions={async (query: string) => {
                  // const result = await client.query({ query: rolesSearchGQLQuery, variables: { name: query } });
                  // return result.data.roles.map((r) => ({ value: r.id, label: r.name }));
                  const result = await client.query({
                    query: rolesSearchESQuery,
                    variables: { search: query, inquiryId: inquiryId },
                  });
                  return result.data.rolesSearch.results.map((r) => ({ value: r.id, label: r.name }));
                }}
                defaultOptions={true}
                value={p.roles.map((r) => ({ value: r.id, label: r.name }))}
                onChange={(values) =>
                  onChange([
                    ...participations.filter((pp) => pp.id !== p.id),
                    { id: p.id, person: p.person, roles: values.map((v) => ({ id: v.value, name: v.label })) },
                  ])
                }
              />
            </div>
          </div>
        ))}
        <div>
          {" "}
          Ajouter une personne
          <div className="d-flex align-items-top">
            <AsyncSelect
              className="flex-grow-1 me-2"
              loadOptions={loadOptions("people")}
              defaultOptions={true}
              value={newPerson}
              onChange={(value) => setNewPerson(value)}
            />
            <button
              className="btn btn-primary p-1"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (newPerson) {
                  onChange([
                    ...participations,
                    { id: null, person: { id: newPerson.value, name: newPerson.label }, roles: [] },
                  ]);
                  setNewPerson(null);
                }
              }}
              disabled={newPerson === null}
              title="ajouter"
            >
              <TiUserAdd size="1.6rem" />
            </button>
          </div>
        </div>
      </details>
    </form>
  );
};
