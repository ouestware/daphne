import { pick } from "lodash";
import { FC } from "react";
import { BsCalendarMinusFill, BsCalendarPlusFill } from "react-icons/bs";
import { DateInlineFragment, FactoidFragment } from "../../core/graphql";

export type FactoidDatesInput = Partial<Pick<DateInlineFragment, "id" | "startIso" | "endIso">>;

export const datesFromFactoid = (factoid?: FactoidFragment): FactoidDatesInput[] => {
  return factoid ? factoid.dates.map((d) => pick(d, ["id", "startIso", "endIso"])) : [];
};
export const generateDateId = (date: FactoidDatesInput) => {
  return `${date.startIso || ""}${date.endIso ? `|${date.endIso}` : ""}`;
};

const isoDatePattern = "^([0-9]{4})(-1[0-2]|-0[1-9])?(-3[01]|-0[1-9]|-[12][0-9])?$";

export const FactoidDatesForm: FC<{
  dates: FactoidDatesInput[];
  onChange: (participations: FactoidDatesInput[]) => void;
}> = ({ dates, onChange }) => {
  return (
    <form onSubmit={(e) => e.preventDefault()}>
      <details open>
        <summary className="fs-4">
          {dates.length} Date{dates.length > 1 ? "s" : ""}
        </summary>
        {dates.map((d, i) => (
          <div key={i} className="mb-3 d-flex align-items-baseline">
            <div className="d-flex me-3 align-items-baseline">
              <label className="form-label me-2" htmlFor="startISO">
                De
              </label>
              <input
                className="form-control validation"
                required={!!d.startIso || !d.endIso}
                type="text"
                value={d.startIso || undefined}
                pattern={isoDatePattern}
                placeholder="YYYY-MM-DD"
                onChange={(e) => {
                  const newDate = { ...dates[i], startIso: e.target.value };
                  onChange([...dates.filter((_, j) => j !== i), { ...newDate, id: generateDateId(newDate) }]);
                }}
              />
            </div>
            <div className="d-flex align-items-baseline me-2">
              <label className="form-label me-2" htmlFor="startISO">
                à
              </label>
              <input
                // TODO: check end >= start
                className="form-control validation"
                type="text"
                required={!!d.endIso || !d.startIso}
                value={d.endIso || undefined}
                pattern={isoDatePattern}
                placeholder="YYYY-MM-DD"
                onChange={(e) => {
                  const newDate = { ...dates[i], endIso: e.target.value };
                  onChange([...dates.filter((_, j) => j !== i), { ...newDate, id: generateDateId(newDate) }]);
                }}
              />
            </div>
            <button
              className="btn btn-danger p-1"
              style={{ lineHeight: "1rem", height: "min-content" }}
              onClick={(e) => {
                e.preventDefault();
                onChange(dates.filter((_, j) => i !== j));
              }}
            >
              <BsCalendarMinusFill />
            </button>
          </div>
        ))}
        <div>
          Ajouter une date{" "}
          <button
            className="btn btn-primary p-1"
            style={{ lineHeight: "1rem" }}
            onClick={(e) => {
              e.preventDefault();
              onChange([...dates, {}]);
            }}
          >
            <BsCalendarPlusFill />
          </button>
        </div>
      </details>
    </form>
  );
};
