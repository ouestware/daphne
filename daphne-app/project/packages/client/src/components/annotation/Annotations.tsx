import { FC } from "react";
import { AnnotationFullFragment } from "../../core/graphql";

const dateFormat = new Intl.DateTimeFormat("fr-FR", { dateStyle: "long", timeStyle: "short" });

export const Annotations: FC<{ annotations: AnnotationFullFragment[] }> = ({ annotations }) => {
  return (
    <div className="row">
      <div className="col-3">Annotation{annotations.length > 1 ? "s" : ""}</div>
      <div className="col-9">
        {annotations.map((a) => (
          <details key={a.id}>
            <summary>
              Le {dateFormat.format(new Date(a.createdAt))}, {a.author} : «{a.comment}»
            </summary>
            <pre>{JSON.stringify(a.patch, undefined, 4)}</pre>
          </details>
        ))}
      </div>
    </div>
  );
};
