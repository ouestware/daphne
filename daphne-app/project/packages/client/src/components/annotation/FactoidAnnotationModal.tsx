import { FC, useCallback, useEffect, useState } from "react";
import { v4 as uuid } from "uuid";
import cx from "classnames";

import { FactoidFragment } from "../../core/graphql";
import { useNotifications } from "../../core/notifications";
import { Modal, ModalProps } from "../../core/modals";
import { isEqual, pick, trimStart } from "lodash";
import { ImportLine } from "packages/client/types/annotation";
import { useMutation } from "@apollo/client";
import { createAnnotation } from "../../core/graphql/queries/annotation";
import {
  FactoidParticipationAnnotationInput,
  FactoidParticipationsForm,
  participationsFromFactoid,
} from "./FactoidParticipationsForm";
import { datesFromFactoid, FactoidDatesForm, FactoidDatesInput } from "./FactoidDatesForm";
import { FactoidTypeAnnotationInput, FactoidTypesForm, factoidTypesFromFactoid } from "./FactoidTypesForm";

export type FactoidAnnotationModalProps = ModalProps<
  { item?: FactoidFragment; inquiryId: string },
  { trimedFactoidId: string }
>;

export const FactoidAnnotationModal: FC<FactoidAnnotationModalProps> = ({
  arguments: { item: factoid, inquiryId },
  cancel,
  submit,
}) => {
  const { notify } = useNotifications();
  const [save] = useMutation(createAnnotation);

  const trimID = useCallback((id: string) => trimStart(id, `${inquiryId}-`), [inquiryId]);

  // form state
  const [annotatedFactoid, setAnnotatedFactoid] = useState<
    Pick<FactoidFragment, "id" | "certainty" | "description"> & {
      participations: FactoidParticipationAnnotationInput[];
      dates: FactoidDatesInput[];
      types: FactoidTypeAnnotationInput[];
    }
  >(
    factoid !== undefined
      ? {
          ...pick(factoid, ["id", "certainty", "description"]),
          participations: participationsFromFactoid(factoid),
          dates: datesFromFactoid(factoid),
          types: factoidTypesFromFactoid(factoid),
        }
      : { id: uuid(), certainty: 0.5, dates: [], participations: [], types: [] },
  );

  // annotation metadata
  const [comment, setcomment] = useState<string | undefined>();
  const [investigatorName, setInvestigatorName] = useState<string | undefined>(
    localStorage.getItem("investigatorName") || undefined,
  );
  const [patch, setPatch] = useState<ImportLine[]>([]);

  // update patch
  useEffect(() => {
    const newPatch: ImportLine[] = [];
    const factoidId = factoid ? trimID(factoid.id) : annotatedFactoid.id;

    /* METADATA */
    const metaChanges = [
      ...(!factoid || annotatedFactoid.certainty !== factoid.certainty ? ["certainty"] : []),
      ...((!factoid && annotatedFactoid.description !== "") || annotatedFactoid.description !== factoid?.description
        ? ["description"]
        : []),
    ];
    if (metaChanges.length > 0) {
      newPatch.push({
        op: "merge_node",
        id: factoidId,
        type: "Factoid",
        properties: pick(annotatedFactoid, metaChanges),
      });
    }
    /* TYPES */
    const preExistingTypes = factoidTypesFromFactoid(factoid);
    // new
    annotatedFactoid.types
      .filter((t) => preExistingTypes.find((_t) => _t.id === t.id) === undefined)
      .forEach((newType) => {
        newPatch.push({
          op: "add_rel",
          type: "HAS_FACTOID_TYPE",
          source: { id: factoidId, type: "Factoid" },
          target: { id: trimID(newType.id), type: "FactoidType" },
          properties: {},
        });
      });
    // deprecated to delete
    preExistingTypes
      .filter((_t) => annotatedFactoid.types.find((t) => t.id === _t.id) === undefined)
      .forEach((toDeleteType) => {
        newPatch.push({
          op: "remove_rel",
          type: "HAS_FACTOID_TYPE",
          source: { id: factoidId, type: "Factoid" },
          target: { id: trimID(toDeleteType.id), type: "FactoidType" },
        });
      });

    /* PARTICIPATIONS */
    const preExistingParticipations = participationsFromFactoid(factoid);

    annotatedFactoid.participations.forEach((p) => {
      const existingP = preExistingParticipations.find((fp) => fp.id === p.id);
      if (p.id && existingP) {
        // something changed: must be roles as person can't be changed in UI
        if (!isEqual(pick(p, "person"), pick(existingP, "person"))) {
          const message = `Une personne ne devrait pas pouvoir être changé sur une participation existante et pourtant ${p.person} != ${existingP.person}`;
          notify({ type: "error", message });
          throw new Error(message);
        }
        const newRoles = p.roles.filter((r) => !existingP.roles.find((rr) => rr.id === r.id));
        const deprecatedRoles = existingP.roles.filter((rr) => !p.roles.find((r) => rr.id === r.id));
        newRoles.forEach((nr) =>
          newPatch.push({
            op: "add_rel",
            type: "WITH_ROLE",
            source: { id: trimID(p.id as string), type: "FactoidPersonParticipate" },
            target: { id: trimID(nr.id), type: "Role" },
            properties: {},
          }),
        );
        deprecatedRoles.forEach((dr) =>
          newPatch.push({
            op: "remove_rel",
            type: "WITH_ROLE",
            source: { id: trimID(p.id as string), type: "FactoidPersonParticipate" },
            target: { id: trimID(dr.id), type: "Role" },
          }),
        );

        // else nothing changed
      } else {
        // creation
        const newPId = uuid();
        newPatch.push({
          op: "add_node",
          type: "FactoidPersonParticipate",
          id: newPId,
          properties: {},
        });
        newPatch.push({
          op: "add_rel",
          type: "PARTICIPATE",
          source: { id: newPId, type: "FactoidPersonParticipate" },
          target: { id: factoidId, type: "Factoid" },
          properties: {},
        });
        newPatch.push({
          op: "add_rel",
          type: "PARTICIPATE",
          source: { id: trimID(p.person.id), type: "PhysicalPerson" },
          target: { id: newPId, type: "FactoidPersonParticipate" },
          properties: {},
        });
        p.roles.forEach((nr) => {
          newPatch.push({
            op: "add_rel",
            type: "WITH_ROLE",
            source: { id: newPId, type: "FactoidPersonParticipate" },
            target: { id: trimID(nr.id), type: "Role" },
            properties: {},
          });
        });
      }
    });
    // Deletion
    preExistingParticipations
      .filter((pep) => pep.id && !annotatedFactoid.participations.find((p) => p.id === pep.id))
      .forEach((toDelete) => {
        // delete participate node
        newPatch.push({
          op: "remove_node",
          id: trimID(toDelete.id as string),
          type: "FactoidPersonParticipate",
        });
      });

    /* Dates */
    const preExistingDates = datesFromFactoid(factoid);
    annotatedFactoid.dates.forEach((date) => {
      // discard empty dates
      if (date.id && (date.startIso || date.endIso)) {
        //modifying a date => replace the data object in db
        const alreadyThere = preExistingDates.find(
          (ped) => ped.startIso === date.startIso && ped.endIso === date.endIso,
        );
        if (!alreadyThere) {
          // add date with a merge
          newPatch.push({ op: "merge_node", id: date.id, type: "DateValue", properties: date });
          // add relation
          newPatch.push({
            op: "add_rel",
            type: "OCCURED_AT",
            source: { id: factoidId, type: "Factoid" },
            target: { id: date.id, type: "DateValue" },
            properties: {},
          });
        }
      }
    });
    preExistingDates
      .filter(
        (ped) => !annotatedFactoid.dates.find((date) => ped.startIso === date.startIso && ped.endIso === date.endIso),
      )
      .forEach((dateToRemove) => {
        //deletion
        if (dateToRemove.id) {
          // only remove relation as date node can be used by other factoids
          newPatch.push({
            op: "remove_rel",
            type: "OCCURED_AT",
            source: { id: factoidId, type: "Factoid" },
            target: { id: trimID(dateToRemove.id), type: "DateValue" },
          });
        }
      });

    setPatch(newPatch);
  }, [annotatedFactoid, factoid, notify, trimID]);

  useEffect(() => {
    if (investigatorName) localStorage.setItem("investigatorName", investigatorName);
    else localStorage.removeItem("investigatorName");
  }, [investigatorName]);

  return (
    <Modal title={`Annoter un factoïde`} showHeader onClose={() => cancel()}>
      <div>
        <form
          onSubmit={async (e) => {
            e.preventDefault();
            try {
              if (comment && investigatorName)
                await save({
                  variables: {
                    id: inquiryId,
                    comment,
                    author: investigatorName,
                    patch,
                  },
                });
              notify({ type: "success", message: "Annotation sauvegardée" });
              submit({ trimedFactoidId: factoid ? trimID(factoid.id) : annotatedFactoid.id });
            } catch (e) {
              notify({ type: "error", message: `Impossible de créer l'annotation ${e}` });
            }
          }}
        >
          {/* Factoi Types */}
          <div className="mb-3">
            <FactoidTypesForm
              inquiryId={inquiryId}
              factoidTypes={annotatedFactoid.types}
              onChange={(types) => setAnnotatedFactoid({ ...annotatedFactoid, types })}
            />
          </div>
          {/* uncertainty */}
          <div className="mb-3">
            <label className="form-label" htmlFor="investigatorName">
              Incertidude
            </label>
            <input
              className="form-control"
              type="number"
              id="certainty"
              value={annotatedFactoid.certainty}
              onChange={(e) => {
                if (e.target.value && +e.target.value)
                  setAnnotatedFactoid({ ...annotatedFactoid, certainty: +e.target.value });
              }}
            />
          </div>
          <div className="mb-3">
            <label className="form-label" htmlFor="investigatorName">
              Description
            </label>
            <textarea
              className="form-control"
              id="description"
              value={annotatedFactoid.description || undefined}
              onChange={(e) => {
                setAnnotatedFactoid({ ...annotatedFactoid, description: e.target.value || undefined });
              }}
            />
          </div>
          <div>
            <FactoidDatesForm
              dates={annotatedFactoid.dates}
              onChange={(dates) => {
                setAnnotatedFactoid({ ...annotatedFactoid, dates });
              }}
            />
          </div>
          <div>
            <FactoidParticipationsForm
              participations={annotatedFactoid.participations}
              inquiryId={inquiryId}
              onChange={(participations: FactoidParticipationAnnotationInput[]) => {
                setAnnotatedFactoid({ ...annotatedFactoid, participations });
              }}
            />
          </div>
          <hr className="mb-4 mt-4" />
          <div className="mb-3">
            <label className="form-label" htmlFor="investigatorName">
              Commentaire
            </label>
            <textarea
              required={patch.length > 0}
              className="form-control validation"
              id="comment"
              value={comment}
              onChange={(e) => {
                setcomment(e.target.value || undefined);
              }}
            />
          </div>
          <div className="mb-3">
            <label className="form-label" htmlFor="investigatorName">
              Votre nom d&apos;enquêteur
            </label>
            <input
              required={patch.length > 0}
              className="form-control validation"
              type="test"
              id="investigatorName"
              value={investigatorName}
              onChange={(e) => {
                setInvestigatorName(e.target.value || undefined);
              }}
            />
          </div>
          <div className="position-relative" style={{ width: "min-content" }}>
            <button
              type="submit"
              className="btn btn-primary "
              disabled={!comment || !investigatorName || patch.length === 0}
              title="Appliquer cette annotation"
            >
              Annoter
            </button>

            <span
              title={
                patch.length === 0
                  ? "Aucune modification effectuée"
                  : !comment
                  ? "Commentaire manquante"
                  : !investigatorName
                  ? "Nom d'enquêteur manquant"
                  : `Annotation prête`
              }
              className={cx(
                "position-absolute top-0 start-100 translate-middle badge rounded-pill",
                !comment || !investigatorName || patch.length === 0 ? "bg-danger" : "bg-success",
              )}
            >
              {!comment || !investigatorName || patch.length === 0 ? "!" : "✓"}
            </span>
          </div>
        </form>
      </div>
    </Modal>
  );
};
