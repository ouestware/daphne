import { FC } from "react";
import { isNil } from "lodash";
import cx from "classnames";

export const Toggle: FC<{
  id: string;
  className?: string;
  label: JSX.Element | string;
  value: boolean;
  onChange: (newValue: boolean) => void;
  title?: string;
  disabled?: boolean;
}> = ({ id, title, value, onChange, label, className, disabled }) => {
  return (
    <div className={cx(className)} title={title}>
      <input
        type="checkbox"
        className="btn-check"
        id={id}
        checked={value}
        onChange={(e) => onChange(e.target.checked)}
        disabled={isNil(disabled) ? false : disabled}
      />
      <label className="btn btn-outline-primary" htmlFor={id}>
        {label}
      </label>
    </div>
  );
};
