export type ImportNodeRef = {
  id: string;
  type: string;
};
export type ImportNodeCommon = ImportNodeRef;

type ImportRelRef = {
  type: string;
  source: ImportNodeRef;
  target: ImportNodeRef;
};
type ImportProperties = { [key: string]: unknown };

type ImportAddNode = { op: "add_node"; properties: ImportProperties } & ImportNodeCommon;
type ImportMergeNode = { op: "merge_node"; properties: ImportProperties } & ImportNodeCommon;
type ImportReplaceNode = { op: "replace_node"; properties: ImportProperties } & ImportNodeCommon;
type ImportRemoveNode = { op: "remove_node" } & ImportNodeCommon;
type ImportAddRel = { op: "add_rel"; properties: ImportProperties } & ImportRelRef;
type ImportMergeRel = { op: "merge_rel"; properties: ImportProperties } & ImportRelRef;
type ImportReplaceRel = { op: "replace_rel"; properties: ImportProperties } & ImportRelRef;
type ImportRemoveRel = { op: "remove_rel" } & ImportRelRef;

export type ImportLine =
  | ImportAddNode
  | ImportMergeNode
  | ImportReplaceNode
  | ImportRemoveNode
  | ImportAddRel
  | ImportMergeRel
  | ImportReplaceRel
  | ImportRemoveRel;

export type ImportOperationType = ImportLine["op"];

export const importOperations = [
  "add_node",
  "merge_node",
  "replace_node",
  "remove_node",
  "add_rel",
  "merge_rel",
  "replace_rel",
  "remove_rel",
];
