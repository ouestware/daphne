# social network

## person nodes
MATCH (p:PhysicalPerson { corpusId: "e3003a99-20ee-40ee-998e-5df4ecf3aba2" })
OPTIONAL MATCH (p)-[:IS_NAMED]->(pn:PersonName)
RETURN p.id AS id, labels(p) AS labels, p.name AS Label, [alt IN collect(pn)
WHERE alt.name <> p.name | alt.name] AS alternative_names

## person edges
MATCH (pp:PhysicalPerson { corpusId: "e3003a99-20ee-40ee-998e-5df4ecf3aba2" })-[:PARTICIPATE]->(:FactoidPersonParticipate)-[:PARTICIPATE]-(f:Factoid)-[:PARTICIPATE]-(:FactoidPersonParticipate)<-[:PARTICIPATE]-(pp2:PhysicalPerson)
WHERE id(pp2)<>id(pp) AND id(pp) > id(pp2)
WITH pp.id AS sourceId, pp2.id AS targetId, count(*) AS Weight
RETURN sourceId + '|'+ targetId AS id, "encountered" AS type, sourceId, targetId, Weight

# Correct date issue
MERGE (d:DateValue)
 SET d.start = datetime(REPLACE(replace(d.start, "datetime(", ""), ")", ""))
 SET d.end = datetime(replace(replace(d.end, "datetime(", ""), ")", ""))

# Correct start/end wrong order
MATCH (d:DateValue)
WHERE d.startIso > d.endIso
 SET d.incorectDateOrder = d.startIso
 SET d.startIso = d.endIso
 SET d.endIso = d.incorectDateOrder
 SET d.incorectDateOrder = true
RETURN d
